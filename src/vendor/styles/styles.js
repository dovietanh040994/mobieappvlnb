const StylesAll = {
    //Global
    paddingLR17: {
        paddingLeft: 17,
        paddingRight: 17


    },
    paddingLR10: {
        paddingLeft: 10,
        paddingRight: 10
    },
    info: {
        backgroundColor: '#6894C5'
    },
    colorWhite: {
        color: 'white'
    },
    top100: {
        top: 100
    },
    top120: {
        top: 120
    },
    padding0: {
        paddingTop: 0,
        paddingRight: 0,
        paddingLeft: 0,
        paddingBottom: 0
    },
    textCapitalize:{
        textTransform: 'capitalize'
    },
    //Loading and Login
    footerLoad: {
        backgroundColor: 'white',

    },
    footerText: {
        fontSize: 12,
        marginTop: 20,
        color: '#999'
    },
    sizeLogo: {
        width: 200,
        height: 100,
    },
    loginButtonText: {
        fontSize: 11,
        paddingLeft: 0,
        // textTransform:'lowercase'
    },
    iconlogin: {
        fontSize: 20,
        marginRight: 10
    },
    loginButton: {
        alignSelf: "center",
        height: 40,
        marginBottom: 15
    },
    loginButtonNext: {
        alignSelf: "center",
        height: 30,

    },
    loginNextText: {
        textAlign: 'center',
        fontSize: 11,
        width: 100
    },

    //Menu
    bgMenuAvatar: {
        backgroundColor: '#6894C5',
        height: 160,
        width: "100%",
        alignSelf: "stretch",
        // position: "absolute"
    },
    menuAvatar: {
        height: 70,
        width: 70,
        // position: "absolute",
        alignSelf: "center",
        marginTop: 20,
        borderRadius: 5
    },
    menuTextAvatar1: {
        // position: "absolute",
        alignSelf: "center",
        color: 'white',
        marginTop: 10,

    },
    menuTextAvatar2: {
        // position: "absolute",
        alignSelf: "center",
        color: 'white',
        // top: 125,
        fontSize: 13,
    },
    //Home
    customIcon: {
        position: "absolute",
        color: 'white',
        fontSize: 20,
        marginTop: 4
    },
    dislayInput: {
        borderBottomWidth: 1,
        borderColor: 'white',
        paddingLeft: 30,
        marginBottom: 20
    },
    //Salary
    tabText:{
        color:'#6894C5',
        textAlign: 'center',
        textTransform:'uppercase',
        fontSize:12,
        paddingLeft:2,
        paddingRight:2
    },
    tabText2:{

    },
    // AdvancedSearch
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        flexDirection:'column',
        paddingTop:10,
        paddingBottom:10
    },
    textAdvanced: {
        color: '#c69264',
        fontWeight: '400',
        fontSize: 14,
        flex:1,
    },
    textAdvanced2: {
        // fontSize: 14,
        // flex:1,
        // color: '#5a5e62',
        // fontWeight: '400',
        // marginTop:3,
        // elevation: 0
    },
    fieldAdvanced:{
        flex:1,
        height:30
    }


};

export default StylesAll;
