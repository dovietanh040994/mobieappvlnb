//quản lý tên form
const SEARCH_FORM = 'searchForm';
const SEARCH_ADVANCED = 'searchAdvanced';
const USER_FORM = 'userForm';
const FORM_EDIT = 'formEdit';
const FORM_CV = 'formCV';
const SELECTINFO = 'selectInfo';
const LOGINFORM = 'loginForm';
const RESETPASS = 'resetPass';
const SINGINFORM = 'singinForm';
const VERIFYFORM = 'verifyForm';
const LOG_IN_EMPLOYER_FORM = 'loginEmployerForm';
const SING_IN_EMPLOYER_FORM = 'singinEmployerForm';
const FORM_CREATE_POST = "FORM_CREATE_POST";
const SEARCH_ADVANCED_EMPLOYER = 'searchAdvancedEmployer';
const FORM_INFO_COMPANY = "FORM_INFO_COMPANY";
const FORM_EDIT_POST = "FORM_EDIT_POST";

export {
    SEARCH_FORM,
    SEARCH_ADVANCED,
    SEARCH_ADVANCED_EMPLOYER,
    USER_FORM,
    FORM_EDIT,
    FORM_CV,
    SELECTINFO,
    LOGINFORM,
    SINGINFORM,
    VERIFYFORM,
    LOG_IN_EMPLOYER_FORM,
    SING_IN_EMPLOYER_FORM,
    FORM_CREATE_POST,
    FORM_INFO_COMPANY,
    FORM_EDIT_POST,
    RESETPASS,
} ;

