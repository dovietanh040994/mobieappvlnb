const Loading = "Loading";
const Home = "Home";
const Posts = "Posts";
const Notification = "Notification";
const ShowProfile = "ShowProfile";
const JobDetail = "JobDetail";
const Search = "Search";

const CarrerAttractive = "CarrerAttractive";
const Login = "Login";
const Singin = "Singin";
const ResetPass = "ResetPass";
const CarrerList = "CarrerList";
const CarrerBoxDetail = "CarrerBoxDetail";
const CarrerDetail = "CarrerDetail";
const AdvancedSearch = "AdvancedSearch";
const SearchDetail = "SearchDetail";
const CarrerSave = "CarrerSave";
const EditProfile = "EditProfile";
const JobDailly = "JobDailly";
const Introduce = "Introduce";
const HomeBox = "HomeBox";
const PostDetail = "PostDetail";

const JobApply = "JobApply";
const Policy = "Policy";
const JobCv = "JobCv";
const JobCare = "JobCare";
const ProfileJobDetail = "ProfileJobDetail";
const SearchAdvanced = "SearchAdvanced";
const AdvancedDetail = "AdvancedDetail";
const SelectInfo = "SelectInfo";
const SelectUser = "SelectUser";
const Index = "Index";
const IndexEmployer = "IndexEmployer";
const Verify = "Verify";

const Media = "Media";
const MediaDetail = "MediaDetail";
// const SearchEmployer="SearchEmployer";
const SearchAdvancedEmployer = "SearchAdvancedEmployer";
const AdvancedDetailEmployer = "AdvancedDetailEmployer";
const UserDetailEmployer = "UserDetailEmployer";

const EmployerManagePost = "EmployerManagePost";
const EditPost = "EditPost";
const CreatePost = "CreatePost";
const LoginEmployer = "LoginEmployer";
const SignInEmployer = "SignInEmployer";
const HomeBoxEmployer = "HomeBoxEmployer";
const ViewCVUser = "ViewCVUser";
const ProfileEmployer = "ProfileEmployer";
const InfoEmployer = "InfoEmployer";
const PostDetailEmployer = "PostDetailEmployer";
const ProfileUserEmployer = "ProfileUserEmployer";
const NotifyEmployer = "NotifyEmployer";
const PolicyEmploy = 'PolicyEmploy';

export {
    Loading,
    Home,
    Login,
    Singin,
    CarrerList,
    CarrerBoxDetail,
    CarrerDetail,
    SearchDetail,
    AdvancedSearch,
    CarrerSave,
    EditProfile,
    ShowProfile,
    CarrerAttractive,
    JobDailly,
    Introduce,
    Posts,
    Notification,
    JobDetail,
    Search,
    HomeBox,
    PostDetail,
    JobApply,
    Policy,
    JobCv,
    JobCare,
    ProfileJobDetail,
    SearchAdvanced,
    AdvancedDetail,
    SelectInfo,
    SelectUser,
    Index,
    IndexEmployer,
    Media,
    MediaDetail,
    Verify,
    ResetPass,

    // Employer,
    SearchAdvancedEmployer,
    AdvancedDetailEmployer,
    UserDetailEmployer,
    LoginEmployer,
    SignInEmployer,
    HomeBoxEmployer,

    EmployerManagePost,
    EditPost,
    CreatePost,
    ViewCVUser,
    ProfileEmployer,
    InfoEmployer,
    ProfileUserEmployer,
    PostDetailEmployer,
    NotifyEmployer,
    PolicyEmploy
} ;
