
import {
    SEARCH_FORM_HOME_SUCCEEDED,
} from '../actions/actionTypes';
import {
    LOADDING_BEGIN,
    FETCH_DATA_FAIL
} from '../actions/actionLoading'
import { NavigationActions } from 'react-navigation';


const initialState = {
    // items: [],
    // currentPage: 0,
    // loading: false,
    error: null,

    loading: false,
    //Loading state used while loading the data for the first time
    items: [],
    //Data Source for the FlatList
    //Loading state used while loading more data
};

const searchHomeReducers = ( salary = [],action,state = initialState) => {
    switch (action.type) {
        // case LOADDING_BEGIN:
        //
        //     return {
        //         ...state,
        //         loading: true,
        //         currentPage: action.listSearch.page,
        //         error: null
        //     };
        case SEARCH_FORM_HOME_SUCCEEDED:

            return {
                // ...state,
                // loading: false,
                // items:   [ ...state.items, ...action.listSearch ]
                items:   action.listSearch

            };
        // case FETCH_DATA_FAIL:
        //     return {
        //         ...state,
        //         loading: false,
        //         error: 'error',
        //         items: [],
        //     };
        default:
            return salary; //state does not change
    }
}

export default searchHomeReducers;
