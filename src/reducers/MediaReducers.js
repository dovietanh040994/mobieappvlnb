import {
    FETCH_MEDIA_RELATION, FETCH_MEDIA_RELATION_SUCCEEDED

} from '../actions/actionTypes';



const initialState = {
    items: [],
    loading: false,
    error: null
};
const mediaReducers = (userCv = [], action, state = initialState) => {
    switch (action.type) {
        case FETCH_MEDIA_RELATION_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.media.news
            };
        default:
            return userCv; //state does not change
    }
}

export default mediaReducers;
