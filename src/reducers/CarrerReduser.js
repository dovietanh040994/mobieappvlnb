import {
    PROVINCE_lIST, PROVINCE_SUCCEEDED, CARRER_SUCCEEDED
} from '../actions/actionTypes';
const carrerReducers = (carrer = [], action) => {
    // console.log(action);
    switch (action.type) {
        case CARRER_SUCCEEDED:
            // console.log(action)
            return action.carrers;
        default:
            return carrer; //state does not change
    }
}

export default carrerReducers;
