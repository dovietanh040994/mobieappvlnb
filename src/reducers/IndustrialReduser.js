import {
    INDUSTRIAL_SUCCEEDED
} from '../actions/actionTypes';
const industrialsReducers = (industrials = [], action) => {
    // console.log(action);
    switch (action.type) {
        case INDUSTRIAL_SUCCEEDED:
            // console.log(action)
            return action.industrials;
        default:
            return industrials; //state does not change
    }
}

export default industrialsReducers;
