import {
    PROVINCE_lIST, PROVINCE_SUCCEEDED, SALARY_SUCCEEDED
} from '../actions/actionTypes';
const salaryReducers = (salary = [], action) => {
    // console.log(action);
    switch (action.type) {
        case SALARY_SUCCEEDED:
            // console.log(action)
            return action.salarys;
        default:
            return salary; //state does not change
    }
}

export default salaryReducers;
