import {
    INDUSTRIAL_SUCCEEDED, JOB_TYPES_SUCCEEDED
} from '../actions/actionTypes';
const jobTypesReducers = (jobTypes = [], action) => {
    // console.log(action);
    switch (action.type) {
        case JOB_TYPES_SUCCEEDED:
            // console.log(action)
            return action.jobTypes;
        default:
            return jobTypes; //state does not change
    }
}

export default jobTypesReducers;
