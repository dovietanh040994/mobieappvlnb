import {
    FETCH_LIST_UNIVERSITY, FETCH_LIST_UNIVERSITY_SUCCEEDED
} from '../actions/actionTypes';


const universityReducers = (university = [], action) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_LIST_UNIVERSITY_SUCCEEDED:
            // console.log(action)
            return action.university.universities;
        default:
            return university; //state does not change
    }
}

export default universityReducers;
