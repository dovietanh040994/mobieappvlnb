
import {
    CARRER_DETAIL_SUCCEEDED,
    SEARCH_FORM_HOME_SUCCEEDED,
} from '../actions/actionTypes';
import {
    LOADDING_CARRER_DETAIL,
    FETCH_CARRER_DETAIL_FAIL
} from '../actions/actionLoading'


const initialState = {
    items: [],
    loading: false,
    error: null
};
const carrerListDetailReducers = ( carrerDetail = [],action,state = initialState) => {
    switch (action.type) {
        // case LOADDING_CARRER_DETAIL:
        //
        //     return {
        //         ...state,
        //         loading: true,
        //         error: null
        //     };
        case CARRER_DETAIL_SUCCEEDED:
            // console.log( NavigationActions.navigate({ routeName: 'Login' }))
            return {
                // ...state,
                // loading: false,
                items: action.listCarrerDetail
            };
        // case FETCH_CARRER_DETAIL_FAIL:
        //     return {
        //         ...state,
        //         loading: false,
        //         error: 'error',
        //         items: []
        //     };
        default:
            return carrerDetail; //state does not change
    }
}

export default carrerListDetailReducers;
