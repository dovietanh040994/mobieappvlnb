import {
    FETCH_INFO_COMPANY, FETCH_INFO_COMPANY_SUCCEEDED,
    UPDATE_INFO_COMPANY, UPDATE_INFO_COMPANY_SUCCEEDED
} from '../actions/actionTypes';


const initialState = {
    items: [],
    loading: false,
    error: null
};

const employerCompanyReducers = (employerCompany = [], action, state = initialState) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_INFO_COMPANY_SUCCEEDED:
            return action.companyInfo.companyInfo;
        case UPDATE_INFO_COMPANY_SUCCEEDED:
            return action.companyInfo.companyInfo;
        default:
            return employerCompany; //state does not change
    }
}

export default employerCompanyReducers;
