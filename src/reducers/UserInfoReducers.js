import {
    FETCH_USER_INFO, FETCH_USER_INFO_SUCCEEDED,
    UPDATE_USER_INFO, UPDATE_USER_INFO_SUCCEEDED

} from '../actions/actionTypes';

import {NavigationActions} from 'react-navigation';


const initialState = {
    items: [],
    loading: false,
    error: null
};

const userInfoReducers = (userInfo = [], action, state = initialState) => {
    switch (action.type) {
        case FETCH_USER_INFO_SUCCEEDED:

            return {
                ...state,
                loading: false,
                items: action.userInfo.length === 0 ? [] : action.userInfo.userInfo
            };
        case UPDATE_USER_INFO_SUCCEEDED:
            // return {
            //     ...state,
            //     loading: false,
            //     items: action.userInfo.userInfo
            // };
        default:
            return userInfo; //state does not change
    }
}

export default userInfoReducers;
