import {
    JOB_DETAIL_SUCCEEDED,

} from '../actions/actionTypes';
import {
    LOADDING_DETAIL_JOB,
    FETCH_DETAIL_JOB_FAIL
} from '../actions/actionLoading'
import {NavigationActions} from 'react-navigation';


const initialState = {
    itemsDetaiJob: [],
    loadingDetaiJob: false,
    errorDetaiJob: null
};
const jobDetailReducers = (job = [], action, state = initialState) => {
    switch (action.type) {
        case LOADDING_DETAIL_JOB:
            return {
                ...state,
                loadingDetaiJob: true,
                errorDetaiJob: null
            };
        case JOB_DETAIL_SUCCEEDED:
            return {
                ...state,
                loadingDetaiJob: false,
                itemsDetaiJob: action.jobDetail
            };
        case FETCH_DETAIL_JOB_FAIL:
            return {
                ...state,
                loadingDetaiJob: false,
                errorDetaiJob: 'error',
                itemsDetaiJob: []
            };
        default:
            return job; //state does not change
    }
}

export default jobDetailReducers;
