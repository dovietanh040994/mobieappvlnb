import {
    LOGIN, EDITUSER_SUCCEEDED, USERDETAIL_SUCCEEDED,
    FETCH_DETAIL_USER_SUCCEEDED, FETCH_DETAIL_USER
} from '../actions/actionTypes';
import {
    LOADDING_USERDETAIL, FETCH_USERDETAIL
} from '../actions/actionLoading';

const initialState = {
    items: [],
    loading: false,
    error: null
};
const loginReducers = (user = [], action, state = initialState) => {
    // console.log(action);
    switch (action.type) {
        case LOADDING_USERDETAIL:

            return {
                ...state,
                loading: true,
                error: null
            };
        case USERDETAIL_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.userInfo.userInfo
            };

        case FETCH_USERDETAIL:
            return {
                ...state,
                loading: false,
                error: 'error',
                items: []
            };
        case FETCH_DETAIL_USER_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.userInfo.userInfo
            };
        case EDITUSER_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.userInfo.userInfo
            };

        default:
            return user; //state does not change
    }
}

export default loginReducers;
