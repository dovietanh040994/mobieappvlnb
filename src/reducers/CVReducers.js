import {
    FETCH_CV_USER, FETCH_CV_USER_SUCCEEDED,
    UPDATE_CV_USER, UPDATE_CV_USER_SUCCEEDED

} from '../actions/actionTypes';

import {NavigationActions} from 'react-navigation';


const initialState = {
    items: [],
    loading: false,
    error: null
};
const cvReducers = (userCv = [], action, state = initialState) => {
    switch (action.type) {
        case FETCH_CV_USER_SUCCEEDED:

            // console.log(action);

            return {
                ...state,
                loading: false,
                items: action.userCV.length === 0 ? [] : action.userCV.userCv
            };
        case UPDATE_CV_USER_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.userCV.userCv
            };
        default:
            return userCv; //state does not change
    }
}

export default cvReducers;
