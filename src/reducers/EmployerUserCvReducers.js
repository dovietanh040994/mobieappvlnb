import {
    FETCH_USER_CV_EMPLOYER, FETCH_USER_CV_EMPLOYER_SUCCEEDED,
    SAVE_CV_USER_SUCCEEDED,
    BUY_CV, BUY_CV_SUCCEEDED,
    UNSAVE_CV_USER_SUCCEEDED
} from '../actions/actionTypes';


const initialState = {
    items: [],
    loading: false,
    error: null
};

const employerUserCvReducers = (userCVEmployer = [], action, state = initialState) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_USER_CV_EMPLOYER_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.userInfo.length === 0 ? [] : action.userInfo.userInfo
            };
        case BUY_CV_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.userInfo.length === 0 ? [] : action.userInfo.userInfo
            };
        case SAVE_CV_USER_SUCCEEDED:
        // return {
        //     ...state,
        //     loading: false,
        //     items: action.userInfo.length === 0 ? [] : action.userInfo.userInfo
        // };
        case UNSAVE_CV_USER_SUCCEEDED:

        default:
            return userCVEmployer; //state does not change
    }
}

export default employerUserCvReducers;
