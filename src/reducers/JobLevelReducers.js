import {
    FETCH_JOB_LEVEL,FETCH_JOB_LEVEL_SUCCEEDED
} from '../actions/actionTypes';
const jobLevelReducers = (jobLevels = [], action) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_JOB_LEVEL_SUCCEEDED:
            // console.log(action)
            return action.jobLevels;
        default:
            return jobLevels; //state does not change
    }
}

export default jobLevelReducers;
