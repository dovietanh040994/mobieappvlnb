import {
    POST_DETAIL_EMPLOYER_SUCCEEDED

} from '../actions/actionTypes';
import {
    LOADDING_DETAIL_EMPLOYER_POST,
    FETCH_DETAIL_EMPLOYER_POST
} from '../actions/actionLoading'
import {NavigationActions} from 'react-navigation';


const initialState = {
    itemsDetaiPostEmployer: [],
    loadingDetaiPostEmployer: false,
    errorDetaiPostEmployer: null
};
const postDetailPostEmployerReducers = (post = [], action, state = initialState) => {
    switch (action.type) {
        case LOADDING_DETAIL_EMPLOYER_POST:
            return {
                ...state,
                loadingDetaiPostEmployer: true,
                errorDetaiPostEmployer: null
            };
        case POST_DETAIL_EMPLOYER_SUCCEEDED:
            return {
                ...state,
                loadingDetaiPostEmployer: false,
                itemsDetaiPostEmployer: action.postDetail
            };
        case FETCH_DETAIL_EMPLOYER_POST:
            return {
                ...state,
                loadingDetaiPostEmployer: false,
                errorDetaiPostEmployer: 'error',
                itemsDetaiPostEmployer: []
            };
        default:
            return post; //state does not change
    }
}

export default postDetailPostEmployerReducers;
