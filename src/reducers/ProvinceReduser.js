import {
    PROVINCE_lIST,PROVINCE_SUCCEEDED
} from '../actions/actionTypes';
const provinceReducers = (province = [], action) => {
    // console.log(action);
    switch (action.type) {
        case PROVINCE_SUCCEEDED:
            // console.log(action)
            return action.provinces;
        default:
            return province; //state does not change
    }
}

export default provinceReducers;
