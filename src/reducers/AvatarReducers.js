import {
   UPDATE_AVATAR_USER,UPDATE_AVATAR_USER_SUCCEEDED

} from '../actions/actionTypes';

import {NavigationActions} from 'react-navigation';


const initialState = {
    items: [],
    loading: false,
    error: null
};
const avatarReducers = (userAvatar = [], action, state = initialState) => {
    switch (action.type) {
        case UPDATE_AVATAR_USER_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.userAvatar.userAvatar
            };
        default:
            return userAvatar; //state does not change
    }
}

export default avatarReducers;
