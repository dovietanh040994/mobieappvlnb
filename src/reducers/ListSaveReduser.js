
import {
    LIST_SAVE_SUCCEEDED,

} from '../actions/actionTypes';
import {
    LOADDING_BEGIN_SAVE,
    FETCH_DATA_FAIL_SAVE
} from '../actions/actionLoading'
import { NavigationActions } from 'react-navigation';


const initialState = {
    items: [],
    loading: false,
    error: null
};
const listSaveReducers = ( salary = [],action,state = initialState) => {
    switch (action.type) {
        case LOADDING_BEGIN_SAVE:
            return {
                ...state,
                loading: true,
                error: null
            };
        case LIST_SAVE_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: action.listSave
            };
        case FETCH_DATA_FAIL_SAVE:
            return {
                ...state,
                loading: false,
                error: 'error',
                items: []
            };
        default:
            return salary; //state does not change
    }
}

export default listSaveReducers;
