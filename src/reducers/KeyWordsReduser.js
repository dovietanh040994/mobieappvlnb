
import {
    JOB_DETAIL_SUCCEEDED, KEY_WORKS_SUCCEEDED,

} from '../actions/actionTypes';
import {
    LOADDING_DETAIL_JOB,
    FETCH_DETAIL_JOB_FAIL, LOADDING_KEY_WORKS, FETCH_KEY_WORKS_FAIL
} from '../actions/actionLoading'
import { NavigationActions } from 'react-navigation';


const initialState = {
    itemsKeyWorks: [],
    loadingKeyWorks: false,
    errorKeyWorks: null
};
const keyWorksReducers = ( job = [],action,state = initialState) => {
    // console.log('action')
    // console.log(action)
    switch (action.type) {
        case LOADDING_KEY_WORKS:
            return {
                ...state,
                loadingKeyWorks: true,
                errorKeyWorks: null
            };
        case KEY_WORKS_SUCCEEDED:
            return {
                ...state,
                loadingKeyWorks: false,
                itemsKeyWorks: action.keywords
            };
        case FETCH_KEY_WORKS_FAIL:
            return {
                ...state,
                loadingKeyWorks: false,
                errorKeyWorks: 'error',
                itemsKeyWorks: []
            };
        default:
            return job; //state does not change
    }
}

export default keyWorksReducers;
