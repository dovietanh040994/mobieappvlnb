import {
    DELETE_JOB_APPLY, DELETE_JOB_APPLY_SUCCEEDED,
    DELETE_JOB_CARE, DELETE_JOB_CARE_SUCCEEDED

} from '../actions/actionTypes';

import {NavigationActions} from 'react-navigation';


const initialState = {
    items: [],
    loading: false,
    error: null
};
const profileJobReducers = (listJob = [], action, state = initialState) => {
    switch (action.type) {
        case DELETE_JOB_APPLY_SUCCEEDED:
            console.log(1234);
            const filteredApply = listJob.filter(eachJob => {
                return eachJob.id.toString() !== action.params.idJob.toString();
            });
            return filteredApply;
        case DELETE_JOB_CARE_SUCCEEDED:
            const filteredCare = listJob.filter(eachJob => {
                return eachJob.id.toString() !== action.params.idJob.toString();
            });
            return filteredCare;
        default:
            return listJob; //state does not change
    }
}

export default profileJobReducers;
