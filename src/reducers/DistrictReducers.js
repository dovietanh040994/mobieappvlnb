import {
   FETCH_DISTRICT_SUCCEEDED,FETCH_DISTRICT
} from '../actions/actionTypes';


const districtReducers = (district = [], action) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_DISTRICT_SUCCEEDED:
            // console.log(action)
            return action.districts.districts;
        default:
            return district; //state does not change
    }
}

export default districtReducers;
