import {
    SWITCH_NOTIFY, SWITCH_NOTIFY_SUCCEEDED

} from '../actions/actionTypes';

import {NavigationActions} from 'react-navigation';


const initialState = {
    items: [],
    loading: false,
    error: null
};
const notifyReducers = (notify = [], action, state = initialState) => {
    switch (action.type) {
        case SWITCH_NOTIFY_SUCCEEDED:
            return action.status;

        default:
            return 1; //state does not change
    }
}

export default notifyReducers;
