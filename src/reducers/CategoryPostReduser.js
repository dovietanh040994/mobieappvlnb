import {
   CATEGORY_POST_SUCCEEDED
} from '../actions/actionTypes';
import {
    FETCH_CATEGORY_POST_FAIL,
    FETCH_DETAIL_JOB_FAIL,
    LOADDING_CATEGORY_POST,
    LOADDING_DETAIL_JOB
} from "../actions/actionLoading";

const initialState = {
    itemsListCate: [],
    loadingListCate: false,
    errorListCate: null
};
const categoryPostReducers = (cate = [],action,state = initialState) => {
    switch (action.type) {
        case LOADDING_CATEGORY_POST:
            return {
                ...state,
                loadingListCate: true,
                errorListCate: null
            };
        case CATEGORY_POST_SUCCEEDED:
            // console.log(action)
            return {
                ...state,
                loadingListCate: false,
                itemsListCate: action.listCatePost
            };
        case FETCH_CATEGORY_POST_FAIL:
            return {
                ...state,
                loadingListCate: false,
                errorListCate: 'error',
                itemsListCate: []
            };
        default:
            return cate; //state does not change
    }
}

export default categoryPostReducers;
