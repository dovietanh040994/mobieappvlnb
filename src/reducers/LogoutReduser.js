
import {
    LOGOUT_SUCCEEDED
} from '../actions/actionTypes';

import {
    LOADDING_LOGOUT_BEGIN,
    FETCH_LOGOUT_FAIL
} from '../actions/actionLoading'


const initialState = {
    items: [],
    loading: false,
    error: null
};
const logoutReducers = (user = [],action,state = initialState) => {
    // console.log(action);
    switch (action.type) {
        case LOADDING_LOGOUT_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };
        case LOGOUT_SUCCEEDED:
            return {
                ...state,
                loading: false,
                items: 'logout'
            };
        case FETCH_LOGOUT_FAIL:
            return {
                ...state,
                loading: false,
                error: 'error',
                items: []
            };
        default:
            return user; //state does not change
    }
}

export default logoutReducers;
