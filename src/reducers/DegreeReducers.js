import {
    FETCH_LIST_DEGREE,FETCH_LIST_DEGREE_SUCCEEDED
} from '../actions/actionTypes';


const degreeReducers = (degree = [], action) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_LIST_DEGREE_SUCCEEDED:
            // console.log(action)
            return action.degree.degrees;
        default:
            return degree; //state does not change
    }
}

export default degreeReducers;
