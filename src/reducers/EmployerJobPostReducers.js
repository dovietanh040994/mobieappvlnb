import {
    CREATE_JOB_POST_SUCCEEDED, UPDATE_JOB_POST_SUCCEEDED

} from '../actions/actionTypes';

import {NavigationActions} from 'react-navigation';

const initialState = {
    itemsDetaiJob: [],
    loadingDetaiJob: false,
    errorDetaiJob: null
};

const jobPostEmployerReducers = (job = [], action, state = initialState) => {
    switch (action.type) {
        case CREATE_JOB_POST_SUCCEEDED:
            return {};
        case UPDATE_JOB_POST_SUCCEEDED:
            return {};
        default:
            return job; //state does not change
    }
}

export default jobPostEmployerReducers;
