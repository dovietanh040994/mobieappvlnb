import {
   CARRERLISTALL_SUCCEEDED
} from '../actions/actionTypes';
const carrerListAllReducers = (carrer = [], action) => {
    // console.log(action);
    switch (action.type) {
        case CARRERLISTALL_SUCCEEDED:
            // console.log(action)
            return action.listCarrerAll;
        default:
            return carrer; //state does not change
    }
}

export default carrerListAllReducers;
