import {
 SINGIN_SUCCEEDED
} from '../actions/actionTypes';
import {
    LOADDING_USERDETAIL, FETCH_USERDETAIL
} from '../actions/actionLoading';


const singinReducers = (user = [], action) => {
    switch (action.type) {

        case SINGIN_SUCCEEDED:
            // console.log('action11',action)
            return action.singIn;
        default:
            return user; //state does not change
    }
}

export default singinReducers;
