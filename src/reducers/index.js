import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

import loginReducers from "./LoginReduser";
import provinceReducers from "./ProvinceReduser";
import salaryReducers from "./SalaryReduser";
import carrerReducers from "./CarrerReduser";
import carrerListAllReducers from './CarrerListAllReduser'
import carrerListDetailReducers from './CarrerListDetaiReduser'
import searchHomeReducers from './SearchHomeReduser'
import listSaveReducers from './ListSaveReduser'
import profileJobReducers from './ProfileJobReducers'
import notifyReducers from './NotifyReducers'
import singinReducers from './SinginReduser'
// import salaryTabReducers from './SalaryTabReduser'
import jobDetailReducers from './JobDetailReduser'
import industrialsReducers from './IndustrialReduser'
import experienceReducers from './ExperienceReduser'
import logoutReducers from './LogoutReduser'
import jobTypesReducers from "./JobTypesReduser";
import categoryPostReducers from "./CategoryPostReduser";
import companyDetailReducers from "./CompanyDetailReduser";
import keyWorksReducers from "./KeyWordsReduser";
import cvReducers from "./CVReducers"
import mediaReducers from './MediaReducers'
import universityReducers from './UniversityReducres'
import majorReducers from './MajorReducers'
import degreeReducers from './DegreeReducers'
import skillReducers from './SkillReducers'
import userInfoReducers from './UserInfoReducers'
import employerUserCvReducers from './EmployerUserCvReducers'
import jobLevelReducers from './JobLevelReducers'
import districtReducers from './DistrictReducers'
import employerCompanyReducers from './EmployerCompanyReducers'
import jobPostEmployerReducers from './EmployerJobPostReducers'
import postDetailPostEmployerReducers from "./EmployerPostDetailReducers";
import updateApprovePostEmployerReducers from "./EmployerUpdateApprovePostReducers"

const reducers = {
    form: formReducer,
    cvReducers,
    loginReducers,
    provinceReducers,
    salaryReducers,
    carrerReducers,
    carrerListAllReducers,
    carrerListDetailReducers,
    searchHomeReducers,
    listSaveReducers,
    // salaryTabReducers,
    jobDetailReducers,
    industrialsReducers,
    experienceReducers,
    logoutReducers,
    jobTypesReducers,
    /////
    categoryPostReducers,
    companyDetailReducers,
    profileJobReducers,
    notifyReducers,
    keyWorksReducers,

    mediaReducers,
    singinReducers,
    universityReducers,
    majorReducers,
    degreeReducers,
    skillReducers,
    userInfoReducers,
    employerUserCvReducers,
    jobLevelReducers,
    districtReducers,
    employerCompanyReducers,
    jobPostEmployerReducers,
    postDetailPostEmployerReducers,
    updateApprovePostEmployerReducers,
};
const AllReducers = combineReducers(reducers);
export default AllReducers;



