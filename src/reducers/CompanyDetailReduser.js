
import {
    COMPANY_DETAIL_SUCCEEDED,

} from '../actions/actionTypes';
import {
   LOADDING_DETAIL_COMPANY, FETCH_DETAIL_COMPANY_FAIL
} from '../actions/actionLoading'
import { NavigationActions } from 'react-navigation';


const initialState = {
    itemsDetaiCompany: [],
    loadingDetaiCompany: false,
    errorDetaiCompany: null
};
const companyDetailReducers = ( company = [],action,state = initialState) => {
    switch (action.type) {
        case LOADDING_DETAIL_COMPANY:
            return {
                ...state,
                loadingDetaiCompany: true,
                errorDetaiCompany: null
            };
        case COMPANY_DETAIL_SUCCEEDED:
            return {
                ...state,
                loadingDetaiCompany: false,
                itemsDetaiCompany: action.companyDetail
            };
        case FETCH_DETAIL_COMPANY_FAIL:
            return {
                ...state,
                loadingDetaiCompany: false,
                errorDetaiCompany: 'error',
                itemsDetaiCompany: []
            };
        default:
            return company; //state does not change
    }
}

export default companyDetailReducers;
