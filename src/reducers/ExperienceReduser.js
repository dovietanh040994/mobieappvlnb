import {
    PROVINCE_lIST, PROVINCE_SUCCEEDED, CARRER_SUCCEEDED, EXPERIENCE_SUCCEEDED
} from '../actions/actionTypes';
const experienceReducers = (experience = [], action) => {
    // console.log(action);
    switch (action.type) {
        case EXPERIENCE_SUCCEEDED:
            // console.log(action)
            return action.jobExperiences;
        default:
            return experience; //state does not change
    }
}

export default experienceReducers;
