import {
    FETCH_LIST_MAJOR, FETCH_LIST_MAJOR_SUCCEEDED
} from '../actions/actionTypes';


const majorReducers = (major = [], action) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_LIST_MAJOR_SUCCEEDED:
            // console.log(action)
            return action.major.majors;
        default:
            return major; //state does not change
    }
}

export default majorReducers;
