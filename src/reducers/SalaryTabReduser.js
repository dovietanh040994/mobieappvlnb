//
// import {
//     SALARY_PROVINCE_SUCCEEDED,
//     SALARY_INDUSTRIAL_SUCCEEDED
// } from '../actions/actionTypes';
// import {
//     LOADDING_SALARY_TAB_DETAIL,
//     FETCH_SALARY_TAB_FAIL
// } from '../actions/actionLoading'
// import { NavigationActions } from 'react-navigation';
//
//
// const initialState = {
//     items: [],
//     loadingSalary: false,
//     errorSalary: null
// };
// const salaryTabReducers = ( data = [],action,state = initialState) => {
//     switch (action.type) {
//         case LOADDING_SALARY_TAB_DETAIL:
//
//             return {
//                 ...state,
//                 loadingSalary: true,
//                 errorSalary: null
//             };
//         case SALARY_PROVINCE_SUCCEEDED:
//
//             return {
//                 ...state,
//                 loadingSalary: false,
//                 items: action.data
//             };
//         case SALARY_INDUSTRIAL_SUCCEEDED:
//
//             return {
//                 ...state,
//                 loadingSalary: false,
//                 items: action.data
//             };
//         case FETCH_SALARY_TAB_FAIL:
//             return {
//                 ...state,
//                 loadingSalary: false,
//                 errorSalary: 'error',
//                 items: []
//             };
//         default:
//             return data; //state does not change
//     }
// }
//
// export default salaryTabReducers;
