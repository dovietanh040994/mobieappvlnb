import {
    FETCH_LIST_SKILL, FETCH_LIST_SKILL_SUCCEEDED
} from '../actions/actionTypes';


const skillReducers = (skills = [], action) => {
    // console.log(action);
    switch (action.type) {
        case FETCH_LIST_SKILL_SUCCEEDED:
            // console.log(action)
            return action.skill.skills;
        default:
            return skills; //state does not change
    }
}

export default skillReducers;
