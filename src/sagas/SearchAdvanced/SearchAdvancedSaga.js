import { Api } from './Api';
import {
    CARRER_LIST, CARRER_LIST_ALL,
    PROVINCE_lIST, SALARY_INDUSTRIAL, SALARY_LIST,
    SAVE_JOB_SUCCEEDED,
    SEARCH_ADVANCED_ACTION, USERDETAIL,

} from '../../actions/actionTypes';
import { put, takeLatest } from 'redux-saga/effects';
import {NavigationActions} from "react-navigation";
import {SearchDetail} from "../../vendor/screen";
import {SEARCH_ADVANCED} from "../../vendor/formNames";

//
// function* SearchAdvancedSaga(action) {
//     try {
//         yield Api.SearchAdvancedApi(action);
//
//         // if (result === true) {
//         //
//         //     yield put({type: SAVE_JOB_SUCCEEDED});
//         // }
//
//     } catch (error) {
//         //do nothing
//     }
// }
// export function* watchSearchAdvancedSaga() {
//     yield takeLatest(SEARCH_ADVANCED_ACTION, SearchAdvancedSaga);
// }
//
