import {Api} from './Api';
import {
    LOGIN,
    USERDETAIL,
    USERDETAIL_SUCCEEDED,
    PROVINCE_lIST,
    PROVINCE_SUCCEEDED,
    SALARY_LIST,
    SALARY_SUCCEEDED,
    CARRER_SUCCEEDED,
    CARRER_LIST,
    EDITUSER_SUCCEEDED,
    EDITUSER, CARRER_LIST_ALL, SALARY_INDUSTRIAL,
    INDUSTRIAL_LIST,
    INDUSTRIAL_SUCCEEDED,
    EXPERIENCE_LIST,
    EXPERIENCE_SUCCEEDED,
    LOGOUT_SUCCEEDED,
    LOGOUT,
    JOB_TYPES_SUCCEEDED,
    JOB_TYPES_LIST,
    FETCH_DETAIL_USER, FETCH_DETAIL_USER_SUCCEEDED, SINGIN, SINGIN_SUCCEEDED, VERIFY,
    FETCH_JOB_LEVEL, FETCH_JOB_LEVEL_SUCCEEDED, RESETPASS

} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';
import firebase from "react-native-firebase";
import {LOADDING_DETAIL_JOB, LOADDING_USERDETAIL} from "../../actions/actionLoading";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../vendor/NavigationService";

// let logoutAcc = () => {
//     firebase.auth().signOut().then(function () {
//         console.log("Logout do tài khoản của bạn bị block")
//     }).catch(function (error) {
//         console.log("Đã có lỗi xảy ra trong quá trình logout. Xin thử lại")
//         console.log(error)
//     });
// }


// LOGIN
function* checkAndAddUser(action) {

    // console.log(action);
    try {
        // console.log(111222)
        if (action.typeLG === 2) {
            // login GG
            const result = yield Api.checkAndAddUser(action.user._user, action.typeLG);
            if (result === true) {
                yield put({type: USERDETAIL});
                NavigationService.navigate('ShowProfile');

            }
        } else {
            //login User pass

            const result = yield Api.checkAndAddUser(action.user, action.typeLG);

            // console.log(result);

            if (result === true) {
                // NavigationService.navigate('ShowProfile',{poinCheck:1});
                yield put({type: USERDETAIL});
                NavigationService.navigate('ShowProfile');


            }

        }

    } catch (error) {
        // console.log('error')
        console.log(error)
    }
}

export function* watchcheckAndAddUser() {
    yield takeLatest(LOGIN, checkAndAddUser);
}

// LOGIN
function* singInUser(action) {
    try {
        //login User pass
        const result = yield Api.singInApi(action.user);
        // console.log('action',result)

        yield put({type: SINGIN_SUCCEEDED, singIn: result});
        // NavigationService.navigate('ShowProfile');


    } catch (error) {
        // console.log('error')/
        console.log(error)
    }
}

export function* watchSingInUser() {
    yield takeLatest(SINGIN, singInUser);
}


// VERIFY
function* verifyUser(action) {
    try {
        // console.log('action',action)
        yield Api.VerifyApi(action);


    } catch (error) {
        // console.log('error')
        console.log(error)
    }
}

export function* watchVerifyUser() {
    yield takeLatest(VERIFY, verifyUser);
}

// RESETPASS
function* resetPass(action) {
    try {
        console.log('actionResetPassApi :',action)
        yield Api.ResetPassApi(action);


    } catch (error) {
        // console.log('error')
        console.log(error)
    }
}

export function* watchResetPass() {
    yield takeLatest(RESETPASS, resetPass);
}

// USERDETAIL
function* logoutSaga() {
    try {
        yield put({type: LOGOUT_SUCCEEDED});

    } catch (error) {
        //do nothing
    }
}

export function* watchLogoutSaga() {
    yield takeLatest(LOGOUT, logoutSaga);
}

// USERDETAIL
function* userDetailSaga() {
    try {
        yield put({type: LOADDING_USERDETAIL});
        const result = yield Api.userDetailApi();
        yield put({type: USERDETAIL_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchUserDetailSaga() {
    yield takeLatest(USERDETAIL, userDetailSaga);
}

// EDITUSER
function* editUserSaga(action) {
    try {

        const result = yield Api.editUserApi(action.userInfo);
        if (result) {
            yield put({type: EDITUSER_SUCCEEDED});
        }
    } catch (error) {
        //do nothing
    }
}

export function* watchEditUserSaga() {
    yield takeLatest(EDITUSER, editUserSaga);
}

// PROVINCE_lIST
function* profileSaga() {
    try {
        const result = yield Api.provinceApi();
        // console.log('userDetailSaga')
        if (result.err_code === 0) {
            // console.log(result.provinces)
            yield put({type: PROVINCE_SUCCEEDED, provinces: result.provinces});

        }
        // yield put({ type: USERDETAIL_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchProfileSaga() {
    yield takeLatest(PROVINCE_lIST, profileSaga);
}


//SALARY_LIST
function* salarySaga() {
    try {
        const result = yield Api.salaryApi();
        if (result.err_code === 0) {
            yield put({type: SALARY_SUCCEEDED, salarys: result.jobSalarys});

        }
        // yield put({ type: USERDETAIL_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchSalarySaga() {
    yield takeLatest(SALARY_LIST, salarySaga);
}

//CARRER_LIST
function* carrerSaga() {
    try {
        // console.log(13123)
        const result = yield Api.carrerApi();
        if (result.err_code === 0) {
            yield put({type: CARRER_SUCCEEDED, carrers: result.jobCarrers});

        }
        // yield put({ type: USERDETAIL_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchcarrerSaga() {
    yield takeLatest(CARRER_LIST, carrerSaga);
}

// INDUSTRIAL_LIST
function* industrialSaga() {
    try {
        const result = yield Api.industrialApi();

        if (result.err_code === 0) {
            yield put({type: INDUSTRIAL_SUCCEEDED, industrials: result.industrials});

        }
        // yield put({ type: USERDETAIL_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchIndustrialSaga() {
    yield takeLatest(INDUSTRIAL_LIST, industrialSaga);
}

function* experienceSaga() {
    try {
        const result = yield Api.experienceApi();

        if (result.err_code === 0) {
            yield put({type: EXPERIENCE_SUCCEEDED, jobExperiences: result.jobExperiences});

        }
        // yield put({ type: USERDETAIL_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchExperienceSaga() {
    yield takeLatest(EXPERIENCE_LIST, experienceSaga);
}


// INDUSTRIAL_LIST
function* jobTypesSaga() {
    try {
        const result = yield Api.jobTypesApi();

        if (result.err_code === 0) {
            yield put({type: JOB_TYPES_SUCCEEDED, jobTypes: result.jobTypes});

        }
        // yield put({ type: USERDETAIL_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchJobTypesSaga() {
    yield takeLatest(JOB_TYPES_LIST, jobTypesSaga);
}

//fetch user detail
function* fetchUserDetailSage(action) {
    try {

        const result = yield Api.apiFetchUserDetail();

        yield put({type: FETCH_DETAIL_USER_SUCCEEDED, userInfo: result});

    } catch (error) {
        //do nothing
    }
}

export function* watchFetchUserDetailSage() {
    yield takeLatest(FETCH_DETAIL_USER, fetchUserDetailSage);
}

//fetch list province
//fetch user detail
function* fetchProvinceListSage(action) {
    try {
        const result = yield Api.provinceApi();

        yield put({type: PROVINCE_SUCCEEDED});

    } catch (error) {
        //do nothing
    }
}

export function* watchFetchProvinceListSage() {
    yield takeLatest(PROVINCE_lIST, fetchProvinceListSage);
}

//joblevel
function* fetchJobLevelSage(action) {
    try {
        const result = yield Api.jobLevelsApi();

        if (result.err_code === 0) {
            yield put({type: FETCH_JOB_LEVEL_SUCCEEDED, jobLevels: result.jobLevels});

        }
    } catch (error) {
        //do nothing
    }
}

export function* watchFetchJobLevelSage() {
    yield takeLatest(FETCH_JOB_LEVEL, fetchJobLevelSage);
}
