import AsyncStorage from '@react-native-community/async-storage';
import {put, takeLatest} from "redux-saga/effects";
import {SWITCH_NOTIFY, SWITCH_NOTIFY_SUCCEEDED} from "../../actions/actionTypes";
import {Toast} from "native-base";
import firebase from "react-native-firebase";
import NavigationService from "../../vendor/NavigationService";

const urlCkeckUser = 'http://vieclamnambo.vn:9002/api/vlnb/user/checkuserexits'


const userDetailurl = 'http://vieclamnambo.vn:9002/api/vlnb/user/detail';
const editUser = 'http://vieclamnambo.vn:9002/api/vlnb/user/edit';

const provinceUrl = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getallprovince';
const salaryUrl = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getjobsalary';
const carrerUrl = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getalljobcarrer';
const industrialUrl = 'http://vieclamnambo.vn:9002/vlnb/api/cat/getallindustrial';
const experienceUrl = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getjobexperience';
const jobTypesUrl = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getjobtype';
const singInUrl = 'http://vieclamnambo.vn:9002/api/vlnb/user/registerbyphone';
const verifyUrl = 'http://vieclamnambo.vn:9002/api/vlnb/user/registerverify';
const resetpass = 'http://vieclamnambo.vn:9002/api/vlnb/user/resetpass';
const urlJobLevels = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getjoblevel';

let _storeData = async (id) => {
    try {
        await AsyncStorage.setItem('userInfo', id)
    } catch (error) {
        // Error saving data
    }
    return
};
const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

function* checkAndAddUser(dataUser,typeLG) {

    if(typeLG === 2){

        //login GG
        var data = dataUser.providerData[0];
        var email = data.email ? data.email : '';
        var name = data.displayName ? data.displayName : '';
        var phone = data.phoneNumber ? data.phoneNumber : '';
        var avatar = data.photoURL ? data.photoURL : '';
        var password = '';
        var accessToken = yield AsyncStorage.getItem('accessToken');
        var device_id = yield AsyncStorage.getItem('fcmToken');

        var link =  `${urlCkeckUser}?type=${typeLG}&email=${email}&name=${name}&phone=${phone}&avatar=${avatar}&access_token=${accessToken.replace(/\"/g, "")}&device_id=${device_id.replace(/\"/g, "")}&password=${password}`;
    }else{
        //login User pass
        // var phone ='0907561996';
        // var password = '13879428a';
        var phone = dataUser.phone ? dataUser.phone : '';
        var password = dataUser.password ? dataUser.password : '';
        var device_id = yield AsyncStorage.getItem('fcmToken');

        var link =  `${urlCkeckUser}?type=${typeLG}&email=${phone}&password=${password}&device_id=${device_id.replace(/\"/g, "")}`;

    }


    const userToken = yield fetch(link, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            if (responseJson.err_code === 0) {
                return responseJson.token;
            } else {
                //tai khoan da bi block
                Toast.show({
                    text: responseJson.err_detail,
                    // buttonText: "Đóng",
                    type: "warning",
                    position: "top"
                })
                return false;
            }
        })

        .catch((error) => {
            console.error(error);
        })
    if (userToken !== false) {
        // console.log('_storeData')
        yield _storeData(`${userToken}`);
        return true
    } else {
        return false
    }


}

//singIn
function* singInApi(dataUser) {
    //login User pass
    var phone = dataUser.phone ? dataUser.phone : '';
    var password = dataUser.password ? dataUser.password : '';
    var link =  `${singInUrl}?phone_number=${phone}&password=${password}`;
    const singIn = yield fetch(link, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',

        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            if (responseJson.err_code === 0) {

                const phoneConvert = '+84' + phone.trim().slice(1)
                firebase.auth().signInWithPhoneNumber(phoneConvert)
                    .then(confirmResult => {

                            // console.log('asdsd',confirmResult)
                            NavigationService.navigate('Verify',{'confirmResult':confirmResult,'phone':phone})
                        }
                    )
                    .catch(error => console.log(error.message));
                return responseJson.key_verify;
            } else {
                Toast.show({
                    text: responseJson.err_detail,
                    // buttonText: "Đóng",
                    type: "warning",
                    position: "top"
                })
                return false;
            }
        })

        .catch((error) => {
            console.error(error);
        })
    return yield singIn
}

//VerifyApi
function* VerifyApi(dataUser) {
    //login User pass
    var phone = dataUser.sdt ? dataUser.sdt : '';
    var key_verify = dataUser.key ? dataUser.key : '';
    var link =  `${verifyUrl}?token_id=${phone}&key_verify=${key_verify}`;
    const Verify = yield fetch(link, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',

        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return true;
            } else {
                // Toast.show({
                //     text: responseJson.err_detail,
                //     // buttonText: "Đóng",
                //     type: "warning",
                //     position: "top"
                // })
                return false;
            }
        })

        .catch((error) => {
            console.error(error);
        })
    return yield Verify
}

//ResetPass
function* ResetPassApi(data) {
    //login User pass
    var phone = data.sdt ? data.sdt : '';
    var password = data.pass ? data.pass : '';
    var link =  `${resetpass}?phone_number=${phone}&password=${password}`;
    console.log('ResetPassApilink :',link)

    const Verify = yield fetch(link, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',

        },
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log('ResetPassApiresponseJson :',responseJson)

            if (responseJson.err_code === 0) {
                return true;
            } else {
                // Toast.show({
                //     text: responseJson.err_detail,
                //     // buttonText: "Đóng",
                //     type: "warning",
                //     position: "top"
                // })
                return false;
            }
        })

        .catch((error) => {
            console.error(error);
        })
    return yield Verify
}

function* userDetailApi() {

    let gettUID = yield getUserId()
    // console.log(userDetailurl +`?id=${gettUID}`)
    const userDetail = yield fetch(userDetailurl + `?id=${gettUID}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        // body: `email=${data.email}&name=${data.displayName}&phone=${data.phoneNumber}&avatar=${data.photoURL}`
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield userDetail

}

//sua thong tin user
function* editUserApi(dataUser) {

    let token = yield getUserId();

    var data = dataUser;

    const params = `?name=${data.name}&phone=${data.phone}&date_of_birth=${data.date}&province_id=${data.province}&email=${data.email}&gender=${data.gender}&address=${data.address}`;

    const userData = yield fetch(editUser + params, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': token
        },
        //  body: `name=${data.name}&phone=${data.phone}&date_of_birth=${data.date}&province_id=${data.province}&email=${data.email}&gender=${data.gender}`
    }).then((response) => response.json())

        .then((responseJson) => {

            // console.log(responseJson);

            if (responseJson.err_code === 0) {
                return true;
            } else {
                return false;
            }
        })
        .catch((error) => {
            console.error(error);
        })

    return userData
}

function* provinceApi() {

    const province = yield fetch(provinceUrl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        // body: `email=${data.email}&name=${data.displayName}&phone=${data.phoneNumber}&avatar=${data.photoURL}`
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    // console.log(province)
    return yield province

}

function* salaryApi() {

    const salary = yield fetch(salaryUrl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        // body: `email=${data.email}&name=${data.displayName}&phone=${data.phoneNumber}&avatar=${data.photoURL}`
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield salary

}

function* carrerApi() {

    const carrer = yield fetch(carrerUrl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        // body: `email=${data.email}&name=${data.displayName}&phone=${data.phoneNumber}&avatar=${data.photoURL}`
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield carrer

}

function* industrialApi() {

    const industrial = yield fetch(industrialUrl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield industrial

}

function* experienceApi() {

    const experience = yield fetch(experienceUrl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield experience

}

function* jobTypesApi() {

    const jobType = yield fetch(jobTypesUrl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield jobType

}


//fetch user detail
function* apiFetchUserDetail() {

    let token = yield getUserId();

    const userInfo = yield fetch(userDetailurl, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield userInfo
}

function* jobLevelsApi() {

    const jobLevels = yield fetch(urlJobLevels, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield jobLevels

}

export const Api = {
    checkAndAddUser,
    userDetailApi,
    editUserApi,
    provinceApi,
    salaryApi,
    carrerApi,
    industrialApi,
    experienceApi,
    jobTypesApi,
    apiFetchUserDetail,
    singInApi,
    VerifyApi,
    jobLevelsApi,
    ResetPassApi
};
