import { Api } from './Api';
import {
    SAVE_JOB_SUCCEEDED,
    SAVE_JOB_ACTION, UN_SAVE_JOB_ACTION, APPLIED_ACTION,

} from '../../actions/actionTypes';
import { put, takeLatest } from 'redux-saga/effects';
import {NavigationActions} from "react-navigation";
import {SearchDetail} from "../../vendor/screen";


function* SaveJobSaga(saveUserID) {
    try {
        yield Api.SaveJobApi(saveUserID);

    } catch (error) {
        //do nothing
    }
}
export function* watchSaveJobSaga() {
    yield takeLatest(SAVE_JOB_ACTION, SaveJobSaga);
}

function* UnSaveJobSaga(saveUserID) {
    try {
        yield Api.UnSaveJobSaga(saveUserID);

    } catch (error) {
        //do nothing
    }
}
export function* watchUnSaveJobSaga() {
    yield takeLatest(UN_SAVE_JOB_ACTION, UnSaveJobSaga);
}
function* appliedSaga(saveUserID) {
    try {
        yield Api.appliedSaga(saveUserID);

    } catch (error) {
        //do nothing
    }
}
export function* watchappliedSaga() {
    yield takeLatest(APPLIED_ACTION, appliedSaga);
}

