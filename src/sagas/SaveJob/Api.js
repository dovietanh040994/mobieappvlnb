import {LOADDING_BEGIN} from "../../actions/actionTypes";
import {put} from 'redux-saga/effects';

const SaveUri = 'http://vieclamnambo.vn:9002/api/vlnb/user/savepost';
const UnSaveUri = 'http://vieclamnambo.vn:9002/api/vlnb/user/unsavepost';
const AppliedUri = 'http://vieclamnambo.vn:9002/api/vlnb/user/applyjob';
import {
    LoaddingBegin,
} from '../../actions/index';
import AsyncStorage from "@react-native-community/async-storage";

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}


function* SaveJobApi(action) {
// console.log('action')
// console.log(SaveUri + `?job_post_id=${action.saveUserID}`)
    let userInfo = yield getUserId()
    return yield fetch(SaveUri + `?job_post_id=${action.saveUserID}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': userInfo
        },


    }).then((response) => response.json())
        .then((responseJson) => {
            // console.log(responseJson)
            if (responseJson.err_code === 0) {
                return true
            }else{
                return false
            }
        })
        .catch((error) => {
            console.error(error);
        });
}

function* UnSaveJobSaga(action) {
    let userInfo = yield getUserId()
    return yield fetch(UnSaveUri + `?job_post_id=${action.saveUserID}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': userInfo
        },


    }).then((response) => response.json())
        .then((responseJson) => {
            // console.log(responseJson)
            if (responseJson.err_code === 0) {
                return true
            }else{
                return false
            }
        })
        .catch((error) => {
            console.error(error);
        });
}

function* appliedSaga(action) {
    let userInfo = yield getUserId()
    return yield fetch(AppliedUri + `?job_post_id=${action.saveUserID}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': userInfo
        },


    }).then((response) => response.json())
        .then((responseJson) => {
            // console.log(responseJson)
            if (responseJson.err_code === 0) {
                return true
            }else{
                return false
            }
        })
        .catch((error) => {
            console.error(error);
        });
}


export const Api = {
    SaveJobApi,
    UnSaveJobSaga,
    appliedSaga
};
