import {Api} from './Api';
import {
    FETCH_LIST_MAJOR, FETCH_LIST_MAJOR_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchMajor(action) {
    try {

        const result = yield Api.apiFetchMajor(action.universityId);

        console.log(result);

        yield put({type: FETCH_LIST_MAJOR_SUCCEEDED, major: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchMajor() {
    yield takeLatest(FETCH_LIST_MAJOR, fetchMajor);
}

