import AsyncStorage from "@react-native-community/async-storage";

const urlMajor = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getmajor';


function* apiFetchMajor(id) {

    console.log(urlMajor + `?university_id=${id}`);
    const listMajor = yield fetch(urlMajor + `?university_id=${id}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield listMajor
}

export const Api = {
    apiFetchMajor
};
