import AsyncStorage from '@react-native-community/async-storage';
import {put, takeLatest} from "redux-saga/effects";
import {SWITCH_NOTIFY, SWITCH_NOTIFY_SUCCEEDED} from "../../actions/actionTypes";
import {Toast} from "native-base";
import NavigationService from "../../vendor/NavigationService";
import {LoginEmployer} from "../../vendor/screen";

const urlCkeckUser = 'http://vieclamnambo.vn:9002/api/vlnb/employee/login'
const singInUrl = 'http://vieclamnambo.vn:9002/api/vlnb/employee/register';

let _storeData = async (id) => {
    try {
        await AsyncStorage.setItem('userInfoEmployer', id)
    } catch (error) {
        // Error saving data
    }
    return
};
const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfoEmployer') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

function* checkAndAddUser(dataUser) {

    //login User pass
    var email = dataUser.email ? dataUser.email : '';
    var password = dataUser.password ? dataUser.password : '';
    var device_id = yield AsyncStorage.getItem('fcmToken');

    var link =  `${urlCkeckUser}?email=${email}&password=${password}&device_id=${device_id.replace(/\"/g, "")}`;
    const userToken = yield fetch(link, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson)
            if (responseJson.err_code === 0) {
                return responseJson.token;
            } else {
                Toast.show({
                    text: responseJson.err_detail,
                    // buttonText: "Đóng",
                    type: "warning",
                    position: "top"
                })
                return false;
            }
        })

        .catch((error) => {
            console.error(error);
        })
    if (userToken !== false) {
        // console.log('_storeData')
        yield _storeData(`${userToken}`);
        return true
    } else {
        return false
    }


}

//singIn
function* singInApi(dataUser) {
    //login User pass
    var email = dataUser.email ? dataUser.email : '';
    var password = dataUser.password ? dataUser.password : '';
    var link =  `${singInUrl}?email=${email}&password=${password}`;
    const singIn = yield fetch(link, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',

        },
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson)
            if (responseJson.err_code === 0) {

                NavigationService.navigate(LoginEmployer)
                    Toast.show({
                        text: 'Đăng ký thành công, mời bạn đăng nhập để trải nghiệm dịch vụ',
                        // buttonText: "Đóng",
                        type: "success",
                        position: "top"
                    })
            } else {
                Toast.show({
                    text: responseJson.err_detail,
                    // buttonText: "Đóng",
                    type: "warning",
                    position: "top"
                })
            }
        })

        .catch((error) => {
            console.error(error);
        })
    return yield singIn
}




export const Api = {
    checkAndAddUser,
    singInApi,
};
