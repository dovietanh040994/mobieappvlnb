import {Api} from './Api';
import {
    LOGIN,
    USERDETAIL,
    USERDETAIL_SUCCEEDED,
    PROVINCE_lIST,
    PROVINCE_SUCCEEDED,
    SALARY_LIST,
    SALARY_SUCCEEDED,
    CARRER_SUCCEEDED,
    CARRER_LIST,
    EDITUSER_SUCCEEDED,
    EDITUSER,
    CARRER_LIST_ALL,
    SALARY_INDUSTRIAL,
    INDUSTRIAL_LIST,
    INDUSTRIAL_SUCCEEDED,
    EXPERIENCE_LIST,
    EXPERIENCE_SUCCEEDED,
    LOGOUT_SUCCEEDED,
    LOGOUT,
    JOB_TYPES_SUCCEEDED,
    JOB_TYPES_LIST,
    FETCH_DETAIL_USER,
    FETCH_DETAIL_USER_SUCCEEDED,
    SINGIN,
    SINGIN_SUCCEEDED,
    VERIFY,
    LOGIN_EMPLOYER,
    SINGIN_EMPLOYER,
    SINGIN_EMPLOYER_SUCCEEDED

} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';
import firebase from "react-native-firebase";
import {LOADDING_DETAIL_JOB, LOADDING_USERDETAIL} from "../../actions/actionLoading";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../vendor/NavigationService";
import {HomeBoxEmployer} from "../../vendor/screen";



// LOGIN
function* checkAndAddUser(action) {

    try {
        //login Email pass
        const result = yield Api.checkAndAddUser(action.user);

        if (result === true) {
            NavigationService.navigate(HomeBoxEmployer);
            // yield put({type: USERDETAIL});

        }

    } catch (error) {
        // console.log('error')
        console.log(error)
    }
}

export function* watchcheckAndAddUserEmployer() {
    yield takeLatest(LOGIN_EMPLOYER, checkAndAddUser);
}
// Signin
function* singInUser(action) {
    try {
        //login User pass
       yield Api.singInApi(action.user);
    } catch (error) {
        console.log(error)
    }
}

export function* watchSingInUserEmployer() {
    yield takeLatest(SINGIN_EMPLOYER, singInUser);
}


// logout
// function* logoutSaga() {
//     try {
//         yield put({type: LOGOUT_SUCCEEDED});
//
//     } catch (error) {
//         //do nothing
//     }
// }
//
// export function* watchLogoutSaga() {
//     yield takeLatest(LOGOUT, logoutSaga);
// }
