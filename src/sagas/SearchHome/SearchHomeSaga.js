import { Api } from './Api';
import {
    KEY_WORKS, KEY_WORKS_SUCCEEDED,
    SEARCH_FORM_HOME,
    SEARCH_FORM_HOME_SUCCEEDED,

} from '../../actions/actionTypes';
import { put, takeLatest } from 'redux-saga/effects';
import {NavigationActions} from "react-navigation";
import {SearchDetail} from "../../vendor/screen";
import {FETCH_DATA_FAIL, LOADDING_BEGIN} from "../../actions/actionLoading";


// function* SearchHomeSaga(action) {
//     try {
//         // yield put({ type: LOADDING_BEGIN });
//         // const result = yield Api.SearchHomeApi(action);
// // console.log(action);
//         yield put({ type: SEARCH_FORM_HOME_SUCCEEDED, listSearch: action});
//
//
//     } catch (error) {
//         console.log(error)
//         // yield put({ type: FETCH_DATA_FAIL, listSearch: { } });
//     }
// }
// export function* watchSearchHomeSaga() {
//     yield takeLatest(SEARCH_FORM_HOME, SearchHomeSaga);
// }



function* keyWordsSaga() {
    try {
        // yield put({ type: LOADDING_BEGIN });
        // console.log('key')
        const result = yield Api.keyWords();
// console.log(result);
        yield put({ type: KEY_WORKS_SUCCEEDED, keywords: result.keywords});


    } catch (error) {
        console.log(error)
        // yield put({ type: FETCH_DATA_FAIL, listSearch: { } });
    }
}
export function* watchkeyWordsSaga() {
    yield takeLatest(KEY_WORKS, keyWordsSaga);
}
