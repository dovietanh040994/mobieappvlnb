// import { Api } from './Api';
// import {
//     DETAIL_NEW_ACTION,
//     DETAIL_BIG_NUMBER_ACTION,
//     DETAIL_SALARY_ACTION,
//     SEARCH_FORM_HOME_SUCCEEDED
//
// } from '../../actions/actionTypes';
// import { put, takeLatest } from 'redux-saga/effects';
// import {NavigationActions} from "react-navigation";
// import {SearchDetail} from "../../vendor/screen";
// import {FETCH_DATA_FAIL, LOADDING_BEGIN} from "../../actions/actionLoading";
//
//
// function* DetailNewSaga(action) {
//     try {
//         yield put({ type: LOADDING_BEGIN, listSearch: { page: action.page } });
//
//         const result = yield Api.DetailNewApi(action.page);
//
//         yield put({ type: SEARCH_FORM_HOME_SUCCEEDED, listSearch: result});
//
//
//     } catch (error) {
//         console.log(error)
//         yield put({ type: FETCH_DATA_FAIL, listSearch: { } });
//     }
// }
// export function* watchDetailNewSaga() {
//     yield takeLatest(DETAIL_NEW_ACTION, DetailNewSaga);
// }
//
// function* DetailSalarySaga(action) {
//     try {
//
//         yield put({ type: LOADDING_BEGIN, listSearch: { page: action.page } });
//         const result = yield Api.DetailSalaryApi(action.page);
//         yield put({ type: SEARCH_FORM_HOME_SUCCEEDED, listSearch: result});
//
//     } catch (error) {
//         console.log(error)
//         yield put({ type: FETCH_DATA_FAIL, listSearch: { } });
//     }
// }
// export function* watchDetailSalarySaga() {
//     yield takeLatest(DETAIL_SALARY_ACTION, DetailSalarySaga);
// }
//
// function* DetailBigNumberSaga(action) {
//     try {
//         yield put({ type: LOADDING_BEGIN, listSearch: { page: action.page } });
//         const result = yield Api.DetailBigNumberApi(action.page);
//
//         yield put({ type: SEARCH_FORM_HOME_SUCCEEDED, listSearch: result});
//
//
//     } catch (error) {
//         console.log(error)
//         yield put({ type: FETCH_DATA_FAIL, listSearch: { } });
//     }
// }
// export function* watchDetailBigNumberSaga() {
//     yield takeLatest(DETAIL_BIG_NUMBER_ACTION, DetailBigNumberSaga);
// }
