import {LOADDING_BEGIN} from "../../actions/actionTypes";
import {put} from 'redux-saga/effects';

const SearchUri = 'http://210.211.124.84:9001/vlnb/api/job/getjobpost';
import {
    LoaddingBegin,
    fetchDataFail,
    SearchFormAction

} from '../../actions/index';
import { NavigationActions } from 'react-navigation';
import {SearchDetail} from "../../vendor/screen";
import AsyncStorage from "@react-native-community/async-storage";

//
// let searchActionData = async (data) => {
//     try {
//         await AsyncStorage.setItem('@searchAction:key', data)
//     } catch (error) {
//         // Error saving data
//     }
//     return
// };
// const getsearchAction = async () => {
//     let data = '';
//     try {
//         data = await AsyncStorage.getItem('@searchAction:key') || 'none';
//         return data;
//
//     } catch (error) {
//         // Error retrieving data
//         console.log(error.message);
//     }
//     return
// }
// const getUserId = async () => {
//     let userId = '';
//     try {
//         userId = await AsyncStorage.getItem('@userId:key') || 'none';
//         return userId;
//
//     } catch (error) {
//         // Error retrieving data
//         console.log(error.message);
//     }
//     return
// }
// function* SearchHomeApi(action) {
//
//     var province = action.info.province?action.info.province:'';
//     var carrer = action.info.carrer?action.info.carrer:'';
//     var salary = action.info.salary?action.info.salary:'';
//     var industrials = action.info.industrials?action.info.industrials:'';
//     var experience = action.info.experience?action.info.experience:'';
//     var dataStorage = `province_id=${province}&job_carrer_id=${carrer}&job_salary_id=${salary}&industrial_zone_id=${industrials}&job_experience_id=${experience}`;
//     let gettUID = yield getUserId()
//     // yield put(NavigationActions.navigate({ routeName: SearchDetail }))
//     // yield put(LoaddingBegin());
//     yield searchActionData(`${dataStorage}`);
//     var data = yield getsearchAction()
//     return yield fetch(SearchUri, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/x-www-form-urlencoded',
//         },
//         body: `${data}&sort_name=job_salary_id&rows_number=10&user_id=${gettUID}&rows_start=0`
//
//     }).then((response) => response.json())
//         .then((responseJson) => {
//
//             if (responseJson.err_code === 0) {
//                 // put(SearchFormAction(responseJson));
//
//                 // put(NavigationActions.navigate({ routeName: 'Login' }))
//                 return responseJson
//
//             }
//         })
//         .catch((error) => {
//             console.error(error);
//             // put(fetchDataFail(error))
//         });
// }
//
// function* DetailNewApi(page) {
//     // yield put(LoaddingBegin());
//     var data = yield getsearchAction()
//     let gettUID = yield getUserId()
//     return yield fetch(SearchUri, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/x-www-form-urlencoded',
//         },
//         body: `${data}&sort_name=ended_at&rows_number=10&user_id=${gettUID}&rows_start=${page}`
//
//     }).then((response) => response.json())
//         .then((responseJson) => {
//             if (responseJson.err_code === 0) {
//                 return responseJson
//             }
//         })
//         .catch((error) => {
//             console.error(error);
//         });
// }
//
// function* DetailSalaryApi(page) {
//     console.log('DetailSalaryApi')
//     console.log(page)
//     // yield put(LoaddingBegin());
//     var data = yield getsearchAction();
//     let gettUID = yield getUserId()
//     return yield fetch(SearchUri, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/x-www-form-urlencoded',
//         },
//         body: `${data}&sort_name=job_salary_id&rows_number=10&user_id=${gettUID}&rows_start=${page}`
//
//     }).then((response) => response.json())
//         .then((responseJson) => {
//             if (responseJson.err_code === 0) {
//                 return responseJson
//             }
//         })
//         .catch((error) => {
//             console.error(error);
//         });
// }
//
// function* DetailBigNumberApi(page) {
//     // yield put(LoaddingBegin());
//     var data = yield getsearchAction()
//     let gettUID = yield getUserId()
//     return yield fetch(SearchUri, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/x-www-form-urlencoded',
//         },
//         body: `${data}&sort_name=number_of_recruitment&rows_number=10&user_id=${gettUID}&rows_start=${page}`
//
//     }).then((response) => response.json())
//         .then((responseJson) => {
//             if (responseJson.err_code === 0) {
//                 return responseJson
//             }
//         })
//         .catch((error) => {
//             console.error(error);
//         });
// }

function* keyWords(page) {
    // yield put(LoaddingBegin());
    const Url = `http://vieclamnambo.vn:9002/api/vlnb/job/keyword`;
    return yield fetch(Url, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            }
        })
        .catch((error) => {
            console.error(error);
        });
}
export const Api = {
    keyWords,
    // DetailNewApi,
    // DetailSalaryApi,
    // DetailBigNumberApi
};
