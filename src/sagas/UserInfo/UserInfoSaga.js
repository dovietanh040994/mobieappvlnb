import {Api} from './Api';
import {
    FETCH_USER_INFO, FETCH_USER_INFO_SUCCEEDED,
    UPDATE_USER_INFO, UPDATE_USER_INFO_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchUserInfoSaga() {
    try {

        const result = yield Api.apiFetchUserInfo();

        yield put({type: FETCH_USER_INFO_SUCCEEDED, userInfo: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchUserInfoSaga() {
    yield takeLatest(FETCH_USER_INFO, fetchUserInfoSaga);
}

function* updateUserInfoSaga(action) {
    try {
        // console.log(action.params);

        const result = yield Api.apiUpdateUserInfo(action.params);

        // console.log(result);

        if (result.err_code === 0) {
            yield put({type: UPDATE_USER_INFO_SUCCEEDED, userInfo: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchUpdateUserInfoSaga() {
    yield takeLatest(UPDATE_USER_INFO, updateUserInfoSaga);
}
