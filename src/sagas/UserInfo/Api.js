import AsyncStorage from "@react-native-community/async-storage";
import {FetchCvUser, UpdateCvUser} from "../../actions";
import {Toast} from "native-base";
import NavigationService from "../../vendor/NavigationService";
import {ShowProfile} from "../../vendor/screen";

const urlFetchUserInfo = 'http://vieclamnambo.vn:9002/api/vlnb/user/getuserinfo';
const urlUpdateUserInfo = 'http://vieclamnambo.vn:9002/api/vlnb/user/updateuser';

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

//send DELETE request to update existing
function* apiFetchUserInfo() {

    let token = yield getUserId();

    const subscriber = yield fetch(urlFetchUserInfo, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield subscriber
}

function* apiUpdateUserInfo(values) {

    let token = yield getUserId();

    let educations = [];
    let experiences = [];
    let skills = [];

    if (values.educations.length !== 0) {
        values.educations.map((item) => {
            let start_date = '01/01/2000';
            let end_date = '01/01/2000';

            if (item.month_start && item.year_start) {
                if (item.month_start.length === 1) {
                    start_date = '01/0' + item.month_start + '/' + item.year_start;
                } else {
                    start_date = '01/' + item.month_start + '/' + item.year_start;
                }
            }
            if (item.month_end && item.year_end) {
                if (item.month_end.length === 1) {
                    end_date = '01/0' + item.month_end + '/' + item.year_end;
                } else {
                    end_date = '01/' + item.month_end + '/' + item.year_end;
                }
            }
            educations.push({
                university_id: item.university_id ? item.university_id : -1,
                major_id: item.major_id ? item.major_id : -1,
                start_date: start_date,
                end_date: end_date,
                degree_id: item.degree_id ? item.degree_id : -1,
                cetificate_id: -1,
            })
        });
        values['educations'] = educations;
    }
    if (values.experiences.length !== 0) {
        values.experiences.map((item) => {
            let start_date = '01/01/2000';
            let end_date = '01/01/2000';
            if (item.month_start && item.year_start) {
                if (item.month_start.length === 1) {
                    start_date = '01/0' + item.month_start + '/' + item.year_start;
                } else {
                    start_date = '01/' + item.month_start + '/' + item.year_start;
                }
            }
            if (item.month_end && item.year_end) {
                if (item.month_end.length === 1) {
                    end_date = '01/0' + item.month_end + '/' + item.year_end;
                } else {
                    end_date = '01/' + item.month_end + '/' + item.year_end;
                }
            }
            experiences.push({
                start_date: start_date,
                end_date: end_date,
                company_name: item.company_name ? item.company_name : '',
                position_name: item.position_name ? item.position_name : '',
                description: '',
            })
        });
        values['experiences'] = experiences;
    }
    if (values.skills.length !== 0) {
        values.skills.map((item) => {
            skills.push({
                skill_name: item.skill_name ? item.skill_name : '',
                level: item.level ? item.level : -1,
            })
        });
        values['skills'] = skills;
    }

    values['job_salary_id'] = values.job_desire.job_salary_id;
    values['job_experience_id'] = values.job_desire.job_experience_id;
    values['job_type_id'] = values.job_desire.job_type_id;
    values['job_carrer_ids'] = values.job_desire.carrer_id !== 'undefined' ? values.job_desire.carrer_id : -1;
    values['short_description'] = values.job_desire.short_description;
    values['desire_for_work'] = values.job_desire.desire_for_work;

    // //values job_desire
    values['job_desire'] = {
        province_id: values.job_desire.province_id,
        carrer_id: values.job_desire.carrer_id,
        job_carrer_ids: values.job_desire.carrer_id !== 'undefined' ? values.job_desire.carrer_id : -1,
        job_salary_id: values.job_desire.job_salary_id,
        job_experience_id: values.job_desire.job_experience_id,
        job_type_id: values.job_desire.job_type_id,
        short_description: values.job_desire.short_description,
        desire_for_work: values.job_desire.desire_for_work
    };

    values['date_of_birth'] = values.date_of_birth === '' ? '01/01/2000' : values.date_of_birth;

    values['address'] = values.address ? values.address : '';


    console.log(values);

    const UserInfo = yield fetch(urlUpdateUserInfo, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify(values)
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield UserInfo
}

export const Api = {
    apiFetchUserInfo,
    apiUpdateUserInfo
};
