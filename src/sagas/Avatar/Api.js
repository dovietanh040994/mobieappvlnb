import AsyncStorage from "@react-native-community/async-storage";

const urlUpdateAvatar = 'http://vieclamnambo.vn:9002/api/vlnb/user/changeavatar2';

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}


function* apiUpdateAvatar(params) {

    let token = yield getUserId();

    const avatar = yield fetch(urlUpdateAvatar, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token,
        },
        body: JSON.stringify({
            data: params.data,
            file_name: params.fileName,
        })
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson);
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield avatar
}

export const Api = {
    apiUpdateAvatar
};
