import {Api} from './Api';
import {
   UPDATE_AVATAR_USER,UPDATE_AVATAR_USER_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* updateAvatarSaga(action) {
    try {
        // console.log(action.params);

        const result = yield Api.apiUpdateAvatar(action.params);

        // console.log(result);

        if (result.err_code === 0) {
            yield put({type: UPDATE_AVATAR_USER_SUCCEEDED, userAvatar: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchUpdateAvatarSaga() {
    yield takeLatest(UPDATE_AVATAR_USER, updateAvatarSaga);
}
