import { Api } from './Api';
import {
    CATEGORY_POST,
    CATEGORY_POST_SUCCEEDED,

} from '../../actions/actionTypes';
import { put, takeLatest } from 'redux-saga/effects';
import {LOADDING_CATEGORY_POST, LOADDING_USERDETAIL} from "../../actions/actionLoading";


function* CatePostSaga() {
    try {
        // console.log('CatePostSaga')
        yield put({type: LOADDING_CATEGORY_POST});
        const result = yield Api.CatePostApi();
        // console.log(144444444)
        // console.log(result.categories)
        //
        yield put({ type: CATEGORY_POST_SUCCEEDED, listCatePost: result.categories});

    } catch (error) {
        //do nothing
    }
}
export function* watchCatePostSaga() {
    yield takeLatest(CATEGORY_POST, CatePostSaga);
}


