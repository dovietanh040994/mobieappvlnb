import AsyncStorage from "@react-native-community/async-storage";
import {put} from "redux-saga/effects";
import {LoaddingCarrerDetail} from "../../actions";

const CarrerList = 'http://210.211.124.84:9001/vlnb/api/cat/getalljobcarrer';
const CarrerDetail = 'http://210.211.124.84:9001/vlnb/api/job/getjobpost';

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('@userId:key') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

function* CarrerAllApi() {

    // console.log(userDetailurl +`?id=${gettUID}`)
    const List = yield fetch(CarrerList, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log('CarrerAllApi')
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield List

}

// function* CarrerDetailApi(action) {
//
//     console.log('CarrerDetailApi')
//     console.log(action)
//     // yield put(LoaddingCarrerDetail());
//     let gettUID = yield getUserId();
//     console.log('page')
//     console.log(action.page)
//     console.log(typeof action.page)
//     let page = action.page;
//
//     const Detail = yield fetch(CarrerDetail, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/x-www-form-urlencoded',
//         },
//         body: `job_carrer_id=${action.carrerId}&user_id=${gettUID}&rows_start=${page}`
//     }).then((response) => response.json())
//
//         .then((responseJson) => {
//             console.log('responseJson')
//             console.log(responseJson)
//             if (responseJson.jobPosts.length > 0) {
//                 return {responseJson:responseJson.jobPosts,carrerName:action.carrerName,isListEnd:false}
//
//             }else{
//                 return {responseJson:responseJson.jobPosts,carrerName:action.carrerName,isListEnd:true}
//
//             }
//         })
//         .catch((error) => {
//             console.error(error);
//         });
//     return yield Detail
//
// }

export const Api = {
    CarrerAllApi,
    // CarrerDetailApi
};
