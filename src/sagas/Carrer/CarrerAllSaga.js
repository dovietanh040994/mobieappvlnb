import { Api } from './Api';
import {
    CARRER_LIST_ALL,
    CARRERLISTALL_SUCCEEDED,
    CARRER_DETAIL_SUCCEEDED,
    CARRER_DETAIL_ALL

} from '../../actions/actionTypes';
import { put, takeLatest } from 'redux-saga/effects';


function* CarrerListAllSaga() {
    try {
        // console.log('CarrerListAllSaga')
        const result = yield Api.CarrerAllApi();
        // console.log(result.jobCarrers)

        yield put({ type: CARRERLISTALL_SUCCEEDED, listCarrerAll: result.jobCarrers});

    } catch (error) {
        //do nothing
    }
}
export function* watchCarrerListAllSaga() {
    yield takeLatest(CARRER_LIST_ALL, CarrerListAllSaga);
}


function* CarrerDetailSaga(carrer) {
    try {
        // const result = yield Api.CarrerDetailApi(carrer);
        //
        // console.log('CarrerDetailSaga')
        // console.log(carrer)

        yield put({ type: CARRER_DETAIL_SUCCEEDED, listCarrerDetail: carrer});

    } catch (error) {
        //do nothing
    }
}
export function* watchCarrerDetailSaga() {
    yield takeLatest(CARRER_DETAIL_ALL, CarrerDetailSaga);
}
