import AsyncStorage from "@react-native-community/async-storage";

const urlUniversity = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getunivesity';


function* apiFetchUniversity() {
    const listUniversity = yield fetch(urlUniversity, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield listUniversity
}

export const Api = {
    apiFetchUniversity
};
