import {Api} from './Api';
import {
    FETCH_LIST_UNIVERSITY, FETCH_LIST_UNIVERSITY_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchUniversity() {
    try {

        const result = yield Api.apiFetchUniversity();

        yield put({type: FETCH_LIST_UNIVERSITY_SUCCEEDED, university: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchUniversity() {
    yield takeLatest(FETCH_LIST_UNIVERSITY, fetchUniversity);
}

