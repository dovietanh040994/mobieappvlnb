import {LOADDING_BEGIN} from "../../actions/actionTypes";
import {put} from 'redux-saga/effects';

const JobDetailURL = 'http://vieclamnambo.vn:9002/api/vlnb/job/detail';
const CompanyDetailURL = 'http://vieclamnambo.vn:9002/api/vlnb/job/getcompanyinfo';
const jobSameCompanyURL = 'http://vieclamnambo.vn:9002/api/vlnb/job/getjobpost';

import {
    LoaddingBegin,
} from '../../actions/index';
import AsyncStorage from "@react-native-community/async-storage";

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

const getUserIdEmployer = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfoEmployer') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

function* jobDetailApi(action) {

    const job = yield fetch(JobDetailURL + `?id=${action.idJob}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {

            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield job

}

function* companyDetailApi(action) {
    const job = yield fetch(CompanyDetailURL + `?job_post_id=${action.idJob}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    }).then((response) => response.json())

        .then((responseJson) => {

            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield job

}


export const Api = {
    jobDetailApi,
    companyDetailApi,
};
