import {Api} from './Api';
import {
    JOB_DETAIL_SUCCEEDED,
    JOB_DETAIL_ACTION,
    COMPANY_DETAIL_ACTION,
    COMPANY_DETAIL_SUCCEEDED,
    JOB_SAME_COMPANY_SUCCEEDED,
    JOB_SAME_COMPANY_ACTION,
    CREATE_JOB_POST, CREATE_JOB_POST_SUCCEEDED,
    UPDATE_JOB_POST, UPDATE_JOB_POST_SUCCEEDED, UPDATE_USER_INFO_SUCCEEDED

} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';
import {NavigationActions} from "react-navigation";
import {SearchDetail} from "../../vendor/screen";
import {LOADDING_DETAIL_JOB, LOADDING_DETAIL_COMPANY, LOADDING_JOB_SAME_COMPANY} from "../../actions/actionLoading";


function* JobDetailSaga(action) {
    try {

        yield put({type: LOADDING_DETAIL_JOB});

        const result = yield Api.jobDetailApi(action);
        yield put({type: JOB_DETAIL_SUCCEEDED, jobDetail: result.jobDetail});


    } catch (error) {
        //do nothing
    }
}

export function* watchJobDetailSaga() {
    yield takeLatest(JOB_DETAIL_ACTION, JobDetailSaga);
}

function* companyDetailSaga(action) {
    try {

        yield put({type: LOADDING_DETAIL_COMPANY});

        const result = yield Api.companyDetailApi(action);

        yield put({type: COMPANY_DETAIL_SUCCEEDED, companyDetail: result.companyInfo});


    } catch (error) {
        //do nothing
    }
}

export function* watchCompanyDetailSaga() {
    yield takeLatest(COMPANY_DETAIL_ACTION, companyDetailSaga);
}




