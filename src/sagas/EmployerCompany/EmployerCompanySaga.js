import {Api} from './Api';
import {
    FETCH_INFO_COMPANY, FETCH_INFO_COMPANY_SUCCEEDED,
    UPDATE_INFO_COMPANY, UPDATE_INFO_COMPANY_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchCompanySaga(action) {
    try {

        const result = yield Api.apiFetchCompany();

        if (result.err_code === 0) {
            yield put({type: FETCH_INFO_COMPANY_SUCCEEDED, companyInfo: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchFetchCompanyEmployerSaga() {
    yield takeLatest(FETCH_INFO_COMPANY, fetchCompanySaga);
}


function* updateCompanySaga(action) {
    try {

        const result = yield Api.apiUpdateCompany(action.params);

        if (result.err_code === 0) {
            yield put({type: UPDATE_INFO_COMPANY_SUCCEEDED, companyInfo: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchUpdateCompanyEmployerSaga() {
    yield takeLatest(UPDATE_INFO_COMPANY, updateCompanySaga);
}
