import AsyncStorage from "@react-native-community/async-storage";

const urlFetchCompany = 'http://vieclamnambo.vn:9002/api/vlnb/employee/getcompanyinfo';
const urlUpdateCompany = 'http://vieclamnambo.vn:9002/api/vlnb/employee/updatecompanyinfo';

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfoEmployer') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}


function* apiFetchCompany() {

    let token = yield getUserId();

    const subscriber = yield fetch(urlFetchCompany, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
             console.log(responseJson);
            if (responseJson.err_code === 0) {
                return responseJson
            }else {
                return [

                ]
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield subscriber
}

function* apiUpdateCompany(params) {

    let token = yield getUserId();

    const request = `?name=${params.name}&address=${params.address}&website=${params.website}&description=${params.description}`;

    console.log(request);

    const infoCompany = yield fetch(urlUpdateCompany + request, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield infoCompany
}

export const Api = {
    apiFetchCompany,
    apiUpdateCompany
};
