import {Api} from './Api';
import {
    FETCH_USER_CV_EMPLOYER, FETCH_USER_CV_EMPLOYER_SUCCEEDED,
    SAVE_CV_USER_SUCCEEDED,SAVE_CV_USER,
    UNSAVE_CV_USER_SUCCEEDED,UNSAVE_CV_USER
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchEmployerUserCv(action) {
    try {

        const result = yield Api.apiFetchEmployerUserCv(action.userId);

        console.log(result);

        yield put({type: FETCH_USER_CV_EMPLOYER_SUCCEEDED, userInfo: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchEmployerUserCv() {
    yield takeLatest(FETCH_USER_CV_EMPLOYER, fetchEmployerUserCv);
}


function* saveEmployerUserCv(action) {
    try {

        const result = yield Api.apiSaveEmployerUserCv(action.userId);

        console.log(result);

        yield put({type: SAVE_CV_USER_SUCCEEDED, userInfo: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchSaveEmployerUserCv() {
    yield takeLatest(SAVE_CV_USER, saveEmployerUserCv);
}

function* unSaveEmployerUserCv(action) {
    try {

        const result = yield Api.apiUnSaveEmployerUserCv(action.userId);

        console.log(result);

        yield put({type: UNSAVE_CV_USER_SUCCEEDED, userInfo: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchUnSaveEmployerUserCv() {
    yield takeLatest(UNSAVE_CV_USER, unSaveEmployerUserCv);
}

