import AsyncStorage from "@react-native-community/async-storage";

const urlEmployerUserCv = 'http://vieclamnambo.vn:9002/api/vlnb/employee/getusercv';
const urlSaveEmployerUserCv = 'http://vieclamnambo.vn:9002/api/vlnb/employee/saveuser';
const urlUnSaveEmployerUserCv = 'http://vieclamnambo.vn:9002/api/vlnb/employee/unsaveuser';

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfoEmployer') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

function* apiFetchEmployerUserCv(id) {
    let token = yield getUserId();

    console.log(token + `?user_id=${id}`);
    const userCv = yield fetch(urlEmployerUserCv + `?user_id=${id}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield userCv
}

function* apiSaveEmployerUserCv(id) {
    let token = yield getUserId();

    console.log(token + `?user_id=${id}`);
    const userCv = yield fetch(urlSaveEmployerUserCv + `?user_id=${id}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield userCv
}

function* apiUnSaveEmployerUserCv(id) {
    let token = yield getUserId();

    console.log(token + `?user_id=${id}`);
    const userCv = yield fetch(urlUnSaveEmployerUserCv + `?user_id=${id}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield userCv
}

export const Api = {
    apiFetchEmployerUserCv,
    apiSaveEmployerUserCv,
    apiUnSaveEmployerUserCv
};
