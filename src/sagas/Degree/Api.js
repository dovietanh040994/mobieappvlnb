import AsyncStorage from "@react-native-community/async-storage";

const urlDegree = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getdegree';


function* apiFetchDegree() {
    const listDegree = yield fetch(urlDegree, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield listDegree
}

export const Api = {
    apiFetchDegree
};
