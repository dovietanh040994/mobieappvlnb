import {Api} from './Api';
import {
    FETCH_LIST_DEGREE, FETCH_LIST_DEGREE_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchDegree() {
    try {

        const result = yield Api.apiFetchDegree();

        yield put({type: FETCH_LIST_DEGREE_SUCCEEDED, degree: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchDegree() {
    yield takeLatest(FETCH_LIST_DEGREE, fetchDegree);
}

