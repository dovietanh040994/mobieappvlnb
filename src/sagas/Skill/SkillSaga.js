import {Api} from './Api';
import {
    FETCH_LIST_SKILL, FETCH_LIST_SKILL_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchSkill() {
    try {

        const result = yield Api.apiFetchSkill();

        yield put({type: FETCH_LIST_SKILL_SUCCEEDED, skill: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchSkill() {
    yield takeLatest(FETCH_LIST_SKILL, fetchSkill);
}

