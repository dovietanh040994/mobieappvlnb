import AsyncStorage from "@react-native-community/async-storage";

const urlSkill = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getskill';


function* apiFetchSkill() {
    const listSkill = yield fetch(urlSkill, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield listSkill
}

export const Api = {
    apiFetchSkill
};
