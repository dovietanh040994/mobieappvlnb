import {LOADDING_BEGIN} from "../../actions/actionTypes";
import {put} from 'redux-saga/effects';

const urlCreatePost = 'http://vieclamnambo.vn:9002/api/vlnb/employee/insertjobpost';
const urlUpdatePost = 'http://vieclamnambo.vn:9002/api/vlnb/employee/updatejobpost';
const urlUpdateApprovePost = 'http://vieclamnambo.vn:9002/api/vlnb/employee/updateapprove';

import AsyncStorage from "@react-native-community/async-storage";

const getUserIdEmployer = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfoEmployer') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}


function* createPostApi(values) {

    let token = yield getUserIdEmployer();

    console.log(values);

    const jobPost = yield fetch(urlCreatePost, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify(values)
    }).then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield jobPost
}

function* updatePostApi(values) {

    let token = yield getUserIdEmployer();

    console.log(values);

    const jobPost = yield fetch(urlUpdatePost, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify(values)
    }).then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield jobPost
}

function* updateApprovePostApi(id) {

    let token = yield getUserIdEmployer();

    console.log(token + `?job_post_id=${id}`);
    const job = yield fetch(urlUpdateApprovePost + `?job_post_id=${id}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield job
}


export const Api = {
    createPostApi,
    updatePostApi,
    updateApprovePostApi
};
