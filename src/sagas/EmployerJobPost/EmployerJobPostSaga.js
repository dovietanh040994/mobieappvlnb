import {Api} from './Api';
import {
    CREATE_JOB_POST, CREATE_JOB_POST_SUCCEEDED,
    UPDATE_JOB_POST,UPDATE_JOB_POST_SUCCEEDED,
    UPDATE_APPROVE_JOB_POST,UPDATE_APPROVE_JOB_POST_SUCCEEDED

} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';

//create post
function* createPostSaga(action) {
    try {

        const result = yield Api.createPostApi(action.params);

        if (result.err_code === 0) {
            yield put({type: CREATE_JOB_POST_SUCCEEDED, jobPost: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchCreatePostSaga() {
    yield takeLatest(CREATE_JOB_POST, createPostSaga);
}


//update post
function* updatePostSaga(action) {
    try {

        const result = yield Api.updatePostApi(action.params);

        if (result.err_code === 0) {
            yield put({type: UPDATE_JOB_POST_SUCCEEDED, jobPost: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchUpdatePostSaga() {
    yield takeLatest(UPDATE_JOB_POST, updatePostSaga);
}

//update approve post
function* updateApprovePostSaga(action) {
    try {

        const result = yield Api.updateApprovePostApi(action.jobPostId);

        if (result.err_code === 0) {
            yield put({type: UPDATE_APPROVE_JOB_POST_SUCCEEDED, jobPost: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchUpdateApprovePostSaga() {
    yield takeLatest(UPDATE_APPROVE_JOB_POST, updateApprovePostSaga);
}




