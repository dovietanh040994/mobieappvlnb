import {Api} from './Api';
import {
    FETCH_DISTRICT,FETCH_DISTRICT_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchDistrict(action) {
    try {

        const result = yield Api.apiFetchDistrict(action.provinceId);

        console.log(result);

        yield put({type: FETCH_DISTRICT_SUCCEEDED, districts: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchDistrict() {
    yield takeLatest(FETCH_DISTRICT, fetchDistrict);
}

