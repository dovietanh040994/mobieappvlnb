import AsyncStorage from "@react-native-community/async-storage";

const urlDistrict = 'http://vieclamnambo.vn:9002/api/vlnb/cat/getdistrict';


function* apiFetchDistrict(id) {

    console.log(urlDistrict + `?province_id=${id}`);
    const listDistrict = yield fetch(urlDistrict + `?province_id=${id}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield listDistrict
}

export const Api = {
    apiFetchDistrict
};
