import AsyncStorage from "@react-native-community/async-storage";

const urlBuyCv = 'http://vieclamnambo.vn:9002/api/vlnb/employee/buycv';

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfoEmployer') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}


function* apiBuyCv(params) {

    let token = yield getUserId();
    let url = urlBuyCv + `?user_id=${params.user_id}&type=1&token=${params.token}&package_id=${params.package_id}`

    console.log(params);

    const avatar = yield fetch(url, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token,
        }
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield avatar
}

export const Api = {
    apiBuyCv
};
