import {Api} from './Api';
import {
    BUY_CV, BUY_CV_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* buyCvSaga(action) {
    try {
        // console.log(action.params);

        const result = yield Api.apiBuyCv(action.params);

        if (result.err_code === 0) {
            yield put({type: BUY_CV_SUCCEEDED, userInfo: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchBuyCvSaga() {
    yield takeLatest(BUY_CV, buyCvSaga);
}
