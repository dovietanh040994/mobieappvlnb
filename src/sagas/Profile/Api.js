import AsyncStorage from "@react-native-community/async-storage";

const urlDeleteJobApply = 'http://vieclamnambo.vn:9002/api/vlnb/user/unapplyjob';
const urlDeleteJobCare = 'http://vieclamnambo.vn:9002/api/vlnb/user/unsavepost';


const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

//send DELETE request to update existing
function* deleteJobApplyFromApi(params) {
    let token = yield getUserId();
    // console.log(params);
    // console.log(urlDeleteJobApply + `?job_post_id=${params.idJob}`);
    const deleteJobApply = yield fetch(urlDeleteJobApply + `?job_post_id=${params.idJob}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield deleteJobApply
}

//send DELETE request to update existing Movie
function* deleteJobCareFromApi(params) {
    let token = yield getUserId();
    // console.log(urlDeleteJobCare + `?job_post_id=${params.idJob}`);
    const deleteJobCare = yield fetch(urlDeleteJobCare + `?job_post_id=${params.idJob}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield deleteJobCare

}


export const Api = {
    deleteJobApplyFromApi,
    deleteJobCareFromApi
};
