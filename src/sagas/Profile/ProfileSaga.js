import {Api} from './Api';
import {
    DELETE_JOB_APPLY_SUCCEEDED, DELETE_JOB_APPLY,
    DELETE_JOB_CARE_SUCCEEDED, DELETE_JOB_CARE
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* deleteJobCareSaga(action) {
    try {

        console.log(action)

        const result = yield Api.deleteJobCareFromApi(action.params);

        if (result.err_code === 0) {
            yield put({type: DELETE_JOB_CARE_SUCCEEDED, params: action.params});

        }

    } catch (error) {
        //do nothing
    }
}

export function* watchDeleteJobCareSaga() {
    yield takeLatest(DELETE_JOB_CARE, deleteJobCareSaga);
}

function* deleteJobApplySaga(action) {
    try {

        console.log(action)

        const result = yield Api.deleteJobApplyFromApi(action.params);

        if (result.err_code === 0) {
            yield put({type: DELETE_JOB_APPLY_SUCCEEDED, params: action.params});

        }

    } catch (error) {
        //do nothing
    }
}

export function* watchDeleteJobApplySaga() {
    yield takeLatest(DELETE_JOB_APPLY, deleteJobApplySaga);
}
