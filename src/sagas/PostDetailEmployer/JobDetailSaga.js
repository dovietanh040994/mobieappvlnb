import { Api } from './Api';
import {
    POST_DETAIL_EMPLOYER_SUCCEEDED,
    POST_DETAIL_EMPLOYER_ACTION,

} from '../../actions/actionTypes';
import { put, takeLatest } from 'redux-saga/effects';
import {NavigationActions} from "react-navigation";
import {SearchDetail} from "../../vendor/screen";
import {LOADDING_DETAIL_EMPLOYER_POST} from "../../actions/actionLoading";


function* PostDetailEmployerSaga(action) {
    try {

        yield put({ type: LOADDING_DETAIL_EMPLOYER_POST });

       const result = yield Api.postDetailEmployerApi(action);
        yield put({type: POST_DETAIL_EMPLOYER_SUCCEEDED,postDetail:result.jobDetail});


    } catch (error) {
        //do nothing
    }
}
export function* watchPostDetailEmployerSaga() {
    yield takeLatest(POST_DETAIL_EMPLOYER_ACTION, PostDetailEmployerSaga);
}



