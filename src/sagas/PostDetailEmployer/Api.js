const PostDetailURL = 'http://vieclamnambo.vn:9002/api/vlnb/employee/getjobdetail';
import AsyncStorage from "@react-native-community/async-storage";

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfoEmployer') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

function* postDetailEmployerApi(action) {
    let gettUID = yield getUserId();
    console.log(gettUID)
    console.log(PostDetailURL + `?id=${action.idPost}`)
    const job = yield fetch(PostDetailURL + `?id=${action.idPost}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': gettUID

        },
    }).then((response) => response.json())

        .then((responseJson) => {
            console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield job

}


export const Api = {
    postDetailEmployerApi,
};
