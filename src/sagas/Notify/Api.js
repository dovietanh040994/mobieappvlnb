import AsyncStorage from "@react-native-community/async-storage";

const urlSubscriber = 'http://vieclamnambo.vn:9002/api/vlnb/user/subscriber';
const urlUnSubscriber = 'http://vieclamnambo.vn:9002/api/vlnb/user/unsubscriber';


const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

//send DELETE request to update existing
function* apiSubscriber(authUser) {
    let token = yield getUserId();
    console.log(urlSubscriber);
    const subscriber = yield fetch(urlSubscriber, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield subscriber
}

function* apiUnSubscriber(authUser) {
    let token = yield getUserId();
    console.log(urlUnSubscriber);
    const subscriber = yield fetch(urlUnSubscriber, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield subscriber
}

export const Api = {
    apiSubscriber,
    apiUnSubscriber
};
