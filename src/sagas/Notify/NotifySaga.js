import {Api} from './Api';
import {
    SWITCH_NOTIFY_SUCCEEDED, SWITCH_NOTIFY
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* subscribereSaga(action) {
    try {

        if (action.status) {
            const result = yield Api.apiSubscriber('a');
        } else {
            const result = yield Api.apiUnSubscriber('a');
        }

        if (result.err_code === 0) {
            yield put({type: SWITCH_NOTIFY_SUCCEEDED, status: action.status});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchSubscribereSaga() {
    yield takeLatest(SWITCH_NOTIFY, subscribereSaga);
}
