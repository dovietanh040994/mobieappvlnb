//call dùng để chạy 1 saga nào đấy,all là chạy nhiều saga cùng 1 thời điểm
//fork: saga cũ đang thực hiện thì saga mới vẫn tiếp tục thực hiện đc(k chặn)
import {fork, call} from 'redux-saga/effects';

import {
    watchcheckAndAddUser,
    watchUserDetailSaga,
    watchEditUserSaga,
    watchProfileSaga,
    watchSalarySaga,
    watchcarrerSaga,
    watchIndustrialSaga,
    watchExperienceSaga,
    watchLogoutSaga,
    watchJobTypesSaga,
    watchFetchUserDetailSage,
    watchFetchProvinceListSage,
    watchSingInUser,
    watchVerifyUser,
    watchFetchJobLevelSage, watchResetPass
} from './User/UserSaga';

import {
    watchCarrerListAllSaga,
    watchCarrerDetailSaga
} from './Carrer/CarrerAllSaga';

import {
    // watchSearchHomeSaga,
    watchkeyWordsSaga,
} from './SearchHome/SearchHomeSaga';

import {
    // watchDetailNewSaga,
    // watchDetailSalarySaga,
    // watchDetailBigNumberSaga
} from './SearchHome/SearchDetailSaga';

import {
    watchSaveJobSaga,
    watchUnSaveJobSaga,
    watchappliedSaga,
} from './SaveJob/SaveJobSaga'

import {
    // watchListSaveSaga
} from './CarrerSaveUID/ListSaveSaga'

import {
    watchJobDetailSaga,
    watchCompanyDetailSaga,
} from './JobDetail/JobDetailSaga'

import {
    // watchSalaryProvinceSaga,
    // watchSalaryIndustrialSaga
} from './SalaryTab/SalaryTabSaga'

// import {
//     watchSearchAdvancedSaga
// } from './SearchAdvanced/SearchAdvancedSaga'

import {
    watchSubscribereSaga
} from './Notify/NotifySaga'

/////
import {
    watchCatePostSaga
} from './CategoryPost/CategoryPostSaga'

import {
    watchDeleteJobCareSaga,
    watchDeleteJobApplySaga
} from './Profile/ProfileSaga';


import {
    watchFetchCVSaga, watchUpdateCVSaga
} from './CV/CVSaga';

import {
    watchUpdateAvatarSaga
} from './Avatar/AvatarSaga';

import {
    watchFetchMediaRelation
} from './Media/MediaSaga';

import {
    watchFetchUniversity
} from './University/UniversitySaga';

import {
    watchFetchMajor
} from './Major/MajorSaga';

import {
    watchFetchDegree
} from './Degree/DegreeSaga';

import {
    watchFetchSkill
} from './Skill/SkillSaga';

import {
    watchFetchUserInfoSaga,
    watchUpdateUserInfoSaga
} from './UserInfo/UserInfoSaga';

import {watchcheckAndAddUserEmployer, watchSingInUserEmployer} from "./UserEmployer/UserSaga";

import {
    watchFetchEmployerUserCv,
    watchSaveEmployerUserCv,
    watchUnSaveEmployerUserCv
} from './EmployerUserCv/EmployerUserCvSaga';

import {
    watchFetchCompanyEmployerSaga, watchUpdateCompanyEmployerSaga
} from './EmployerCompany/EmployerCompanySaga';

import {watchFetchDistrict} from "./District/DistrictSaga";

import {
    watchCreatePostSaga,
    watchUpdatePostSaga,
    watchUpdateApprovePostSaga
} from "./EmployerJobPost/EmployerJobPostSaga"
import {watchPostDetailEmployerSaga} from "./PostDetailEmployer/JobDetailSaga";

import {watchBuyCvSaga} from "./BuyCv/BuyCvSaga"

export default function* rootSaga() {

    yield fork(watchcheckAndAddUser);
    yield fork(watchLogoutSaga);
    yield fork(watchUserDetailSaga);
    yield fork(watchEditUserSaga);
    yield fork(watchProfileSaga);
    yield fork(watchSalarySaga);
    yield fork(watchcarrerSaga);
    yield fork(watchIndustrialSaga);
    yield fork(watchExperienceSaga);
    yield fork(watchJobTypesSaga);

    yield fork(watchCarrerListAllSaga);
    yield fork(watchCarrerDetailSaga);


    yield fork(watchJobDetailSaga);
    yield fork(watchResetPass);

    // yield fork(watchSearchAdvancedSaga);


    // yield fork(watchDetailNewSaga);
    // yield fork(watchDetailSalarySaga);
    // yield fork(watchDetailBigNumberSaga);

    // yield fork(watchListSaveSaga);

    // yield fork(watchSalaryProvinceSaga);
    // yield fork(watchSalaryIndustrialSaga);

    /////
    yield fork(watchCatePostSaga);
    yield fork(watchCompanyDetailSaga);
    yield fork(watchDeleteJobCareSaga);
    yield fork(watchSubscribereSaga);
    yield fork(watchFetchUserDetailSage);
    yield fork(watchFetchProvinceListSage);
    yield fork(watchkeyWordsSaga);
    yield fork(watchDeleteJobApplySaga);
    yield fork(watchFetchCVSaga);
    yield fork(watchUpdateCVSaga);
    yield fork(watchUpdateAvatarSaga);
    yield fork(watchSaveJobSaga);
    yield fork(watchUnSaveJobSaga);
    yield fork(watchFetchMediaRelation);
    yield fork(watchappliedSaga);
    yield fork(watchSingInUser);
    yield fork(watchVerifyUser);
    yield fork(watchFetchUniversity);
    yield fork(watchFetchMajor);
    yield fork(watchFetchDegree);
    yield fork(watchFetchSkill);
    yield fork(watchFetchUserInfoSaga);
    yield fork(watchUpdateUserInfoSaga);
    yield fork(watchcheckAndAddUserEmployer);
    yield fork(watchSingInUserEmployer);
    yield fork(watchFetchEmployerUserCv);
    yield fork(watchFetchJobLevelSage);
    yield fork(watchFetchDistrict);
    yield fork(watchCreatePostSaga);
    yield fork(watchFetchCompanyEmployerSaga);
    yield fork(watchUpdateCompanyEmployerSaga);
    yield fork(watchPostDetailEmployerSaga);
    yield fork(watchUpdatePostSaga);
    yield fork(watchSaveEmployerUserCv);
    yield fork(watchUpdateApprovePostSaga);
    yield fork(watchBuyCvSaga);
    yield fork(watchUnSaveEmployerUserCv);
}
