import AsyncStorage from "@react-native-community/async-storage";

const urlMediaRelation = 'http://vieclamnambo.vn:9002/api/vlnb/news/getrelateds';

//send DELETE request to update existing
function* apiFetchMediaRelation(id) {
    const listMedia = yield fetch(urlMediaRelation + `?id=${id}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            if (responseJson.err_code === 0) {
                return responseJson
            } else {
                return []
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield listMedia
}

export const Api = {
    apiFetchMediaRelation
};
