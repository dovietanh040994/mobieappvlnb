import {Api} from './Api';
import {
    FETCH_MEDIA_RELATION, FETCH_MEDIA_RELATION_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchMediaRelation(action) {
    try {

        const result = yield Api.apiFetchMediaRelation(action.id);

        console.log('saga');
        // console.log(result);

        yield put({type: FETCH_MEDIA_RELATION_SUCCEEDED, media: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchMediaRelation() {
    yield takeLatest(FETCH_MEDIA_RELATION, fetchMediaRelation);
}

