import {Api} from './Api';
import {
    FETCH_CV_USER, FETCH_CV_USER_SUCCEEDED,
    UPDATE_CV_USER, UPDATE_CV_USER_SUCCEEDED
} from '../../actions/actionTypes';
import {put, takeLatest} from 'redux-saga/effects';


function* fetchCVSaga(action) {
    try {

        const result = yield Api.apiFetchCV();

        yield put({type: FETCH_CV_USER_SUCCEEDED, userCV: result});


    } catch (error) {
        //do nothing
    }
}

export function* watchFetchCVSaga() {
    yield takeLatest(FETCH_CV_USER, fetchCVSaga);
}

function* updateCVSaga(action) {
    try {
        // console.log(action.params);

        const result = yield Api.apiUpdateCV(action.params);

        // console.log(result);

        if (result.err_code === 0) {
            yield put({type: UPDATE_CV_USER_SUCCEEDED, userCV: result});
        }

    } catch (error) {
        //do nothing
    }
}

export function* watchUpdateCVSaga() {
    yield takeLatest(UPDATE_CV_USER, updateCVSaga);
}
