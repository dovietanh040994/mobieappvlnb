import AsyncStorage from "@react-native-community/async-storage";

const urlFetchCV = 'http://vieclamnambo.vn:9002/api/vlnb/user/getcv';
const urlUpdateCV = 'http://vieclamnambo.vn:9002/api/vlnb/user/updatecv';

const getUserId = async () => {
    let userId = '';
    try {
        userId = await AsyncStorage.getItem('userInfo') || 'none';
        return userId;

    } catch (error) {
        // Error retrieving data
        console.log(error.message);
    }
    return
}

//send DELETE request to update existing
function* apiFetchCV() {

    let token = yield getUserId();

    const subscriber = yield fetch(urlFetchCV, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.err_code === 0) {
                return responseJson
            }else {
                return [

                ]
            }
        })
        .catch((error) => {
            console.error(error);
        });
    return yield subscriber
}

function* apiUpdateCV(params) {

    let token = yield getUserId();

    // console.log(token);

    const request = `?province_id=${params.province}&job_carrer_ids=${params.carrer}&job_salary_id=${params.salary}&job_experience_id=${params.experience}&job_type_id=${params.jobTypes}&short_description=${params.short_description}&desire_for_work=${params.desire_for_work}`;

    const cv = yield fetch(urlUpdateCV + request, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    }).then((response) => response.json())

        .then((responseJson) => {
            // console.log(responseJson)
            return responseJson
        })
        .catch((error) => {
            console.error(error);
        });
    return yield cv
}

export const Api = {
    apiFetchCV,
    apiUpdateCV
};
