import React, {Component} from "react";
import {Image, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text, Thumbnail} from "native-base";
import {Fade, Placeholder, PlaceholderLine, PlaceholderMedia} from "rn-placeholder";


class PlaceholderLoadingPost extends Component {

    render() {

        return (
            <View style={{flex: 1, flexDirection: 'column'}}>

                <CardItem
                    style={{
                        backgroundColor: 'white',
                        borderTopRightRadius: 5,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        borderBottomRightRadius: 5,
                        marginRight: 5,
                        marginLeft: 5,
                        borderBottom: 0,
                        marginTop: 5,
                        marginBottom:10,
                        paddingBottom:0

                    }}>
                    <Placeholder
                        style={{marginBottom: 20}}
                        Left={props => (
                            <PlaceholderMedia
                                // isRound={true}
                                style={[{width: 100, height: 100}, props.style]}
                            />
                        )}
                        // Right={PlaceholderMedia}
                        Animation={Fade}>
                        <PlaceholderLine style={{marginTop:10}}/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>

                    </Placeholder>
                </CardItem>
                <CardItem
                    style={{
                        backgroundColor: 'white',
                        borderTopRightRadius: 5,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        borderBottomRightRadius: 5,
                        marginRight: 5,
                        marginLeft: 5,
                        borderBottom: 0,
                        marginTop: 5,
                        marginBottom:10,
                        paddingBottom:0

                    }}>
                    <Placeholder
                        style={{marginBottom: 20}}
                        Left={props => (
                            <PlaceholderMedia
                                // isRound={true}
                                style={[{width: 100, height: 100}, props.style]}
                            />
                        )}
                        // Right={PlaceholderMedia}
                        Animation={Fade}>
                        <PlaceholderLine style={{marginTop:10}}/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>

                    </Placeholder>
                </CardItem>
                <CardItem
                    style={{
                        backgroundColor: 'white',
                        borderTopRightRadius: 5,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        borderBottomRightRadius: 5,
                        marginRight: 5,
                        marginLeft: 5,
                        borderBottom: 0,
                        marginTop: 5,
                        marginBottom:10,
                        paddingBottom:0

                    }}>
                    <Placeholder
                        style={{marginBottom: 20}}
                        Left={props => (
                            <PlaceholderMedia
                                // isRound={true}
                                style={[{width: 100, height: 100}, props.style]}
                            />
                        )}
                        // Right={PlaceholderMedia}
                        Animation={Fade}>
                        <PlaceholderLine style={{marginTop:10}}/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>

                    </Placeholder>
                </CardItem>
                <CardItem
                    style={{
                        backgroundColor: 'white',
                        borderTopRightRadius: 5,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        borderBottomRightRadius: 5,
                        marginRight: 5,
                        marginLeft: 5,
                        borderBottom: 0,
                        marginTop: 5,
                        marginBottom:10,
                        paddingBottom:0

                    }}>
                    <Placeholder
                        style={{marginBottom: 20}}
                        Left={props => (
                            <PlaceholderMedia
                                // isRound={true}
                                style={[{width: 100, height: 100}, props.style]}
                            />
                        )}
                        // Right={PlaceholderMedia}
                        Animation={Fade}>
                        <PlaceholderLine style={{marginTop:10}}/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>

                    </Placeholder>
                </CardItem>
                <CardItem
                    style={{
                        backgroundColor: 'white',
                        borderTopRightRadius: 5,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        borderBottomRightRadius: 5,
                        marginRight: 5,
                        marginLeft: 5,
                        borderBottom: 0,
                        marginTop: 5,
                        marginBottom:10,
                        paddingBottom:0

                    }}>
                    <Placeholder
                        style={{marginBottom: 20}}
                        Left={props => (
                            <PlaceholderMedia
                                // isRound={true}
                                style={[{width: 100, height: 100}, props.style]}
                            />
                        )}
                        // Right={PlaceholderMedia}
                        Animation={Fade}>
                        <PlaceholderLine style={{marginTop:10}}/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>
                        <PlaceholderLine/>

                    </Placeholder>
                </CardItem>
            </View>
        );
    }
}


export default PlaceholderLoadingPost
