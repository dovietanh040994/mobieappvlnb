import React, {Component} from 'react';
import firebase from "react-native-firebase";

export default class AdMob extends Component {

    render() {

        const Banner = firebase.admob.Banner;
        const AdRequest = firebase.admob.AdRequest;
        const request = new AdRequest();
        const unitId =
            'ca-app-pub-3717610865114273/9641339113';

        return (
            <Banner
                unitId={unitId}
                size={'SMART_BANNER'}
                request={request.build()}
                onAdLoaded={() => {
                    console.log('Advert loaded');
                }}
            />


        );
    }
}


