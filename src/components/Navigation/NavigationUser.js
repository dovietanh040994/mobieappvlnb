import {
    createSwitchNavigator,
    createStackNavigator,
    createAppContainer,
    createBottomTabNavigator
} from 'react-navigation';

import React from "react";
import {Home, HomeBox, JobDetail, Search, ShowProfile} from "../../vendor/screen";
import HomeComponent from "../UserView/Home/HomeComponent";
import JobDetailComponent from "../UserView/Home/JobDetailComponent";
import SearchComponent from "../UserView/Home/Search/SearchComponent";
import AdvancedSearchComponent from "../UserView/Home/Search/AdvancedSearchComponent";
import AdvancedSearchDetail from "../UserView/Home/Search/AdvancedSearchDetail";
import NotificationComponents from "../UserView/Notification/NotificationComponents";
import {Text} from "native-base";
import {Image} from "react-native";
import PostsNavigator from "../UserView/Posts";
import MediaNavigator from "../UserView/Media";
import ProfileNavigator from "../UserView/Profile";
import {NavigationActions,StackActions} from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";


//HomeNavigator
const HomeNavigator = createStackNavigator(
    {
        HomeBox: {
            screen:  HomeComponent,

        },
        JobDetail: {
            screen:  JobDetailComponent,

        },
        Search: {
            screen:  SearchComponent,

        },
        SearchAdvanced: {
            screen:  AdvancedSearchComponent,

        },
        AdvancedDetail: {
            screen:  AdvancedSearchDetail,

        },
    },{
        initialRouteName:'HomeBox'
    }

);

HomeNavigator.navigationOptions = ({ navigation }) => {
    let { routeName } = navigation.state.routes[navigation.state.index];
    let navigationOptions = {};

    if (routeName === JobDetail ) {
        navigationOptions.tabBarVisible = false;
    }

    return navigationOptions;
};

//HomeNavigator
const NotificationNavigator = createStackNavigator(
    {
        Notification: {
            screen:  NotificationComponents,
            navigationOptions: {
                header: null,
            },
        },


    },{
        initialRouteName:'Notification'
    }

)
//createBottomTabNavigator
const NavigationUser = createBottomTabNavigator(
    {
        Home: {
            screen:  HomeNavigator,
            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Home</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/ic_tab_home.png')}  style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarOnPress: () => {
                        const { routeName, key } = navigation.state.routes[0];
                        console.log(routeName, key)
                        navigation.navigate(routeName);
                        AsyncStorage.setItem('homeReload', JSON.stringify(true));
                    },
                    // tabBarVisible : navigation.state.index > 0 ?false:true

                }

            }
        },
        Posts: {
            screen:  PostsNavigator ,

            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Khám phá</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/Map_Inactive.png')}  style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarVisible : navigation.state.index > 0 ?false:true
                }

            }
        },
        Media: {
            screen:  MediaNavigator ,

            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Media</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/media/icon_media.png')}  style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarVisible : navigation.state.index > 0 ?false:true
                }

            }
        },
        Notification: {
            screen: NotificationNavigator,

            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Thông báo</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/Notification_Inactive.png')}  style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarOnPress: () => {
                        const { routeName, key } = navigation.state.routes[0];
                        console.log(routeName, key)
                        navigation.navigate(routeName);
                        AsyncStorage.setItem('notificationReload', JSON.stringify(true));
                    },
                    tabBarVisible : navigation.state.index > 0 ?false:true

                }

            }
        },
        ShowProfile: {
            screen: ProfileNavigator,
            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Cá nhân</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/Profile_Inactive.png')}  style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarVisible : navigation.state.index > 0 ?false:true
                }

            }
        },

    },
    {
        initialRouteName:'Home',
        tabBarPosition: 'bottom',
        tabBarOptions: {
            activeTintColor: '#43BDE2',
            inactiveTintColor: '#D0C9D6',
            style: {
                backgroundColor: '#fff',
                height:70,
                paddingBottom:10,
                paddingTop:10,
                borderTopWidth:1,
                borderColor:'#ccc'
            },
        },

    }
);

export default createAppContainer(NavigationUser);
