import {
    createSwitchNavigator,
    createStackNavigator,
    createAppContainer,
    createBottomTabNavigator
} from 'react-navigation';

import React from "react";
import {JobDetail, ShowProfile} from "../../vendor/screen";
import HomeComponent from "../EmployerView/Home/HomeComponent";
import AdvancedSearchComponent from "../EmployerView/Home/Search/AdvancedSearchComponent";
import AdvancedSearchDetail from "../EmployerView/Home/Search/AdvancedSearchDetail";
import EmployerPostNavigator from "../EmployerView/Post/index";
import ProfileUserComponent from "../EmployerView/ProfileUser/ProfileUserComponent";
import NotificationComponents from "../UserView/Notification/NotificationComponents";
import {Text} from "native-base";
import {Image} from "react-native";
import PostsNavigator from "../UserView/Posts";
import MediaNavigator from "../UserView/Media";
import CVFullComponent from "../UserView/Profile/CVFullComponent";
import ProfileEmployerNavigator from '../EmployerView/Profile/index'
import CVUserComponent from "../EmployerView/Post/CVUserComponent";

//HomeNavigator
const HomeNavigator = createStackNavigator(
    {
        HomeBoxEmployer: {
            screen:  HomeComponent,

        },
        ViewCVUser: {
            screen: CVUserComponent
        },
        // UserDetailEmployer: {
        //     screen:  UserDetailComponent,
        //
        // },
        // SearchEmployer: {
        //     screen:  SearchComponent,
        //
        // },
        SearchAdvancedEmployer: {
            screen:  AdvancedSearchComponent,

        },
        AdvancedDetailEmployer: {
            screen:  AdvancedSearchDetail,

        },
    },{
        initialRouteName:'HomeBoxEmployer'
    }

);

//createBottomTabNavigator
const NavigationEmployer = createBottomTabNavigator(
    {
        HomeEmployer: {
            screen:  HomeNavigator,
            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Ứng viên</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/ic_tab_home.png')}  style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    // tabBarVisible : navigation.state.index > 0 ?false:true
                }

            }
        },
        PostsEmployer: {
            screen:  EmployerPostNavigator ,

            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Tuyển dụng</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/icon_post.png')} style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarVisible : navigation.state.index > 0 ?false:true
                }

            }
        },
        ProfileUserEmployer: {
            screen:  ProfileUserComponent ,
            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Hồ sơ</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/icon_profile.png')} style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarVisible : navigation.state.index > 0 ?false:true
                }

            }
        },


        ShowProfileEmployer: {
            screen: ProfileEmployerNavigator,
            navigationOptions: ({navigation}) =>{
                return {

                    tabBarLabel: ({tintColor})=>(
                        <Text style={{fontSize: 10,fontWeight:'700',alignSelf:'center',color:tintColor}}>Cá nhân</Text>
                    ) ,
                    tabBarIcon:({tintColor})=>(
                        <Image source={require('../../vendor/images/Profile_Inactive.png')}  style={{
                            tintColor: tintColor
                        }}/>
                    ),
                    tabBarVisible : navigation.state.index > 0 ?false:true
                }

            }
        },

    },
    {
        initialRouteName:'HomeEmployer',
        tabBarPosition: 'bottom',
        tabBarOptions: {
            activeTintColor: '#43BDE2',
            inactiveTintColor: '#D0C9D6',
            style: {
                backgroundColor: '#fff',
                height:70,
                paddingBottom:10,
                paddingTop:10,
                borderTopWidth:1,
                borderColor:'#ccc'
            },
        },
    }
);

export default createAppContainer(NavigationEmployer);
