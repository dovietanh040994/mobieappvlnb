//LoadingNavigator
import {createAppContainer, createStackNavigator} from "react-navigation";
import LoadingComponent from "../Loadding/LoadingComponent";
import SelectUser from "../Loadding/SelectUser";
import Page from "../Loadding/Test";
import SelectInformation from "../Loadding/SelectInformation";
import NavigationUser from "./NavigationUser";
import NavigationEmployer from "./NavigationEmployer";
import LoginComponent from "../EmployerView/Login/LoginComponent";
import SinginComponent from "../EmployerView/Login/SinginComponent";

const  LoadingNavigator = createStackNavigator(
    {
        Loading: {
            screen:  LoadingComponent,
            navigationOptions: {
                header: null,
            },

        },
        SelectUser: {
            screen:  SelectUser,
            navigationOptions: {
                header: null,
            },

        },
        PageTest: {
            screen:  Page,
            navigationOptions: {
                header: null,
            },

        },
        SelectInfo: {
            screen:  SelectInformation,
            navigationOptions: {
                header: null,
            },

        },
        LoginEmployer: {
            screen:  LoginComponent,
            navigationOptions: {
                header: null,
            },

        },
        SignInEmployer: {
            screen:  SinginComponent,
            navigationOptions: {
                header: null,
            },

        },
        Index: {
            screen:  NavigationUser,
            navigationOptions: {
                header: null,
            },

        },
        IndexEmployer: {
            screen:  NavigationEmployer,
            navigationOptions: {
                header: null,
            },

        },

    },{
        initialRouteName:'Loading',
    }

)

export default createAppContainer(LoadingNavigator);
