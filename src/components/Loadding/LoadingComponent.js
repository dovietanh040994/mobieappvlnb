import React, {Component} from 'react';
import {Image, Animated, Alert, View, Platform} from 'react-native';
import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem,
    Toast
} from "native-base";

//Styles
import {StylesAll} from '../../vendor/styles'
import {
    Login,
    Home,
    JobDetail,
    JobDailly,
    EditProfile,
    SelectInfo,
    Index,
    HomeBox,
    SelectUser,
    ViewCVUser, PostDetailEmployer
} from "../../vendor/screen";
import firebase from "react-native-firebase";
import {connect} from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import NetInfo from "@react-native-community/netinfo";
import {getStatusBarHeight} from "react-native-status-bar-height";
import {CompanyDetailAction, FetchUserCVEmployer, JobDetailAction, PostEmployerDetailAction} from "../../actions";


class Loading extends Component {
    state = {
        fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
        connection_Status: "",
        checkNetwork: 0,
        initialNotification:false
    }

    async componentDidMount() {

        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this._handleConnectivityChange
        );

        Animated.timing(                  // Animate over time
            this.state.fadeAnim,            // The animated value to drive
            {
                toValue: 1,                   // Animate to opacity: 1 (opaque)
                duration: 2000,              // Make it take a while
            }
        ).start(() => {
            if (this.state.connection_Status === 'Online' && this.state.initialNotification === false) {
                // AsyncStorage.removeItem('selectInfo');

                this.props.navigation.navigate(SelectUser);
            }

            // // hàm giúp kiểm tra đã đăng nhập hay chưa
            // const user = firebase.auth().currentUser;
            // if (user) {
            //     let loginUser = async () => {
            //         //khi đã đăng nhập rồi,lấy thông tin đã đăng nhập,đẩy vào state mới để vào thằng màn hình ứng dụng
            //
            //         if (this.state.connection_Status === 'Online'){
            //             firebase.auth().onAuthStateChanged(user2 => this.props.clickLogin(user2));
            //             this.props.navigation.navigate(Home);
            //         }
            //
            //
            //
            //     }
            //     loginUser()
            // }
            // else {
            //     if (this.state.connection_Status === 'Online'){
            //         // this.props.navigation.navigate(Login);
            //         this.props.navigation.navigate('Index');
            //     }
            //     console.log("Opps :D Bạn chưa đăng nhập rùi")
            // }


        });

        //notification
        this.checkPermission();
        this.createNotificationListeners(); //add this line
    }


    //notification
    componentWillUnmount() {

        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
        );

        this.notificationListener.bind(this);
        this.notificationOpenedListener.bind(this);
    }

    _handleConnectivityChange = (isConnected) => {

        if (isConnected === true) {
            this.setState({connection_Status: "Online"}, () => {
                if (this.state.checkNetwork === 1 && this.state.initialNotification === false) {
                    this.props.navigation.navigate(SelectUser);
                }
            })

        } else {
            this.setState({connection_Status: "Offline", checkNetwork: 1})
        }
    };
    //1
    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        await firebase.messaging().subscribeToTopic('VLNB');
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log('onNotification1:');
            if (Platform.OS === 'android') {
                // const { title, body } = notification;
                console.log('onNotification:');
                // console.log(notificationOpen.notification);
                // this.showAlert(title, body);
                // alert('message');

                const localNotification = new firebase.notifications.Notification({
                    // sound: 'sampleaudio',
                    show_in_foreground: true,
                })
                    .setNotificationId(notification.notificationId)
                    .setTitle(notification.title)
                    // .setSubtitle(notification.subtitle)
                    .setBody(notification.body)
                    .setData(notification.data)
                    .android.setChannelId('fcm_default_channel') // e.g. the id you chose above
                    .android.setSmallIcon('@mipmap/ic_launcher_round') // create this icon in Android Studio
                    // .android.setColor('#000000') // you can set a color here
                    .android.setPriority(firebase.notifications.Android.Priority.High);

                firebase.notifications()
                    .displayNotification(localNotification)
                    .catch(err => console.error(err));


            } else if (Platform.OS === 'ios') {

                const localNotification = new firebase.notifications.Notification()
                    .setNotificationId(notification.notificationId)
                    .setTitle(notification.title)
                    // .setSubtitle(notification.subtitle)
                    .setBody(notification.body)
                    .setData(notification.data)
                    .ios.setBadge(notification.ios.badge);

                firebase.notifications()
                    .displayNotification(localNotification)
                    .catch(err => console.error(err));

            }

        });


        const channel = new firebase.notifications.Android.Channel('fcm_default_channel', 'Demo app name', firebase.notifications.Android.Importance.High)
            .setDescription('Demo app description')
        // .setSound('sampleaudio.mp3');
        firebase.notifications().android.createChannel(channel);

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            // const { title, body } = notificationOpen.notification;
            this.setState({initialNotification:false})
            console.log('onNotificationOpened:');
            // console.log(title, body);
            this.showAlert(notificationOpen.notification);
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            // const { title, body } = notificationOpen.notification;
            this.setState({initialNotification:true})
            console.log('getInitialNotification:');
            // console.log(title);
            // console.log(title, body);
            this.showAlert(notificationOpen.notification);
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage((message) => {
            //process data message
            console.log(JSON.stringify(message));
        });
    }

    //3
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                console.log('fcmToken1:', fcmToken);
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
        console.log('fcmToken2:', fcmToken);
    }

    //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

    async showAlert(data) {
        const item = JSON.parse(data.data.data)

        console.log('data:',item)
        if (data) {
            if (data.data.type === '3' || data.data.type === '4') {
                // console.log(3)
                // if(initialNotification !== 'initialNotification'){
                //     this.props.navigation.navigate(JobDailly, {
                //         data: data,
                //     });
                // }else{
                //     setTimeout(() => {
                //         this.props.navigation.navigate(JobDailly, {
                //             data: data,
                //         });
                //     }, 2000);
                // }


            } else if (data.data.type === '2') {
                console.log(2)
                if(this.state.initialNotification === false){
                    this.props.jobDetail(item.id);
                    this.props.componyDetail(item.id);
                    this.props.navigation.navigate(JobDetail, {
                        provine: item.provine_name,
                        nameScreen: 'Notify',
                        saveHeart: item.saved,
                        applied: item.applied,
                        randomImageProp: 0

                    });
                }else{
                setTimeout(() => {

                    this.props.jobDetail(item.id);
                    this.props.componyDetail(item.id);

                    this.props.navigation.navigate(JobDetail, {
                        provine: item.provine_name,
                        nameScreen: 'Notify',
                        saveHeart: item.saved,
                        applied: item.applied,
                        randomImageProp: 0


                    });
                }, 2000);
                }

            } else if (data.data.type === '1') {
                console.log(1)

                if(this.state.initialNotification === false){
                    this.props.navigation.navigate('ShowProfile')
                }else{
                    setTimeout(() => {
                        this.props.navigation.navigate('ShowProfile')
                    }, 2000);
                }
            }  else if (data.data.type === '5') {
                if(this.state.initialNotification === false){
                    this.props.fetchUserCVEmployer(item.user_id);
                    this.props.navigation.navigate(ViewCVUser,{
                        nameScreen: 'NotifyEmployer',
                    })
                }else{
                    setTimeout(() => {
                        console.log('item.user_id:',item.user_id)
                        this.props.fetchUserCVEmployer(item.user_id);
                        this.props.navigation.navigate(ViewCVUser,{
                            nameScreen: 'NotifyEmployer',
                        })
                    }, 2000);
                }


            } else if(item.job_post_id) {
                this.props.PostDetail(item.job_post_id);
                this.props.navigation.navigate(PostDetailEmployer);
            }
        }


    }

    //end notification
    render() {
        let {fadeAnim} = this.state;

        return (
            <Container style={{backgroundColor: '#75d1ed'}}>

                <Content padder contentContainerStyle={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#75d1ed'
                }}>
                    <Animated.Image
                        resizeMode={'contain'} source={require('../../vendor/images/VLNB.png')}
                        style={[StylesAll.sizeLogo, {opacity: fadeAnim, tintColor: '#fff',}]}
                    />

                </Content>

                {this.state.connection_Status !== 'Online' ?
                    <Footer style={StylesAll.footerLoad}>
                        <Animated.Text style={[StylesAll.footerText, {opacity: fadeAnim, color: 'red'}]}>Không có kết
                            nối mạng</Animated.Text>
                    </Footer> : null}
            </Container>
        );


    }


}
const mapStateToProps = (state) => {
    return {

    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserCVEmployer: (userId) => {
            dispatch(FetchUserCVEmployer(userId))
        },

        jobDetail: (idJob) => {
            dispatch(JobDetailAction(idJob));

        },
        componyDetail: (idJob) => {

            dispatch(CompanyDetailAction(idJob));
        },
        PostDetail: (idPost) => {
            dispatch(PostEmployerDetailAction(idPost));

        },
    };
}
const LoadingComponent = connect(mapStateToProps, mapDispatchToProps)(Loading);
export default LoadingComponent;
