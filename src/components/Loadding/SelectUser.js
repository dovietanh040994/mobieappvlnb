import React, {PureComponent} from 'react';
import {
    Image,
    View,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import {
    Container,
    Content,
    Text,
} from "native-base";

import {HomeBox, SelectInfo, LoginEmployer, IndexEmployer} from "../../vendor/screen";
import {connect} from "react-redux";

import {
    FetchCarrerList, FetchExperienceList, FetchJobTypesList,
    FetchProvinceList, FetchSalaryList, FetchJobLevel
} from "../../actions";

import AsyncStorage from "@react-native-community/async-storage";


class SelectU extends PureComponent {

    constructor(props) {
        super(props);
    }

    componentWillMount() {

    }

    componentDidMount() {
        this.props.fetchProvinceList();
        this.props.fetchCarrerList();
        this.props.fetchSalaryList();
        this.props.fetchExperienceList();
        this.props.fetchJobTypesList();
        this.props.fetchJobLevel();
        AsyncStorage.setItem('adsNew', '0');
        AsyncStorage.setItem('adsPost', '0');
        AsyncStorage.setItem('adsMedia', '0');
    }

    render() {
        return (

            <Container>
                <Image style={{width: '100%', height: '100%', position: 'absolute', top: 0, left: 0}}
                       source={require('../../vendor/images/bgSelect.png')}/>
                <Content>
                    <View style={{width: 280, alignSelf: 'center', marginTop: 130, marginBottom: 20}}>
                        <Text style={{color: '#33B8E0', fontSize: 24, fontWeight: 'bold', textAlign: 'center'}}>Bạn là
                            Ứng Viên</Text>
                        <Text style={{color: '#33B8E0', fontSize: 24, fontWeight: 'bold', textAlign: 'center'}}>hay Nhà
                            Tuyển Dụng ?</Text>
                    </View>
                    <View style={{
                        borderRadius: 5,
                        overflow: 'hidden',
                        paddingLeft: 15,
                        paddingRight: 15,
                        marginTop: 30
                    }}>
                        <TouchableOpacity
                            onPress={() => {
                                AsyncStorage.getItem('userInfoEmployer', (err, result) => {
                                    if (result !== null) {
                                        this.props.navigation.navigate(IndexEmployer);
                                    } else {
                                        this.props.navigation.navigate(LoginEmployer);
                                    }
                                });
                            }}
                            style={styles.button}
                        >
                            <View style={styles.field}>
                                <Text style={styles.fieldText}>Nhà tuyển dụng</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={async () => {

                                // this.props.navigation.navigate('PageTest');

                                await AsyncStorage.getItem('selectInfo', (err, result) => {
                                    if (result === null) {
                                        this.props.navigation.navigate(SelectInfo);
                                    } else {
                                        const data = `province_id=${JSON.parse(result).province}&job_carrer_id=${JSON.parse(result).carrer}`;
                                        this.props.navigation.navigate(HomeBox, {selectInfo: data});
                                    }

                                });
                            }}
                            style={styles.button}
                        >
                            <View style={styles.field}>
                                <Text style={styles.fieldText}>Ứng viên</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

        fetchProvinceList: () => {
            dispatch(FetchProvinceList());
        },
        fetchCarrerList: () => {
            dispatch(FetchCarrerList());
        },
        fetchSalaryList: () => {
            dispatch(FetchSalaryList());
        },
        fetchExperienceList: () => {
            dispatch(FetchExperienceList());
        },
        fetchJobTypesList: () => {
            dispatch(FetchJobTypesList());
        },
        fetchJobLevel: () => {
            dispatch(FetchJobLevel());
        }
    };
}
const SelectUser = connect(null, mapDispatchToProps)(SelectU);
export default SelectUser


const styles = StyleSheet.create({
    boxAdvanced: {
        flexDirection: 'column',
        paddingBottom: 0,
        paddingTop: 0,
        marginTop: 10
    },
    title: {
        fontSize: 20,
        color: '#33B8E0',
        marginBottom: 5
    },
    button: {
        borderWidth: 1,
        borderColor: '#ccc',
        borderBottomWidth: 1,
        borderRadius: 24,
        backgroundColor: '#33B8E0',
        marginTop: 20

    },
    field: {
        color: '#525252',
        height: 48

    },
    fieldText: {
        color: '#fff',
        fontSize: 17,
        fontWeight: 'bold',
        alignSelf: 'center',
        marginTop: 12
    },
    iconDropdown: {
        tintColor: '#33B8E0',
        position: 'absolute',
        right: 20, top: 20
    }

});
