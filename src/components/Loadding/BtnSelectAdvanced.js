import React from 'react';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import {Text, TouchableOpacity} from 'react-native';
import {SELECTINFO} from "../../vendor/formNames";
// import {CarrerBoxDetail, SearchDetail} from "../../../vendor/screen";

// const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
//dispatch(submit(CONTACT_FORM)) taoj luôn ra 1 action là submit chứ k chạy qua action(tạo sẵn) như redux
// const {navigate} = this.props.navigation;
const BtnSelectAdvanced = ({ dispatch }) => {
    return (
            <TouchableOpacity  style={{backgroundColor:'#33b8e6',borderRadius:5,height:48,flex:1,justifyContent:'center'}}
                    onPress={() => {
                        dispatch(submit(SELECTINFO))

                        // this.props.navigation.navigate(SearchDetail);
                    }}
            >
                <Text style={{alignSelf:'center',fontSize:17,fontWeight:'bold',color:'#fff'}}>Tiếp tục</Text>
            </TouchableOpacity>

    );
};

//connect()(RemoteSubmitButton) la container,RemoteSubmitButton laf component
export default connect()(BtnSelectAdvanced);
