import React, {Component, PureComponent} from 'react';
import {
    Image,
    Platform,
    View,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    ScrollView, Animated, StyleSheet, ActivityIndicator, ImageBackground
} from 'react-native';
import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Input,
    Card,
    CardItem,
    Item, Picker, Form, Toast, Textarea, FooterTab
} from "native-base";
import {Field, reduxForm} from 'redux-form';
import {SEARCH_ADVANCED, SELECTINFO, USER_FORM} from '../../vendor/formNames'

//Styles
import {StylesAll} from '../../vendor/styles'
import {AdvancedDetail, HomeBox, Profile, Search, ShowProfile} from "../../vendor/screen";
import {connect} from "react-redux";
import BtnSearchAdvanced from '../UserView/Home/Search/BtnSearchAdvanced'
import DatePicker from 'react-native-datepicker'
// import submitEditForm from './SubmitServer/submitEditForm'
import {EditUserAction, SearchAdvancedAction, SearchFormAction} from "../../actions";
import {getStatusBarHeight} from "react-native-status-bar-height";
import Modal from 'react-native-modalbox';
import {bindActionCreators} from "redux/es/redux";
import ListDetailModal from "../UserView/Home/Search/ListDetailModal"
import NavigationService from "../../vendor/NavigationService";
import BtnSelectAdvanced from "./BtnSelectAdvanced";
import AsyncStorage from "@react-native-community/async-storage";


// const validate = values => {
//     console.log('asdasd111')
//     console.log(values)
//
//     const errors = {};
//     if (!values.province) {
//         errors.province = 'Tỉnh/thành phố không được để trống'
//     }
//     if (!values.carrer) {
//         errors.carrer = 'Ngành nghề không được để trống'
//     }
//     if (!values.salary) {
//         errors.salary = 'Mức lương không được để trống'
//     }
//
//
//     return errors
// }


// const warn = values => {
//     const warnings = {};
//     if (values.age < 19) {
//         warnings.age = 'You seem a bit young...'
//     }
//     return warnings
// }
const submitEditForm = (values, dispatch) => {
    const errors = {};
    if (!values.province) {
        errors.province = 'Tỉnh/thành phố không được để trống';
        Toast.show({
            text: errors.province,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (!values.carrer) {

        errors.carrer = 'Ngành nghề không được để trống';
        Toast.show({
            text: errors.carrer,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else {
        AsyncStorage.setItem('selectInfo', JSON.stringify(values), () => {
            AsyncStorage.getItem('selectInfo', (err, result) => {
                const data =  `province_id=${JSON.parse(result).province}&job_carrer_id=${JSON.parse(result).carrer}`
                NavigationService.navigate(HomeBox,{ selectInfo: data });
            });
        });

    }



    // dispatch(SearchFormAction(values));




};


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

class Select extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            dislay:false,
            dataList:'',
            title:'',
            functionPropIDHere:'',
            PropNameHere:'',
            province:'',
            jobTypes:'',
            experience:'',
            carrer:'',
            salary:'',
        }

    }



    componentWillMount () {
        //tạo đầu vào mặc định cho trường trong redux-form

    }


    renderPickerInput = ({label,placeholder, keyboardType, meta: {touched, error, warning}, input: {onChange, ...restInput}}) => {

        return (
            <View>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{opacity: 0,position:'absolute'}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}  editable={false}
                />
                {/*{touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||*/}
                {/*    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}*/}


            </View>
        );
    }

    render() {
        // const DismissKeyboard = ({ children }) => (
        //     <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        //         {children}
        //     </TouchableWithoutFeedback>
        // );
        // const pushData = ( value ) => (
        //
        // );
        // console.log(this.state.dataList)

        return (
            // ,marginTop: getStatusBarHeight(true)

            <Container >
                <Image style={{width: '100%', height: '100%', position: 'absolute', top:0, left:0 }} source={require('../../vendor/images/bgSelect.png')} />
                <Content >
                    {/*<Content padder  keyboardShouldPersistTaps={'handled'}>*/}
                    {/*<DismissKeyboard>*/}
                    <View style={{width:280,alignSelf:'center',marginTop:80,marginBottom:20}}>
                        <Text style={{color:'#33B8E0',fontSize: 24,fontWeight:'bold',textAlign:'center'}}>Vui lòng chọn địa điểm và ngành nghề bạn mong muốn</Text>
                    </View>
                    <View style={{
                        // borderWidth: 1,
                        borderRadius: 5,
                        overflow: 'hidden',
                        // borderColor: '#ccc',
                        paddingLeft: 15,
                        paddingRight: 15,
                        // backgroundColor: 'white'

                    }}>
                        <View style={[styles.boxAdvanced]} >
                            <Text style={styles.title}>
                                Địa điểm
                            </Text>
                            <TouchableOpacity onPress={ async () => {
                                await this.setState({
                                    dataList:this.props.province,
                                    title:'Chọn địa điểm',
                                    functionPropIDHere:'province',
                                });
                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()
                            }} style={styles.button}
                            >
                                <View style={styles.field}>
                                    <Text style={styles.fieldText}>{this.state.province}</Text>
                                </View>
                                <Field   name="province" component={this.renderPickerInput}   >
                                </Field>

                                <Image source={require('../../vendor/images/Polygon.png')} style={styles.iconDropdown}/>
                            </TouchableOpacity>

                        </View>
                        <View style={[styles.boxAdvanced]} >
                            <Text style={styles.title}>
                                Ngành nghề
                            </Text>
                            <TouchableOpacity onPress={ async () => {
                                await this.setState({
                                    dataList:this.props.carrer,
                                    title:'Chọn ngành nghề',
                                    functionPropIDHere:'carrer'
                                });
                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()
                            }} style={styles.button}
                            >
                                <View style={styles.field}>
                                    <Text style={styles.fieldText}>{this.state.carrer}</Text>
                                </View>
                                <Field   name="carrer" component={this.renderPickerInput}   >
                                </Field>
                                <Image source={require('../../vendor/images/Polygon.png')} style={styles.iconDropdown}/>
                            </TouchableOpacity>

                        </View>

                    </View>
                    {/*</DismissKeyboard>*/}
                    {/*</ImageBackground>*/}

                </Content>
                <Footer style={{backgroundColor:'#fff',paddingLeft:15,paddingRight:15,marginBottom:15,elevation:0}}>
                    <BtnSelectAdvanced/>
                </Footer>
                {this.state.dataList !== ''?
                    <ListDetailModal
                        ref={'listModel'}
                        dataList={this.state.dataList}
                        title={this.state.title}
                        functionPropIDHere={(value)=>{this.props.change(this.state.functionPropIDHere, `${value}`)}}
                        PropNameHere={(value)=>{
                            if(this.state.functionPropIDHere === 'carrer'){
                                this.setState({carrer:`${value}`})
                            }else if (this.state.functionPropIDHere === 'province'){
                                this.setState({province:`${value}`})
                            }
                        }}
                    />
                    :null}


            </Container>


        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.loginReducers,
        province: state.provinceReducers,
        salary: state.salaryReducers,
        carrer: state.carrerReducers,
        jobTypes: state.jobTypesReducers,
        experience: state.experienceReducers,
        search: state.searchHomeReducers,
    }
};

const SelectInformation = connect(mapStateToProps, null)(Select);
//
export default reduxForm({
    form: SELECTINFO, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    // validate,
    // warn,
    onSubmit: submitEditForm
})(SelectInformation)


const styles = StyleSheet.create({
    boxAdvanced: {
        // borderBottomWidth: 1,
        // borderColor: '#ccc',
        flexDirection:'column',
        paddingBottom: 0,
        paddingTop: 0,
        marginTop: 10
    },
    title: {
        fontSize:20,
        color:'#33B8E0',
        marginBottom:5
    },
    button:{
        borderWidth:1,
        borderColor:'#ccc',
        borderBottomWidth:1,
        borderRadius:5
    },
    field:{
        color:'#525252',
        height:48

    },
    fieldText:{
        color:'#525252',
        marginTop:13,
        marginLeft:15
    },
    iconDropdown:{
        tintColor: '#33B8E0',
        position:'absolute',
        right:20,top:20
    }

});
