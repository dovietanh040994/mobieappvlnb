import React, { Component } from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList, ActivityIndicator, ImageBackground, TouchableOpacity, Image, TextInput,ScrollView,Dimensions
} from 'react-native';
import FlatListItemJob from "../ChildComponent/FlatListItemJob";
import {Body, Button, Container, Header, Icon, Item, Left, Title} from "native-base";
import {Home, Posts, Search} from "../../../src/vendor/screen";


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export default class SearchComponent extends React.PureComponent {

    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);
        //hàm tạo ref
        this.detailModal = React.createRef();
        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                0,
            ),
            refreshing: false,
            bgButton: 'SuitableWork',
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            query:''
        };
        this.page = 1
    }

    componentDidMount() {
        this.loadMoreData();
    }

    loadMoreData = async () => {
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})


            const Url = `http://vieclamnambo.vn:9002/api/vlnb/job/getjobpost?rows_start=${this.page}`;
            console.log(Url)
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {

                    if (responseJson.jobPosts.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.jobPosts],
                            fetching_from_server: false,
                        });
                        console.log(this.state.serverData)
                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (
            <View style={{ padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row'}}>
                {this.state.fetching_from_server ? (
                    <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30,zIndex:999}}/>
                ) : null}
            </View>
        );
    }

    findData(query) {
        console.log(query)
        if (query === '') {
            return [];
        }else {
            this.loadMoreData(query.trim())

        }
        //
        // return serverData.filter(serverData => this.state.film.title.search(query.trim()) >= 0);
    }

    render() {
        console.log(1111)
        console.log(this.state.serverData)
        return (

            <View>

                <Header rounded style={{backgroundColor:'#fff',height:80,borderBottomWidth:0.5,borderColor:'#ccc',paddingLeft:0,paddingRight:0}}>
                    <ImageBackground source={require('../../vendor/images/Rectangle.png')} style={{width: '100%', height: '100%'}}>
                        <View style={{flex:1,flexDirection:'row',marginTop:20,paddingLeft:15,paddingRight:10}}>
                            <View style={{flex:1}}>
                                {/*<Button style={{backgroundColor:'#33B8E0',borderRadius:5,height:40,paddingTop:0,paddingBottom:0,paddingLeft:12,paddingRight:12}}>*/}
                                {/*    <Image source={require('../../vendor/images/Group.png')} style={{*/}
                                {/*        tintColor: '#fff'*/}
                                {/*    }}/>*/}
                                {/*</Button>*/}
                                <TouchableOpacity style={{width:35}} transparent onPress={() => {
                                    this.props.navigation.navigate(Home)
                                }}>
                                    <Image source={require('../../vendor/images/arr_left.jpg')} style={{
                                        tintColor: '#33B8E0', marginTop: 11,marginLeft:10
                                    }}/>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:7,paddingLeft:10,paddingRight:10}}>


                                <View  style={{position:'absolute',left:15,top:7}}>
                                    <Image source={require('../../vendor/images/Search.png')} style={{
                                        tintColor: '#33B8E0'
                                    }}/>
                                </View>
                                <TextInput
                                    style={{width:'100%',borderWidth:1,borderColor: '#ECEBED',borderRadius:5,top:0,height:40,fontSize:15,paddingLeft:50,paddingRight:10}}
                                    placeholder="Nhập vào từ khóa"
                                    onChangeText={
                                        (query) => {
                                            this.setState({
                                                query : query,
                                                isListEnd: false,
                                                serverData: [],
                                                fetching_from_server: false,
                                            })
                                            this.page = 1
                                            this.findData(query)
                                            // sleep(1000).then(() => {
                                            //     navigation.navigate(SearchDetail);
                                            // })
                                        }
                                    }
                                />


                            </View>
                            <View style={{flex:1}}>
                                <Button style={{backgroundColor:'#33B8E0',borderRadius:5,height:40,paddingTop:0,paddingBottom:0,paddingLeft:12,paddingRight:12}}>
                                    <Image source={require('../../vendor/images/Group.png')} style={{
                                        tintColor: '#fff'
                                    }}/>
                                </Button>
                            </View>
                        </View>
                    </ImageBackground>


                </Header>
                <FlatList
                    style={{ flex: 1,backgroundColor: '#F5F4F4'}}
                    // data={this.props.salary.items.jobPosts}
                    data={this.state.serverData}
                    onEndReached={() => this.loadMoreData(this.state.query)}
                    onEndReachedThreshold={0.5}
                    // ListHeaderComponent={<View style={{height:130}}/>}
                    renderItem={({item, index}) => {
                        return (
                            <View style={{marginBottom: 15 ,marginTop:index === 0 ?15:0}}>
                                <Text>asdsds</Text>
                            </View>
                        );
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    // ItemSeparatorComponent={() => <View/>}
                    ListFooterComponent={this.renderFooter.bind(this)}
                />
                <Text>asdsd</Text>

            </View>
        );
    }
}

