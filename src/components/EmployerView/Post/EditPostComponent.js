import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, TextInput, Dimensions, BackHandler
} from 'react-native';
import {
    Body,
    CardItem,
    Container,
    Left,
    Right,
    Switch,
    Button,
    Input,
    Picker,
    Textarea,
    Title,
    Header, Toast, Content, Footer, FooterTab
} from "native-base";
import {Field, reduxForm, SubmissionError, submit} from 'redux-form';
import {FORM_EDIT_POST} from "../../../vendor/formNames";
import Modal from 'react-native-modalbox';
import {connect} from "react-redux";
import {StylesAll} from "../../../vendor/styles";
import {editPost, EmployerManagePost} from "../../../vendor/screen";
import ListDetailModal from "../Home/Search/ListDetailModal";
import {FetchCvUser, UpdateCvUser, FetchDistrict, CreateJobPost, UpdateJobPost} from "../../../actions";
import NavigationService from "../../../vendor/NavigationService";
// import SubmitCVUser from "../../Profile/SubmitCVUser";
import BtnSearchAdvanced from "../Home/Search/BtnSearchAdvanced";
import DatePicker from "react-native-datepicker";
import districtReducers from "../../../reducers/DistrictReducers";
import SubmitEditPost from "./SubmitEditPost";

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screen = Dimensions.get('window');


const listGender = [
    {
        id: 1,
        name: "Nam",
    },
    {
        id: 2,
        name: "Nữ",
    },
    {
        id: 3,
        name: "Tất cả",
    }
];

const submitEditPost = async (values, dispatch) => {

    if (values.job_title.trim().length === 0) {
        toastError("Vui lòng nhập tiêu đề công việc!")
    } else if (values.number_of_recruitment === 0) {
        toastError("Vui lòng nhập số lượng cần tuyển!")
    } else if (values.job_salary_id.trim().length === 0) {
        toastError("Vui lòng chọn mức lương!")
    } else if (values.job_experience_id.trim().length === 0) {
        toastError("Vui lòng chọn kinh nghiệm!")
    } else if (values.job_level_id.trim().length === 0) {
        toastError("Vui lòng chọn trình độ!")
    } else if (values.job_carrer_id.trim().length === 0) {
        toastError("Vui lòng chọn ngành nghề!")
    } else if (values.job_province_id.trim().length === 0) {
        toastError("Vui lòng chọn tình thành!")
    } else if (values.district_id.trim().length === 0) {
        toastError("Vui lòng chọn quận huyện!")
    } else if (values.gender.trim().length === 0) {
        toastError("Vui lòng chọn giới tính!")
    } else if (values.end_at.trim().length === 0) {
        toastError("Vui lòng chọn hạn tuyển!")
    } else if (values.job_type_id.trim().length === 0) {
        toastError("Vui lòng chọn hình thức làm việc!")
    } else if (values.contact_name.trim().length === 0) {
        toastError("Vui lòng nhập người liên hệ!")
    } else if (values.contact_phone.trim().length === 0) {
        toastError("Vui lòng nhập điện thoại liên hệ!")
    } else if (values.contact_email.trim().length === 0) {
        toastError("Vui lòng nhập email liên hệ!")
    } else if (values.contact_address.trim().length === 0) {
        toastError("Vui lòng nhập địa chỉ liên hệ!")
    } else if (values.job_description.trim().length === 0) {
        toastError("Vui lòng nhập mô tả công việc!")
    } else if (values.job_requirement.trim().length === 0) {
        toastError("Vui lòng nhập yêu cầu công việc!")
    } else if (values.job_benefit.trim().length === 0) {
        toastError("Vui lòng nhập quyền lợi công việc!")
    } else if (values.job_cv.trim().length === 0) {
        toastError("Vui lòng nhập yêu cầu hồ sơ!")
    } else {
        await dispatch(UpdateJobPost(values));
        await Toast.show({
            text: "Cập nhật công việc thành công!",
            buttonText: "Đóng",
            type: "success",
            position: 'top',
        });
        await NavigationService.navigate(EmployerManagePost);
    }

};

const toastError = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};
const toastWarning = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};


const validate = values => {

    const errors = {};

    return errors

};

export class EditPostComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            scrollEnabled: true,
            dislay: false,
            dataList: '',
            title: '',
            functionPropIDHere: '',
            PropNameHere: '',

            job_salary_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_salary_name : "",
            job_experience_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_experience_name : "",
            job_level_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_level_name : "",
            job_carrer_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_carrer_name : "",
            job_province_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.province_name : "",
            district_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.district_name : "",
            gender: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.gender_name : "",
            end_at: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.ended_at : "",
            job_type_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_type_name : "",
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    componentWillMount() {

        //tạo đầu vào mặc định cho trường trong redux-form

        this.props.initialize({
            job_post_id: this.props.postContent.itemsDetaiPostEmployer.id,
            job_title: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_title : "",
            number_of_recruitment: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.number_of_recruitment : 1,
            job_salary_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_salary_id.toString() : "",
            job_experience_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_experience_id.toString() : "",
            job_level_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_level_id.toString() : "",
            job_carrer_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_carrer_id.toString() : "",
            job_province_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.province_id.toString() : "",
            district_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.district_id.toString() : "",
            gender: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.gender.toString() : "",
            end_at: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.ended_at : "",
            job_type_id: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_type_id.toString() : "",
            contact_name: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.contact_name : "",
            contact_email: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.contact_email : "",
            contact_address: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.contact_address : "",
            contact_phone: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.contact_phone : "",
            job_description: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_description : "",
            job_requirement: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_requirement : "",
            job_benefit: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_benefit : "",
            job_cv: this.props.postContent ? this.props.postContent.itemsDetaiPostEmployer.job_cv : "",
        });

        BackHandler.addEventListener('EmployerManagePost', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('EmployerManagePost', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('EmployerManagePost')
        return true;
    }


    renderPickerTextarea = ({label, placeholder, keyboardType, meta: {touched, error, warning}, input: {onChange, ...restInput}}) => {

        return (
            <View>
                <Textarea placeholderTextColor={'#E0E0E0'} rowSpan={6} bordered placeholder={placeholder}
                          keyboardType={keyboardType}
                          onChangeText={onChange} {...restInput} style={{
                    borderWidth: 1,
                    borderColor: '#ECEBED',
                    borderRadius: 5,
                    paddingLeft: 15
                }}/>
                {touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||
                    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}
            </View>
        );
    }

    renderPickerInput = ({label, placeholder, keyboardType, meta: {touched, error, warning}, input: {onChange, ...restInput}}) => {

        return (
            <View>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{opacity: 0, position: 'absolute'}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput} editable={false}
                />
            </View>
        );
    };

    renderInput = ({label, placeholder, type, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View>
                <Input placeholder={placeholder} placeholderTextColor={'#E0E0E0'}
                       style={{
                           width: '100%',
                           borderWidth: 1,
                           borderColor: '#ECEBED',
                           borderRadius: 5,
                           paddingLeft: 15
                       }}
                       keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                       value={`${value}`}
                />
                {touched && ((error && toastError(error)) ||
                    (warning && toastWarning(warning)))}
            </View>
        );
    };

    renderPickerDate = ({input: {onChange, value, ...inputProps}, label, meta: {touched, error, warning}, ...restInput}) => {
        return (
            <View>
                <DatePicker
                    // style={{width: 150}}
                    date={this.state.end_at === null ? '' : this.state.end_at}
                    mode="date"
                    placeholder="Chọn ngày..."
                    format="DD/MM/YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    hideText={true}
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginRight: 20
                        },
                        dateInput: {
                            width: 0,
                            height: 0,
                            borderWidth: 0,
                            alignItems: 'flex-start',
                        },
                        placeholderText: {
                            color: '#5a5e62'
                        }
                    }}
                    onDateChange={value => {
                        this.setState({end_at: value})
                        this.props.change('end_at', value)
                    }}
                    {...restInput}
                />
                {touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||
                    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}
            </View>

        );
    };

    static navigationOptions = () => ({
        header: null
    });

    render() {
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );

        console.log(this.props.postContent);

        return (

            <View style={[styles.fill]}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(EmployerManagePost)}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Đăng tin</Title>
                    </Body>
                    <Right>

                    </Right>
                </Header>
                <ScrollView scrollEnabled={this.state.scrollEnabled}>
                    <Content style={{paddingTop: 5}}>
                        <View style={{
                            // borderWidth: 1,
                            borderRadius: 5,
                            overflow: 'hidden',
                            // borderColor: '#ccc',
                            paddingLeft: 15,
                            paddingRight: 15,
                            backgroundColor: 'white'

                        }}>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Tiêu đề công việc *
                                </Text>
                                <Field type={'text'} name="job_title" component={this.renderInput}
                                       placeholder="Vui lòng nhập tiêu đề công việc">
                                </Field>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Số lượng cần tuyển *
                                </Text>
                                <Field type={'text'} keyboardType='numeric' name="number_of_recruitment"
                                       component={this.renderInput}
                                       placeholder="Vui lòng nhập số lượng cần tuyển">
                                </Field>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Mức lương *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: this.props.salary,
                                        title: 'Chọn mức lương',
                                        functionPropIDHere: 'job_salary_id'
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.job_salary_id}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="job_salary_id"
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Kinh nghiệm *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: this.props.experience,
                                        title: 'Chọn kinh nghiệm',
                                        functionPropIDHere: 'job_experience_id'
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.job_experience_id}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="job_experience_id"
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Trình độ *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: this.props.levels,
                                        title: 'Chọn trình độ',
                                        functionPropIDHere: 'job_level_id'
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.job_level_id}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="job_level_id"
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Ngành nghề *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: this.props.carrer,
                                        title: 'Chọn ngành nghề',
                                        functionPropIDHere: 'job_carrer_id'
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.job_carrer_id}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="job_carrer_id"
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>

                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Tỉnh thành *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: this.props.province,
                                        title: 'Chọn địa điểm',
                                        functionPropIDHere: 'job_province_id',
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.job_province_id}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="job_province_id"
                                           component={this.renderPickerInput}>
                                    </Field>

                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Quận huyện *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: this.props.districts,
                                        title: 'Chọn quận huyện',
                                        functionPropIDHere: 'district_id',
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.district_id}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="district_id"
                                           component={this.renderPickerInput}>
                                    </Field>

                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Giới tính *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: listGender,
                                        title: 'Chọn giới tính',
                                        functionPropIDHere: 'gender',
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.gender}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="gender"
                                           component={this.renderPickerInput}>
                                    </Field>

                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>
                            </View>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Hạn nộp *
                                </Text>
                                <View style={{
                                    borderWidth: 1,
                                    borderColor: '#ccc',
                                    borderBottomWidth: 1,
                                    borderRadius: 5,
                                    height: 48,
                                    flex: 1,
                                    flexDirection: 'row'
                                }}>
                                    <Text style={styles.fieldText}>{this.state.end_at}</Text>
                                    <Right>
                                        <Field type="text" name="end_at" component={this.renderPickerDate}>
                                        </Field>
                                    </Right>
                                </View>
                            </View>


                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Hình thức làm việc *
                                </Text>
                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: this.props.jobTypes,
                                        title: 'Chọn hình thức làm việc',
                                        functionPropIDHere: 'job_type_id',
                                    });
                                    await this.refs.listModel.changeDataList();
                                    await this.refs.listModel.showAddModal();
                                }} style={styles.button}
                                >
                                    <View style={styles.field}>
                                        <Text style={styles.fieldText}>{this.state.job_type_id}</Text>
                                    </View>
                                    <Field type={'text'} keyboardType='numeric' name="job_type_id"
                                           component={this.renderPickerInput}>
                                    </Field>

                                    <Image source={require('../../../vendor/images/Polygon.png')}
                                           style={styles.iconDropdown}/>
                                </TouchableOpacity>
                            </View>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Người liên hệ *
                                </Text>
                                <Field type={'text'} name="contact_name" component={this.renderInput}
                                       placeholder="Vui lòng nhập tên người liên hệ">
                                </Field>
                            </View>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Điện thoại *
                                </Text>
                                <Field type={'text'} keyboardType='numeric' name="contact_phone"
                                       component={this.renderInput}
                                       placeholder="Vui lòng nhập điện thoại liên hệ">
                                </Field>
                            </View>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Email *
                                </Text>
                                <Field type={'text'} name="contact_email" component={this.renderInput}
                                       placeholder="Vui lòng nhập email liên hệ">
                                </Field>
                            </View>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Trụ sở *
                                </Text>
                                <Field type={'text'} name="contact_address" component={this.renderInput}
                                       placeholder="Vui lòng nhập trụ sở liên hệ">
                                </Field>
                            </View>


                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Mô tả công việc *
                                </Text>
                                <Field type={'text'} name="job_description" component={this.renderPickerTextarea}
                                       placeholder="Nhập vào mô tả công việc">
                                </Field>
                            </View>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Yêu cầu công việc *
                                </Text>
                                <Field type={'text'} name="job_requirement" component={this.renderPickerTextarea}
                                       placeholder="Nhập vào yêu cầu công việc">
                                </Field>
                            </View>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Quyền lợi được hưởng *
                                </Text>
                                <Field type={'text'} name="job_benefit" component={this.renderPickerTextarea}
                                       placeholder="Nhập vào yêu quyền lợi được hưởng">
                                </Field>
                            </View>

                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.title}>
                                    Yêu cầu hồ sơ *
                                </Text>
                                <Field type={'text'} name="job_cv" component={this.renderPickerTextarea}
                                       placeholder="Nhập vào yêu cầu hồ sơ">
                                </Field>
                            </View>

                            <View style={{flex: 1, paddingRight: 10, paddingLeft: 5, marginTop: 8, marginBottom: 70}}>
                                <SubmitEditPost/>
                            </View>


                        </View>
                        {/*</DismissKeyboard>*/}

                    </Content>
                </ScrollView>


                {this.state.dataList !== '' ?
                    <ListDetailModal
                        ref={'listModel'}
                        dataList={this.state.dataList}
                        title={this.state.title}
                        functionPropIDHere={(value) => {
                            this.props.change(this.state.functionPropIDHere, `${value}`);


                            //neu chon tinh
                            if (this.state.functionPropIDHere === 'job_province_id') {
                                this.props.onFetchDistrict(`${value}`)
                                this.props.change('district_id', '');
                                this.setState({district_id: ''})
                            }
                        }}
                        PropNameHere={(value) => {
                            if (this.state.functionPropIDHere === 'job_carrer_id') {
                                this.setState({job_carrer_id: `${value}`})
                            } else if (this.state.functionPropIDHere === 'job_province_id') {
                                this.setState({job_province_id: `${value}`})
                            } else if (this.state.functionPropIDHere === 'job_salary_id') {
                                this.setState({job_salary_id: `${value}`})
                            } else if (this.state.functionPropIDHere === 'job_experience_id') {
                                this.setState({job_experience_id: `${value}`})
                            } else if (this.state.functionPropIDHere === 'job_type_id') {
                                this.setState({job_type_id: `${value}`})
                            } else if (this.state.functionPropIDHere === 'job_level_id') {
                                this.setState({job_level_id: `${value}`})
                            } else if (this.state.functionPropIDHere === 'gender') {
                                this.setState({gender: `${value}`})
                            } else if (this.state.functionPropIDHere === 'district_id') {
                                this.setState({district_id: `${value}`})
                            }
                        }}
                    />
                    : null}

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.loginReducers,
        province: state.provinceReducers,
        salary: state.salaryReducers,
        carrer: state.carrerReducers,
        jobTypes: state.jobTypesReducers,
        experience: state.experienceReducers,
        levels: state.jobLevelReducers,
        districts: state.districtReducers,
        companyInfo: state.employerCompanyReducers,
        postContent: state.postDetailPostEmployerReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        onFetchDistrict: (id) => {
            dispatch(FetchDistrict(id));
        }
    };
}

const edit = connect(mapStateToProps, mapDispatchToProps)(EditPostComponent);
//
export default reduxForm({
    form: FORM_EDIT_POST, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    validate,
    onSubmit: submitEditPost
})(edit)

const styles = StyleSheet.create({
    boxAdvanced: {
        // borderBottomWidth: 1,
        // borderColor: '#ccc',
        flexDirection: 'column',
        paddingBottom: 0,
        paddingTop: 0,
        marginTop: 15
    },
    title: {
        fontSize: 20,
        color: '#33B8E0',
        lineHeight: 23,
        marginBottom: 10,
        fontWeight: '500'
    },
    button: {
        borderWidth: 1,
        borderColor: '#ccc',
        borderBottomWidth: 1,
        borderRadius: 5
    },
    field: {
        color: '#525252',
        height: 44

    },
    fieldText: {
        color: '#525252',
        marginTop: 13,
        marginLeft: 15
    },
    iconDropdown: {
        tintColor: '#33B8E0',
        position: 'absolute',
        right: 20, top: 20
    },
    textPlaceholder: {
        fontSize: 16,
        fontWeight: 'normal'
    }
});
