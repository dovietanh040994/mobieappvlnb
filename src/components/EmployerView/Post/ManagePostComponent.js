import React, {PureComponent} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler, Keyboard
} from 'react-native';
import FlatListItemPost from "../ChildComponent/FlatListItemPost";
import {Button, Container, Header, Item, Toast, Left, Right} from "native-base";
import {connect} from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import {withNavigation} from "react-navigation";
import {CreatePost, Posts, SearchEmployer, ViewCVUser} from "../../../vendor/screen";
import PlaceholderLoadingJob from "../../Helpers/PlaceholderLoadingJob";
import {FetchUserCVEmployer, FetchInfoCompany} from "../../../actions";

import FlatListItemListUser from "../ChildComponent/FlatListItemListUser";
import FlatListItemJob from "../../UserView/Profile/FlatListItemJob";


const HEADER_MAX_HEIGHT = 180;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;


class ManagePostComponent extends PureComponent {

    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                0,
            ),
            refreshing: false,
            bgButton: 0,
            idCategory: 1,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            userInfo: null,
            notData: false,
            doubleBackToExitPressedOnce: false,
        };
        this.page = 1;
    }

    componentDidMount() {
        const {navigation} = this.props;
        this.focusListener = navigation.addListener("didFocus", () => {
            AsyncStorage.getItem('userInfoEmployer', (err, result) => {
                this.setState({
                    userInfo: result
                }, () => {
                    this.loadMoreData();
                })
            });
            this.props.fetchInfoCompany();
        });


    }

    loadMoreData = async () => {
        if (!this.state.fetching_from_server && !this.state.isListEnd) {

            this.setState({fetching_from_server: true})

            const Url = `http://vieclamnambo.vn:9002/api/vlnb/employee/getjobpost?row_start=${this.page}`;

            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo
                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {
                    console.log(responseJson);
                    if (responseJson.total === 0) {
                        this.setState({
                            notData: true
                        });
                    }
                    if (responseJson.jobPosts.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.jobPosts],
                            fetching_from_server: false,
                            refreshing: false,
                            notData: false
                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            refreshing: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    _handleRefresh = () => {
        this.setState({
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            refreshing: true
        }, () => {
            this.page = 1
            this.loadMoreData()
        });
    };

    _handleBack = () => {
        this.setState({
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            refreshing: true
        }, () => {
            this.page = 1
        });
    };


    renderFooter() {
        return (

            <View style={styles.footer}>
                {this.state.fetching_from_server ? (this.page === 1 ?
                        <PlaceholderLoadingJob/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'}
                                           style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    render() {

        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 1],
            extrapolate: 'clamp',
        });

        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 6, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.9],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 6, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -127],
            extrapolate: 'clamp',
        });

        return (
            <View style={[styles.fill]}>
                {
                    this.state.notData === false ?
                        <Animated.FlatList
                            style={styles.fill}
                            // data={this.props.salary.items.jobPosts}
                            onScroll={Animated.event(
                                [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                                {useNativeDriver: true},
                            )}
                            data={this.state.serverData}
                            onEndReached={() => this.loadMoreData(this.state.query)}
                            onEndReachedThreshold={0.5}
                            ListHeaderComponent={<View style={{height: 180}}/>}
                            renderItem={({item, index}) => {
                                console.log('item',item)
                                return (
                                    <View style={{marginBottom: 15, marginTop: index === 0 ? 15 : 0}}>
                                        <FlatListItemPost
                                            item={item}
                                            index={index}
                                            parentFlatList={this}
                                            navigation={this.props.navigation}
                                            random={index % 4}
                                            handleChange={this._handleBack}
                                        />
                                    </View>
                                );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                            ItemSeparatorComponent={() => <View style={styles.separator}/>}
                            ListFooterComponent={this.renderFooter.bind(this)}
                            refreshing={this.state.refreshing}
                            onRefresh={this._handleRefresh}
                        />
                        :
                        <View>
                            <View style={{height: 220}}/>
                            <View style={{alignItems: 'center', marginTop: 60}}>
                                <Image source={require('../../../vendor/images/Loading_Post.png')}/>
                                <Text style={{color: '#8C8888', fontWeight: '500', fontSize: 16, marginTop: 30}}>Chưa
                                    tìm thấy tin tức phù hợp</Text>
                            </View>
                        </View>
                }
                <Animated.View
                    // pointerEvents="none"
                    style={[
                        styles.header,
                        {transform: [{translateY: headerTranslate}]},
                    ]}
                >
                    <Animated.Image
                        style={[
                            styles.backgroundImage,
                            {
                                opacity: imageOpacity,
                                transform: [{translateY: imageTranslate}],
                            },
                        ]}
                        source={require('../../../vendor/images/employer/banner_manage_post.png')}
                    />
                </Animated.View>
                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                {scale: titleScale},
                                {translateY: titleTranslate},
                            ],
                        },
                    ]}
                >

                    <View style={{alignItems: 'flex-start', position: 'relative', top: 50}}>
                        <Text style={{
                            fontWeight: '900',
                            color: '#fff',
                            fontSize: 32,
                        }}>Tuyển dụng</Text>
                    </View>
                    <View style={{alignItems: 'flex-end'}}>
                        <TouchableOpacity style={{
                            width: 53,
                            zIndex: 9999,
                            height: 53,
                            borderRadius: 50,
                            marginBottom: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: '#FFFFFF'
                        }} transparent onPress={async () => {
                            if (this.props.companyInfo) {
                                await this._handleBack();
                                await this.props.navigation.navigate(CreatePost)
                            } else {
                                //chuyen sang man cap nhat thong tin company
                                Toast.show({
                                    text: ' Bạn cập nhật thông tin công ty trước khi đăng tin.',
                                    // buttonText: "Đóng",
                                    type: "warning",
                                    position: "top"
                                })
                            }
                        }}>
                            <Image source={require('../../../vendor/images/employer/add.png')}/>

                        </TouchableOpacity>
                    </View>

                </Animated.View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        companyInfo: state.employerCompanyReducers
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserCVEmployer: (userId) => {
            dispatch(FetchUserCVEmployer(userId))
        },
        fetchInfoCompany: () => {
            dispatch(FetchInfoCompany());
        }
    };
}

const ManagePost = connect(mapStateToProps, mapDispatchToProps)(ManagePostComponent);
export default withNavigation(ManagePost)
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4',
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        // height: 45,
        width: '100%',
        zIndex: 99,
        // alignItems: 'center',
        // justifyContent: 'center',
        position: 'absolute',
        top: 36,
        paddingLeft: 15,
        paddingRight: 15,
        flex: 1
        // left: 0,
        // right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
