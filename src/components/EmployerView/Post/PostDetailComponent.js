import React, {PureComponent} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler,
    Alert
} from 'react-native';
import FlatListItemPost from "../../UserView/ChildComponent/FlatListItemPost";
import InformationJob from "../Home/InformationJob";
import CompanyJob from "../Home/CompanyJob";
import {
    Body,
    Button,
    CardItem,
    Container,
    Header,
    Item,
    Left,
    Picker,
    Title,
    Footer,
    FooterTab,
    Icon, Toast, Right
} from "native-base";
import {connect} from "react-redux";

import categoryPostReducers from "../../../reducers/CategoryPostReduser";
import {
    appliedAction,
    CategoryPost, CompanyDetailAction, FetchUserCVEmployer,
    JobDetailAction,
    JobSameCompanyAction,
    LoginAction, PostEmployerDetailAction,
    SaveJobAction,
    UnSaveJobAction,
    UpdateApproveJobPost
} from "../../../actions";
import {
    EditPost,
    EmployerManagePost, HomeBox, NotifyEmployer, PostDetailEmployer, ViewCVUser
} from "../../../vendor/screen";
import FlatListItemJob from "../../UserView/ChildComponent/FlatListItemJob";
import companyDetailReducers from "../../../reducers/CompanyDetailReduser";
import AsyncStorage from "@react-native-community/async-storage";


const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;
const menu = [
    {
        id: 1,
        name: "CHI TIẾT TIN ĐĂNG",
    },
    {
        id: 2,
        name: "HỒ SƠ ỨNG VIÊN",
    },

]

class PostDetail extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                0,
            ),
            refreshing: false,
            notData: false,
            bgButton: 0,
            idCategory: 1,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            userInfo: null,

        };
        this.page = 1;
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount() {
        AsyncStorage.getItem('userInfoEmployer', (err, result) => {
            const {navigation} = this.props;

            const approve = navigation.getParam('approve', 'approve');

            this.setState({
                userInfo: result,
                approve: approve
            })
        });
    }

    componentWillMount() {
        BackHandler.addEventListener('PostDetailEmployer', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('PostDetailEmployer', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.navigatePage()
        return true;

    }

    navigatePage = () => {
        const {navigation} = this.props;
        const nameScreen = navigation.getParam('nameScreen', null);
        console.log(nameScreen)
        navigation.popToTop()

        if(nameScreen === 'Notify') {
            this.props.navigation.navigate(NotifyEmployer)


        }else {
            this.props.navigation.navigate(EmployerManagePost)

        }
    }
    loadMoreData = async () => {

        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})
            const Url = `http://vieclamnambo.vn:9002/api/vlnb/employee/getuserapply?job_post_id=${this.props.postContent.itemsDetaiPostEmployer.id}&row_start=${this.page}`;
            console.log(this.state.userInfo);
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo

                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {
                    console.log('responseJson', responseJson);
                    if (responseJson.total === 0) {
                        this.setState({
                            notData: true
                        });
                    }
                    if (responseJson.users.length > 0) {

                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.users],
                            fetching_from_server: false,
                            notData: false
                        });

                    } else {

                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (
            <View style={styles.footer}>
                {this.state.fetching_from_server ? (
                    <ActivityIndicator size="large" color={'#ccc'}
                                       style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    rednderContent() {
        if (this.state.idCategory === 1) {
            const {navigation} = this.props;
            const province = navigation.getParam('provine', null);
            const youtubeId = navigation.getParam('youtubeId', null);
            return (
                <Animated.ScrollView
                    style={styles.fill}
                    // data={this.props.salary.items.jobPosts}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true},
                    )}>
                    <View style={{marginTop: 265}}/>
                    <InformationJob parentJob={this.props.postContent.itemsDetaiPostEmployer} province={province}
                                    youtubeId={youtubeId}/>
                </Animated.ScrollView>
            );
        } else if (this.state.idCategory === 2) {
            // console.log(11111111)
            console.log('this.state.serverData',this.state.notData === false)

            return (

                <View>
                    {this.state.notData === false ?

                        <Animated.FlatList
                            // style={styles.fill}
                            onScroll={Animated.event(
                                [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                                {useNativeDriver: true},
                            )}
                            data={this.state.serverData}
                            onEndReached={() => this.loadMoreData()}
                            onEndReachedThreshold={0.5}
                            ListHeaderComponent={<View style={{height: 250}}/>}
                            renderItem={({item, index}) => {
                                console.log('item',item)
                                return (
                                    <TouchableOpacity  style={{
                                        marginTop: index === 0 ? 10 : 0,
                                        paddingLeft: 20,
                                        paddingRight: 20,
                                        backgroundColor: '#fff',
                                    }} onPress={
                                        async () => {
                                            await this.props.fetchUserCVEmployer(item.id);
                                            await this.props.navigation.navigate(ViewCVUser,{
                                                nameScreen: 'ManagePost',
                                            })
                                        }
                                    }>
                                        <CardItem
                                            style={{
                                                borderBottomWidth: 1,
                                                borderColor: '#33bbe0',
                                                paddingLeft: 0,
                                                paddingRight: 0
                                            }}>

                                            <Body style={{paddingTop: 10, paddingBottom: 10}}>
                                             <Text numberOfLines={2} style={{
                                                    fontSize: 18,
                                                    color: '#33bbe0',
                                                    fontWeight: 'bold',
                                                    marginBottom: 10
                                                }}>{item.name} </Text>

                                                <View style={{flexDirection: 'row', marginBottom: 1}}>
                                                    <Image source={require('../../../vendor/images/Shape.png')}
                                                           style={{marginTop: 4, marginRight: 5}}/>
                                                    <Text style={{color: '#33B8E0', fontSize: 11, fontWeight: '500'}}>
                                                        {item.province_id !== 0 ? item.province_name : 'Chưa cập nhật'}
                                                    </Text>
                                                </View>
                                                <View style={{flexDirection: 'row'}}>
                                                    <Left>
                                                        <View style={{flexDirection: 'row'}}>
                                                            <Image source={require('../../../vendor/images/money.png')}
                                                                   style={{marginTop: 4, marginRight: 5}}/>
                                                            <Text style={{
                                                                color: '#33B8E0',
                                                                fontSize: 11,
                                                                fontWeight: '500'
                                                            }}>
                                                                {item.job_salary_name !== '' ? item.job_salary_name : 'Thỏa thuận'}
                                                            </Text>
                                                        </View>
                                                    </Left>
                                                    <Right>
                                                        <View style={{flexDirection: 'row'}}>
                                                            <Image source={require('../../../vendor/images/clock.png')}
                                                                   style={{marginTop: 4, marginRight: 5}}/>
                                                            <Text style={{
                                                                color: '#33B8E0',
                                                                fontSize: 11,
                                                                fontWeight: '500'
                                                            }}>
                                                                {item.job_experience_id !== 0 ? item.job_experience_name : 'Chưa cập nhật'}
                                                            </Text>
                                                        </View>
                                                    </Right>

                                                </View>
                                            </Body>
                                        </CardItem>

                                    </TouchableOpacity>
                                );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                            // ItemSeparatorComponent={() => <View style={styles.separator}/>}
                            ListFooterComponent={this.renderFooter.bind(this)}
                        />
                        :
                        <View>
                            <View style={{height: 250}}/>
                            <View style={{alignItems: 'center', marginTop: 60}}>
                                <Image source={require('../../../vendor/images/Loading_Job.png')}/>
                                <Text style={{color: '#8C8888', fontWeight: '500', fontSize: 16, marginTop: 30}}>Không
                                    tìm
                                    thấy ứng viên phù hợp</Text>
                            </View>
                        </View>
                    }
                </View>
            );
        }
    }

    TabName(stateName, idCategory) {
        console.log(1)
        this.setState({
            bgButton: stateName,
            idCategory: idCategory,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
        }, () => {
            this.page = 1
            this.loadMoreData()

        });
    }

    static navigationOptions = () => ({
        header: null
    });


    render() {

        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -100, -200],
            extrapolate: 'clamp',
        });

        const {loadingDetaiPostEmployer} = this.props.postContent;
        console.log('this.props.postContent.itemsDetaiPostEmployer',this.props.postContent.loadingDetaiPostEmployer )
        return (
            <View style={styles.fill}>
                {this.rednderContent()}
                <Footer style={{borderTopWidth: 1, borderColor: '#ccc', position: 'absolute', bottom: 0,}}>
                    <FooterTab style={{backgroundColor: '#fff', flex: 1}}>
                        <View style={{flex: 1, paddingRight: 5, paddingLeft: 10, marginTop: 8,}}>
                            {
                                this.state.approve === "approve" ?
                                    (
                                        <TouchableOpacity style={{
                                            borderRadius: 50,
                                            borderColor: this.state.saveHeart === 0 ? '#33B8E0' : 'red',
                                            backgroundColor: this.state.saveHeart === 0 ? '#fff' : 'red',
                                            borderWidth: 1,
                                            height: 40,
                                            textAlign: 'center'
                                        }}
                                                          onPress={async () => {
                                                              Alert.alert(
                                                                  'Thông báo',
                                                                  `Tin tuyển dụng đang được chúng tôi kiểm duyệt ?`,
                                                                  [
                                                                      {
                                                                          text: 'Đóng',
                                                                          onPress: () => console.log('Cancel Pressed'),
                                                                          style: 'cancel',
                                                                      }
                                                                  ],
                                                                  {cancelable: false},
                                                              );
                                                          }}>
                                            <Text style={{
                                                color: this.state.saveHeart === 0 ? '#33B8E0' : 'white',
                                                fontWeight: 'bold',
                                                fontSize: 16,
                                                alignSelf: 'center',
                                                marginTop: 8
                                            }}>Chờ duyệt</Text>
                                        </TouchableOpacity>

                                    ) :

                                    <TouchableOpacity style={{
                                        borderRadius: 50,
                                        borderColor: this.state.saveHeart === 0 ? '#33B8E0' : 'red',
                                        backgroundColor: this.state.saveHeart === 0 ? '#fff' : 'red',
                                        borderWidth: 1,
                                        height: 40,
                                        textAlign: 'center'
                                    }}
                                                      onPress={async () => {
                                                          await this.props.updateApproveJobPost(this.props.postContent.itemsDetaiPostEmployer.id);
                                                          await this.setState({
                                                              approve: 'approve'
                                                          })
                                                      }}>
                                        <Text style={{
                                            color: this.state.saveHeart === 0 ? '#33B8E0' : 'white',
                                            fontWeight: 'bold',
                                            fontSize: 16,
                                            alignSelf: 'center',
                                            marginTop: 8
                                        }}>Tạm ngừng</Text>
                                    </TouchableOpacity>
                            }


                        </View>
                        <View style={{flex: 1, paddingRight: 10, paddingLeft: 5, marginTop: 8}}>
                            <TouchableOpacity style={{
                                borderRadius: 50,
                                borderColor: this.state.applied === 0 ? '#33B8E0' : '#ccc',
                                backgroundColor: this.state.applied === 0 ? '#33B8E0' : '#ccc',
                                borderWidth: 1,
                                height: 40,
                                textAlign: 'center'
                            }}
                                              onPress={() => {
                                                  this.props.navigation.navigate(EditPost
                                                  );
                                              }}>
                                <Text style={{
                                    color: '#fff',
                                    fontWeight: 'bold',
                                    fontSize: 16,
                                    alignSelf: 'center',
                                    marginTop: 8
                                }}>{this.state.applied === 0 ? 'Chỉnh sửa tin đăng' : 'Chỉnh sửa tin đăng'}</Text>
                            </TouchableOpacity>
                        </View>
                    </FooterTab>
                </Footer>
                <Animated.View
                    // pointerEvents="none"
                    style={[
                        styles.header,
                        {transform: [{translateY: headerTranslate}]},
                    ]}
                >

                    <Animated.View style={[

                        {
                            opacity: imageOpacity,
                            transform: [{translateY: imageTranslate}],
                        },
                    ]}>
                        <Header style={{
                            backgroundColor: '#fff',
                            paddingLeft: 20,
                            height: 50,
                            borderBottomWidth: 0,
                            borderBottomColor: '#ccc',
                            elevation: 1
                        }}>
                            <Left>
                                <TouchableOpacity
                                    style={{width: 50, height: 30, position: 'absolute', left: -20, top: -13}}
                                    transparent onPress={() => {
                                    this.navigatePage()
                                }}
                                >
                                    <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                                        tintColor: '#33B8E0', marginTop: 5, marginLeft: 23
                                    }}/>
                                </TouchableOpacity>
                            </Left>
                            <Body style={{flex: 3}}>
                                <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Chi
                                    tiết việc làm</Title>
                            </Body>
                        </Header>
                        <Header
                            style={{backgroundColor: '#fff', height: 151, borderBottomWidth: 0.5, borderColor: '#ccc'}}>
                            {/*<ImageBackground source={require('../../vendor/images/Rectangle8.png')} style={{width: '100%', height: '100%'}}>*/}
                            {/*    <Text style={{fontWeight:'900',color:'#fff',fontSize:25,alignSelf:'center',marginTop:65}}>KHÁM PHÁ</Text>*/}

                            {/*</ImageBackground>*/}
                            {loadingDetaiPostEmployer ? <ActivityIndicator size="large" color="#ccc"/> :
                                <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>

                                    <View style={{width: 130}}>

                                        {
                                            this.props.postContent.itemsDetaiPostEmployer.logo !== ''?
                                                <Image resizeMode={'contain'}
                                                       source={{uri: `${ this.props.postContent.itemsDetaiPostEmployer.logo }`}}
                                                       style={{width: '100%', height: '100%'}}
                                                />
                                                :
                                                <Image resizeMode={'contain'}
                                                       source={{uri: `https://vieclamnambo.vn/images/Footer.png`}}
                                                       style={{width: '100%', height: '100%'}}
                                                />
                                        }
                                    </View>

                                    <View style={{marginLeft: 10, width: screenWidth - 150}}>
                                        <Text numberOfLines={3} style={{
                                            color: '#33B8E0',
                                            fontSize: 16,
                                            fontWeight: '600',
                                            lineHeight: 20
                                        }}>{this.props.postContent.itemsDetaiPostEmployer.job_title} </Text>
                                        <Text style={{
                                            color: '#525252',
                                            fontSize: 12,
                                            fontWeight: '500',
                                            marginTop: 5,
                                            marginBottom: 5
                                        }}
                                              numberOfLines={1}>{this.props.postContent.itemsDetaiPostEmployer.company_name}</Text>
                                        <View style={{flexDirection: 'row', marginBottom: 5}}>
                                            <Image source={require('../../../vendor/images/luotxem.png')} style={{
                                                marginTop: 5, marginRight: 5
                                            }}/>
                                            <Text style={{color: '#888686', fontSize: 12}}>Lượt
                                                xem: {this.props.postContent.itemsDetaiPostEmployer.all_time_stats}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row'}}>
                                            <Image source={require('../../../vendor/images/hannop.png')} style={{
                                                marginTop: 5, marginRight: 5, marginLeft: 1
                                            }}/>
                                            <Text style={{color: '#888686', fontSize: 12}}>Hạn nộp hồ
                                                sơ: {this.props.postContent.itemsDetaiPostEmployer.ended_at !== '00/00/0000' ? this.props.postContent.itemsDetaiPostEmployer.ended_at : 'Luôn tuyển'}</Text>

                                        </View>


                                    </View>
                                </View>}

                        </Header>

                    </Animated.View>

                </Animated.View>
                {loadingDetaiPostEmployer ? null :
                    <Animated.View
                        style={[
                            styles.bar,
                            {
                                transform: [
                                    // { scale: titleScale },
                                    {translateY: titleTranslate},
                                ],
                            },
                        ]}
                    >
                        <FlatList
                            numColumns={2}
                            data={menu}
                            ref={(ref) => {
                                this.flatListRef = ref;
                            }}
                            style={{width: '100%',}}
                            keyExtractor={(item, index) => index.toString()}
                            // ListHeaderComponent={<View style={{height: 180}}/>}
                            renderItem={({item, index}) =>
                                <View style={{
                                    backgroundColor: this.state.bgButton === index ? '#33B8E0' : '#fff',
                                    borderTopWidth: 1,
                                    borderColor: this.state.bgButton === index ? '#33B8E0' : '#ECEBED',
                                    paddingLeft: 5,
                                    paddingRight: 5,
                                    width: '100%',
                                    flex: 1
                                }}>
                                    <TouchableOpacity style={styles.ScrollTextContainer} onPress={() => {
                                        // this.flatListRef.scrollToIndex({animated: true, index: index});
                                        const stateName = index;
                                        const idCategory = item.id;
                                        // if(idCategory === 3){
//                                         console.log(this.props.jobContent.itemsDetaiPostEmployer.company_id)
                                        // this.props.jobSameCompany(this.props.jobContent.itemsDetaiPostEmployer.company_id)
                                        // }
                                        this.TabName(stateName, idCategory)
                                    }}>
                                        <Text style={{
                                            fontSize: 14,
                                            fontWeight: 'bold',
                                            color: this.state.bgButton === index ? '#fff' : '#D9D3DE',
                                            alignSelf: 'center'
                                        }}>{item.name}</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        />

                    </Animated.View>

                }
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        // user: state.loginReducers,
        postContent: state.postDetailPostEmployerReducers,
        // companyContent: state.companyDetailReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        category: () => {

            dispatch(CategoryPost());
        },

        PostDetail: (idPost) => {
            dispatch(PostEmployerDetailAction(idPost));

        },
        fetchUserCVEmployer: (userId) => {
            dispatch(FetchUserCVEmployer(userId))
        },
        SaveHeartJob: (id) => {

            dispatch(SaveJobAction(id));
        },
        UnSaveHeartJob: (id) => {

            dispatch(UnSaveJobAction(id));
        },
        appliedJob: (id) => {

            dispatch(appliedAction(id));
        },
        updateApproveJobPost: (id) => {
            dispatch(UpdateApproveJobPost(id));
        }
    };
}
const PostDetailComponent = connect(mapStateToProps, mapDispatchToProps)(PostDetail);
export default PostDetailComponent

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fdfdfd'
        // paddingTop:70
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 200,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 200,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        borderBottomWidth: 1,
        borderColor: '#ECEBED',
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 16,
        paddingBottom: 16,
        color: '#000',
        textAlign: 'center',
        width: '100%'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
});
