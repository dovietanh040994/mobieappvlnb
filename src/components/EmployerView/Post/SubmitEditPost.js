import React from 'react';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import {StyleSheet, View, TouchableOpacity, TextInput, Text, Animated} from 'react-native';
import {Button, Left} from 'native-base'
import {FORM_EDIT_POST} from "../../../vendor/formNames";



//dispatch(submit(CONTACT_FORM)) taoj luôn ra 1 action là submit chứ k chạy qua action(tạo sẵn) như redux
const submitEditPost = ({dispatch}) => {
    return (
        <TouchableOpacity style={{
            borderRadius: 50,
            borderColor: '#33B8E0',
            backgroundColor: '#33B8E0',
            borderWidth: 1,
            height: 58,
            alignItems: 'center',
            justifyContent: 'center'
        }}
                          onPress={() => {
                              dispatch(submit(FORM_EDIT_POST))
                          }}>
            <Text style={{
                color: '#fff',
                fontWeight: 'bold',
                fontSize: 17,
            }}>Cập nhật</Text>
        </TouchableOpacity>
    );
};

export default connect()(submitEditPost);
