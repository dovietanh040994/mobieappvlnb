import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
import React from "react";
import ManagePostComponent from './ManagePostComponent'
import CreatePostComponent from './CreatePostComponent'
import EditPostComponent from './EditPostComponent'
import CVUserComponent from './CVUserComponent'
import PostDetailComponent from './PostDetailComponent'


const EmployerPostNavigator = createStackNavigator(
    {
        EmployerManagePost: {
            screen: ManagePostComponent,
        },
        CreatePost: {
            screen: CreatePostComponent,
        },
        EditPost: {
            screen: EditPostComponent
        },

        PostDetailEmployer: {
            screen: PostDetailComponent
        }
    }, {
        initialRouteName: 'EmployerManagePost'
    }
);

export default createAppContainer(EmployerPostNavigator);
