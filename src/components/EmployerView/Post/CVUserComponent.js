import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    ImageBackground,
    Image,
    TouchableOpacity,
    ScrollView,
    FlatList,
    TextInput,
    Dimensions,
    BackHandler,
    Alert
} from 'react-native';
import {
    Body,
    CardItem,
    Container,
    Left,
    Right,
    Switch,
    Button,
    Input,
    Picker,
    Textarea,
    Title,
    Header, Toast, Footer, FooterTab
} from "native-base";
import {connect} from "react-redux";
import {
    HomeBox,
    Login,
    HomeBoxEmployer,
    ViewCVUser,
    ProfileUserEmployer,
    NotifyEmployer,
    EmployerManagePost
} from "../../../vendor/screen";
import ListDetailModal from "../Home/Search/ListDetailModal";
import {
    FetchUserCVEmployer, FetchListSkill, SaveUserCVEmployer, BuyCv, UnSaveUserCVEmployer
} from "../../../actions";
import {withNavigation} from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import Modal from "react-native-modalbox";
import {submit} from "redux-form";
import {FORM_EDIT_POST} from "../../../vendor/formNames";
import ProfileUserComponent from "../ProfileUser/ProfileUserComponent";
import {
    Product,
    ProductPurchase,
    acknowledgePurchaseAndroid,
    purchaseUpdatedListener,
    purchaseErrorListener,
    PurchaseError,
} from 'react-native-iap';
import * as RNIap from 'react-native-iap';
import {Linking} from 'react-native'

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

const menu = [
    {
        id: 1,
        name: "THÔNG TIN",
    },
    {
        id: 2,
        name: "HỌC VẤN",
    },
    {
        id: 3,
        name: "KINH NGHIỆM",
    },
    {
        id: 4,
        name: "MONG MUỐN",
    },
    {
        id: 5,
        name: "KỸ NĂNG",
    },
];

const itemSkus = Platform.select({

    android: [
        'pk_ttv_1', 'pk_ttv_2', 'pk_ttv_3', 'pk_ttv_4'
    ],
});

let purchaseUpdateSubscription;
let purchaseErrorSubscription;

class CVUserComponent extends Component {
    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            scrollEnabled: true,
            dislay: false,
            dataList: '',
            title: '',
            bgButton: 1,
            avatarSource: null,
            saved: 0,
            productList: [],
            receipt: '',
            availableItemsMessage: '',
        };

        this.arrayvar = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }


    componentWillMount() {
        //tạo đầu vào mặc định cho trường trong redux-form

        if (purchaseUpdateSubscription) {
            purchaseUpdateSubscription.remove();
            purchaseUpdateSubscription = null;
        }
        if (purchaseErrorSubscription) {
            purchaseErrorSubscription.remove();
            purchaseErrorSubscription = null;
        }

        BackHandler.addEventListener('HomeBoxEmployer', this.handleBackButtonClick);
    }

    async componentDidMount() {

        try {
            console.log('result1');
            const result = await RNIap.initConnection();
            console.log('result', result);

            await RNIap.consumeAllItemsAndroid();
        } catch (err) {
            console.warn(err.code, err.message);
        }

        purchaseUpdateSubscription = purchaseUpdatedListener(async (purchase) => {
            console.log('purchaseUpdatedListener', purchase);
            if (purchase.purchaseStateAndroid === 1 && !purchase.isAcknowledgedAndroid) {
                try {
                    const ackResult = await acknowledgePurchaseAndroid(purchase.purchaseToken);
                    console.log('ackResult', ackResult);
                    await this.buyAndFetchCV({
                        user_id: this.props.userInfo.items.id,
                        token: purchase.purchaseToken,
                        package_id: purchase.productId
                    });
                } catch (ackErr) {
                    console.warn('ackErr', ackErr);
                }
            }
            this.setState({receipt: purchase.transactionReceipt}, () => this.goNext());
        });

        purchaseErrorSubscription = purchaseErrorListener((error) => {
            console.log('purchaseErrorListener', error);
            Alert.alert('purchase error', JSON.stringify(error));
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('HomeBoxEmployer', this.handleBackButtonClick);
    }

    goNext = () => {
        Alert.alert('Thành công', "Bạn đã mua cv thành công và có thể liên hệ ngay với ứng viên !");
    }

    buyAndFetchCV = async (params) => {
        await this.props.buyCv(params);
        await this.props.fetchUserCVEmployer(params.user_id);
    }

    getItems = async () => {
        try {
            const products = await RNIap.getProducts(itemSkus);
            // const products = await RNIap.getSubscriptions(itemSkus);
            console.log('Products', products);
            this.setState({productList: products});
        } catch (err) {
            console.warn(err.code, err.message);
        }
    }

    getAvailablePurchases = async () => {
        try {
            console.info('Get available purchases (non-consumable or unconsumed consumable)');
            const purchases = await RNIap.getAvailablePurchases();
            console.info('Available purchases :: ', purchases);
            if (purchases && purchases.length > 0) {
                this.setState({
                    availableItemsMessage: `Got ${purchases.length} items.`,
                    receipt: purchases[0].transactionReceipt,
                });
            }
        } catch (err) {
            console.warn(err.code, err.message);
            Alert.alert(err.message);
        }
    }

    // Deprecated apis
    buyItem = async (sku) => {
        console.log('buyItem', sku);
        // const purchase = await RNIap.buyProduct(sku);
        // const products = await RNIap.buySubscription(sku);
        // const purchase = await RNIap.buyProductWithoutFinishTransaction(sku);
        try {
            const purchase = await RNIap.requestPurchase(sku);
            console.log('purchase', purchase);
            // await RNIap.consumePurchaseAndroid(purchase.purchaseToken);
            this.setState({receipt: purchase.transactionReceipt}, () => this.goNext());
            console.log('purchase1', this.state.receipt);
        } catch (err) {
            console.log(err.code, err.message);
            // const subscription = RNIap.addAdditionalSuccessPurchaseListenerIOS(async(purchase) => {
            //     this.setState({ receipt: purchase.transactionReceipt }, () => this.goNext());
            //     subscription.remove();
            // });
        }
    }

    handleBackButtonClick() {

        this.navigatePage()
        return true;

    }

    navigatePage = () => {
        const {navigation} = this.props;
        navigation.popToTop()
        const nameScreen = navigation.getParam('nameScreen', null);
        if (nameScreen === 'HomeEmployer') {
            navigation.navigate(HomeBoxEmployer);

        } else if (nameScreen === "ProfileUserEmployer") {
            navigation.navigate(ProfileUserEmployer);

        } else if (nameScreen === "NotifyEmployer") {
            navigation.navigate(NotifyEmployer);

        }else if (nameScreen === "ManagePost") {
            navigation.navigate(EmployerManagePost);

        }
    }

    static
    navigationOptions = () => ({
        header: null
    });

    static
    navigationOptions = () => ({
        header: null
    });


    render() {
        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -60, -80],
            extrapolate: 'clamp',
        });

        const {productList, receipt, availableItemsMessage} = this.state;
        const receipt100 = receipt.substring(0, 100);

        return (
            <View style={[styles.fill]}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.navigatePage()}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Chi tiết hồ sơ</Title>
                    </Body>
                    <Right>

                    </Right>
                </Header>


                {
                    this.props.userInfo.length !== 0 ? (

                            this.state.bgButton === 1 ?
                                <ScrollView style={[styles.fill]}
                                            scrollEnabled={this.state.scrollEnabled}>
                                    <Animated.View style={{
                                        top: 70, marginBottom: 20
                                    }}>
                                        <Animated.Image
                                            style={{
                                                width: 160,
                                                height: 160,
                                                borderRadius: 163,
                                                borderWidth: 4,
                                                borderColor: "white",
                                                marginBottom: 10,
                                                alignSelf: 'center',
                                                backgroundColor: 'red'
                                            }}
                                            source={this.state.avatarSource === null ? (this.props.userInfo && this.props.userInfo.items.avatar !== '' ? {uri: `${this.props.userInfo.items.avatar}`} : require('../../../vendor/images/profile/avatar_default.png')) : this.state.avatarSource}
                                        />
                                    </Animated.View>

                                    <View style={{
                                        top: 90, marginBottom: 200
                                    }}>
                                        <View style={[styles.boxAdvanced]}>
                                            <Text style={styles.textAdvanced}>
                                                Họ và tên
                                            </Text>
                                            <Text style={styles.textBody}> {this.props.userInfo.items.name}</Text>
                                        </View>

                                        <View style={[styles.boxAdvanced]}>
                                            <Text style={styles.textAdvanced}>
                                                Số điện thoại
                                            </Text>
                                            <Text style={styles.textBody}> {this.props.userInfo.items.phone}</Text>
                                        </View>

                                        <View style={[styles.boxAdvanced]}>
                                            <Text style={styles.textAdvanced}>
                                                Địa chỉ hiện tại
                                            </Text>
                                            <Text style={styles.textBody}> {this.props.userInfo.items.address}</Text>
                                        </View>


                                        <View style={[styles.boxAdvanced]}>
                                            <Text style={styles.textAdvanced}>
                                                Ngày sinh
                                            </Text>
                                            <Text style={styles.textBody}> {this.props.userInfo.items.date_of_birth}</Text>
                                        </View>


                                        <View style={[styles.boxAdvanced]}>
                                            <Text style={styles.textAdvanced}>
                                                Giới tính
                                            </Text>

                                            <Text
                                                style={styles.textBody}> {this.props.userInfo.items.gender === '1' ? 'Nam' : "Nữ"}</Text>
                                        </View>


                                        <View style={[styles.boxAdvanced]}>
                                            <Text style={styles.textAdvanced}>
                                                Email
                                            </Text>
                                            <Text style={styles.textBody}> {this.props.userInfo.items.email}</Text>
                                        </View>

                                        <View style={[styles.boxAdvanced]}>
                                            <Text style={styles.textAdvanced}>
                                                Hôn nhân
                                            </Text>

                                            <Text
                                                style={styles.textBody}> {this.props.userInfo.items.is_married === 0 ? 'Độc thân' : "Đã kết hôn"}</Text>
                                        </View>

                                    </View>
                                </ScrollView>
                                : (
                                    this.state.bgButton === 2 ?
                                        <ScrollView style={[styles.fill]}
                                                    scrollEnabled={this.state.scrollEnabled}>
                                            <View style={{
                                                top: 50, marginBottom: 200
                                            }}>
                                                {this.props.userInfo.items.educations.length !== 0 ?
                                                    (this.props.userInfo.items.educations.map((item, index) => (
                                                        <View key={index} style={index > 0 ? {marginTop: 30} : ''}>

                                                            <View style={[styles.boxAdvanced, {borderTopWidth: 1}]}>
                                                                <Text style={styles.textAdvanced}>
                                                                    Trường
                                                                </Text>
                                                                <Text numberOfLines={1}
                                                                      style={[styles.textBody, {width: 200}]}>{item.university_name}</Text>
                                                            </View>
                                                            <View style={[styles.boxAdvanced]}>
                                                                <Text style={styles.textAdvanced}>
                                                                    Thời gian
                                                                </Text>
                                                                <Text numberOfLines={1}
                                                                      style={[styles.textBody, {width: 200}]}>{item.start_date} - {item.end_date}</Text>
                                                            </View>
                                                            <View style={[styles.boxAdvanced]}>
                                                                <Text style={styles.textAdvanced}>
                                                                    Chuyên ngành
                                                                </Text>
                                                                <Text numberOfLines={1}
                                                                      style={[styles.textBody, {width: 200}]}>{item.major_name}</Text>
                                                            </View>
                                                            <View style={[styles.boxAdvanced]}>
                                                                <Text style={styles.textAdvanced}>
                                                                    Bằng cấp
                                                                </Text>
                                                                <Text numberOfLines={1}
                                                                      style={[styles.textBody, {width: 200}]}>{item.degree_name}</Text>
                                                            </View>
                                                        </View>
                                                    ))) :
                                                    <View>
                                                        <Animated.Image
                                                            style={{
                                                                width: 325,
                                                                height: 220,
                                                                alignSelf: 'center',
                                                                marginTop: 50
                                                            }}
                                                            source={require('../../../vendor/images/employer/notupdate.png')}
                                                        />
                                                        <Text style={{
                                                            fontSize: 16,
                                                            color: "#8C8888",
                                                            fontWeight: '500',
                                                            marginTop: 50,
                                                            textAlign: 'center',
                                                        }}>
                                                            Ứng viên chưa cập nhật thông tin
                                                        </Text>
                                                    </View>}
                                            </View>
                                        </ScrollView> : (
                                            this.state.bgButton === 3 ?
                                                <ScrollView style={[styles.fill]}
                                                            scrollEnabled={this.state.scrollEnabled}>
                                                    <View style={{
                                                        top: 50, marginBottom: 200
                                                    }}>
                                                        {this.props.userInfo.items.experiences.length !== 0 ? (this.props.userInfo.items.experiences.map((item, index) => (
                                                            <View key={index} style={index > 0 ? {marginTop: 30} : ''}>
                                                                <View style={[styles.boxAdvanced, {borderTopWidth: 1}]}>
                                                                    <Text style={styles.textAdvanced}>
                                                                        Tên công ty
                                                                    </Text>
                                                                    <Text numberOfLines={1}
                                                                          style={[styles.textBody, {width: 200}]}>{item.company_name}</Text>
                                                                </View>

                                                                <View style={[styles.boxAdvanced]}>
                                                                    <Text style={styles.textAdvanced}>
                                                                        Thời gian
                                                                    </Text>
                                                                    <Text numberOfLines={1}
                                                                          style={[styles.textBody, {width: 200}]}>{item.start_date} - {item.end_date}</Text>
                                                                </View>

                                                                <View style={[styles.boxAdvanced]}>
                                                                    <Text style={styles.textAdvanced}>
                                                                        Chuyên ngành
                                                                    </Text>
                                                                    <Text numberOfLines={1}
                                                                          style={[styles.textBody, {width: 200}]}>{item.position_name}</Text>
                                                                </View>
                                                            </View>
                                                        ))) : <View>
                                                            <Animated.Image
                                                                style={{
                                                                    width: 325,
                                                                    height: 220,
                                                                    alignSelf: 'center',
                                                                    marginTop: 50
                                                                }}
                                                                source={require('../../../vendor/images/employer/notupdate.png')}
                                                            />
                                                            <Text style={{
                                                                fontSize: 16,
                                                                color: "#8C8888",
                                                                fontWeight: '500',
                                                                marginTop: 50,
                                                                textAlign: 'center',
                                                            }}>
                                                                Ứng viên chưa cập nhật thông tin
                                                            </Text>
                                                        </View>}
                                                    </View>

                                                </ScrollView> : (
                                                    this.state.bgButton === 4 ?
                                                        <ScrollView style={[styles.fill]}
                                                                    scrollEnabled={this.state.scrollEnabled}>
                                                            {this.props.userInfo.items.job_desire !== null ?
                                                                <View style={{
                                                                    top: 50, marginBottom: 200
                                                                }}>
                                                                    <View style={[styles.boxAdvanced]}>
                                                                        <Text style={styles.textAdvanced}>
                                                                            Ngành nghề
                                                                        </Text>
                                                                        <Text numberOfLines={1}
                                                                              style={[styles.textBody, {width: 200}]}>{this.props.userInfo.items.job_desire.carrer_name}</Text>
                                                                    </View>

                                                                    <View style={[styles.boxAdvanced]}>
                                                                        <Text style={styles.textAdvanced}>
                                                                            Nơi làm việc
                                                                        </Text>
                                                                        <Text numberOfLines={1}
                                                                              style={[styles.textBody, {width: 200}]}>{this.props.userInfo.items.job_desire.province_name}</Text>
                                                                    </View>

                                                                    <View style={[styles.boxAdvanced]}>
                                                                        <Text style={styles.textAdvanced}>
                                                                            Mức lương
                                                                        </Text>
                                                                        <Text numberOfLines={1}
                                                                              style={[styles.textBody, {width: 200}]}>{this.props.userInfo.items.job_desire.job_salary_name}</Text>
                                                                    </View>
                                                                </View> :
                                                                <View>
                                                                    <Animated.Image
                                                                        style={{
                                                                            width: 325,
                                                                            height: 220,
                                                                            alignSelf: 'center',
                                                                            marginTop: 50
                                                                        }}
                                                                        source={require('../../../vendor/images/employer/notupdate.png')}
                                                                    />
                                                                    <Text style={{
                                                                        fontSize: 16,
                                                                        color: "#8C8888",
                                                                        fontWeight: '500',
                                                                        marginTop: 50,
                                                                        textAlign: 'center',
                                                                    }}>
                                                                        Ứng viên chưa cập nhật thông tin
                                                                    </Text>
                                                                </View>
                                                            }
                                                        </ScrollView> :
                                                        <ScrollView style={[styles.fill]}
                                                                    scrollEnabled={this.state.scrollEnabled}>
                                                            <View style={{
                                                                top: 50, marginBottom: 200
                                                            }}>
                                                                {this.props.userInfo.items.skills.length !== 0 ? (this.props.userInfo.items.skills.map((item, index) => (
                                                                    <View key={index} style={index > 0 ? {marginTop: 30} : ''}>

                                                                        <View style={[styles.boxAdvanced, {borderTopWidth: 1}]}>
                                                                            <Text style={styles.textAdvanced}>
                                                                                Kỹ năng
                                                                            </Text>
                                                                            <Text numberOfLines={1}
                                                                                  style={[styles.textBody, {width: 200}]}>{item.skill_name}</Text>
                                                                        </View>

                                                                        <View style={[styles.boxAdvanced]}>
                                                                            <Text style={styles.textAdvanced}>
                                                                                Thành thạo
                                                                            </Text>
                                                                            <Text numberOfLines={1}
                                                                                  style={[styles.textBody, {width: 200}]}>{item.level}</Text>
                                                                        </View>
                                                                    </View>
                                                                ))) : <View>
                                                                    <Animated.Image
                                                                        style={{
                                                                            width: 325,
                                                                            height: 220,
                                                                            alignSelf: 'center',
                                                                            marginTop: 50
                                                                        }}
                                                                        source={require('../../../vendor/images/employer/notupdate.png')}
                                                                    />
                                                                    <Text style={{
                                                                        fontSize: 16,
                                                                        color: "#8C8888",
                                                                        fontWeight: '500',
                                                                        marginTop: 50,
                                                                        textAlign: 'center',
                                                                    }}>
                                                                        Ứng viên chưa cập nhật thông tin
                                                                    </Text>
                                                                </View>}
                                                            </View>
                                                        </ScrollView>
                                                )
                                        )
                                )) :
                        <View></View>
                }

                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                // { scale: titleScale },
                                {translateY: titleTranslate},
                            ],
                        },
                    ]}
                >
                    <FlatList
                        horizontal={true}
                        data={menu}
                        showsHorizontalScrollIndicator={false}
                        ref={(ref) => {
                            this.flatListRef = ref;
                        }}
                        style={{
                            backgroundColor: '#fff',
                            borderBottomWidth: 1,
                            borderTopWidth: 1,
                            borderColor: '#ECEBED',
                            width: '100%'
                        }}
                        keyExtractor={(item, index) => item.id.toString()}
                        renderItem={({item, index}) =>
                            <View style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: this.state.bgButton === item.id ? '#33B8E0' : '#FFFFFF',
                            }}>
                                <TouchableOpacity style={styles.ScrollTextContainer} onPress={() => {
                                    this.flatListRef.scrollToIndex({animated: true, index: index});
                                    this.setState({
                                        bgButton: item.id
                                    })
                                }}>
                                    <Text style={{
                                        fontSize: 16,
                                        fontWeight: 'bold',
                                        color: this.state.bgButton === item.id ? '#FFFFFF' : '#D9D3DE'
                                    }}>{item.name}</Text>
                                </TouchableOpacity>
                            </View>


                        }
                    />
                </Animated.View>
                {this.props.userInfo.length !== 0 ? <Footer style={{borderTopWidth: 1, borderColor: '#ccc'}}>
                        <FooterTab style={{backgroundColor: '#fff', flex: 1}}>
                            <View style={{flex: 1, paddingRight: 5, paddingLeft: 10, marginTop: 8}}>

                                <TouchableOpacity style={{
                                    borderRadius: 50,
                                    borderColor: '#F2C94C',
                                    backgroundColor: '#fff',
                                    borderWidth: 1,
                                    height: 40,
                                    textAlign: 'center'
                                }}
                                                  onPress={async () => {
                                                      if (this.props.userInfo.length !== 0) {
                                                          if (this.props.userInfo.items.saved === 0) {
                                                              await this.props.saveCV(this.props.userInfo.items.id);
                                                              await this.props.fetchUserCVEmployer(this.props.userInfo.items.id);
                                                          } else {
                                                              await this.props.unSaveCV(this.props.userInfo.items.id);
                                                              await this.props.fetchUserCVEmployer(this.props.userInfo.items.id);
                                                          }
                                                      }
                                                  }}>
                                    <Text style={{
                                        color: '#F2C94C',
                                        fontWeight: 'bold',
                                        fontSize: 16,
                                        alignSelf: 'center',
                                        marginTop: 8
                                    }}>{this.props.userInfo.length !== 0 && this.props.userInfo.items.saved === 1 ? "Bỏ lưu" : "Lưu hồ sơ"}</Text>
                                </TouchableOpacity>


                            </View>
                            <View style={{flex: 1, paddingRight: 10, paddingLeft: 5, marginTop: 8}}>
                                <TouchableOpacity style={{
                                    borderRadius: 50,
                                    borderColor: '#33B8E0',
                                    backgroundColor: '#33B8E0',
                                    borderWidth: 1,
                                    height: 40,
                                    textAlign: 'center'
                                }}
                                                  onPress={() => {
                                                      if (this.props.userInfo.items.buyed === 1) {
                                                          Linking.openURL(`tel:${this.props.userInfo.items.phone}`)
                                                      } else {
                                                          this.getItems();
                                                          this.refs.modelSelect.open();
                                                      }
                                                  }}>
                                    <Text style={{
                                        color: '#fff',
                                        fontWeight: 'bold',
                                        fontSize: 16,
                                        alignSelf: 'center',
                                        marginTop: 8
                                    }}>{'Liên hệ ứng viên'}</Text>
                                </TouchableOpacity>
                            </View>
                        </FooterTab>
                    </Footer> :
                    <View></View>}


                <Modal
                    style={[styles.modal]}
                    position={'center'}
                    ref={'modelSelect'}
                    swipeToClose={false}
                >
                    <Animated.Image
                        style={{
                            width: 184,
                            height: 117,
                            margin: 30,
                            alignSelf: 'center',
                        }}
                        source={require('../../../vendor/images/employer/logomuacv.png')}
                    />
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 'bold',
                        lineHeight: 20,
                        color: '#828282',
                        padding: 30,
                        textAlign: 'center'
                    }}>
                        Vui lòng mua CV để biết thêm thông tin ứng viên
                    </Text>

                    {
                        productList.map((product, i) => {
                            if (product.productId.toString() === this.props.userInfo.items.package_id) {
                                return (
                                    <View key={i}>
                                        <Text style={{
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            lineHeight: 20,
                                            color: '#33B8E0',
                                            padding: 30,
                                            textAlign: 'center'
                                        }}>
                                            Giá cv {product.localizedPrice}
                                        </Text>
                                        <TouchableOpacity style={{
                                            borderRadius: 50,
                                            borderColor: '#33B8E0',
                                            backgroundColor: '#33B8E0',
                                            borderWidth: 1,
                                            height: 48,
                                            width: 165,
                                            marginTop: 30,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                                          onPress={() => {
                                                              console.log(product.productId);
                                                              this.buyItem(product.productId);
                                                              this.refs.modelSelect.close();
                                                          }}>
                                            <Text style={{
                                                color: '#FDFDFD',
                                                fontWeight: 'bold',
                                                fontSize: 17,
                                            }}>Mua CV</Text>
                                        </TouchableOpacity>
                                    </View>
                                );
                            }
                        })
                    }

                    <TouchableOpacity style={{
                        borderRadius: 50,
                        borderColor: '#FFFFFF',
                        backgroundColor: '#FFFFFF',
                        borderWidth: 1,
                        height: 48,
                        width: 165,
                        marginTop: 30,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                                      onPress={() => {
                                          this.refs.modelSelect.close();
                                      }}>
                        <Text style={{
                            color: '#33B8E0',
                            fontWeight: 'bold',
                            fontSize: 17,
                        }}>Bỏ qua</Text>
                    </TouchableOpacity>

                </Modal>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userInfo: state.employerUserCvReducers,
        listSkill: state.skillReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserCVEmployer: (userId) => {
            dispatch(FetchUserCVEmployer(userId))
        },
        fetchSkill: () => {
            dispatch(FetchListSkill());
        },
        saveCV: (userId) => {
            dispatch(SaveUserCVEmployer(userId));
        },
        buyCv: (params) => {
            dispatch(BuyCv(params));
        },
        unSaveCV: (userId) => {
            dispatch(UnSaveUserCVEmployer(userId));
        }
    };
};


const cvUser = connect(mapStateToProps, mapDispatchToProps)(CVUserComponent);

export default withNavigation(cvUser)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4',
        flexDirection: 'column'

    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 80,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 50,
        height: 38,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#D0C9D6',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    textAdvanced: {
        width: 150,
        left: 20,
        fontSize: 15,
        color: '#33B8E0',
        fontWeight: '500',
    },
    textBody: {
        fontSize: 15,
        color: '#525252',
        fontWeight: '500'
    },
    textPlaceholder: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },
    wrapper: {
        paddingTop: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        width: screenWidth - 40,
    },
    text: {
        color: 'black',
        fontSize: 16,
        textAlign: 'left',
        left: 10,
        marginBottom: 10
    },
    button: {
        backgroundColor: 'green',
        width: 300,
        marginTop: 16,
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        color: '#d0d0d0',
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF'
    }
});

