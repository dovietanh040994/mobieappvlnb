import React from 'react';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import {Text, TouchableOpacity, View} from 'react-native';
import {LOGINFORM, SELECTINFO, SING_IN_EMPLOYER_FORM, SINGINFORM} from "../../../vendor/formNames";
// import {CarrerBoxDetail, SearchDetail} from "../../../vendor/screen";

// const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
//dispatch(submit(CONTACT_FORM)) taoj luôn ra 1 action là submit chứ k chạy qua action(tạo sẵn) như redux
// const {navigate} = this.props.navigation;
const BtnSingin = ({dispatch}) => {
    return (

        <TouchableOpacity style={{borderRadius: 5, backgroundColor: '#33B8E0', padding: 13}}
                          onPress={() => {
                              dispatch(submit(SING_IN_EMPLOYER_FORM))

                              // this.props.navigation.navigate(SearchDetail);
                          }}
        >
            <View style={{alignItems: 'center', alignSelf: 'center'}}>
                <Text style={{fontSize: 17, color: '#fff', fontWeight: '700'}}>Đăng ký</Text>
            </View>
        </TouchableOpacity>


    );
};

//connect()(RemoteSubmitButton) la container,RemoteSubmitButton laf component
export default connect()(BtnSingin);
