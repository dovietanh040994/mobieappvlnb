import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions,
    TextInput, Animated, TouchableOpacity, ScrollView, BackHandler
} from 'react-native';
import firebase from 'react-native-firebase';

import { AccessToken, LoginManager, LoginButton } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import Modal from 'react-native-modalbox';
import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem, Toast, Input
} from "native-base";
import {StylesAll} from '../../../vendor/styles'

//screen
import {ResetPass, SelectUser, SignInEmployer} from '../../../vendor/screen'
import {connect} from "react-redux";
import {LoginAction, LoginEmployerAction} from '../../../actions'
import {getStatusBarHeight} from "react-native-status-bar-height";
import {Field, reduxForm, submit} from "redux-form";
import {LOG_IN_EMPLOYER_FORM, LOGINFORM, SELECTINFO} from "../../../vendor/formNames";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../../vendor/NavigationService";
import BtnLogin from "./BtnLogin";


const submitForm = (values, dispatch) => {
    const errors = {};
    if (!values.email || values.email.trim().length <= 0) {
        errors.email = 'Email không được để trống';
        Toast.show({
            text: errors.email,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (!values.password || values.password.trim().length <= 0) {

        errors.password = 'Mật khẩu không được để trống';
        Toast.show({
            text: errors.password,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else {

        dispatch(LoginEmployerAction(values))

    }
};
class Login extends Component {
    constructor(props) {
        super(props);
        this.unsubscriber = null;
        this.state = {
            secureTextEntry:true
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    static navigationOptions = () => ({
        header: null
    });

    componentWillMount() {

        BackHandler.addEventListener('LoginEmploy', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('LoginEmploy', this.handleBackButtonClick);

        if (this.unsubscriber) {
            this.unsubscriber();
        }
    }

    handleBackButtonClick() {
        this.navigatePage();
        return true;

    }

    navigatePage = () => {
        const { navigation } = this.props;
        navigation.navigate(SelectUser)
    };

    renderPickerEmail = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:15}}
                    onChangeText={onChange} {...restInput}
                    secureTextEntry={secureTextEntry}
                    value={`${value}`}

                />


            </View>
        );
    }
    renderPickerPass = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: { onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:50}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={secureTextEntry}

                />
                    <TouchableOpacity style={{position:'absolute',top:0,right:0,width:50,height:50,flexDirection:'row',justifyContent:'center'}}
                      onPress={() => {
                          this.setState({
                              secureTextEntry:!secureTextEntry
                          })
                      }}
                    >
                        <Image source={require('../../../vendor/images/eya.png')} style={{alignSelf:'center'}} />
                    </TouchableOpacity>

                {/*{touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||*/}
                {/*    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}*/}


            </View>
        );
    }
    render() {

        return (
            <View style={{height: '100%'}}>
                <ScrollView >
                    <Header style={{
                        backgroundColor: '#fff',
                        paddingLeft: 20,
                        height: 50,
                        borderBottomWidth: 1,
                        borderBottomColor: '#ccc',
                        elevation: 0
                    }}>
                        <Left>
                            <TouchableOpacity style={{width:50,height:30,position:'absolute',left:-20,top:-13}} transparent
                                              onPress={() => {
                                                  this.navigatePage()
                                              }}
                            >
                                <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                                    tintColor: '#33B8E0', marginTop: 5,marginLeft:23
                                }}/>
                            </TouchableOpacity>
                        </Left>
                        <Body style={{flex: 3}}>
                            <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Quay lại</Title>
                        </Body>
                    </Header>
                    <Content  contentContainerStyle={{flex: 1, marginTop: 30,paddingLeft:20,paddingRight:20}}>
                        <Image
                            resizeMode={'contain'} source={require('../../../vendor/images/VLNB.png')} style={[{tintColor: '#33B8E0',width: 121,
                            height: 76,}]}
                        />
                        <View>
                            <Text style={{fontSize:22,color: '#33B8E0',marginTop:20,marginBottom: 15,fontWeight:'500'}}>Đăng nhập</Text>
                        </View>
                        <View>
                            <Text style={{fontSize:15,color: '#3f3356',marginTop:15,marginBottom: 25,fontWeight:'normal'}}>Đăng nhập để tuyển dụng và trải nhiệm dịch vụ một cách tốt nhất</Text>
                        </View>
                        <Field   name="email" component={this.renderPickerEmail} placeholder={'Địa chỉ Email'} >
                        </Field>
                        <Field   name="password" component={this.renderPickerPass}  placeholder={'Mật khẩu'}  secureTextEntry={this.state.secureTextEntry}>

                        </Field>
                        <TouchableOpacity style={{height:30}}
                                          onPress={() => {
                                              this.refs.modal1.open()
                                          }}
                        >
                            <Text style={{fontSize:13,color:'#33B8E0'}}> Quên mật khẩu ?</Text>
                        </TouchableOpacity>
                        <View >
                            <BtnLogin/>
                        </View>
                        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',alignSelf:'center',height:30,marginTop:25,marginBottom:15}}
                                          onPress={() => {
                                              this.props.navigation.navigate(SignInEmployer)
                                          }}
                        >
                            <Text style={{fontSize:13,color:'#3f3356'}}>Bạn chưa có tài khoản?</Text>
                            <Text style={{fontSize:13,color:'#33B8E0'}}> Đăng ký</Text>
                        </TouchableOpacity>
                    </Content>

                </ScrollView>
                <Modal style={[styles.modal, styles.modal3]} position={"center"} ref={"modal1"} >
                    <Text style={styles.text}>Vui lòng liên hệ tổng đài</Text>
                    <Text style={styles.text2}>0898686021</Text>
                    <Text style={styles.text}>để được tư vấn và hỗ trợ !</Text>
                </Modal>
            </View>



        );
    }
}




const LoginComponent = connect(null,null)(Login);
export default reduxForm({
    form: LOG_IN_EMPLOYER_FORM, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    // validate,
    // warn,
    onSubmit: submitForm
})(LoginComponent)
const styles = StyleSheet.create({
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal3: {
        height: 300,
        width: 300
    },
    text:{
        fontSize:18,
        color:'#3f3356'
    },
    text2:{
        fontSize:25,
        color:'#33B8E0',
        marginTop:20,
        marginBottom:20,
        fontWeight:'700'
    }

});
