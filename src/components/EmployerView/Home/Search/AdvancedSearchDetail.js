import React, {Component, PureComponent} from 'react';
import {
    Image,
    Platform,
    View,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    ScrollView, Animated, StyleSheet, ActivityIndicator, FlatList, BackHandler
} from 'react-native';
import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Input,
    Card,
    CardItem,
    Item, Picker, Form, Toast,Textarea
} from "native-base";
import {Field, reduxForm} from 'redux-form';
import {SEARCH_ADVANCED, USER_FORM} from '../../../../vendor/formNames'

//Styles
import {StylesAll} from '../../../../vendor/styles'
import {HomeBox, Profile, Search, SearchAdvancedEmployer, ShowProfile} from "../../../../vendor/screen";
import {connect} from "react-redux";
import BtnSearchAdvanced from './BtnSearchAdvanced'
import DatePicker from 'react-native-datepicker'
// import submitEditForm from './SubmitServer/submitEditForm'
import {EditUserAction, SearchAdvancedAction, SearchFormAction} from "../../../../actions";
import {getStatusBarHeight} from "react-native-status-bar-height";
import Modal from 'react-native-modalbox';
import {bindActionCreators} from "redux/es/redux";
import ListDetailModal from "./ListDetailModal"
import NavigationService from "../../../../vendor/NavigationService";
import FlatListItemListUser from "../../ChildComponent/FlatListItemListUser";
import AsyncStorage from "@react-native-community/async-storage";
import PlaceholderLoadingJob from "../../../Helpers/PlaceholderLoadingJob";



class AdvancedSearchDetail extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            userInfo:null,
            notData: false
        };
        this.page = 1;
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    static navigationOptions = () => ({
        header: null
    });

    componentWillMount () {
        BackHandler.addEventListener('AdvancedSearchDetailEmployer', this.handleBackButtonClick);


    }

    componentDidMount() {
        AsyncStorage.getItem('userInfoEmployer',(err, result) =>{
            this.setState({
                userInfo:result
            },()=>{
                this.loadMoreData()

            })
        })

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('AdvancedSearchDetailEmployer', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(SearchAdvancedEmployer)
        return true;

    }

    loadMoreData = async () => {
        const { navigation } = this.props;
        const searchId = navigation.getParam('search', null);
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})


            const province = searchId.province ? searchId.province : '';
            const carrer = searchId.carrer ? searchId.carrer : '';
            const salary = searchId.salary ? searchId.salary : '';
            const jobTypes = searchId.jobTypes ? searchId.jobTypes : '';
            const experience = searchId.experience ? searchId.experience : '';
            const data = `province_id=${province}&carrer_id=${carrer}&job_salary_id=${salary}&job_type_id=${jobTypes}&job_experience_id=${experience}`;

            const Url = `http://vieclamnambo.vn:9002/api/vlnb/employee/getuser?${data}&row_start=${this.page}`;
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo
                },
            }).then((response) => response.json())

                .then((responseJson) => {
                    console.log('responseJson',responseJson)
                    if (responseJson.total === 0){
                        this.setState({
                            notData: true
                        });
                    }
                    if (responseJson.users.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.users],
                            fetching_from_server: false,
                            notData: false

                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (

            <View style={styles.footer}>
                {this.state.fetching_from_server ? (  this.page === 1?
                        <PlaceholderLoadingJob/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }



    render() {
        return (
            <View style={{ backgroundColor: '#F5F4F4',flex: 1,}}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 1,
                    borderBottomColor: '#ccc',
                    elevation: 0
                }}>
                    <Left>
                        <TouchableOpacity style={{width:50,height:30,position:'absolute',left:-20,top:-13}} transparent onPress={() => {
                            this.props.navigation.navigate(SearchAdvancedEmployer)
                        }}>
                            <Image source={require('../../../../vendor/images/arr_left.jpg')} style={{
                                tintColor: '#33B8E0', marginTop: 5,marginLeft:23
                            }}/>
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Tìm kiếm nâng cao</Title>
                    </Body>
                </Header>
                {
                    this.state.notData === false?
                    <FlatList
                        style={[styles.fill]}
                        // data={this.props.salary.items.jobPosts}
                        data={this.state.serverData}
                        onEndReached={() => this.loadMoreData()}
                        onEndReachedThreshold={0.5}
                        renderItem={({item, index}) => {
                            return (
                                <View style={{marginBottom: 15 ,marginTop:index === 0 ?15:0}}>
                                    <FlatListItemListUser
                                        item={item}
                                        index={index}
                                        parentFlatList={this}
                                        navigation={this.props.navigation}
                                        random = {index % 4}
                                    />
                                </View>
                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        ListFooterComponent={this.renderFooter.bind(this)}
                    />
                :
                    <View>
                        <View style={{alignItems: 'center',marginTop:90}}>
                            <Image source={require('../../../../vendor/images/Frame.png')} />
                            <Text style={{color:'#8C8888',fontWeight:'500',fontSize:16,marginTop:30}}>Không tìm thấy công việc phù hợp</Text>
                        </View>
                    </View>
                }

            </View>

        );
    }
}

export default AdvancedSearchDetail
const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },

});
