import React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';
import { View, Text,TouchableOpacity } from 'react-native';

import {Button, Right} from "native-base";
import {SEARCH_ADVANCED, SEARCH_ADVANCED_EMPLOYER, SEARCH_FORM} from "../../../../vendor/formNames";
import {SearchDetail} from "../../../../vendor/screen";
import {StylesAll} from "../../../../vendor/styles";
// import {CarrerBoxDetail, SearchDetail} from "../../../../vendor/screen";

// const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
//dispatch(submit(CONTACT_FORM)) taoj luôn ra 1 action là submit chứ k chạy qua action(tạo sẵn) như redux
// const {navigate} = this.props.navigation;
const BtnSearchAdvanced = ({ dispatch }) => {
    return (
            <TouchableOpacity  style={{backgroundColor:'#33b8e6',marginBottom:20,marginTop:30,borderRadius:5,height:50,flex:1,justifyContent:'center'}}
                    onPress={() => {
                        // console.log(12323213)
                        dispatch(submit(SEARCH_ADVANCED_EMPLOYER))
                    }}
            >
                <Text style={{alignSelf:'center',fontSize:17,fontWeight:'bold',color:'#fff'}}>Tìm kiếm</Text>
            </TouchableOpacity>

    );
};

//connect()(RemoteSubmitButton) la container,RemoteSubmitButton laf component
export default connect()(BtnSearchAdvanced);
