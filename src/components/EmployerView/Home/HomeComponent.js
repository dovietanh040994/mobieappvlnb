import React, {PureComponent} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler
} from 'react-native';
import FlatListItemListUser from "../ChildComponent/FlatListItemListUser";
import {Button, Container, Header, Item, Toast} from "native-base";
import {
    HomeBox,
    JobDetail,
    SearchEmployer,
    SearchAdvanced,
    SelectInfo,
    SearchAdvancedEmployer
} from "../../../vendor/screen";
import {
    CategoryPost,
    FetchCarrerList,
    FetchExperienceList, FetchJobTypesList,
    FetchProvinceList,
    FetchSalaryList,
    keyWorksAction
} from "../../../actions";
import {connect} from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import {withNavigation} from "react-navigation";
import {GoogleSignin} from "react-native-google-signin";
import PlaceholderLoadingJob from "../../Helpers/PlaceholderLoadingJob";
import FlatListItemJob from "../../UserView/ChildComponent/FlatListItemJob";

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;


class Home extends PureComponent {

    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            bgButton: 0,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            userInfo: null,
            notData: false,
            doubleBackToExitPressedOnce: false,
            totalUser:''
        };
        this.page = 1;
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    componentDidMount() {
        AsyncStorage.getItem('userInfoEmployer', (err, result) => {
            this.setState({
                userInfo: result
            }, () => {
                this.loadMoreData();
            })
        })
    }

    componentWillMount() {
        BackHandler.addEventListener('HomeEmployerComponent', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('HomeEmployerComponent', this.handleBackButton);
    }

    handleBackButton = () => {
        if (this.state.doubleBackToExitPressedOnce) {
            BackHandler.exitApp();
        } else {
            Toast.show({
                text: "Bấm thêm một lần nữa để thoát",
            })
            this.setState({doubleBackToExitPressedOnce: true});
            setTimeout(() => {
                this.setState({doubleBackToExitPressedOnce: false});
            }, 2000);
        }

        return true;
    }

    loadMoreData = async () => {
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})

            const Url = `http://vieclamnambo.vn:9002/api/vlnb/employee/getuser?row_start=${this.page}`;
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo

                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {
                    console.log(responseJson)
                    if (responseJson.total === 0) {
                        this.setState({
                            notData: true
                        });
                    }else{
                        this.setState({
                            totalUser: responseJson.total
                        });
                    }
                    if (responseJson.users.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.users],
                            fetching_from_server: false,
                            notData: false
                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (

            <View style={styles.footer}>
                {this.state.fetching_from_server ? (  this.page === 1?
                        <PlaceholderLoadingJob/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    renderHeader() {
        return (
            <View>
                <Header rounded style={{
                    backgroundColor: '#fff',
                    height: 80,
                    borderBottomWidth: 0.5,
                    borderColor: '#ccc',
                    paddingLeft: 0,
                    paddingRight: 0
                }}>
                    <ImageBackground source={require('../../../vendor/images/Rectangle.png')}
                                     style={{width: '100%', height: '100%'}}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            marginTop: 20,
                            paddingLeft: 15,
                            paddingRight: 15
                        }}>
                            <View style={{flex: 1}}>
                                <TouchableOpacity style={{backgroundColor: '#fff', borderBottomWidth: 0}}
                                                  onPress={() => {
                                                      this.props.navigation.navigate(SearchAdvancedEmployer);
                                                  }}>


                                    <View style={{position: 'absolute', left: 15, top: 7}}>
                                        <Image source={require('../../../vendor/images/Search.png')} style={{
                                            tintColor: '#33B8E0'
                                        }}/>
                                    </View>
                                    <TextInput editable={false} style={{
                                        width: '100%',
                                        borderWidth: 1,
                                        borderColor: '#ECEBED',
                                        borderRadius: 5,
                                        top: 0,
                                        height: 40,
                                        fontSize: 15,
                                        paddingLeft: 50,
                                        paddingRight: 10
                                    }} placeholder="Tìm kiếm hồ sơ ứng viên"/>

                                </TouchableOpacity>

                            </View>
                        </View>
                    </ImageBackground>


                </Header>
                {this.state.totalUser !== '' ? <View style={{paddingTop:20,paddingBottom:5,paddingLeft:15}}>
                    <Text style={{fontSize:15,color:'#33B8E0'}}>Tìm thấy <Text style={{fontWeight:'bold'}}> {this.state.totalUser} </Text> ứng viên</Text>
                </View>: null}

            </View>
        );
    }




    render() {


        return (
            <View style={[styles.fill]}>

                {this.state.notData === false ?

                    <FlatList
                        style={[styles.fill]}
                        data={this.state.serverData}
                        onEndReached={() => this.loadMoreData()}
                        onEndReachedThreshold={0.5}
                        ListHeaderComponent={this.renderHeader.bind(this)}
                        renderItem={({item, index}) => {
                            // console.log('kkkas',)
                            return (


                                <View style={{marginBottom: 15, marginTop: index === 0 ? 15 : 0}}>
                                    <FlatListItemListUser
                                        item={item}
                                        index={index}
                                        parentFlatList={this}
                                        navigation={this.props.navigation}
                                        random={index % 4}
                                        nameScreen={'HomeEmployer'}
                                    />
                                </View>


                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => <View style={styles.separator}/>}
                        ListFooterComponent={this.renderFooter.bind(this)}
                    />
                    :
                    <View>
                        <View style={{height: 130}}/>
                        <View style={{alignItems: 'center', marginTop: 60}}>
                            <Image source={require('../../../vendor/images/Loading_Job.png')}/>
                            <Text style={{color: '#8C8888', fontWeight: '500', fontSize: 16, marginTop: 30}}>Không tìm
                                thấy công việc phù hợp</Text>
                        </View>
                    </View>
                }


            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        // user: state.loginReducers,
        // jobContent: state.jobDetailReducers,
        // companyContent: state.companyDetailReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        category: () => {
            dispatch(CategoryPost());
        },
        keyWorks: () => {
            dispatch(keyWorksAction());

        },

    };
}

const HomeComponent = connect(mapStateToProps, mapDispatchToProps)(Home);
export default withNavigation(HomeComponent)
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4',
        // paddingTop:70

    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 80,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 78,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
