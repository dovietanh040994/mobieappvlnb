import React, {PureComponent} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler,
    Alert
} from 'react-native';
import FlatListItemPost from "../../UserView/ChildComponent/FlatListItemPost";
import InformationJob from "../Home/InformationJob";
import CompanyJob from "../Home/CompanyJob";
import {
    Body,
    Button,
    CardItem,
    Container,
    Header,
    Item,
    Left,
    Picker,
    Title,
    Footer,
    FooterTab,
    Icon, Toast, Right
} from "native-base";
import {connect} from "react-redux";

import categoryPostReducers from "../../../reducers/CategoryPostReduser";
import {
    appliedAction,
    CategoryPost, CompanyDetailAction,
    JobDetailAction,
    JobSameCompanyAction,
    LoginAction, PostEmployerDetailAction,
    SaveJobAction,
    UnSaveJobAction
} from "../../../actions";
import {
    Home,
    HomeBox,
    JobApply,
    JobCare,
    Login,
    Search,
    Notification,
    EmployerManagePost
} from "../../../vendor/screen";
import FlatListItemJob from "../../UserView/ChildComponent/FlatListItemJob";
import companyDetailReducers from "../../../reducers/CompanyDetailReduser";
import AsyncStorage from "@react-native-community/async-storage";
import FlatListItemListUser from "../ChildComponent/FlatListItemListUser";
import PlaceholderLoadingJob from "../../Helpers/PlaceholderLoadingJob";


const HEADER_MAX_HEIGHT = 178;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;
const menu = [
    {
        id: 1,
        name: "HỒ SƠ ĐÃ LƯU",
    },
    {
        id: 2,
        name: "HỒ SƠ ĐÃ MUA",
    },

]

class ProfileUser extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                0,
            ),
            refreshing: false,
            notData: false,
            bgButton: 0,
            idCategory: 1,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            userInfo: null,

        };
        this.page = 1;
    }

    componentDidMount() {
        AsyncStorage.getItem('userInfoEmployer', (err, result) => {
            this.setState({
                userInfo: result
            }, () => {
                this.loadMoreData();
            })
        })
    }




    loadMoreData = async () => {
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})
                const Url = this.state.idCategory === 1
                    ?
                    `http://vieclamnambo.vn:9002/api/vlnb/employee/getuser?saved=1&row_start=${this.page}`
                    :
                    `http://vieclamnambo.vn:9002/api/vlnb/employee/getuser?buyed=1&row_start=${this.page}`;

        console.log(Url)
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo

                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {
                    console.log(responseJson)
                    if (responseJson.total === 0) {
                        this.setState({
                            notData: true
                        });
                    }else{
                        this.setState({
                            totalUser: responseJson.total
                        });
                    }
                    if (responseJson.users.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.users],
                            fetching_from_server: false,
                            notData: false
                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (
            <View style={styles.footer}>
                {this.state.fetching_from_server ? (  this.page === 1?
                        <PlaceholderLoadingJob/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }


    TabName(stateName, idCategory) {
        this.setState({
            bgButton: stateName,
            idCategory: idCategory,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
        }, () => {
            this.page = 1
            this.loadMoreData()

        });
    }

    static navigationOptions = () => ({
        header: null
    });


    render() {

        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -100, -178],
            extrapolate: 'clamp',
        });


        return (
            <View style={styles.fill}>

                {this.state.notData === false ?

                    <Animated.FlatList
                        style={[styles.fill]}
                        data={this.state.serverData}
                        onScroll={Animated.event(
                            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                            {useNativeDriver: true},
                        )}
                        onEndReached={() => this.loadMoreData()}
                        ListHeaderComponent={<View style={{height: 230}}/>}
                        onEndReachedThreshold={0.5}
                        renderItem={({item, index}) => {
                            // console.log('kkkas',)
                            return (


                                <View style={{marginBottom: 15, marginTop: index === 0 ? 15 : 0}}>
                                    <FlatListItemListUser
                                        item={item}
                                        index={index}
                                        parentFlatList={this}
                                        navigation={this.props.navigation}
                                        random={index % 4}
                                        nameScreen={'ProfileUserEmployer'}
                                    />
                                </View>


                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => <View style={styles.separator}/>}
                        ListFooterComponent={this.renderFooter.bind(this)}
                    />
                    :
                    <View>
                        <View style={{height: 230}}/>
                        <View style={{alignItems: 'center', marginTop: 60}}>
                            <Image source={require('../../../vendor/images/Loading_Job.png')}/>
                            <Text style={{color: '#8C8888', fontWeight: '500', fontSize: 16, marginTop: 30}}>Không tìm
                                thấy công việc phù hợp</Text>
                        </View>
                    </View>
                }


                <Animated.View
                    // pointerEvents="none"
                    style={[
                        styles.header,
                        {transform: [{translateY: headerTranslate}]},
                    ]}
                >

                    <Animated.View style={[

                        {
                            opacity: imageOpacity,
                            transform: [{translateY: imageTranslate}],
                        },
                    ]}>
                        <Header style={{backgroundColor: '#fff', height: 178,elevation:0,paddingLeft:0,paddingRight:0}}>
                            <ImageBackground source={require('../../../vendor/images/bgProfileUser.png')} style={{width: '100%', height: '100%'}}>
                                <Text style={{fontWeight:'900',color:'#fff',fontSize:32,alignSelf:'center',marginTop:70}}>Hồ sơ</Text>

                            </ImageBackground>

                        </Header>

                    </Animated.View>

                </Animated.View>
                    <Animated.View
                        style={[
                            styles.bar,
                            {
                                transform: [
                                    // { scale: titleScale },
                                    {translateY: titleTranslate},
                                ],
                            },
                        ]}
                    >
                        <FlatList
                            numColumns={2}
                            data={menu}
                            ref={(ref) => {
                            this.flatListRef = ref;
                        }}
                            style={{width: '100%',}}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) =>
                            <View style={{
                                backgroundColor: this.state.bgButton === index ? '#33B8E0' : '#fff',
                                paddingLeft: 5,
                                paddingRight: 5,
                                width: '100%',
                                flex:1
                            }}>
                            <TouchableOpacity style={styles.ScrollTextContainer} onPress={() => {
                                const stateName = index;
                                const idCategory = item.id;
                                this.TabName(stateName, idCategory)
                            }}>
                            <Text style={{
                                fontSize: 16,
                                fontWeight: 'bold',
                                color: this.state.bgButton === index ? '#fff' : '#D9D3DE',
                                alignSelf:'center'
                            }}>{item.name}</Text>
                            </TouchableOpacity>
                            </View>
                        }
                            />

                    </Animated.View>

            </View>
        );
    }
}


const ProfileUserComponent = connect(null, null)(ProfileUser);
export default ProfileUserComponent

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4'
        // paddingTop:70
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 178,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 178,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        borderBottomWidth: 1,
        borderColor: '#ECEBED',
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center',
        width:'100%'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
