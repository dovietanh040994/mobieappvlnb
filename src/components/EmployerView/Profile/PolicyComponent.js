import React, {Component} from "react";
import {WebView, View, TouchableOpacity, Animated, ScrollView,Text} from "react-native";
import {Body, Header, Left, Title} from "native-base";
import {ProfileEmployer} from "../../../vendor/screen";
import MyWebView from "react-native-webview-autoheight";

export default class PolicyComponent extends Component {

    constructor(props) {
        super(props);

    }

    static navigationOptions = () => ({
        header: null
    });
    render() {
        return (
            <View style={{flex:1}}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(ProfileEmployer)}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Chính sách và quyền riêng tư</Title>
                    </Body>
                </Header>

                <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white' }} >
                    <MyWebView
                        //sets the activity indicator before loading content
                        startInLoadingState={true}
                        source={{uri: "https://vieclamnambo.vn/m/dieu-khoan-su-dung"}}/>
                </View>
            </View>


        );
    }
}


