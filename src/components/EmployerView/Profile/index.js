import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
import ProfileComponent from "./ProfileComponent";
import NotificationEmployerComponents from "./NotificationComponents";
import InfoCompanyComponent from "./InfoCompanyComponent"

import React from "react";
import {ProfileEmployer} from "../../../vendor/screen";
import PolicyComponent from "./PolicyComponent";

const ProfileEmployerNavigator = createStackNavigator(
    {
        ProfileEmployer: {
            screen: ProfileComponent,
        },
        NotifyEmployer: {
            screen: NotificationEmployerComponents,
        },
        InfoEmployer: {
            screen: InfoCompanyComponent,
        }, PolicyEmploy: {
            screen: PolicyComponent,

        },

    }, {
        initialRouteName: 'ProfileEmployer'
    }
);

export default createAppContainer(ProfileEmployerNavigator);
