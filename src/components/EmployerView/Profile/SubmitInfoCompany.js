import React from 'react';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import {StyleSheet, View, TouchableOpacity, TextInput, Text, Animated} from 'react-native';
import {Button, Left} from 'native-base'
import {FORM_INFO_COMPANY} from "../../../vendor/formNames";

const submitInfoCompany = ({dispatch}) => {
    return (
        <TouchableOpacity style={{
            borderRadius: 50,
            borderColor: '#33B8E0',
            backgroundColor: '#33B8E0',
            borderWidth: 1,
            height: 48,
            alignItems: 'center',
            justifyContent: 'center'
        }}
                          onPress={() => {
                              dispatch(submit(FORM_INFO_COMPANY))
                          }}>
            <Text style={{
                color: '#fff',
                fontWeight: 'bold',
                fontSize: 17,
            }}>Lưu</Text>
        </TouchableOpacity>
    );
};

export default connect()(submitInfoCompany);
