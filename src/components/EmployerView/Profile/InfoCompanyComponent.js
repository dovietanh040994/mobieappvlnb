import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, TextInput, Dimensions, BackHandler
} from 'react-native';
import {
    Body,
    CardItem,
    Container,
    Left,
    Right,
    Switch,
    Button,
    Input,
    Picker,
    Icon,
    Title,
    Header,
    Toast, Textarea
} from "native-base";
import {StylesAll} from "../../../vendor/styles";
import {Field, reduxForm, SubmissionError, submit} from 'redux-form';
import {FORM_INFO_COMPANY} from "../../../vendor/formNames";
import DatePicker from "react-native-datepicker";
import Modal from 'react-native-modalbox';
import {connect} from "react-redux";
import {AdvancedDetail, Home, HomeBox, Search, ProfileEmployer} from "../../../vendor/screen";
import {UpdateAvatarUser} from "../../../actions";
import NavigationService from "../../../vendor/NavigationService";
import ImagePicker from 'react-native-image-picker';
import SubmitInfoCompany from "./SubmitInfoCompany";
import {
    UpdateInfoCompany
} from "../../../actions";

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


var screen = Dimensions.get('window');

const submitInfoCompany = async (values, dispatch) => {

    if (!values.name || values.name.trim().length <= 0) {
        toastError('Tên công ty không được để trống');
    } else if (values.address.trim().length <= 0) {
        toastError('Địa chỉ công ty không được để trống');
    } else {
        await dispatch(UpdateInfoCompany(values));

        await Toast.show({
            text: "Cập nhật thông tin thành công!",
            buttonText: "Đóng",
            type: "success",
            position: 'top',
        });
        await NavigationService.navigate(ProfileEmployer);
    }


};


const toastError = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};
const toastWarning = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};

const validate = values => {

    const errors = {};

    return errors

};

export class InfoCompanyComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            enableScrollViewScroll: true,
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('ProfileEmployer', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(ProfileEmployer)
        return true;

    }

    componentWillMount() {
        //tạo đầu vào mặc định cho trường trong redux-form
        this.props.initialize({
            name: this.props.infoCompany ? this.props.infoCompany.name : '',
            address: this.props.infoCompany ? this.props.infoCompany.address : '',
            website: this.props.infoCompany ? this.props.infoCompany.website : '',
            description: this.props.infoCompany ? this.props.infoCompany.description : '',
        });

        BackHandler.addEventListener('ProfileEmployer', this.handleBackButtonClick);
    }

    onEnableScroll = (value) => {
        this.setState({
            enableScrollViewScroll: value,
        });
    };

    renderPickerInput = ({label, placeholder, type, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View>
                <Input placeholder={placeholder} placeholderTextColor={'#E0E0E0'}
                       style={[styles.textPlaceholder, {height: 40, paddingLeft: 0}]}
                       keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                       value={`${value}`}
                />
                {touched && ((error && toastError(error)) ||
                    (warning && toastWarning(warning)))}
            </View>
        );
    };

    renderPickerTextarea = ({label, placeholder, keyboardType, meta: {touched, error, warning}, input: {onChange, ...restInput}}) => {

        return (
            <View>
                <Textarea placeholderTextColor={'#E0E0E0'} rowSpan={10} bordered placeholder={placeholder}
                          keyboardType={keyboardType}
                          onChangeText={onChange} {...restInput} style={{
                    borderWidth: 1,
                    borderColor: '#ECEBED',
                    borderRadius: 5,
                    paddingLeft: 15
                }}/>
                {touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||
                    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}
            </View>
        );
    }

    static navigationOptions = () => ({
        header: null
    });


    render() {
        console.log(this.props.infoCompany);
        return (
            <ScrollView scrollEnabled={this.state.enableScrollViewScroll}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(ProfileEmployer)}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Thông tin công ty</Title>
                    </Body>
                    <Right>

                    </Right>
                </Header>

                <View style={[styles.fill, {marginTop: 20, marginBottom: 40}]}>
                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Tên công ty
                        </Text>
                        <Field type={'text'} name="name" component={this.renderPickerInput}
                               style={styles.textPlaceholder}
                               placeholder="Nhập tên công ty.">
                        </Field>
                    </View>
                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Địa chỉ công ty
                        </Text>
                        <Field name="address" type={'text'} component={this.renderPickerInput}
                               style={styles.textPlaceholder}
                               placeholder="Nhập địa chỉ công ty">
                        </Field>
                    </View>
                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Website công ty
                        </Text>
                        <Field name="website" type={'text'} component={this.renderPickerInput}
                               style={styles.textPlaceholder}
                               placeholder="Nhập website công ty">
                        </Field>
                    </View>

                    <View style={{
                        margin: 20,
                    }}>
                        <Field name="description" component={this.renderPickerTextarea}
                               style={[styles.textPlaceholder]}
                               placeholder="Giới thiệu về công ty">
                        </Field>
                    </View>
                    <View style={{flex: 1, paddingRight: 10, paddingLeft: 5, marginTop: 8, marginBottom: 70,  margin: 20}}>
                        <SubmitInfoCompany/>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.loginReducers,
        infoCompany: state.employerCompanyReducers
    }
};
const mapDispatchToProps = (dispatch) => {
    return {};
}

const InfoCompany = connect(mapStateToProps, mapDispatchToProps)(InfoCompanyComponent);
//
export default reduxForm({
    form: FORM_INFO_COMPANY, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    validate,
    onSubmit: submitInfoCompany
})(InfoCompany)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    textAdvanced: {
        width: 150,
        left: 20,
        fontSize: 15,
        color: '#33B8E0',
        fontWeight: '500',
    },
    textBody: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    textPlaceholder: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },
    wrapper: {
        paddingTop: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal4: {
        height: 400,
        width: screen.width
    },
    gender: {
        height: 200,
        width: screen.width
    },
    text: {
        color: 'black',
        fontSize: 16,
        textAlign: 'left',
        paddingLeft: 20,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderColor: '#ccc',
        height: 40,
        alignItems: 'center'
    },
    button: {
        backgroundColor: 'green',
        width: 300,
        marginTop: 16,
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        color: '#d0d0d0',
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF'
    }
});
