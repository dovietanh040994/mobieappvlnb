import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ScrollView, Alert
} from 'react-native';
import {Body, CardItem, Container, Left, Right, Switch, Button, ListItem, Toast} from "native-base";
import {StylesAll} from "../../../vendor/styles";
import {Field} from "redux-form";
import {withNavigation, withNavigationFocus} from 'react-navigation';
import {
    InfoEmployer,
    Policy,
    JobCare,
    Search,
    Login,
    LoginEmployer,
    SelectUser,
    HomeBox,
    NotifyEmployer,PolicyEmploy
} from "../../../vendor/screen";
import {
    DeleteJobCare,
    SwitchNotify,
    FetchDetailUser,
    FetchCvUser,
    FetchUserInfo,
    FetchListUniversity, FetchListMajor, FetchListDegree, FetchListSkill, FetchInfoCompany
} from "../../../actions";
import {connect} from "react-redux";
import notifyReducers from "../../../reducers/NotifyReducers";
import ToggleSwitch from 'toggle-switch-react-native'
import firebase from "react-native-firebase";
import {LoginManager} from "react-native-fbsdk";
import {GoogleSignin} from "react-native-google-signin";
import AsyncStorage from "@react-native-community/async-storage";

const HEADER_MAX_HEIGHT = 159;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class ProfileComponent extends Component {

    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            switchNoti: false,
            userInfo: false,
            token: false,
        };
    }

    toggleSwitch = () => {

        Alert.alert(
            'Thông báo',
            `Bạn muốn sử dụng app với vai trò là ứng viên tìm việc?`,
            [
                {
                    text: 'Đóng',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Xác nhận', onPress: () => {
                        this.props.navigation.navigate(HomeBox);
                    }
                },
            ],
            {cancelable: false},
        );
    };

    componentDidMount() {
        const {navigation} = this.props;
        this.focusListener = navigation.addListener("didFocus", () => {
            AsyncStorage.getItem('userInfoEmployer', (err, result) => {
                console.log(result)
                if (result !== null) {
                    this.props.fetchInfoCompany();
                    this.setState({userInfo: true, token: result});

                } else {
                    this.setState({userInfo: false})
                }
            });
        });

    }

    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
    }

    //hàm logout
    logoutAcc = () => {
        AsyncStorage.removeItem('userInfoEmployer');
        AsyncStorage.removeItem('accessToken');
        this.setState({userInfo: false});
        this.props.navigation.navigate(SelectUser);

    }

    refresh() {
        this.setState({userInfo: true})
    }

    render() {
        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 1],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -55],
            extrapolate: 'clamp',
        });

        const {navigation} = this.props;

        const poinCheck = navigation.getParam('poinCheck', null);

        if (poinCheck === 1 && this.poinCheck === 0) {
            this.refresh()
        }

        return (

            <ScrollView>

                <View style={styles.fill}>
                    <Animated.View
                        pointerEvents="none"
                        style={[
                            styles.header,
                            {transform: [{translateY: headerTranslate}]},
                        ]}
                    >
                        <Animated.Image
                            style={[
                                styles.backgroundImage,
                                {
                                    opacity: imageOpacity,
                                    transform: [{translateY: imageTranslate}],
                                },
                            ]}
                            source={require('../../../vendor/images/profile/profile_header.png')}
                        />
                    </Animated.View>

                    <Animated.View
                        style={[
                            styles.bar,
                            {
                                transform: [
                                    {scale: titleScale},
                                    {translateY: titleTranslate},
                                ],
                            },
                        ]}
                    >
                        <Text style={{
                            fontWeight: '900',
                            color: '#fff',
                            fontSize: 25,
                            alignSelf: 'center',
                            marginTop: 50
                        }}>Cá nhân</Text>

                    </Animated.View>

                    <Animated.View>

                        <View style={{
                            flex: 1, flexDirection: 'column', alignItems: 'center', position: 'relative'
                        }}>

                            <Animated.Image
                                style={{
                                    width: 100,
                                    height: 100,
                                    borderRadius: 100,
                                    borderWidth: 4,
                                    borderColor: "white",
                                    marginBottom: 10,
                                    alignSelf: 'center',
                                    marginTop: 110,
                                }}
                                source={require('../../../vendor/images/profile/avatar_default.png')}
                            />
                            <View style={{
                                alignSelf: 'center',
                                position: 'absolute',
                                marginTop: 110,
                                right: 0
                            }}>
                            </View>

                        </View>
                    </Animated.View>


                    <Animated.View>
                        <Animated.View style={{}}>

                            <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate(InfoEmployer);
                            }}>
                                <View style={styles.boxAdvanced}>
                                    <Text style={styles.title_item}>Thông tin công ty</Text>

                                    <Right style={styles.love_item}>
                                        <Animated.Image
                                            source={require('../../../vendor/images/profile/apply.png')}
                                        />
                                    </Right>
                                    <Right style={styles.arrow_item}>
                                        <Animated.Image
                                            source={require('../../../vendor/images/profile/arrow_right.png')}
                                        />
                                    </Right>
                                </View>
                            </TouchableOpacity>
                        </Animated.View>
                        <Animated.View style={{}}>

                            <TouchableOpacity onPress={() => {
                                // console.log(2123)
                                this.props.navigation.navigate(NotifyEmployer);
                            }}>
                                <View style={styles.boxAdvanced}>
                                    <Text style={styles.title_item}>Thông báo</Text>
                                </View>
                            </TouchableOpacity>
                        </Animated.View>


                        <Animated.View>
                            <View style={styles.boxAdvanced}>
                                <Text style={styles.title_item}>Chuyển quyền ứng dụng</Text>
                                <Right style={{right: 10}}>
                                    <ToggleSwitch
                                        isOn={true}
                                        onColor='#33B8E0'
                                        onToggle={this.toggleSwitch}
                                    />

                                    {/*<Switch onValueChange={this.toggleSwitch}*/}
                                    {/*        value={this.state.switchNoti} style={{right: 10}}/>*/}
                                </Right>
                            </View>
                        </Animated.View>

                        <Animated.View>
                            <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate(PolicyEmploy);
                            }}>
                                <View style={styles.boxAdvanced}>
                                    <Text style={styles.title_item}>Chính sách và quyền riêng tư</Text>
                                    <Right>
                                        <Animated.Image style={styles.arrow_item}
                                                        source={require('../../../vendor/images/profile/arrow_right.png')}
                                        />
                                    </Right>
                                </View>
                            </TouchableOpacity>
                        </Animated.View>
                    </Animated.View>

                    <View style={{padding: 20}}>
                        <Button style={{
                            borderRadius: 6,
                            backgroundColor: 'red',
                            height: 48
                        }} full info onPress={() => {
                            this.logoutAcc();
                            // this.props.navigation.navigate(Login)
                        }}>
                            <Text style={styles.logout_name}>Đăng xuất</Text>
                        </Button>
                    </View>

                </View>
            </ScrollView>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        userCv: state.cvReducers,
        user: state.loginReducers.items,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        onSwitchAction: (value) => {
            dispatch(SwitchNotify(value))
        },
        fetchInfoCompany: () => {
            dispatch(FetchInfoCompany());
        }
    };
}
const Profile = connect(mapStateToProps, mapDispatchToProps)(ProfileComponent);

export default withNavigation(Profile)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    title_item: {
        fontSize: 15,
        color: "#33B8E0",
        fontWeight: "500",
        left: 25

    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },

    love_item: {
        width: 16,
        height: 16,
        left: 29
    },
    apply_item: {
        width: 16,
        height: 16,
        left: 26
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF',
        fontWeight: 'bold'
    }

});
