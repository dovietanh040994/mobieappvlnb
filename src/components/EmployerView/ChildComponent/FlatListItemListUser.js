import React, {Component} from "react";
import {Image, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text, Thumbnail} from "native-base";
import {JobDetailAction, SaveJobAction, CompanyDetailAction, FetchUserCVEmployer} from "../../../actions";
import {connect} from "react-redux";
import Share from "react-native-share";
import {ViewCVUser, JobDetail, PostDetail, CreatePost} from "../../../vendor/screen";

class FlatListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imageLoading: true,
        };

    }

    ImageLoading_Error() {
        this.setState({imageLoading: false});

        // Write Your Code Here Which You Want To Execute If Image Not Found On Server.
    }

    randomImageProp() {
        if (this.props.random === 0) {
            return 0
        } else if (this.props.random === 1) {
            return 1
        } else if (this.props.random === 2) {
            return 2
        } else if (this.props.random === 3) {
            return 3
        }
    }

    render() {

        return (
            <TouchableOpacity onPress={
                async () => {
                    await this.props.fetchUserCVEmployer(this.props.item.id);
                    await this.props.navigation.navigate(ViewCVUser,{
                        nameScreen: this.props.nameScreen,
                    })
                }
            }>
            <CardItem
                style={{
                    backgroundColor: 'white',
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    marginRight: 15,
                    marginLeft: 15,
                    borderBottom: 0
                }}>
                <Left>
                    {this.props.random === 0 && (
                        <Image
                            resizeMode={'contain'}
                            source={
                                this.state.imageLoading && this.props.item.avatar !== ''
                                    ?
                                    {uri: `${this.props.item.avatar}`}
                                    :
                                    require('../../../vendor/images/VLNB5.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}
                    {this.props.random === 1 && (
                        <Image
                            resizeMode={'contain'}
                            source={
                                this.state.imageLoading && this.props.item.avatar !== ''
                                    ?
                                    {uri: `${this.props.item.avatar}`}
                                    :
                                    require('../../../vendor/images/VLNB6.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}
                    {this.props.random === 2 && (
                        <Image
                            resizeMode={'contain'}
                            source={
                                this.state.imageLoading && this.props.item.avatar !== ''
                                    ?
                                    {uri: `${this.props.item.avatar}`}
                                    :
                                    require('../../../vendor/images/VLNB7.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}
                    {this.props.random === 3 && (
                        <Image
                            resizeMode={'contain'}
                            source={
                                this.state.imageLoading && this.props.item.avatar !== ''
                                    ?
                                    {uri: `${this.props.item.avatar}`}
                                    :
                                    require('../../../vendor/images/VLNB8.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}

                    <Body>

                            <Text numberOfLines={2} style={{fontSize: 13, color: '#474747', fontWeight: 'bold'}}>
                                {this.props.item.name !== '' ? this.props.item.name.toUpperCase() : 'ẨN DANH'}
                            </Text>
                        <Text style={{fontSize: 10, marginTop: 3, marginBottom: 3}}>
                            {this.props.item.carrer_name}
                        </Text>
                        <View style={{flexDirection: 'row', marginBottom: 1}}>
                            <Image source={require('../../../vendor/images/Shape.png')}
                                   style={{marginTop: 4, marginRight: 5}}/>
                            <Text style={{color: '#33B8E0', fontSize: 11, fontWeight: '500'}} numberOfLines={1}>
                                {this.props.item.province_id !== 0 ? this.props.item.province_name : 'Chưa cập nhật'}
                            </Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Left>
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('../../../vendor/images/money.png')}
                                           style={{marginTop: 4, marginRight: 5}}/>
                                    <Text style={{color: '#33B8E0', fontSize: 11, fontWeight: '500'}} numberOfLines={1}>
                                        {this.props.item.job_salary_id !== 0 ? this.props.item.job_salary_name : 'Thỏa thuận'}
                                    </Text>
                                </View>
                            </Left>
                            <Right>
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('../../../vendor/images/clock.png')}
                                           style={{marginTop: 4, marginRight: 5}}/>
                                    <Text style={{color: '#33B8E0', fontSize: 11, fontWeight: '500'}} numberOfLines={1}>
                                        {this.props.item.job_experience_id !== 0 ? this.props.item.job_experience_name : 'Chưa cập nhật'}
                                    </Text>
                                </View>
                            </Right>

                        </View>

                    </Body>

                </Left>
            </CardItem>
            </TouchableOpacity>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        jobContent: state.jobDetailReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchUserCVEmployer: (userId) => {
            dispatch(FetchUserCVEmployer(userId))
        },

        jobDetail: (idJob) => {
            dispatch(JobDetailAction(idJob));

        },
        componyDetail: (idJob) => {

            dispatch(CompanyDetailAction(idJob));
        },

    };
}
const FlatListItemListUser = connect(mapStateToProps, mapDispatchToProps)(FlatListItem);

export default FlatListItemListUser
