import React, {Component} from 'react';
import Modal from 'react-native-modalbox';
import {Button, Container, Icon, Text,Left,Body,Header} from "native-base";
import {ActivityIndicator, Image, ScrollView, StyleSheet, View,Linking,TouchableOpacity} from "react-native";
import YouTube from "react-native-youtube";
import {connect} from "react-redux";
import email from 'react-native-email'
import {getStatusBarHeight} from "react-native-status-bar-height";

 class ItemDetail extends Component{
    constructor(props) {
        super(props)

        //hàm tạo ref
        this.setMyModal = element => {this.myModal = element};
    }


    showAddModal = () => {
        return (
            this.myModal.open()
        )
    }

    render() {
        console.log( this.props.jobContent);
        const {loadingDetaiJob} = this.props.jobContent;
        return (
            <Modal
                style={[styles.modal,{marginTop: getStatusBarHeight(true)}]}
                ref={this.setMyModal}
                swipeToClose={false}
            >
                <Button style={{position: 'absolute', top: -7, right: 0, zIndex: 1}} transparent
                        onPress={() => {
                            this.myModal.close()
                        }}>
                    <Icon type={'AntDesign'} name='closesquareo'
                          style={{marginRight: 5, backgroundColor: '#fff',}}/>
                </Button>
                {loadingDetaiJob ? <ActivityIndicator size="large" color="#ccc"/> :
                    this.props.jobContent.itemsDetaiJob ?
                        <ScrollView style={{marginTop: 5}}>
                            <Header style={{backgroundColor: '#fff',paddingBottom:10,height:'auto'}}>
                                <Left>
                                    <Image source={{uri: `${this.props.jobContent.itemsDetaiJob.logo}`}}
                                           style={{
                                               height: 70, width: 90
                                           }}/>
                                </Left>
                                <Body>


                                    <Text style={{
                                        fontSize: 14,
                                        fontWeight: '400',
                                        marginTop: 10
                                    }}>{this.props.jobContent.itemsDetaiJob.job_title}</Text>
                                    <View style={{ flex:1,flexDirection:'row',marginTop: 5}}>
                                        <Text note style={{color: '#545454',flex:7}}> <Icon
                                            type={'AntDesign'}
                                            name={'clockcircleo'}
                                            style={{fontSize: 16, color: 'gray',marginTop: 4}}
                                        /> {this.props.jobContent.itemsDetaiJob.ended_at}
                                        </Text>

                                        {this.props.jobContent.itemsDetaiJob.contact_phone ?


                                            <TouchableOpacity style={{flex:1}}
                                            onPress={() => {
                                                Linking.openURL(`tel:${this.props.jobContent.itemsDetaiJob.contact_phone}`)
                                            }}>
                                                <Icon
                                                    type={'AntDesign'}
                                                    name={'phone'}
                                                    style={{fontSize: 18, color: 'gray'}}
                                                />
                                            </TouchableOpacity>
                                            : null}
                                        {this.props.jobContent.itemsDetaiJob.contact_email ?
                                            <TouchableOpacity style={{flex:1}}
                                                onPress={() => {
                                                    const to = [`${this.props.jobContent.itemsDetaiJob.contact_email}`] // string or array of email addresses
                                                    email(to, {
                                                        // Optional additional arguments
                                                        // cc: ['bazzy@moo.com', 'doooo@daaa.com'], // string or array of email addresses
                                                        // bcc: 'mee@mee.com', // string or array of email addresses
                                                        // subject: 'Show how to use',
                                                        // body: 'Some body right here'
                                                    }).catch(console.error)
                                                }}>
                                                <Icon
                                                    type={'AntDesign'}
                                                    name={'mail'}
                                                    style={{fontSize: 20, color: 'gray'}}
                                                />
                                            </TouchableOpacity>

                                            : null}

                                    </View>

                                </Body>
                            </Header>

                            {this.props.jobContent.itemsDetaiJob.video_id ?
                                <View><YouTube
                                    apiKey='AIzaSyCSi4zmrk7iusmID8zBNugozgT8UCfIXgI'
                                    videoId={this.props.jobContent.itemsDetaiJob.video_id}   // The YouTube video ID
                                    play={true}             // control playback of video with true/false
                                    fullscreen={false}       // control whether the video should play in fullscreen or inline
                                    loop={true}             // control whether the video should loop when ended
                                    style={{alignSelf: 'stretch', height: 250}}
                                /></View> : null}
                            <Text style={styles.textHeader}>Thông tin chi tiết</Text>

                            <View>
                                {this.props.jobContent.itemsDetaiJob.ended_at ?
                                    <Text style={styles.textTitle}><Icon
                                        type={'AntDesign'}
                                        name={'dashboard'}
                                        style={{fontSize: 16, color: 'gray',marginRight: 5}}
                                    /> <Text>Hạn nộp hồ sơ : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.ended_at !== '00/00/0000' ? this.props.jobContent.itemsDetaiJob.ended_at : 'Luôn tuyển'}</Text></Text>
                                    </Text>
                                    : null}
                            </View>
                            <View>

                                {this.props.jobContent.itemsDetaiJob.gender ?
                                    <Text style={styles.textTitle}><Icon
                                        type={'AntDesign'}
                                        name={'user'}
                                        style={{fontSize: 16, color: 'gray',marginRight: 5}}
                                    /> <Text>Giới tính : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.gender}</Text></Text>
                                    </Text>
                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.job_experience ?

                                    <Text style={styles.textTitle}><Icon
                                        type={'AntDesign'}
                                        name={'solution1'}
                                        style={{fontSize: 16, color: 'gray',marginRight: 5}}
                                    /> <Text>Kinh nghiệm : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.job_experience}</Text></Text>
                                    </Text>
                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.job_level ?
                                    <Text style={styles.textTitle}><Icon
                                        type={'AntDesign'}
                                        name={'barchart'}
                                        style={{fontSize: 16, color: 'gray',marginRight: 5}}
                                    /> <Text>Trình độ : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.job_level}</Text></Text>
                                    </Text>
                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.job_type ?
                                    <Text style={styles.textTitle}><Icon
                                        type={'AntDesign'}
                                        name={'clockcircleo'}
                                        style={{fontSize: 16, color: 'gray',marginRight: 5}}
                                    /> <Text>Thời gian làm việc : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.job_type}</Text></Text>
                                    </Text>

                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.number_of_recruitment ?
                                    <Text style={styles.textTitle}><Icon
                                        type={'AntDesign'}
                                        name={'addusergroup'}
                                        style={{fontSize: 16, color: 'gray',marginRight: 5}}
                                    /> <Text>Số lượng tuyển dụng : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.number_of_recruitment}</Text></Text>
                                    </Text>

                                    : null}
                            </View>

                            <View>
                                {this.props.jobContent.itemsDetaiJob.job_description ?
                                    <View>
                                        <Text style={styles.textHeader}>Mô tả công việc</Text>
                                        <Text
                                            style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.job_description}</Text>
                                    </View>
                                    : null
                                }
                            </View>


                            <View>
                                {this.props.jobContent.itemsDetaiJob.job_requirement ?
                                    <View>
                                        <Text style={styles.textHeader}>Yêu cầu ứng viên</Text>
                                        <Text
                                            style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.job_requirement}</Text>
                                    </View>
                                    : null}
                            </View>


                            <View>
                                {this.props.jobContent.itemsDetaiJob.job_benefit ?
                                    <View>
                                        <Text style={styles.textHeader}>Quyền lợi được hưởng</Text>
                                        <Text
                                            style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.job_benefit}</Text>
                                    </View>
                                    : null}
                            </View>



                            <Text style={styles.textHeader}>Liên hệ</Text>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.company_name ?
                                    <Text style={styles.textTitle}>Tên công ty : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.company_name}</Text></Text>
                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.contact_name ?
                                    <Text style={styles.textTitle}>Người liên hệ : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.contact_name}</Text></Text>
                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.contact_address ?
                                    <Text style={styles.textTitle}>Địa chỉ : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.contact_address}</Text></Text>
                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.contact_email ?
                                    <Text style={styles.textTitle}>Email :<Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.contact_email}</Text></Text>
                                    : null}
                            </View>
                            <View>
                                {this.props.jobContent.itemsDetaiJob.contact_phone ?
                                    <Text style={styles.textTitle}>Điện thoại : <Text
                                        style={styles.textContent}>{this.props.jobContent.itemsDetaiJob.contact_phone}</Text></Text>
                                    : null}
                            </View>


                        </ScrollView>
                        : <View/>

                }


            </Modal>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        jobContent: state.jobDetailReducers,
    }
};

const ItemDetailModal = connect(mapStateToProps, null, null, { forwardRef: true })(ItemDetail);

export default ItemDetailModal
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    item: {
        padding: 10,
    },

    textContent: {
        fontSize: 15,
        color: 'black',
    },
    textTitle: {
        fontSize: 15,
        color: 'green',
    },
    textHeader: {
        fontSize: 15,
        fontWeight: '500',
        marginTop: 15,
        marginBottom: 5,

    },

    modal: {
        // justifyContent: 'center',
        // alignItems: 'center'
        padding: 15
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 300
    },


});
