import React, {Component} from "react";
import {Image, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text, Thumbnail} from "native-base";
import {JobDetailAction, SaveJobAction, CompanyDetailAction, PostEmployerDetailAction} from "../../../actions";
import {connect} from "react-redux";
import Share from "react-native-share";
import {JobDetail, PostDetail, PostDetailEmployer} from "../../../vendor/screen";

class FlatListItem extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <TouchableOpacity onPress={
                () => {
                    this.props.handleChange();
                    this.props.PostDetail(this.props.item.id);
                    this.props.navigation.navigate(PostDetailEmployer, {
                        provine: this.props.item.provine_name,
                        youtubeId: this.props.item.video_youtube_id,
                        approve:this.props.item.approve,
                        nameScreen: 'Post'
                    });
                }
            }>
            <CardItem
                style={{
                    backgroundColor: 'white',
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    marginRight: 15,
                    marginLeft: 15,
                    borderBottom: 0
                }}>
                <Left>

                    <Image
                        resizeMode={'contain'}
                        source={{uri: `${this.props.item.logo}`}}
                        style={{height: 100, width: 100}}
                    />
                    <Body>
                            <Text numberOfLines={2} style={{fontSize: 13, color: '#474747', fontWeight: 'bold'}}>
                                {this.props.item.job_title.toUpperCase()}
                            </Text>
                        <Text style={{fontSize: 10, marginTop: 3, marginBottom: 3}}>
                            {this.props.item.company_name}
                        </Text>

                        <View style={{flexDirection: 'row', marginBottom: 7}}>
                            <Left>
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('../../../vendor/images/Shape.png')}
                                           style={{marginTop: 4, marginRight: 5}}/>
                                    <Text style={{
                                        color: '#33B8E0',
                                        fontSize: 12,
                                        fontWeight: 'normal'
                                    }}>{this.props.item.provine_name}</Text>
                                </View>
                            </Left>
                            <Right style={{left: 3}}>
                                <View style={{
                                    height: 16,
                                    width: 76,
                                    backgroundColor: this.props.item.approve === "approve" ? "#EB5757" : (this.props.item.approve === "yes" ? "#27AE60" : "#EB5757"),
                                    borderRadius: 20
                                }}>
                                    <Text style={{fontSize: 10, color: '#FFFFFF', alignSelf: 'center'}}>
                                        {this.props.item.approve === "approve" ? "Chờ duyệt" : (this.props.item.approve === "yes" ? "Đang chạy" : "Tạm ngừng")}</Text>
                                </View>
                            </Right>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <Left>
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('../../../vendor/images/money.png')}
                                           style={{marginTop: 4, marginRight: 5}}/>
                                    <Text style={{color: '#33B8E0', fontSize: 11, fontWeight: '500'}}>
                                        {this.props.item.job_salary}
                                    </Text>
                                </View>
                            </Left>
                            <Right>
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('../../../vendor/images/clock.png')}
                                           style={{marginTop: 4, marginRight: 5}}/>
                                    <Text style={{color: '#33B8E0', fontSize: 11, fontWeight: '500'}}>
                                        {this.props.item.ended_at}
                                    </Text>
                                </View>
                            </Right>

                        </View>

                    </Body>

                </Left>
            </CardItem>
            </TouchableOpacity>

        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         jobContent: state.jobDetailReducers,
//     }
// };
const mapDispatchToProps = (dispatch) => {
    return {


        PostDetail: (idPost) => {
            dispatch(PostEmployerDetailAction(idPost));

        },

    };
}
const FlatListItemPost = connect(null, mapDispatchToProps)(FlatListItem);

export default FlatListItemPost
