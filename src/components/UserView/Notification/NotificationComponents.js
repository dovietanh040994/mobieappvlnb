import React, { Component } from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import {Body, CardItem, Container, Left,Thumbnail} from "native-base";
import AsyncStorage from "@react-native-community/async-storage";
import FlatListItemJob from "../ChildComponent/FlatListItemJob";
import {withNavigation} from "react-navigation";
import {GoogleSignin} from "react-native-google-signin";
import {JobDetail, JobDetailNotify} from "../../../vendor/screen";
import {CompanyDetailAction, JobDetailAction, SaveJobAction} from "../../../actions";
import {connect} from "react-redux";
import PlaceholderLoadingNoti from "../../Helpers/PlaceholderLoadingNoti";

const HEADER_MAX_HEIGHT = 159;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


 class Notification extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                 0,
            ),
            refreshing: false,
            userInfo:null,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            notData: false,

        };
        this.page = 1;
        // this.random = Array(1,2,3,4)

    }

     componentDidMount() {
        const {navigation} = this.props;
         AsyncStorage.getItem('userInfo',(err, result) =>{
             console.log('kkkkkk');

             this.setState({
                 userInfo:result
             },()=>{
                 if( this.state.userInfo !== null ){
                     this.setState({
                         isListEnd: false,
                         serverData: [],
                         fetching_from_server: false
                     }, () => {
                         this.page = 1
                         this.loadMoreData()

                     });
                 }
             })
         })

        this.focusListener = navigation.addListener("didFocus", () => {

            AsyncStorage.getItem('notificationReload', (err1, result1) => {
                if(result1){
                    AsyncStorage.getItem('userInfo',(err, result) =>{
                        this.setState({
                            userInfo:result
                        },()=>{
                            if( this.state.userInfo !== null ){
                                this.setState({
                                    isListEnd: false,
                                    serverData: [],
                                    fetching_from_server: false
                                }, () => {
                                    this.page = 1
                                    this.loadMoreData()

                                });
                            }
                        })
                    })
                    AsyncStorage.removeItem('notificationReload');

                }

            });

        });
    }

     componentWillUnmount() {
         // Remove the event listener
         this.focusListener.remove();
     }


    loadMoreData = async () => {
        console.log('hahaha123123')


        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})


            const Url = `http://vieclamnambo.vn:9002/api/vlnb/user/getnotifications?rows_start=${this.page}`;
            console.log('asdasd', this.state.userInfo)
            console.log('asdasd', Url)

            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo

                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {
                    console.log('asdasd',responseJson)
                    if (responseJson.total === 0){
                        this.setState({
                            notData: true
                        });
                    }
                    if (responseJson.notifications.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.notifications],
                            fetching_from_server: false,
                            notData: false
                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

     renderFooter() {
         return (

             <View style={styles.footer}>
                 {this.state.fetching_from_server ? (  this.page === 1?
                         <PlaceholderLoadingNoti/>
                         :
                         <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                 ) : null}
             </View>
         );
     }

     randomImageProp(index){
         if(index === 0){
             return 0
         }else if (index === 1){
             return 1
         }else if (index === 2){
             return 2
         }else if (index === 3){
             return 3
         }
     }
    render() {
        console.log(this.state.notData, this.state.userInfo)

        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 1],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -55],
            extrapolate: 'clamp',
        });
console.log(this.state.serverData)
        return (
            <View style={styles.fill}>

                {  this.state.userInfo === null  ?
                    <View>
                        <View style={{height:130}}/>
                        <View style={{alignItems: 'center',marginTop:60}}>
                            <Image source={require('../../../vendor/images/notify.png')}  />
                            <Text style={{color:'#8C8888',fontWeight:'500',fontSize:16,marginTop:30}}>Đăng nhập để nhận thông báo</Text>
                        </View>
                    </View>
                : this.state.notData !== false ?
                    <View>
                        <View style={{height:130}}/>
                        <View style={{alignItems: 'center',marginTop:60}}>
                            <Image source={require('../../../vendor/images/notify.png')}  />
                            <Text style={{color:'#8C8888',fontWeight:'500',fontSize:16,marginTop:30}}>Bạn chưa có thông báo nào</Text>
                        </View>
                    </View>
                    :
                    <Animated.FlatList
                        style={[styles.fill]}
                        // data={this.props.salary.items.jobPosts}
                        onScroll={Animated.event(
                            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                            {useNativeDriver: true},
                        )}
                        data={this.state.serverData}
                        onEndReached={() => this.loadMoreData()}
                        onEndReachedThreshold={0.5}
                        ListHeaderComponent={<View style={{height: HEADER_MAX_HEIGHT}}/>}
                        renderItem={({item, index}) => {
                            // this.ramdomImage()
                            return (

                                    <TouchableOpacity onPress={
                                        () => {
                                            if (item.type === 2){
                                                // this.props.navigation.navigate(JobDetailNotify, {
                                                //     'body': JSON.parse(item.data),
                                                // });
                                                const data = JSON.parse(item.data)
                                                this.props.jobDetail(data.id);
                                                this.props.componyDetail(data.id);

                                                this.props.navigation.navigate(JobDetail, {
                                                    provine: data.provine_name,
                                                    nameScreen: 'Notify',
                                                    saveHeart: data.saved,
                                                    applied: data.applied,
                                                    randomImageProp: this.randomImageProp(index % 4)

                                                });
                                            }else if (item.type === 1){
                                                this.props.navigation.navigate('ShowProfile')
                                            }else if (item.type === 3){

                                            }
                                        }
                                    }>
                                        <CardItem
                                            style={{
                                                backgroundColor: 'white',
                                                borderTopRightRadius: 5,
                                                borderTopLeftRadius: 5,
                                                borderBottomLeftRadius: 5,
                                                borderBottomRightRadius: 5,
                                                marginRight:15,
                                                marginLeft:15,
                                                borderBottom:0,
                                                paddingLeft:0,
                                                paddingRight:0,
                                                paddingTop:0,
                                                paddingBottom:0,
                                                marginTop: 15
                                            }}>
                                            <Left>
                                                {index % 4 === 0 && (
                                                     <Thumbnail  style={{width: 68,height:68,borderRadius:68}} source={require(`../../../vendor/images/VLNB1.png`)}   />
                                                )}
                                                {index % 4 === 1 && (
                                                    <Thumbnail  style={{width: 68,height:68,borderRadius:68}} source={require(`../../../vendor/images/VLNB2.png`)}   />
                                                )}
                                                {index % 4 === 2 && (
                                                    <Thumbnail  style={{width: 68,height:68,borderRadius:68}} source={require(`../../../vendor/images/VLNB3.png`)}   />
                                                )}
                                                {index % 4 === 3 && (
                                                    <Thumbnail  style={{width: 68,height:68,borderRadius:68}} source={require(`../../../vendor/images/VLNB4.png`)}   />
                                                )}
                                                <Body style={{flex:1,alignSelf:'flex-start',padding:10,paddingLeft:5}}>
                                                    <Text numberOfLines={2} style={{fontSize:14,color:'#525252',fontWeight:'normal'}}>{item.body}</Text>
                                                    <View style={{flexDirection:'row',marginTop:6}}>
                                                        <Image style={{marginTop:4,marginRight:5}} source={require(`../../../vendor/images/time1.png`)}   />
                                                        <Text style={{color:'#33b8e0'}}>{item.send_time}</Text>
                                                    </View>

                                                </Body>
                                            </Left>
                                        </CardItem>
                                    </TouchableOpacity>
                            );
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => <View style={styles.separator}/>}
                        ListFooterComponent={this.renderFooter.bind(this)}

                    />

                }
                <Animated.View
                    pointerEvents="none"
                    style={[
                        styles.header,
                        { transform: [{ translateY: headerTranslate }] },
                    ]}
                >
                    <Animated.Image
                        style={[
                            styles.backgroundImage,
                            {
                                opacity: imageOpacity,
                                transform: [{ translateY: imageTranslate }],
                            },
                        ]}
                        source={require('../../../vendor/images/Noti.png')}
                    />
                </Animated.View>
                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                { scale: titleScale },
                                { translateY: titleTranslate },
                            ],
                        },
                    ]}
                >
                    <Text style={{fontWeight:'900',color:'#fff',fontSize:25,alignSelf:'center',marginTop:50}}>THÔNG BÁO</Text>

                    {/*<Text style={styles.title}>Title</Text>*/}
                </Animated.View>
            </View>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {


        jobDetail: (idJob) => {
            dispatch(JobDetailAction(idJob));

        },
        componyDetail: (idJob) => {

            dispatch(CompanyDetailAction(idJob));
        },

    };
}
const NotificationComponents = connect(null, mapDispatchToProps)(Notification);
export default withNavigation(NotificationComponents)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
