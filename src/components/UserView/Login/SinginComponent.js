import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions,
    TextInput, Animated, TouchableOpacity, ScrollView, BackHandler
} from 'react-native';
import firebase from 'react-native-firebase';

import { AccessToken, LoginManager, LoginButton } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';

import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem, Toast, Input
} from "native-base";
import {StylesAll} from '../../../vendor/styles'

//screen
import {Home, HomeBox, Login, SearchAdvanced} from '../../../vendor/screen'
import {connect} from "react-redux";
import {LoginAction, SinginAction, VerifyAction} from '../../../actions'
import {getStatusBarHeight} from "react-native-status-bar-height";
import {Field, reduxForm, submit} from "redux-form";
import {LOGINFORM, SELECTINFO, SINGINFORM} from "../../../vendor/formNames";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../../vendor/NavigationService";
import BtnSingin from "./BtnSingin";


const submitForm = (values, dispatch) => {
    const errors = {};
    if (!values.phone || values.phone.trim().length <= 0) {
        errors.phone = 'Số điện thoại không được để trống';
        Toast.show({
            text: errors.phone,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (isNaN(Number(values.phone))) {

        errors.phone = 'Số điện thoại phải là số';
        Toast.show({
            text: errors.phone,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (values.phone.trim().slice(0,1) !== '0' || values.phone.trim().length < 10 ) {

        errors.phone = 'Số điện thoại không hợp lệ';
        Toast.show({
            text: errors.phone,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (!values.password || values.password.trim().length <= 0) {

        errors.password = 'Mật khẩu không được để trống';
        Toast.show({
            text: errors.password,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (!values.rePassword || values.rePassword.trim().length <= 0) {

        errors.rePassword = 'Mật khẩu nhập lại không được để trống';
        Toast.show({
            text: errors.rePassword,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (values.password !== values.rePassword) {

        errors.rePassword = 'Mật khẩu nhập lại không trùng khớp';
        Toast.show({
            text: errors.rePassword,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else {

        dispatch(SinginAction(values))

    }



    // dispatch(SearchFormAction(values));




};
class Singin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            phoneNumber:'0972215031',
            // secureTextEntry:true
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    static navigationOptions = () => ({
        header: null
    });

    componentWillMount() {
        BackHandler.addEventListener('Singin', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('Singin', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(Login)
        return true;

    }


    renderPickerPhone = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:15}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={secureTextEntry}
                    value={`${value}`}

                />


            </View>
        );
    }
    renderPickerPass = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: { onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:50}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={true}
                    ref={input => { this.textInputPass = input }}

                />
                    <TouchableOpacity style={{position:'absolute',top:0,right:0,width:50,height:50,flexDirection:'row',justifyContent:'center'}}
                      onPress={() => {
                          this.textInputPass.clear()
                      }}
                    >
                        <Icon type={'AntDesign'} name={'closesquareo'} style={[{fontSize:16,alignSelf:'center',color:'#525252'}]}/>

                        {/*<Image source={require('../../vendor/images/eya.png')} style={{alignSelf:'center'}} />*/}
                    </TouchableOpacity>

                {/*{touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||*/}
                {/*    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}*/}


            </View>
        );
    }

    renderPickerRePass = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: { onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:50}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={true}
                    ref={input => { this.textInputRePass = input }}

                />
                <TouchableOpacity style={{position:'absolute',top:0,right:0,width:50,height:50,flexDirection:'row',justifyContent:'center'}}
                                  onPress={() => {
                                      this.textInputRePass.clear()
                                  }}
                >
                    <Icon type={'AntDesign'} name={'closesquareo'} style={[{fontSize:16,alignSelf:'center',color:'#525252'}]}/>

                    {/*<Image source={require('../../vendor/images/eya.png')} style={{alignSelf:'center'}} />*/}
                </TouchableOpacity>

                {/*{touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||*/}
                {/*    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}*/}


            </View>
        );
    }



    render() {
        const {navigate} = this.props.navigation;
        return (
            // style={{marginTop: getStatusBarHeight(true)}}
            <ScrollView >
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 1,
                    borderBottomColor: '#ccc',
                    elevation: 0
                }}>
                    <Left>
                        <TouchableOpacity style={{width:50,height:30,position:'absolute',left:-20,top:-13}} transparent onPress={() => {
                            this.props.navigation.navigate(Login)
                        }}>
                            <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                                tintColor: '#33B8E0', marginTop: 5,marginLeft:23
                            }}/>
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Quay lại</Title>
                    </Body>
                </Header>
                <Content  contentContainerStyle={{flex: 1, marginTop: 30,paddingLeft:20,paddingRight:20}}>
                    <Image
                        resizeMode={'contain'} source={require('../../../vendor/images/VLNB.png')} style={[{tintColor: '#33B8E0',width: 121,
                        height: 76,}]}
                    />
                    <View>
                        <Text style={{fontSize:22,color: '#33B8E0',marginTop:20,marginBottom: 15,fontWeight:'500'}}>Đăng ký</Text>
                    </View>
                    <View>
                        <Text style={{fontSize:15,color: '#3f3356',marginTop:15,marginBottom: 15,fontWeight:'normal'}}>Đăng nhập để ứng tuyển và trải nhiệm dịch vụ một cách hoàn hảo</Text>
                    </View>
                    <Field   name="phone" component={this.renderPickerPhone} keyboardType={'numeric'}  placeholder={'Số điện thoại'} />
                    <Field   name="password" component={this.renderPickerPass}  placeholder={'Mật khẩu'} />
                    <Field   name="rePassword" component={this.renderPickerRePass}  placeholder={'Nhập lại mật khẩu'} />

                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:1}}>
                            <BtnSingin/>
                        </View>
                    </View>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',alignSelf:'center',height:30,marginTop:15,marginBottom:15}}
                                      onPress={() => {
                                          this.props.navigation.navigate(Login)
                                      }}
                    >
                        <Text style={{fontSize:13,color:'#3f3356'}}>Bạn đã có tài khoản?</Text>
                        <Text style={{fontSize:13,color:'#33B8E0'}}> Đăng nhập</Text>
                    </TouchableOpacity>
                    {/*<Button rounded info style={StylesAll.loginButtonNext}*/}
                    {/*onPress={() => {*/}
                    {/*navigate(Home);*/}
                    {/*}}*/}
                    {/*>*/}
                    {/*<Text style={StylesAll.loginNextText}>Bỏ qua</Text>*/}
                    {/*</Button>*/}
                </Content>


            </ScrollView>

        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         singin: state.singinReducers,
//     }
// };

const mapDispatchToProps = (dispatch) => {
    return {
        // clickSingin: (newUser) => {
        //
        //     dispatch(SinginAction(newUser));
        // },
    };
}
const SinginComponent = connect(null,mapDispatchToProps)(Singin);
export default reduxForm({
    form: SINGINFORM, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    // validate,
    // warn,
    onSubmit: submitForm
})(SinginComponent)
