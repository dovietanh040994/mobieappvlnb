import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions,
    TextInput, Animated, TouchableOpacity, ScrollView, BackHandler
} from 'react-native';
import firebase from 'react-native-firebase';

import { AccessToken, LoginManager, LoginButton } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';

import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem, Toast, Input
} from "native-base";
import {StylesAll} from '../../../vendor/styles'

//screen
import {Home, HomeBox, SearchAdvanced, Singin, ResetPass} from '../../../vendor/screen'
import {connect} from "react-redux";
import {LoginAction} from '../../../actions'
import {getStatusBarHeight} from "react-native-status-bar-height";
import {Field, reduxForm, submit} from "redux-form";
import {LOGINFORM, SELECTINFO} from "../../../vendor/formNames";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../../vendor/NavigationService";
import BtnLogin from "./BtnLogin";


const submitForm = (values, dispatch) => {
    const errors = {};
    if (!values.phone || values.phone.trim().length <= 0) {
        errors.phone = 'Số điện thoại không được để trống';
        Toast.show({
            text: errors.phone,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (!values.password || values.password.trim().length <= 0) {

        errors.password = 'Mật khẩu không được để trống';
        Toast.show({
            text: errors.password,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else {

        dispatch(LoginAction(values,3))

    }



    // dispatch(SearchFormAction(values));




};
class Login extends Component {
    constructor(props) {
        super(props);
        this.unsubscriber = null;
        this.state = {
            user: null,
            checkLogin:true,
            secureTextEntry:true
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    static navigationOptions = () => ({
        header: null
    });

    componentDidMount() {
        this.unsubscriber = firebase.auth().onAuthStateChanged((changedUser) => {
            // console.log(`changed User : ${JSON.stringify(changedUser.toJSON())}`);
            console.log('LoginComponent')
            console.log(changedUser)
            this.setState({ user:  changedUser ? changedUser: null });
        });
        GoogleSignin.configure({
            webClientId: '188924080895-jihd852be68j03sdt4gdlop9qrm4572g.apps.googleusercontent.com',
        })
        // .then(() => {
        //     // you can now call currentUserAsync()
        // });
    }

    componentWillMount() {
        BackHandler.addEventListener('Login', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('Login', this.handleBackButtonClick);

        if (this.unsubscriber) {
            this.unsubscriber();
        }
    }

    handleBackButtonClick() {
        this.navigatePage()
        return true;

    }

    navigatePage = () => {
        const { navigation } = this.props;
        AsyncStorage.getItem('screen', (err, result) => {
            if(result === 'jobDetail'){
                navigation.popToTop()
                navigation.navigate(HomeBox)
                AsyncStorage.removeItem('screen');
            }else{
                navigation.navigate('ShowProfile')
            }

        });
    }
    // onLoginFacebook = () => {
    //     console.warn('aaaa')
    //     this.setState({checkLogin:true})
    //     LoginManager
    //         .logInWithReadPermissions(['public_profile', 'email'])
    //         .then((result) => {
    //             console.log(1);
    //             console.log(result);
    //             if (result.isCancelled) {
    //                 return Promise.reject(new Error('The user cancelled the request'));
    //             }
    //             // console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
    //             // get the access token
    //             return AccessToken.getCurrentAccessToken();
    //         })
    //         .then(data => {
    //             console.log(333)
    //             console.log(data)
    //             AsyncStorage.setItem('fcmToken', JSON.stringify(data.accessToken));
    //             const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
    //             return firebase.auth().signInWithCredential(credential);
    //         })
    //         .then((currentUser) => {
    //             console.log(1123123)
    //             console.log(this.state.user)
    //
    //             this.props.clickLogin(this.state.user);
    //             this.props.navigation.navigate(Home)
    //             console.log(`Facebook Login with user : ${JSON.stringify(currentUser)}`);
    //             // alert(`Facebook Login with user : ${JSON.stringify(currentUser)}`);
    //         })
    //         .catch((error) => {
    //             // alert(`Facebook login fail with error: ${error}`)
    //             this.setState({checkLogin:false})
    //             console.log(`Facebook login fail with error: ${error}`);
    //         });
    // }

    onLoginGoogle = () => {
        this.setState({checkLogin:true})
        GoogleSignin
            .signIn()
            .then((data) => {

                AsyncStorage.setItem('accessToken', JSON.stringify(data.idToken));
                // create a new firebase credential with the token
                console.log(123123)
                console.log(data.idToken, data.accessToken)
                const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
                console.log(12312323)
                console.log(credential)
                // login with credential
                return firebase.auth().signInWithCredential(credential);
            })
            .then((currentUser) => {
                // console.log(`${JSON.stringify(currentUser)}`);
                this.props.clickLogin(this.state.user,2);

                // this.props.navigation.state.params.onGoBack();
                // this.props.navigation.navigate('ShowProfile')

                // alert(`Google Login with user : ${JSON.stringify(currentUser)}`);
            })
            .catch((error) => {
                // alert(`Google login fail with error: ${error}`)
                this.setState({checkLogin:false})
                console.log(`Login fail with error: ${error}`);
            });
    }

    renderPickerPhone = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:15}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={secureTextEntry}
                    value={`${value}`}

                />


            </View>
        );
    }
    renderPickerPass = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: { onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:50}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={secureTextEntry}

                />
                    <TouchableOpacity style={{position:'absolute',top:0,right:0,width:50,height:50,flexDirection:'row',justifyContent:'center'}}
                      onPress={() => {
                          this.setState({
                              secureTextEntry:!secureTextEntry
                          })
                      }}
                    >
                        <Image source={require('../../../vendor/images/eya.png')} style={{alignSelf:'center'}} />
                    </TouchableOpacity>

                {/*{touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||*/}
                {/*    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}*/}


            </View>
        );
    }
    render() {

        return (
            // style={{marginTop: getStatusBarHeight(true)}}
            <ScrollView >
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 1,
                    borderBottomColor: '#ccc',
                    elevation: 0
                }}>
                    <Left>
                        <TouchableOpacity style={{width:50,height:30,position:'absolute',left:-20,top:-13}} transparent
                                          onPress={() => {
                                              this.navigatePage()
                                          }}
                        >
                            <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                                tintColor: '#33B8E0', marginTop: 5,marginLeft:23
                            }}/>
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Quay lại</Title>
                    </Body>
                </Header>
                <Content  contentContainerStyle={{flex: 1, marginTop: 30,paddingLeft:20,paddingRight:20}}>
                    <Image
                        resizeMode={'contain'} source={require('../../../vendor/images/VLNB.png')} style={[{tintColor: '#33B8E0',width: 121,
                        height: 76,}]}
                    />
                    <View>
                        <Text style={{fontSize:22,color: '#33B8E0',marginTop:20,marginBottom: 15,fontWeight:'500'}}>Đăng nhập</Text>
                    </View>
                    <View>
                        <Text style={{fontSize:15,color: '#3f3356',marginTop:15,marginBottom: 25,fontWeight:'normal'}}>Đăng nhập để ứng tuyển và trải nhiệm dịch vụ một cách tốt nhất</Text>
                    </View>
                    <Field   name="phone" component={this.renderPickerPhone} keyboardType={'numeric'}  placeholder={'Số điện thoại'} >
                    </Field>
                    <Field   name="password" component={this.renderPickerPass}  placeholder={'Mật khẩu'}  secureTextEntry={this.state.secureTextEntry}>

                    </Field>
                    <TouchableOpacity style={{height:30}}
                      onPress={() => {
                          this.props.navigation.navigate(ResetPass)
                      }}
                    >
                        <Text style={{fontSize:13,color:'#33B8E0'}}> Quên mật khẩu ?</Text>
                    </TouchableOpacity>

                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:1,paddingRight:8}}>
                            <BtnLogin/>
                        </View>
                        <View style={{flex:1,paddingLeft:8}}>
                            <TouchableOpacity   style={{borderRadius:5,backgroundColor:'#f5f1f1',padding:13}}
                                    onPress={this.onLoginGoogle}
                            >
                                <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center'}}>
                                    <Icon type={"AntDesign"} name='google' style={{color:'#dd5246',fontSize:20,marginRight:5}}/>
                                    <Text style={{fontSize:17,color:'#dd5246',fontWeight:'500'}}>Google</Text>
                                </View>

                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',alignSelf:'center',height:30,marginTop:25,marginBottom:15}}
                                      onPress={() => {
                                          this.props.navigation.navigate(Singin)
                                      }}
                    >
                        <Text style={{fontSize:13,color:'#3f3356'}}>Bạn chưa có tài khoản?</Text>
                        <Text style={{fontSize:13,color:'#33B8E0'}}> Đăng ký</Text>
                    </TouchableOpacity>
                    {/*<Button rounded info style={StylesAll.loginButtonNext}*/}
                    {/*onPress={() => {*/}
                    {/*navigate(Home);*/}
                    {/*}}*/}
                    {/*>*/}
                    {/*<Text style={StylesAll.loginNextText}>Bỏ qua</Text>*/}
                    {/*</Button>*/}
                </Content>
                <View style={{alignItems:'center'}}>
                    {this.state.checkLogin === false? <Text style={[StylesAll.footerText,{color:'red',marginTop: 0,marginBottom:10}]}>Đăng nhập không thành công</Text> :null}
                </View>

            </ScrollView>

        );
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        clickLogin: (newUser,typeLG) => {

            dispatch(LoginAction(newUser,typeLG));
        },
    };
}
const LoginComponent = connect(null,mapDispatchToProps)(Login);
export default reduxForm({
    form: LOGINFORM, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    // validate,
    // warn,
    onSubmit: submitForm
})(LoginComponent)
