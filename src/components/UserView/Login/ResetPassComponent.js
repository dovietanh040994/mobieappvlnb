import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions,
    TextInput, Animated, TouchableOpacity, ScrollView, BackHandler
} from 'react-native';
import firebase from 'react-native-firebase';

import { AccessToken, LoginManager, LoginButton } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';

import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem, Toast, Input
} from "native-base";
import {StylesAll} from '../../../vendor/styles'

//screen
import {Home, HomeBox, SearchAdvanced, Singin} from '../../../vendor/screen'
import {connect} from "react-redux";
import {LoginAction} from '../../../actions'
import {getStatusBarHeight} from "react-native-status-bar-height";
import {Field, reduxForm, submit} from "redux-form";
import {LOGINFORM, RESETPASS, SELECTINFO} from "../../../vendor/formNames";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../../vendor/NavigationService";
import BtnReset from "./BtnReset";


const submitForm = (values, dispatch) => {
    const errors = {};
    if (!values.phone || values.phone.trim().length <= 0) {
        errors.phone = 'Số điện thoại không được để trống';
        Toast.show({
            text: errors.phone,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (isNaN(Number(values.phone))) {

        errors.phone = 'Số điện thoại phải là số';
        Toast.show({
            text: errors.phone,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (values.phone.trim().slice(0,1) !== '0' || values.phone.trim().length < 10 ) {

        errors.phone = 'Số điện thoại không hợp lệ';
        Toast.show({
            text: errors.phone,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (!values.password || values.password.trim().length <= 0) {

        errors.password = 'Mật khẩu không được để trống';
        Toast.show({
            text: errors.password,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (!values.passwordConfirm || values.passwordConfirm.trim().length <= 0) {

        errors.passwordConfirm = 'Mật khẩu nhập lại không được để trống';
        Toast.show({
            text: errors.passwordConfirm,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else if (values.passwordConfirm !== values.password) {

        errors.passwordConfirm = 'Mật khẩu nhập lại không trùng khớp';
        Toast.show({
            text: errors.passwordConfirm,
            // buttonText: "Đóng",
            type: "warning",
            position: "top"
        })
    }else {
        const phoneConvert = '+84' + values.phone.trim().slice(1)
        console.log('values:',values)
        console.log('phoneConvert:',phoneConvert)
        // dispatch(LoginAction(values,3))
        firebase.auth().signInWithPhoneNumber(phoneConvert)
            .then(confirmResult => {

                    // console.log('asdsd',confirmResult)
                    NavigationService.navigate('VerifyResetPass',{'confirmResult':confirmResult,'password':values.password,'phone':values.phone})
                }
            )
            .catch(error => console.log(error.message));
    }



    // dispatch(SearchFormAction(values));




};
class ResetPass extends Component {
    constructor(props) {
        super(props);
        this.unsubscriber = null;
        this.state = {
            user: null,
            checkLogin:true,
            secureTextEntry:true

        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    static navigationOptions = () => ({
        header: null
    });

    componentDidMount() {
        this.unsubscriber = firebase.auth().onAuthStateChanged((changedUser) => {
            // console.log(`changed User : ${JSON.stringify(changedUser.toJSON())}`);
            console.log('LoginComponent')
            console.log(changedUser)
            this.setState({ user:  changedUser ? changedUser: null });
        });
        GoogleSignin.configure({
            webClientId: '188924080895-jihd852be68j03sdt4gdlop9qrm4572g.apps.googleusercontent.com',
        })
        // .then(() => {
        //     // you can now call currentUserAsync()
        // });
    }

    componentWillMount() {
        BackHandler.addEventListener('Resetpass', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('Resetpass', this.handleBackButtonClick);

        if (this.unsubscriber) {
            this.unsubscriber();
        }
    }

    handleBackButtonClick() {
        this.navigatePage()
        return true;

    }

    navigatePage = () => {
        const { navigation } = this.props;
        navigation.navigate('Login')
    }



    renderPickerPhone = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:15}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={secureTextEntry}
                    value={`${value}`}

                />


            </View>
        );
    }
    renderPickerPass = ({label,placeholder,secureTextEntry, keyboardType, meta: {touched, error, warning}, input: { onChange, ...restInput}}) => {

        return (
            <View style={{marginBottom:15}}>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:50}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    secureTextEntry={secureTextEntry}

                />
                {/*<TouchableOpacity style={{position:'absolute',top:0,right:0,width:50,height:50,flexDirection:'row',justifyContent:'center'}}*/}
                {/*                  onPress={() => {*/}
                {/*                      this.setState({*/}
                {/*                          secureTextEntry:!secureTextEntry*/}
                {/*                      })*/}
                {/*                  }}*/}
                {/*>*/}
                {/*    <Image source={require('../../../vendor/images/eya.png')} style={{alignSelf:'center'}} />*/}
                {/*</TouchableOpacity>*/}

                {/*{touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||*/}
                {/*    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}*/}


            </View>
        );
    }
    render() {

        return (
            // style={{marginTop: getStatusBarHeight(true)}}
            <ScrollView >
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 1,
                    borderBottomColor: '#ccc',
                    elevation: 0
                }}>
                    <Left>
                        <TouchableOpacity style={{width:50,height:30,position:'absolute',left:-20,top:-13}} transparent
                                          onPress={() => {
                                              this.navigatePage()
                                          }}
                        >
                            <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                                tintColor: '#33B8E0', marginTop: 5,marginLeft:23
                            }}/>
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Quay lại</Title>
                    </Body>
                </Header>
                <Content  contentContainerStyle={{flex: 1, marginTop: 30,paddingLeft:20,paddingRight:20}}>
                    <Image
                        resizeMode={'contain'} source={require('../../../vendor/images/VLNB.png')} style={[{tintColor: '#33B8E0',width: 121,
                        height: 76,}]}
                    />
                    <View>
                        <Text style={{fontSize:22,color: '#33B8E0',marginTop:20,marginBottom: 35,fontWeight:'500'}}>Thay đổi mật khẩu</Text>
                    </View>
                    <Field   name="phone" component={this.renderPickerPhone} keyboardType={'numeric'}  placeholder={'Số điện thoại'} >
                    </Field>
                    <Field   name="password" component={this.renderPickerPass}   placeholder={'Mật khẩu mới'} secureTextEntry={this.state.secureTextEntry} />
                    <Field   name="passwordConfirm" component={this.renderPickerPass}  placeholder={'Nhập lại mật khẩu mới'} secureTextEntry={this.state.secureTextEntry}/>

                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:1}}>
                            <BtnReset/>
                        </View>
                    </View>
                </Content>

            </ScrollView>

        );
    }
}




const ResetPassComponent = connect(null,null)(ResetPass);
export default reduxForm({
    form: RESETPASS, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    // validate,
    // warn,
    onSubmit: submitForm
})(ResetPassComponent)
