import React, { Component } from 'react';
import {
    AppRegistry, FlatList, StyleSheet, View, Image, Alert,
    Platform, TouchableHighlight, Dimensions,
    TextInput, Animated, TouchableOpacity, ScrollView, BackHandler
} from 'react-native';
import firebase from 'react-native-firebase';

import { AccessToken, LoginManager, LoginButton } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';

import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Card,
    CardItem, Toast, Input
} from "native-base";
import {StylesAll} from '../../../vendor/styles'

//screen
import {Home, HomeBox, Login, SearchAdvanced} from '../../../vendor/screen'
import {connect} from "react-redux";
import {LoginAction, ResetPass, VerifyAction} from '../../../actions'
import {getStatusBarHeight} from "react-native-status-bar-height";
import {Field, reduxForm, submit} from "redux-form";
import {LOGINFORM, SELECTINFO, SINGINFORM, VERIFYFORM} from "../../../vendor/formNames";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "../../../vendor/NavigationService";


class Verify extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            codeInput: '',
            confirmResult: this.props.navigation.getParam('confirmResult',null),
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    static navigationOptions = () => ({
        header: null
    });

    componentDidMount() {

        this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                console.log('user:',user)
                if(user){
                    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
                    sleep(1000).then(() => {
                        this.props.clickResetPass(this.props.navigation.getParam('phone',null),this.props.navigation.getParam('password',null))

                        this.setState({ user: user.toJSON() });

                    })

                }

            } else {
                // User has been signed out, reset the state
                this.setState({
                    user: null,
                    confirmResult: this.props.navigation.getParam('confirmResult',null),
                });
            }
        });

    }

    componentWillMount() {
        BackHandler.addEventListener('VerifyResetPass', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        if (this.unsubscribe) this.unsubscribe();

        BackHandler.removeEventListener('VerifyResetPass', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.navigatePage()
        return true;

    }

    navigatePage = () => {
        this.props.navigation.navigate('ResetPass');
    }

    confirmCode = () => {
        const { codeInput, confirmResult } = this.state;
        if (confirmResult && codeInput.length) {
            confirmResult.confirm(codeInput)
                .then((user) => {
                   console.log('Code Confirmed!',user)
                    if (user) {
                        console.log('user:',user)
                        if(user){
                           this.props.clickResetPass(this.props.navigation.getParam('phone',null),this.props.navigation.getParam('password',null))
                           this.setState({ user: user.toJSON() });
                        }

                    } else {
                        // User has been signed out, reset the state
                        this.setState({
                            user: null,
                            confirmResult: this.props.navigation.getParam('confirmResult',null),
                        });
                    }
                })
                .catch(error => console.log(`Code Confirm Error: ${error.message}`));
        }
    };

    renderVerificationCodeInput() {
        return (
            <ScrollView >
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 1,
                    borderBottomColor: '#ccc',
                    elevation: 0
                }}>
                    <Left>
                        <TouchableOpacity style={{width:50,height:30,position:'absolute',left:-20,top:-13}} transparent onPress={() => {
                            this.navigatePage()

                        }}>
                            <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                                tintColor: '#33B8E0', marginTop: 5,marginLeft:23
                            }}/>
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Quay lại</Title>
                    </Body>
                </Header>
                <Content  contentContainerStyle={{flex: 1, marginTop: 30,paddingLeft:20,paddingRight:20}}>

                    <View>
                        <Text style={{fontSize:22,color: '#33B8E0',marginTop:20,marginBottom: 15,fontWeight:'500'}}>Nhập mã bảo mật</Text>
                    </View>
                    <View>
                        <Text style={{fontSize:15,color: '#3f3356',marginTop:15,marginBottom: 15,fontWeight:'normal'}}>Chúng tôi đã gửi mã xác nhận vào số điện thoại {this.props.navigation.getParam('phone',null)}</Text>
                    </View>
                    <TextInput
                        underlineColorAndroid={'transparent'}
                        placeholder={'Mã bảo mật'}
                        style={{borderWidth:1,borderColor:'#ccc',borderRadius: 5,fontSize:15,paddingLeft:15,paddingRight:15,marginBottom:15,marginTop:15}}
                        keyboardType={'numeric'}
                        onChangeText={value => this.setState({ codeInput: value })}
                        secureTextEntry={true}
                        // editable={false}
                    />
                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:1}}>
                            <TouchableOpacity style={{borderRadius: 5, backgroundColor: '#33B8E0', padding: 13}}
                                              onPress={() => {
                                                    this.confirmCode()
                                                  // this.props.navigation.navigate(SearchDetail);
                                              }}
                            >
                                <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                    <Text style={{fontSize: 17, color: '#fff', fontWeight: '700'}}>Xác nhận mã</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Content>


            </ScrollView>
        );
    }

    signOut = () => {
        firebase.auth().signOut();
    }

    render() {
        // const { navigation } = this.props;
        //
        // console.log('phone',navigation.getParam('confirmResult',null))
        const { user, confirmResult } = this.state;
        return (
            <View>

                {/*{!user && !confirmResult && this.renderPhoneNumberInput()}*/}


                {!user && confirmResult && this.renderVerificationCodeInput()}

                {user && (
                    <ScrollView >
                        <Content  contentContainerStyle={{flex: 1, marginTop: 30,paddingLeft:20,paddingRight:20}}>

                            <View>
                                <View style={{alignItems: 'center',marginTop:100}}>
                                    <Image source={require('../../../vendor/images/Verify.png')}  />
                                    <Text style={{color:'#33b8e0',fontSize:22,fontWeight:'500',marginTop:30}}>Thành công</Text>
                                    {/*<Text style={{color:'#525252',fontWeight:'normal',fontSize:13,marginTop:10}}></Text>*/}
                                    <Text style={{color:'#525252',fontWeight:'normal',fontSize:13,marginTop:10}}>Mời bạn tiếp tục đăng nhập để sử dụng dịch vụ</Text>
                                </View>
                            </View>

                            <TouchableOpacity style={{borderRadius: 5, backgroundColor: '#33B8E0', padding: 13,marginTop:30}}
                                              onPress={() => {

                                                  this.props.navigation.navigate(Login)
                                              }}
                            >
                                <View style={{alignItems: 'center', alignSelf: 'center'}}>
                                    <Text style={{fontSize: 17, color: '#fff', fontWeight: '700'}}>Trải nghiệm ngay</Text>
                                </View>
                            </TouchableOpacity>

                        </Content>
                    </ScrollView>
                )}
            </View>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        singin: state.singinReducers,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {

        clickResetPass: (sdt,pass) => {

            dispatch(ResetPass(sdt,pass));
        },
    };
}
const VerifyComponent = connect(mapStateToProps,mapDispatchToProps)(Verify);
export default VerifyComponent
