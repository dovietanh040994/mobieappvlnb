import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, Dimensions, BackHandler,
} from 'react-native';
import {Body, CardItem, Container, Header, Left, Title} from "native-base";
import {Login, Posts} from "../../../vendor/screen";
import MyWebView from "react-native-webview-autoheight";
import firebase from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";

const HEADER_MAX_HEIGHT = 159;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;
export default class PostDetailComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    static navigationOptions = () => ({
        header: null
    });
    // _renderScrollViewContent() {
    //     const data = Array.from({ length: 30 });
    //     return (
    //         <View style={styles.scrollViewContent}>
    //             {data.map((_, i) => (
    //                 <View key={i} style={styles.row}>
    //                     <Text>{i}</Text>
    //                 </View>
    //             ))}
    //         </View>
    //     );
    // }

    componentWillMount() {
        BackHandler.addEventListener('PostDetail', this.handleBackButtonClick);
    }

    componentDidMount() {
        AsyncStorage.getItem('adsNew', (err, result) => {
            AsyncStorage.setItem('adsNew', (parseInt(result) + 1).toString());
            if (parseInt(result) === 2 || parseInt(result) === 7) {
                const unitId = 'ca-app-pub-3717610865114273/3047817089';
                const advert = firebase.admob().interstitial(unitId);
                const AdRequest = firebase.admob.AdRequest;
                const request = new AdRequest();
                advert.loadAd(request.build());
                advert.on('onAdLoaded', () => {
                    console.log('Advert ready to show.');
                    advert.show();
                });

            }
        });

    }

    componentWillUnmount() {
        BackHandler.removeEventListener('PostDetail', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(Posts)
        return true;

    }

    render() {
        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 1],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -55],
            extrapolate: 'clamp',
        });


        const {navigation} = this.props;
        const slug = navigation.getParam('slug', null);
        const thumb = navigation.getParam('thumb', null);
        console.log(slug)

        return (


            <View style={styles.fill}>

                <Animated.ScrollView
                    style={styles.fill}
                    scrollEventThrottle={1}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true},
                    )}

                >
                    <View style={{marginTop: HEADER_MAX_HEIGHT}}>
                        <View style={{flex: 1, flexDirection: 'column', backgroundColor: 'white'}}>
                            <MyWebView
                                //sets the activity indicator before loading content
                                startInLoadingState={true}
                                source={{uri: slug}}/>
                        </View>
                    </View>
                </Animated.ScrollView>

                <Animated.View
                    pointerEvents="none"
                    style={[
                        styles.header,
                        {transform: [{translateY: headerTranslate}]},
                    ]}
                >

                    <Animated.Image
                        style={[
                            styles.backgroundImage,
                            {
                                opacity: imageOpacity,
                                transform: [{translateY: imageTranslate}],
                            },
                        ]}
                        // source={require('../../vendor/images/Noti.png')}
                        source={{uri: `${thumb}`}}
                    />
                    {/*<Image  style={{height: 100, width: 100}} />*/}

                </Animated.View>
                <Animated.View
                    style={[
                        styles.bar,
                        // {
                        //     transform: [
                        //         { scale: titleScale },
                        //         { translateY: titleTranslate },
                        //     ],
                        // },
                    ]}
                >
                    <TouchableOpacity style={{width: 25}} transparent onPress={() => {
                        this.props.navigation.navigate(Posts)
                    }}>
                        <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                            tintColor: '#fff', marginTop: 2
                        }}/>
                    </TouchableOpacity>


                    {/*<Text style={{fontWeight:'900',color:'#fff',fontSize:25,alignSelf:'center',marginTop:50}}>THÔNG BÁO</Text>*/}

                    {/*<Text style={styles.title}>Title</Text>*/}
                </Animated.View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fill: {
        flex: 1,
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
        backgroundColor: '#03A9F4',
    },
    bar: {
        backgroundColor: 'transparent',
        // marginTop:10,
        height: 50,
        width: '100%',
        // alignItems: 'center',
        // justifyContent: 'center',
        position: 'absolute',
        paddingTop: 15,
        // top: 0,
        left: 0,
        paddingLeft: 20

        // right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
