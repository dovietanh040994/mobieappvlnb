import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import PostsComponent from "./PostsComponent";
import PostDetailComponent from "./PostDetailComponent";
import {Text} from "native-base";
import {Image} from "react-native";
import React from "react";
// Implementation of HomeScreen, OtherScreen, SignInScreen, AuthLoadingScreen
// goes here.



const PostsNavigator = createStackNavigator(
    {
        Posts: {
            screen:  PostsComponent,

        },
        PostDetail: {
            screen:  PostDetailComponent,

        },

    },{
        initialRouteName:'Posts'
    }

);

export default createAppContainer(PostsNavigator);
