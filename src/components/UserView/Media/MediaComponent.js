import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList, ActivityIndicator, ImageBackground, TouchableOpacity, Image, TextInput, ScrollView, Dimensions, Keyboard
} from 'react-native';
import FlatListItemPost from "../ChildComponent/FlatListItemPost";
import {Body, Button, CardItem, Container, Header, Item, Left, Picker} from "native-base";
import {connect} from "react-redux";

import categoryPostReducers from "../../../reducers/CategoryPostReduser";
import {CategoryPost, LoginAction} from "../../../actions";
import FlatListItemMedia from "./FlatListItemMedia";
import {Search} from "../../../vendor/screen";
import AsyncStorage from "@react-native-community/async-storage";
import PlaceholderLoadingPost from "../../Helpers/PlaceholderLoadingPost";
import firebase from "react-native-firebase";


const HEADER_MAX_HEIGHT = 159;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

class MediaComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        //hàm tạo ref
        this.detailModal = React.createRef();
        this.state = {
            scrollY: new Animated.Value(
                0,
            ),
            refreshing: false,
            bgButton: 0,
            idCategory: 1,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            notData: false,
            query: ''
        };
        this.page = 1;
        this.check1 = 0;
        this.check2 = 0;
    }

    static navigationOptions = () => ({
        header: null
    });

    componentDidMount() {
        this.loadMoreData('');

        AsyncStorage.getItem('adsMedia', (err, result) => {
            AsyncStorage.setItem('adsMedia', (parseInt(result) + 1).toString());
            if (parseInt(result) === 2 || parseInt(result) === 7) {
                const unitId = 'ca-app-pub-3717610865114273/3047817089';
                const advert = firebase.admob().interstitial(unitId);
                const AdRequest = firebase.admob.AdRequest;
                const request = new AdRequest();
                advert.loadAd(request.build());
                advert.on('onAdLoaded', () => {
                    console.log('Advert ready to show.');
                    advert.show();
                });

            }
        });
    }

    onChange = value => {
        this.check1 = this.check1 + 1
        sleep(500).then(() => {
            this.check2 = this.check2 + 1
            if (this.check1 === this.check2) {
                // if (value.trim() !== '') {
                    this.setState({
                        isListEnd: false,
                        serverData: [],
                        fetching_from_server: false,
                    });
                    this.page = 1;
                    this.loadMoreData(value.trim());

                // }
            }
        })
    };

    loadMoreData = async (value) => {
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})

            const Url = `http://vieclamnambo.vn:9002/api/vlnb/news/getmedias?key_search=${value}&row_start=${this.page}`;
console.log(Url);
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                },

            }).then((response) => response.json())
                .then((responseJson) => {
                    console.log(responseJson)
                    if (responseJson.total === 0) {
                        this.setState({
                            notData: true
                        });
                    }
                    if (responseJson.news.length > 0) {

                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.news],
                            fetching_from_server: false,
                            notData: false
                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    };

    renderFooter() {
        return (

            <View style={styles.footer}>
                {this.state.fetching_from_server ? (  this.page === 1?
                        <PlaceholderLoadingPost/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    render() {
        // console.log(this.props.categoryPost[0].id)

        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -80, -159],
            extrapolate: 'clamp',
        });
        return (

            <View style={styles.fill}>
                {/*<StatusBar*/}
                {/*    barStyle="light-content"*/}
                {/*    backgroundColor="rgba(0, 0, 0, 0.251)"*/}
                {/*/>*/}

                {
                    this.state.notData === false ?
                        <Animated.FlatList
                            style={styles.fill}
                            // data={this.props.salary.items.jobPosts}
                            onScroll={Animated.event(
                                [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                                {useNativeDriver: true},
                            )}
                            data={this.state.serverData}
                            onEndReached={() => this.loadMoreData(this.state.query)}
                            onEndReachedThreshold={0.5}
                            ListHeaderComponent={<View style={{height: 210}}/>}
                            renderItem={({item, index}) => {
                                return (
                                    <View style={{marginBottom: 15, marginTop: index === 0 ? 15 : 0}}>
                                        <FlatListItemMedia
                                            item={item}
                                            index={index}
                                            parentFlatList={this}
                                            navigation={this.props.navigation}
                                        >

                                        </FlatListItemMedia>
                                    </View>
                                );
                            }}
                            keyExtractor={(item, index) => index.toString()}
                            ItemSeparatorComponent={() => <View style={styles.separator}/>}
                            ListFooterComponent={this.renderFooter.bind(this)}
                        />
                        :
                        <View>
                            <View style={{height: 220}}/>
                            <View style={{alignItems: 'center', marginTop: 60}}>
                                <Image source={require('../../../vendor/images/Loading_Post.png')}/>
                                <Text style={{color: '#8C8888', fontWeight: '500', fontSize: 16, marginTop: 30}}>Chưa
                                    tìm thấy tin tức phù hợp</Text>
                            </View>
                        </View>

                }

                <Animated.View
                    // pointerEvents="none"
                    style={[
                        styles.header,
                        {transform: [{translateY: headerTranslate}]},
                    ]}
                >

                    <Animated.View style={[

                        {
                            opacity: imageOpacity,
                            transform: [{translateY: imageTranslate}],
                        },
                    ]}>
                        <Header style={{
                            backgroundColor: '#fff',
                            height: 159,
                            borderBottomWidth: 0.5,
                            borderColor: '#ccc',
                            paddingLeft: 0,
                            paddingRight: 0
                        }}>
                            <ImageBackground source={require('../../../vendor/images/media/banner.png')}
                                             style={{width: '100%', height: '100%'}}>
                                <Text style={{
                                    fontWeight: '900',
                                    color: '#fff',
                                    fontSize: 25,
                                    alignSelf: 'center',
                                    marginTop: 65
                                }}>MEDIA</Text>

                            </ImageBackground>

                        </Header>
                    </Animated.View>
                </Animated.View>

                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                // { scale: titleScale },
                                {translateY: titleTranslate},
                            ],
                        },
                    ]}
                >
                    <View style={{
                        flex: 1, flexDirection: 'row', marginTop: 12, paddingLeft: 15, paddingRight: 15
                    }}>
                        <View style={{flex: 1}}>
                            <TouchableOpacity style={{backgroundColor: '#fff', borderBottomWidth: 0}}
                                              onPress={Keyboard.dismiss}
                                              accessible={false}>
                                <View style={{position: 'absolute', left: 15, top: 7}}>
                                    <Image source={require('../../../vendor/images/Search.png')} style={{
                                        tintColor: '#33B8E0'
                                    }}/>
                                </View>
                                <TextInput style={{
                                    width: '100%',
                                    borderWidth: 1,
                                    borderColor: '#ECEBED',
                                    borderRadius: 5,
                                    top: 0,
                                    height: 40,
                                    fontSize: 15,
                                    paddingLeft: 50,
                                    paddingRight: 10
                                }} placeholder="Nhập từ khoá bạn muốn tìm kiếm"

                                           onChangeText={(text) => {
                                               this.setState({query: text})
                                               this.onChange(text)
                                           }}
                                           value={this.state.query}

                                />

                            </TouchableOpacity>

                        </View>
                    </View>

                </Animated.View>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        // user: state.loginReducers,
        categoryPost: state.categoryPostReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        // category: () => {
        //
        //     dispatch(CategoryPost());
        // },
    };
}
const Media = connect(mapStateToProps, mapDispatchToProps)(MediaComponent);
export default Media

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4'
        // paddingTop:70
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 159,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        // backgroundColor: 'transparent',
        backgroundColor: '#fff',
        marginTop: Platform.OS === 'ios' ? 28 : 159,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
