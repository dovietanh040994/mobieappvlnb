import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    ImageBackground,
    Image,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    FlatList,
    ActivityIndicator,
    Alert, BackHandler
} from 'react-native';
import {Body, CardItem, Container, Header, Left, Right, Title, Button} from "native-base";
import {EditProfile, Media, ShowProfile} from "../../../vendor/screen";
import MyWebView from "react-native-webview-autoheight";
import SubmitEditUser from "../Profile/SubmitEditUser";
import {Field, submit} from "redux-form";
import Modal from "react-native-modalbox";
import {EditUserAction, FetchMediaRelation, FetchProvinceList, UpdateAvatarUser} from "../../../actions";
import {connect} from "react-redux";
import {EditProfileComponent} from "../Profile/EditProfileComponent";
import {withNavigation, withNavigationFocus} from "react-navigation";
import FlatListItemMedia from "./FlatListItemMedia";
import Sound from 'react-native-sound';
import Slider from '@react-native-community/slider';
import VolumeSlider from 'react-native-volume-slider';
import Share from "react-native-share";
import {FORM_CV} from "../../../vendor/formNames";
import firebase from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";

const HEADER_MAX_HEIGHT = 159;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var screen = Dimensions.get('window');


class MediaDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            media: '',
            volume: .5,
            isPlaying: false,
            songLength: 0,
            currentTime: 0,
            interval: null,
            time: '00:00',
            sound: null,
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    handleChange(event) {
        const filteredData = this.state.serverData.filter(item => item.id !== event.idJob);
        this.setState({serverData: filteredData});
    }

    componentWillMount() {
        BackHandler.addEventListener('Media', this.handleBackButtonClick);
    }


    handleBackButtonClick() {
        this._stop();
        this.props.navigation.navigate(Media)
        return true;

    }

    componentDidMount() {
        const {navigation} = this.props;
        // this.focusListener = navigation.addListener("didFocus", () => {
        //     this._stop();
        //
        // });
        const media = navigation.getParam('media', null);

        this.setState({media: media});

        this.props.onFetchMediaRelation(media.id);

        const infoMedia = {
            title: media.title,
            url: media.link_media,
        };

        const sound = new Sound(infoMedia.url, '', error => {
            if (error) {
                Alert.alert('error', error.message);
            }

            sound.play(() => {
                sound.release();
            });

            this.setState({
                sound: sound,
                isPlaying: true,
                interval: setInterval(() => {
                    sound.getCurrentTime((seconds) => {
                        this.setState({
                            currentTime: seconds,
                            time: this._toHHMMSS(seconds)
                        });
                    });
                }, 1000)
            });

            this.setState({
                songLength: sound.getDuration()
            });

            console.log('duration in seconds: ' + sound.getDuration() + 'number of channels: ' + sound.getNumberOfChannels());

        });


        AsyncStorage.getItem('adsMedia', (err, result) => {
            AsyncStorage.setItem('adsMedia', (parseInt(result) + 1).toString());
            if (parseInt(result) === 2 || parseInt(result) === 7) {
                const unitId = 'ca-app-pub-3717610865114273/3047817089';
                const advert = firebase.admob().interstitial(unitId);
                const AdRequest = firebase.admob.AdRequest;
                const request = new AdRequest();
                advert.loadAd(request.build());
                advert.on('onAdLoaded', () => {
                    console.log('Advert ready to show.');
                    advert.show();
                });

            }
        });
    }


    componentWillUnmount() {
        this._stop();
        BackHandler.removeEventListener('Media', this.handleBackButtonClick);
    }

    _toHHMMSS = (secs) => {
        var sec_num = parseInt(secs, 10)
        var hours = Math.floor(sec_num / 3600)
        var minutes = Math.floor(sec_num / 60) % 60
        var seconds = sec_num % 60

        return [hours, minutes, seconds]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v, i) => v !== "00" || i > 0)
            .join(":")
    };

    _play = () => {

        const infoMedia = {
            title: this.state.media.title,
            url: this.state.media.link_media,
        };

        const sound = new Sound(infoMedia.url, '', error => {
            if (error) {
                Alert.alert('error', error.message);
            }

            sound.play(() => {
                sound.release();
            });

            this.setState({
                sound: sound,
                isPlaying: true,
                interval: setInterval(() => {
                    sound.getCurrentTime((seconds) => {
                        this.setState({
                            currentTime: seconds,
                            time: this._toHHMMSS(seconds)
                        });
                        console.log(seconds);
                    });
                }, 1000),
                songLength: sound.getDuration()
            });

            console.log('duration in seconds: ' + sound.getDuration() + 'number of channels: ' + sound.getNumberOfChannels());

        });

    };

    _resume = () => {
        this.setState({
            isPlaying: true,
            interval: setInterval(() => {
                this.state.sound.getCurrentTime((seconds) => {
                    this.setState({
                        currentTime: seconds,
                        time: this._toHHMMSS(seconds)
                    });
                    console.log(seconds);
                });
            }, 1000)
        });

        this.state.sound.setCurrentTime(this.state.currentTime);

        this.state.sound.play(() => {

        });
    }


    _pause = () => {
        this.state.sound.pause();
        this.setState({
            isPlaying: false,
            interval: clearInterval(this.state.interval),
        });
        this.state.sound.setCurrentTime(this.state.currentTime);
    };

    _stop = () => {
        if (this.state.sound) {
            this.state.sound.stop();
            this.setState({
                isPlaying: false,
                songLength: 0,
                currentTime: 0,
                interval: clearInterval(this.state.interval),
                time: '00:00',
                sound: null
            })
        }
    };

    volumeChange(value) {
        // body
    }


    tick() {
        this.state.sound.getCurrentTime((seconds) => {
            this.setState({
                currentTime: seconds
            })
        });
        console.log(this.state.currentTime);
    }

    static navigationOptions = () => ({
        header: null
    });

    renderFooter() {
        return (
            <View style={styles.footer}>
                {this.state.fetching_from_server ? (
                    <ActivityIndicator size="large" color={'#ccc'}
                                       style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }



    render() {


        return (

            <ScrollView ref={'mediaDetail'} scrollEnabled={this.state.enableScrollViewScroll}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => {
                            this._stop();
                            this.props.navigation.navigate(Media)
                        }}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Media</Title>
                    </Body>
                </Header>

                <View style={[styles.fill, {marginTop: 20}]}>
                    <Animated.View style={styles.bodyContent}>
                        <Animated.Text style={{fontSize: 20, color: '#33B8E0', fontWeight: 'bold', marginBottom: 20}}>Phía
                            sau tay
                            lái</Animated.Text>
                    </Animated.View>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        alignItems: 'center',
                        position: 'relative',
                        marginBottom: 15,
                        justifyContent: 'center'
                    }}>

                        <Image source={{uri: `${this.props.navigation.getParam('media', null).thumb}`}}
                               style={{width: screenWidth - 30, height: 315}}/>

                        <View style={{
                            alignSelf: 'center',
                            position: 'absolute',
                            // marginTop: 140,
                        }}>
                            <TouchableOpacity onPress={() => {
                                if (this.state.sound) {
                                    if (this.state.isPlaying) {
                                        this._pause();
                                    } else {
                                        this._resume();
                                    }
                                }
                            }}>
                                <Image
                                    source={!this.state.isPlaying ? require('../../../vendor/images/media/pause.png') : require('../../../vendor/images/media/play.png')}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/*<VolumeSlider*/}
                    {/*    style={styles.slider}*/}
                    {/*    thumbSize={{*/}
                    {/*        width: 8,*/}
                    {/*        height: 8*/}
                    {/*    }}*/}
                    {/*    thumbTintColor="rgb(146,146,157)"*/}
                    {/*    minimumTrackTintColor="rgb(146,146,157)"*/}
                    {/*    maximumTrackTintColor="rgba(255,255,255, 0.1)"*/}
                    {/*    showsRouteButton={true}*/}
                    {/*    onValueChange={this.volumeChange.bind(this)}/>*/}

                    <Slider
                        maximumValue={this.state.songLength}
                        minimumValue={0}
                        minimumTrackTintColor="#33B8E0"
                        maximumTrackTintColor="#C4C4C4"
                        thumbTintColor="#33B8E0"
                        value={this.state.currentTime}
                        onValueChange={(sliderValue) => {
                            this.setState({
                                currentTime: sliderValue
                            });
                            this.state.sound.setCurrentTime(sliderValue);

                            this.state.sound.play(() => {

                            });
                        }}
                    />
                    <View style={{
                        flex: 1,
                        flexDirection: 'row', paddingTop: 10, paddingLeft: 15, paddingRight: 15
                    }}>
                        <Left>
                            <Text style={{color: "#33B8E0", fontSize: 16}}>{this.state.time}</Text>
                        </Left>
                        <Right>
                            <Text
                                style={{color: "#33B8E0", fontSize: 16}}>{this._toHHMMSS(this.state.songLength)}</Text>
                        </Right>
                    </View>

                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', margin: 15}}>
                        <Image style={{width: 10, height: 12}}
                               source={require('../../../vendor/images/media/time.png')}
                        />
                        <Text style={{
                            color: '#33B8E0', fontSize: 16,
                        }}> {this.state.media.update_at}  </Text>
                    </View>
                    <View style={{flex: 1, marginLeft: 15, marginRight: 15}}>
                        <Text style={{
                            color: '#4F4F4F', fontSize: 20, fontWeight: '500', marginBottom: 20
                        }}> {this.state.media.title}  </Text>
                        <Text style={{
                            color: '#525252', fontSize: 16, lineHeight: 20, fontWeight: 'normal', textAlign: 'justify'

                        }}>
                            {this.state.media.description}
                        </Text>
                        <Text style={{
                            color: '#525252', fontSize: 16, lineHeight: 20, textAlign: 'justify'

                        }}>
                            {this.state.media.body}
                        </Text>
                        <Button full style={{backgroundColor: '#33B8E0', borderRadius: 6, marginTop: 10}}>
                            <TouchableOpacity onPress={
                                ()=>{
                                    const shareOptions = {
                                        url: `${this.state.media.slug}`,
                                        social: Share.Social.FACEBOOK
                                    }

                                    Share.shareSingle(Object.assign(shareOptions));
                                }
                            }
                            >
                                <Text
                                    style={{color: '#FFFFFF', fontSize: 17}}> Chia sẻ
                                    với bạn bè </Text>
                            </TouchableOpacity></Button>
                    </View>

                    <Animated.Text style={{
                        fontSize: 20,
                        color: '#33B8E0',
                        marginLeft: 15,
                        marginRight: 15,
                        marginTop: 15,
                        fontWeight: '500'
                    }}>Tin liên
                        quan</Animated.Text>

                    <FlatList style={{margin: 5}}
                              horizontal
                              data={this.props.mediaRelation.items}
                              renderItem={({item, index}) => {
                                  return (
                                      <TouchableOpacity onPress={
                                          async () => {
                                              await this._stop();
                                              await this.setState({media: item});
                                              await this.props.onFetchMediaRelation(item.id);
                                              await this.refs.mediaDetail.scrollTo({x: 5, y: 5, animated: true});
                                              await this._play();
                                          }
                                      }>
                                          <View
                                              style={{
                                                  flex: 1,
                                                  flexDirection: 'column',
                                                  alignItems: 'center',
                                                  margin: 10,
                                                  width: 192
                                              }}
                                          >
                                              <Image source={{uri: item.thumb}}
                                                     style={{height: 130, width: 190, marginBottom: 20}}/>
                                              <Text numberOfLines={2}
                                                    style={{
                                                        fontSize: 16,
                                                        color: '#4F4F4F',
                                                        marginBottom: 10,
                                                        fontWeight: '500'
                                                    }}>
                                                  {item.title}
                                              </Text>
                                              <Text numberOfLines={3}
                                                    style={{fontSize: 12, color: '#4F4F4F', marginBottom: 20}}>
                                                  {item.description}
                                              </Text>
                                          </View>
                                      </TouchableOpacity>
                                  );
                              }}
                              keyExtractor={(item) => item.id.toString()}
                    />
                </View>

            </ScrollView>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        mediaRelation: state.mediaReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        onFetchMediaRelation: (id) => {
            dispatch(FetchMediaRelation(id))
        }
    };
}

const media = connect(mapStateToProps, mapDispatchToProps)(MediaDetail);

export default withNavigation(media);

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    textAdvanced: {
        width: 120,
        left: 20,
        fontSize: 17,
        color: '#33B8E0',
        fontWeight: 'normal',
    },
    textBody: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    textPlaceholder: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },
    wrapper: {
        paddingTop: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal4: {
        height: 400,
        width: screen.width
    },
    gender: {
        height: 200,
        width: screen.width
    },
    text: {
        color: 'black',
        fontSize: 16,
        textAlign: 'left',
        paddingLeft: 20,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderColor: '#ccc',
        height: 40,
        alignItems: 'center'
    },
    button: {
        backgroundColor: 'green',
        width: 300,
        marginTop: 16,
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        color: '#d0d0d0',
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF'
    },
    slider: {
        height: 30,
        marginLeft: 7,
    }
});
