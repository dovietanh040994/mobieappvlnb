import React, {Component} from "react";
import {Animated, Image, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text} from "native-base";
import {JobDetailAction, SaveJobAction} from "../../../actions";
import {connect} from "react-redux";
import Share from "react-native-share";
import {MediaDetail} from "../../../vendor/screen";
import {FetchMediaRelation} from "../../../actions";

class FlatListItemMedia extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saved: [],
            shareId: 0
        };

    }

    render() {

        return (

            <CardItem
                style={{
                    backgroundColor: 'white',
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    //
                    // marginRight: 15,
                    // marginLeft: 15,
                    borderBottom: 0,
                    paddingLeft: 15,
                    paddingRight: 0,
                    paddingTop: 0,
                    paddingBottom: 0,
                }}>
                <Left>

                    <Image source={{uri: `${this.props.item.thumb}`}} style={{height: 100, width: 100}}/>
                    <Body style={{flex: 1, alignSelf: 'flex-start', padding: 10}}>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                            <Image style={{width: 10, height: 12}}
                                   source={require('../../../vendor/images/media/time.png')}
                            />
                            <Text style={{
                                color: '#33B8E0', fontSize: 12,
                            }}> {this.props.item.update_at}  </Text>
                        </View>
                        <TouchableOpacity onPress={
                            () => {
                                this.props.navigation.navigate(MediaDetail, {media: this.props.item});
                            }
                        }>
                            <Text numberOfLines={2} style={{
                                fontSize: 16,
                                color: '#3F3356',
                                fontWeight: 'bold'
                            }}>{this.props.item.title}  </Text>
                        </TouchableOpacity>
                        <Text numberOfLines={3}
                              style={{fontSize: 12, color: '#3F3356'}}> {this.props.item.description} </Text>


                    </Body>

                </Left>
            </CardItem>
        );
    }
}

const mapStateToProps = (state) => {
    return {}
};
const mapDispatchToProps = (dispatch) => {
    return {

    };
}
const ItemMedia = connect(mapStateToProps, mapDispatchToProps)(FlatListItemMedia);

export default ItemMedia
