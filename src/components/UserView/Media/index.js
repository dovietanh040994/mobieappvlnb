import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
import MediaComponent from "./MediaComponent";
import MediaDetail from "./MediaDetail";
import MainView from "./MainView";
import React from "react";


const MediaNavigator = createStackNavigator(
    {
        Media: {
            screen: MediaComponent,

        },
        MediaDetail: {
            screen: MediaDetail,
        }
    }, {
        initialRouteName: 'Media'
    }
);

export default createAppContainer(MediaNavigator);
