import React, {Component} from "react";
import {Image, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text, Thumbnail} from "native-base";
import {JobDetailAction, SaveJobAction,CompanyDetailAction} from "../../../actions";
import {connect} from "react-redux";
import Share from "react-native-share";
import {JobDetail, PostDetail} from "../../../vendor/screen";

class FlatListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saved: [],
            shareId: 0,
            imageLoading : true,
        };

    }
    ImageLoading_Error(){
        this.setState({ imageLoading: false });

        // Write Your Code Here Which You Want To Execute If Image Not Found On Server.
    }
    randomImageProp(){
        if(this.props.random === 0){
            return 0
        }else if (this.props.random === 1){
            return 1
        }else if (this.props.random === 2){
            return 2
        }else if (this.props.random === 3){
            return 3
        }
    }
    render() {
        // const shareFB = () => {
        //     if (this.props.jobContent.itemsDetaiJob && this.state.shareId === this.props.jobContent.itemsDetaiJob.id) {
        //
        //         const shareOptions = {
        //             url: `${this.props.jobContent.itemsDetaiJob.slug}`,
        //             social: Share.Social.FACEBOOK
        //         }
        //         Share.shareSingle(Object.assign(shareOptions));
        //         this.setState({shareId: 0})
        //
        //     }
        // };
        // shareFB();

        return (
            <TouchableOpacity onPress={
                () => {
                    this.props.jobDetail(this.props.item.id);
                    this.props.componyDetail(this.props.item.id);
                    if(this.props.nameScreen !== undefined){
                        this.props.navigation.navigate(JobDetail, {
                            provine: this.props.item.provine_name,
                            nameScreen: this.props.nameScreen,
                            saveHeart: this.props.saveHeart,
                            applied: this.props.applied,
                            randomImageProp: this.randomImageProp()

                        });
                    }else{
                        this.props.navigation.navigate(JobDetail, {
                            provine: this.props.item.provine_name,
                            saveHeart: this.props.saveHeart,
                            applied: this.props.applied,
                            randomImageProp: this.randomImageProp()
                        });
                    }

                }
            }>
            <CardItem
                style={{
                    backgroundColor: 'white',
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    marginRight:15,
                    marginLeft:15,
                    borderBottom:0
                }}>
                <Left>
                    {this.props.random === 0 && (
                        <Image
                            resizeMode={'contain'}
                            source = {
                                this.state.imageLoading
                                    ?
                                    { uri: `${this.props.item.logo}` }
                                    :
                                    require('../../../vendor/images/VLNB1.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}
                    {this.props.random === 1 && (
                        <Image
                            resizeMode={'contain'}
                            source = {
                                this.state.imageLoading
                                    ?
                                    { uri: `${this.props.item.logo}` }
                                    :
                                    require('../../../vendor/images/VLNB2.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}
                    {this.props.random === 2 && (
                        <Image
                            resizeMode={'contain'}
                            source = {
                                this.state.imageLoading
                                    ?
                                    { uri: `${this.props.item.logo}` }
                                    :
                                    require('../../../vendor/images/VLNB3.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}
                    {this.props.random === 3 && (
                        <Image
                            resizeMode={'contain'}
                            source = {
                                this.state.imageLoading
                                    ?
                                    { uri: `${this.props.item.logo}` }
                                    :
                                    require('../../../vendor/images/VLNB4.png')
                            }
                            onError={this.ImageLoading_Error.bind(this)}
                            style={{height: 100, width: 100}}
                        />
                    )}

                    <Body >
                        <Text numberOfLines={2} style={{fontSize:13,color:'#474747',fontWeight:'bold'}}>{this.props.item.job_title} </Text>
                        <Text style={{fontSize:10,marginTop:3,marginBottom:3}}>{this.props.item.company_name}</Text>
                        <View style={{flexDirection:'row',marginBottom:1}} >
                            <Image source={require('../../../vendor/images/Shape.png')} style={{marginTop:4,marginRight:5}} />
                            <Text style={{color: '#33B8E0',fontSize: 11,fontWeight: '500'}}>{this.props.item.provine_name}</Text>
                        </View>
                        <View style={{flexDirection:'row'}} >
                            <Left>
                                <View style={{flexDirection:'row'}} >
                                    <Image source={require('../../../vendor/images/money.png')} style={{marginTop:4,marginRight:5}}/>
                                    <Text style={{color: '#33B8E0',fontSize: 11,fontWeight: '500'}}>{this.props.item.job_salary}</Text>
                                </View>
                            </Left>
                            <Right>
                                <View style={{flexDirection:'row'}} >
                                    <Image source={require('../../../vendor/images/clock.png')} style={{marginTop:4,marginRight:5}}/>
                                    <Text style={{color: '#33B8E0',fontSize: 11,fontWeight: '500'}}>{this.props.item.ended_at !== '00/00/0000' ? this.props.item.ended_at : 'Luôn tuyển'}</Text>
                                </View>
                            </Right>

                        </View>

                    </Body>

                </Left>
            </CardItem>
            </TouchableOpacity>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        jobContent: state.jobDetailReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {

        SaveHeartJob: (id) => {

            dispatch(SaveJobAction(id));
        },
        jobDetail: (idJob) => {
            dispatch(JobDetailAction(idJob));

        },
        componyDetail: (idJob) => {

            dispatch(CompanyDetailAction(idJob));
        },

    };
}
const FlatListItemJob = connect(mapStateToProps, mapDispatchToProps)(FlatListItem);

export default FlatListItemJob
