import React, {Component} from "react";
import {Image, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text} from "native-base";
import {JobDetailAction, SaveJobAction} from "../../../actions";
import {connect} from "react-redux";
import Share from "react-native-share";
import {PostDetail} from "../../../vendor/screen";

class FlatListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saved: [],
            shareId: 0
        };

    }

    render() {
        // const shareFB = () => {
        //     if (this.props.jobContent.itemsDetaiJob && this.state.shareId === this.props.jobContent.itemsDetaiJob.id) {
        //
        //         const shareOptions = {
        //             url: `${this.props.jobContent.itemsDetaiJob.slug}`,
        //             social: Share.Social.FACEBOOK
        //         }
        //         Share.shareSingle(Object.assign(shareOptions));
        //         this.setState({shareId: 0})
        //
        //     }
        // };
        // shareFB();

        // console.log(this.props.item)

        return (
            <TouchableOpacity onPress={
                () => {

                    this.props.navigation.navigate(PostDetail, {
                        slug: this.props.item.slug,
                        thumb: this.props.item.thumb,
                    });
                }
            }>
                <CardItem
                    style={{
                        backgroundColor: 'white',
                        borderTopRightRadius: 5,
                        borderTopLeftRadius: 5,
                        borderBottomLeftRadius: 5,
                        borderBottomRightRadius: 5,

                        marginRight:15,
                        marginLeft:15,
                        borderBottom:0,
                        paddingLeft:15,
                        paddingRight:0,
                        paddingTop:0,
                        paddingBottom:0,
                    }}>
                    <Left>

                        <Image source={{uri: `${this.props.item.thumb}`}} style={{height: 100, width: 100}} />
                        <Body style={{flex:1,alignSelf:'flex-start',padding:10,paddingLeft:5}}>
                          <Text numberOfLines={2} style={{fontSize:16,color:'#3F3356',fontWeight:'bold'}}>{this.props.item.title}  </Text>
                            <Text numberOfLines={4} style={{fontSize:12,color:'#3F3356'}}> {this.props.item.description} </Text>


                        </Body>

                    </Left>
                </CardItem>
            </TouchableOpacity>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        jobContent: state.jobDetailReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {

        SaveHeartJob: (id) => {

            dispatch(SaveJobAction(id));
        },
        jobDetail: (idJob) => {
            dispatch(JobDetailAction(idJob));

        }

    };
}
const FlatListItemPost = connect(mapStateToProps, mapDispatchToProps)(FlatListItem);

export default FlatListItemPost
