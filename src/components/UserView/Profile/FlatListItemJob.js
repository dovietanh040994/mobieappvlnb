import React, {Component} from "react";
import {Animated, Image, StyleSheet, TouchableOpacity, View, Alert} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text, Toast} from "native-base";
import {DeleteJobApply, DeleteJobCare, JobDetailAction} from "../../../actions";
import {connect} from "react-redux";
import Share from "react-native-share";
import Swipeout from 'react-native-swipeout';
import {JobDetail, ProfileJobDetail} from "../../../vendor/screen";

class FlatListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saved: [],
            shareId: 0,
            token: this.props.token,
            type: this.props.screen
        };

    }

    actionDelete = (prams) => {
        if (this.state.type === "JobCare") {
            this.props.onDeleteItemCareAction(prams);
        } else {
            this.props.onDeleteItemApplyAction(prams);
        }

        this.props.handleChange(prams);
    };

    render() {
        const itemDelete =
            <View style={{
                flex: 1, flexDirection: 'column', alignItems: 'center', position: 'relative'
            }}>
                <Animated.Image
                    style={{
                        width: 22,
                        height: 46,
                        alignSelf: 'center',
                    }}
                    source={require('../../../vendor/images/profile/delete.png')}
                />
            </View>
        ;

        const swipeSetting = {
            autoClose: true,
            onClose: () => {

            },
            onOpen: () => {

            },
            right: [{
                onPress: () => {
                    Alert.alert(
                        'Alert',
                        this.state.type === "JobCare" ? 'Bạn có muốn xoá việc đã quan tâm?' :"'Bạn có muốn xoá việc đã ứng tuyển?'",
                        [
                            {text: 'Không', onPress: () => console.log("no"), style: 'cancel'},
                            {
                                text: 'Đồng ý', onPress: () => {
                                    this.actionDelete({
                                        idJob: this.props.item.id,
                                        token: this.state.token
                                    })
                                }
                            }
                        ]
                    )
                },
                text: itemDelete, type: 'delete'
            }],
            rowID: this.props.item.id,
            sectionID: 1
        }

        return (
        /* <Swipeout {...swipeSetting}>*/

                <View style={{
                    flex: 1,
                    borderBottomWidth: 1,
                    borderColor: '#D9D9D9',
                }}>
                    <CardItem
                        style={{
                            backgroundColor: 'white',
                            flex: 1
                        }}>
                        <Left>
                            <Image source={{uri: `${this.props.item.logo}`}}
                                   style={{height: 68, width: 68, borderRadius: 63, justifyContent: 'flex-start'}}/>
                            <Body>
                                <View style={{flex:1, flexDirection:'row'}}>
                                    <TouchableOpacity onPress={
                                        () => {
                                            this.props.jobDetail(this.props.item.id);
                                            // this.props.companyDetail(this.props.item.id);
                                            if (this.props.nameScreen !== undefined) {
                                                this.props.navigation.navigate(JobDetail, {
                                                    provine: this.props.item.provine_name,
                                                    nameScreen: this.props.nameScreen,
                                                    saveHeart: this.props.saveHeart,
                                                    applied: this.props.applied,

                                                });
                                            } else {
                                                this.props.navigation.navigate(JobDetail, {
                                                    provine: this.props.item.provine_name,
                                                    saveHeart: this.props.saveHeart,
                                                    applied: this.props.applied,
                                                });
                                            }
                                        }
                                    }>

                                        <Text numberOfLines={1} style={[styles.company_name,{marginTop:15}]}>{this.props.item.company_name}</Text>

                                        <Text numberOfLines={1}
                                              style={styles.title_name}>{this.props.item.job_title} </Text>
                                    </TouchableOpacity>


                                </View>


                                <View>
                                    <View style={styles.item}>
                                        <Image source={require('../../../vendor/images/profile/location.png')}
                                               style={[styles.icon, {width: 10, height: 12}]}/>

                                        <Text style={styles.icon_desc}>Địa điểm làm việc: </Text>
                                        <Text style={styles.icon_boby}>{this.props.item.provine_name}</Text>
                                    </View>

                                    <View style={styles.item}>
                                        <Image source={require('../../../vendor/images/profile/salary.png')}
                                               style={[styles.icon, {width: 10}]}/>
                                        <Text style={styles.icon_desc}>Mức lương: </Text>
                                        <Text style={styles.icon_boby}>{this.props.item.job_salary}</Text>
                                    </View>

                                    <View style={styles.item}>
                                        <Image source={require('../../../vendor/images/profile/time.png')}
                                               style={[styles.icon, {width: 11}]}/>
                                        <Text style={styles.icon_desc}>Hạn nộp: </Text>
                                        <Text
                                            style={styles.icon_boby}>{this.props.item.ended_at !== '00/00/0000' ? this.props.item.ended_at : 'Luôn tuyển'}</Text>

                                    </View>
                                </View>
                                <View>
                                    <TouchableOpacity
                                        style={{
                                            width: 20,
                                            height: 20,
                                            bottom:15,
                                            right:0,
                                            position:'absolute',
                                            alignSelf:'center',
                                        }}
                                        onPress={
                                            () => {
                                                Alert.alert(
                                                    'Xác nhận',
                                                    this.state.type === "JobCare" ? 'Bạn có muốn xoá việc đã quan tâm?' :"'Bạn có muốn xoá việc đã ứng tuyển?'",
                                                    [
                                                        {text: 'Không', onPress: () => console.log("no"), style: 'cancel'},
                                                        {
                                                            text: 'Đồng ý', onPress: () => {
                                                                this.actionDelete({
                                                                    idJob: this.props.item.id,
                                                                    token: this.state.token
                                                                })
                                                            }
                                                        }
                                                    ]
                                                )
                                            }
                                        }>
                                        <Image
                                            style={{
                                                tintColor: 'red',
                                                width: 20,
                                                height: 20,
                                            }}
                                            source={require('../../../vendor/images/remove3.png')}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </Body>
                        </Left>
                    </CardItem>
                </View>
        /*  </Swipeout>*/


        );
    }
}

const mapStateToProps = (state) => {
    return {
        // user: state.loginReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        onDeleteItemCareAction: (params) => {
            dispatch(DeleteJobCare(params))
        },
        onDeleteItemApplyAction: (params) => {
            dispatch(DeleteJobApply(params))
        },
        jobDetail: (idJob) => {
            dispatch(JobDetailAction(idJob));

        }
    };
}
const FlatListItemJob = connect(mapStateToProps, mapDispatchToProps)(FlatListItem);

export default FlatListItemJob

const styles = StyleSheet.create({
    company_name: {
        fontSize: 16,
        fontWeight: '500',
        marginTop: 5,
        marginBottom: 5,
        color: "#525252"
    },
    title_name: {
        fontSize: 15,
        color: '#33B8E0',
        fontWeight: 'bold',
        lineHeight: 21,
    },
    icon: {
        marginTop: 4,
        marginRight: 10,
    },
    icon_desc: {
        fontSize: 14,
        color: '#D0C9D6',
        lineHeight: 20
    },
    icon_boby: {
        fontSize: 14,
        color: '#525252',
        lineHeight: 20
    },
    item: {
        flexDirection: 'row',
        marginBottom: 8
    }
});
