import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, TextInput, Dimensions, BackHandler
} from 'react-native';
import {
    Body,
    CardItem,
    Container,
    Left,
    Right,
    Switch,
    Button,
    Input,
    Picker,
    Textarea,
    Title,
    Header, Toast
} from "native-base";
import {Field, FieldArray, reduxForm, SubmissionError} from 'redux-form';
import {FORM_EDIT, FORM_CV} from "../../../vendor/formNames";
import Modal from 'react-native-modalbox';
import {connect} from "react-redux";
import {StylesAll} from "../../../vendor/styles";
import {HomeBox, ShowProfile} from "../../../vendor/screen";
import ListDetailModal from "../Home/Search/ListDetailModal";
import {
    FetchListMajor,
    UpdateUserInfo, FetchUserInfo, UpdateAvatarUser
} from "../../../actions";
import {EditProfileComponent} from "./EditProfileComponent";
import SubmitCVUser from "./SubmitCVUser";
import NavigationService from "../../../vendor/NavigationService";
import DatePicker from "react-native-datepicker";
import FlatListItemJob from "../ChildComponent/FlatListItemJob";
import universityReducers from "../../../reducers/UniversityReducres";
import ImagePicker from "react-native-image-picker";

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screen = Dimensions.get('window');
var screenWidth = Dimensions.get('window').width;

const options = {
    title: 'Select Avatar',
    // customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const menu = [
    {
        id: 1,
        name: "THÔNG TIN",
    },
    {
        id: 2,
        name: "HỌC VẤN",
    },
    {
        id: 3,
        name: "KINH NGHIỆM",
    },
    {
        id: 4,
        name: "MONG MUỐN",
    },
    {
        id: 5,
        name: "KỸ NĂNG",
    },
];

const listGender = [
    {
        id: 1,
        name: "Nam",
    },
    {
        id: 2,
        name: "Nữ",
    }
];

const listStatusMarital = [
    {
        id: 0,
        name: "Độc thân",
    },
    {
        id: 1,
        name: "Đã kết hôn",
    }
];

const listLevel = [
    {
        id: 1,
        name: "Sơ cấp",
    },
    {
        id: 2,
        name: "Trung bình",
    }, {
        id: 3,
        name: "Khá",
    },
    {
        id: 4,
        name: "Tốt",
    }
];


const months = [
    {
        id: 1,
        name: '01',
    },
    {
        id: 2,
        name: '02',
    },
    {
        id: 3,
        name: '03',
    },
    {
        id: 4,
        name: '04',
    }, {
        id: 5,
        name: '05',
    }, {
        id: 6,
        name: '06',
    }, {
        id: 7,
        name: '07',
    }, {
        id: 8,
        name: '08',
    }, {
        id: 9,
        name: '09',
    }, {
        id: 10,
        name: '10',
    }, {
        id: 11,
        name: '11',
    }, {
        id: 12,
        name: '12',
    }
];

const submitEditCV = async (values, dispatch) => {
    if (!values.job_desire.carrer_id || values.job_desire.carrer_id === 0) {
        toastError("Vui lòng chọn ngành nghề !")
    } else if (!values.job_desire.province_id || values.job_desire.province_id === 0) {
        toastError("Vui lòng chọn nơi muốn làm !")
    } else if (!values.job_desire.job_salary_id || values.job_desire.job_salary_id === 0) {
        toastError("Vui lòng chọn mức lương!")
    } else {
        await dispatch(UpdateUserInfo(values));
        await dispatch(FetchUserInfo());
        await Toast.show({
            text: "Cập nhật CV thành công!",
            buttonText: "Đóng",
            type: "success",
            position: 'top',
        });
        await NavigationService.navigate(ShowProfile);
    }
};

const toastError = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};
const toastWarning = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};


const validate = values => {

    const errors = {};

    return errors

};

class CVFullComponent extends Component {
    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            scrollEnabled: true,
            dislay: false,
            dataList: '',
            title: '',
            functionPropIDHere: '',
            PropNameHere: '',
            bgButton: 1,
            date_of_birth: this.props.userInfo.length !== 0 && this.props.userInfo.items.date_of_birth !== 'undefined' ? this.props.userInfo.items.date_of_birth : '',
            gender: this.props.userInfo.length !== 0 && this.props.userInfo.items.gender !== 'undefined' ? (this.props.userInfo.items.gender === '1' ? "Nam" : 'Nữ') : '',
            is_married: this.props.userInfo.length !== 0 && this.props.userInfo.items.is_married !== 'undefined' ? (this.props.userInfo.items.is_married === 1 ? 'Đã kết hôn' : 'Độc thân') : 0,
            arrayvar: [],
            avatarSource: null
        };

        this.arrayvar = {};

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

        this.years = this.forYear();
    }


    forYear() {
        let arr = [];
        for (var i = 2019; i > 1970; i--) {
            arr.push({
                id: i,
                name: i
            })
        }
        return arr;
    }


    componentWillMount() {
        //tạo đầu vào mặc định cho trường trong redux-form

        this.props.initialize({
            name: this.props.userInfo.length !== 0 && this.props.userInfo.items.name !== 'undefined' ? this.props.userInfo.items.name : '',
            phone: this.props.userInfo.length !== 0 && this.props.userInfo.items.phone !== 'undefined' ? this.props.userInfo.items.phone : '',
            address: this.props.userInfo.length !== 0 && this.props.userInfo.items.address !== 'undefined' ? this.props.userInfo.items.address : '',
            date_of_birth: this.props.userInfo.length !== 0 && this.props.userInfo.items.date_of_birth !== 'undefined' && this.props.userInfo.items.date_of_birth !== '01/01/2000' ? this.props.userInfo.items.date_of_birth : '',
            gender: this.props.userInfo.length !== 0 && this.props.userInfo.items.gender !== 'undefined' ? this.props.userInfo.items.gender : '',
            email: this.props.userInfo.length !== 0 && this.props.userInfo.items.email !== 'undefined' ? this.props.userInfo.items.email : '',
            is_married: this.props.userInfo.length !== 0 && this.props.userInfo.items.is_married !== 0 ? this.props.userInfo.items.is_married : -1,

            educations: this.props.userInfo.length !== 0 && this.props.userInfo.items.educations.length !== 0 ? this.setInitializeEducations() : [{}],

            experiences: this.props.userInfo.length !== 0 && this.props.userInfo.items.experiences.length !== 0 ? this.setInitializeExperiences() : [{}],

            job_desire: this.props.userInfo.length !== 0 && this.props.userInfo.items.job_desire !== null ? this.props.userInfo.items.job_desire : this.setInitializeJobDesire(),

            skills: this.props.userInfo.length !== 0 && this.props.userInfo.items.skills.length !== 0 ? this.props.userInfo.items.skills : [{}],
        });

        BackHandler.addEventListener('ShowProfile', this.handleBackButtonClick);
    }

    componentDidMount() {
        this.setStateJobDesire();
        this.setStateJobSkills();
        this.setStateEducations()
        this.setStateEx();
    }

    setInitializeEducations() {
        let educations = [];
        if (this.props.userInfo.items.educations.length !== 0) {
            this.props.userInfo.items.educations.map((item, index) => {
                educations.push({
                    university_id: item.university_id ? item.university_id : -1,
                    major_id: item.major_id ? item.major_id : -1,
                    month_start: this.splitDate(item.start_date, 1),
                    year_start: this.splitDate(item.start_date, 2),
                    month_end: this.splitDate(item.end_date, 1),
                    year_end: this.splitDate(item.end_date, 2),
                    degree_id: item.degree_id ? item.degree_id : -1,
                    cetificate_id: -1,
                });
            });
            return educations;
        }
    }

    setInitializeExperiences() {
        let experiences = [];
        if (this.props.userInfo.items.experiences.length !== 0) {
            this.props.userInfo.items.experiences.map((item, index) => {
                experiences.push({
                    company_name: item.company_name ? item.company_name : '',
                    position_name: item.position_name ? item.position_name : '',
                    month_start: this.splitDate(item.start_date, 1),
                    year_start: this.splitDate(item.start_date, 2),
                    month_end: this.splitDate(item.end_date, 1),
                    year_end: this.splitDate(item.end_date, 2),
                    degree_id: item.description ? item.description : '',
                });
            });
            return experiences;
        }

    }

    setInitializeJobDesire() {
        return {
            province_id: -1,
            carrer_id: -1,
            job_salary_id: -1,
            job_experience_id: -1,
            job_type_id: -1,
            short_description: '',
            desire_for_work: ''
        }
    }

    setStateJobDesire() {
        let jobDesire = this.props.userInfo.items.job_desire;

        if (jobDesire === null) {
            this.arrayvar['job_desire.carrer_id'] = null;
            this.arrayvar['job_desire.province_id'] = null;
            this.arrayvar['job_desire.job_salary_id'] = null;
        } else {
            this.arrayvar['job_desire.carrer_id'] = jobDesire.carrer_name;
            this.arrayvar['job_desire.province_id'] = jobDesire.province_name;
            this.arrayvar['job_desire.job_salary_id'] = jobDesire.job_salary_name;
        }

        this.setState({arrayvar: this.arrayvar});
    };

    setStateJobSkills() {
        let skills = this.props.userInfo.items.skills;
        if (skills.length !== 0) {
            skills.map((item, index) => {
                this.findNameSetState(this.props.listSkill, item.skill_name, `skills[${index}].skill_name`);
                this.findNameSetState(listLevel, item.level, `skills[${index}].level`);
            })
        }
    }

    findNameSetState(data, id, key) {
        data.map((item) => {
            if (parseInt(item.id) === parseInt(id)) {
                this.setStateKeyValue(key, item.name);
            }
        });
    }

    setStateEducations() {
        let educations = this.props.userInfo.items.educations;
        if (educations.length !== 0) {
            educations.map((item, index) => {
                this.findNameSetState(this.props.listUniversity, item.university_id, `educations[${index}].university_id`);
                this.findNameSetState(this.props.listMajor, item.major_id, `educations[${index}].major_id`);
                this.findNameSetState(this.props.listDegree, item.degree_id, `educations[${index}].degree_id`);
                if (item.start_date !== '01/01/2000') {
                    this.setStateKeyValue(`educations[${index}].month_start`, this.splitDate(item.start_date, 1))
                    this.setStateKeyValue(`educations[${index}].year_start`, this.splitDate(item.start_date, 2))
                }
                if (item.end_date !== '01/01/2000') {
                    this.setStateKeyValue(`educations[${index}].month_end`, this.splitDate(item.end_date, 1))
                    this.setStateKeyValue(`educations[${index}].year_end`, this.splitDate(item.end_date, 2))
                }

            })
        }
    }

    setStateEx() {
        let experiences = this.props.userInfo.items.experiences;
        if (experiences.length !== 0) {
            experiences.map((item, index) => {
                if (item.start_date !== '01/01/2000') {
                    this.setStateKeyValue(`experiences[${index}].month_start`, this.splitDate(item.start_date, 1))
                    this.setStateKeyValue(`experiences[${index}].year_start`, this.splitDate(item.start_date, 2))
                }
                if (item.end_date !== '01/01/2000') {
                    this.setStateKeyValue(`experiences[${index}].month_end`, this.splitDate(item.end_date, 1))
                    this.setStateKeyValue(`experiences[${index}].year_end`, this.splitDate(item.end_date, 2))
                }

            })
        }
    }

    setStateKeyValue(key, value) {
        this.arrayvar[key] = value;
        this.setState({arrayvar: this.arrayvar});
    }

    splitDate(date, index) {
        var str = date.split("/");
        return str[index];
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('ShowProfile', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(ShowProfile);
        return true;

    }

    renderEducations = ({fields}) => {

        return (
            <View style={{
                top: 50, marginBottom: 200
            }}>
                {fields.map((item, index) => (
                    <View key={index} style={index > 0 ? {marginTop: 30} : ''}>
                        <TouchableOpacity onPress={async () => {
                            await this.setState({
                                dataList: this.props.listUniversity,
                                title: 'Trường học',
                                functionPropIDHere: `${item}.university_id`
                            });

                            await this.refs.listModel.changeDataList()
                            await this.refs.listModel.showAddModal()

                        }}>
                            <View style={[styles.boxAdvanced, {borderTopWidth: 1}]}>
                                <Text style={styles.textAdvanced}>
                                    Trường
                                </Text>

                                <View>
                                    <Text numberOfLines={1}
                                          style={this.state.arrayvar[`${item}.university_id`] ? [styles.textBody, {width: 200}] : {
                                              fontSize: 15,
                                              color: '#E0E0E0',
                                              fontWeight: '500'
                                          }}>{this.state.arrayvar[`${item}.university_id`] ? this.state.arrayvar[`${item}.university_id`] : 'Chọn trường'}</Text>
                                </View>
                                <Field type="text" name={`${item}.university_id`}
                                       component={this.renderPickerInput}>
                                </Field>
                                <Right style={styles.arrow_item}>
                                    <Animated.Image
                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                    />
                                </Right>
                            </View>
                        </TouchableOpacity>

                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Bắt đầu
                            </Text>

                            <View style={{flex: 1, flexDirection: 'row'}}>
                                <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                    await this.setState({
                                        dataList: months,
                                        title: 'Chọn tháng',
                                        functionPropIDHere: `${item}.month_start`
                                    });

                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()

                                }}>
                                    <View style={{
                                        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                    }}>
                                        <View>
                                            <Text
                                                style={this.state.arrayvar[`${item}.month_start`] ? styles.textBody : {
                                                    fontSize: 15,
                                                    color: '#E0E0E0',
                                                    fontWeight: '500'
                                                }}>{this.state.arrayvar[`${item}.month_start`] ? this.state.arrayvar[`${item}.month_start`] : 'Tháng'}</Text>
                                        </View>
                                        <Field type="text" name={`${item}.month_start`}
                                               component={this.renderPickerInput}>
                                        </Field>
                                        <Animated.Image style={styles.arrow_item}
                                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                        />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                    await this.setState({
                                        dataList: this.years,
                                        title: 'Chọn năm',
                                        functionPropIDHere: `${item}.year_start`
                                    });

                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()

                                }}>
                                    <View style={{
                                        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                    }}>
                                        <View>
                                            <Text
                                                style={this.state.arrayvar[`${item}.year_start`] ? styles.textBody : {
                                                    fontSize: 15,
                                                    color: '#E0E0E0',
                                                    fontWeight: '500'
                                                }}>{this.state.arrayvar[`${item}.year_start`] ? this.state.arrayvar[`${item}.year_start`] : 'Năm'}</Text>
                                        </View>
                                        <Field type="text" name={`${item}.year_start`}
                                               component={this.renderPickerInput}>
                                        </Field>
                                        <Animated.Image style={styles.arrow_item}
                                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Kết thúc
                            </Text>

                            <View style={{flex: 1, flexDirection: 'row'}}>
                                <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                    await this.setState({
                                        dataList: months,
                                        title: 'Chọn tháng',
                                        functionPropIDHere: `${item}.month_end`
                                    });

                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()

                                }}>
                                    <View style={{
                                        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                    }}>
                                        <View>
                                            <Text
                                                style={this.state.arrayvar[`${item}.month_end`] ? styles.textBody : {
                                                    fontSize: 15,
                                                    color: '#E0E0E0',
                                                    fontWeight: '500'
                                                }}>{this.state.arrayvar[`${item}.month_end`] ? this.state.arrayvar[`${item}.month_end`] : 'Tháng'}</Text>
                                        </View>
                                        <Field type="text" name={`${item}.month_end`}
                                               component={this.renderPickerInput}>
                                        </Field>
                                        <Animated.Image style={styles.arrow_item}
                                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                        />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                    await this.setState({
                                        dataList: this.years,
                                        title: 'Chọn năm',
                                        functionPropIDHere: `${item}.year_end`
                                    });

                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()

                                }}>
                                    <View style={{
                                        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                    }}>
                                        <View>
                                            <Text
                                                style={this.state.arrayvar[`${item}.year_end`] ? styles.textBody : {
                                                    fontSize: 15,
                                                    color: '#E0E0E0',
                                                    fontWeight: '500'
                                                }}>{this.state.arrayvar[`${item}.year_end`] ? this.state.arrayvar[`${item}.year_end`] : 'Năm'}</Text>
                                        </View>
                                        <Field type="text" name={`${item}.year_end`}
                                               component={this.renderPickerInput}>
                                        </Field>
                                        <Animated.Image style={styles.arrow_item}
                                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <TouchableOpacity onPress={async () => {
                            await this.setState({
                                dataList: this.props.listMajor,
                                title: 'Chuyên ngành',
                                functionPropIDHere: `${item}.major_id`
                            });

                            await this.refs.listModel.changeDataList()
                            await this.refs.listModel.showAddModal()

                        }}>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.textAdvanced}>
                                    Chuyên ngành
                                </Text>

                                <View>
                                    <Text numberOfLines={1}
                                          style={this.state.arrayvar[`${item}.major_id`] ? [styles.textBody, {width: 200}] : {
                                              fontSize: 15,
                                              color: '#E0E0E0',
                                              fontWeight: '500'
                                          }}>{this.state.arrayvar[`${item}.major_id`] ? this.state.arrayvar[`${item}.major_id`] : 'Chọn chuyên ngành'}</Text>
                                </View>
                                <Field type="text" name={`${item}.major_id`}
                                       component={this.renderPickerInput}>
                                </Field>
                                <Right style={styles.arrow_item}>
                                    <Animated.Image
                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                    />
                                </Right>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={async () => {
                            await this.setState({
                                dataList: this.props.listDegree,
                                title: 'Bằng cấp',
                                functionPropIDHere: `${item}.degree_id`
                            });

                            await this.refs.listModel.changeDataList()
                            await this.refs.listModel.showAddModal()

                        }}>
                            <View style={[styles.boxAdvanced]}>
                                <Text style={styles.textAdvanced}>
                                    Bằng cấp
                                </Text>

                                <View>

                                    <Text
                                        style={this.state.arrayvar[`${item}.degree_id`] ? styles.textBody : {
                                            fontSize: 15,
                                            color: '#E0E0E0',
                                            fontWeight: '500'
                                        }}>{this.state.arrayvar[`${item}.degree_id`] ? this.state.arrayvar[`${item}.degree_id`] : 'Loại tốt nghiệp'}</Text>
                                </View>
                                <Field type="text" name={`${item}.degree_id`}
                                       component={this.renderPickerInput}>
                                </Field>
                                <Right style={styles.arrow_item}>
                                    <Animated.Image
                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                    />
                                </Right>
                            </View>
                        </TouchableOpacity>

                    </View>
                ))}

                <TouchableOpacity onPress={() => fields.push({})}>
                    <View style={[styles.boxAdvanced]}>

                        <View style={{
                            width: 150,
                            left: 20
                        }}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/add.png')}
                            />
                        </View>

                        <View>
                            <Text style={{fontSize: 15, color: '#33B8E0', fontWeight: '500'}}>Thêm
                                học
                                vấn</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    };

    renderExp = ({fields}) => {
        return (<View style={{
            top: 50, marginBottom: 200
        }}>
            {fields.map((item, index) => (
                <View key={index} style={index > 0 ? {marginTop: 30} : ''}>
                    <View style={[styles.boxAdvanced, {borderTopWidth: 1}]}>
                        <Text style={styles.textAdvanced}>
                            Tên công ty
                        </Text>
                        <Field type={'text'} name={`${item}.company_name`} component={this.renderInput}
                               style={styles.textPlaceholder}
                               placeholder="Nhập tên công ty">
                        </Field>
                    </View>

                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Bắt đầu
                        </Text>

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                await this.setState({
                                    dataList: months,
                                    title: 'Chọn tháng',
                                    functionPropIDHere: `${item}.month_start`
                                });

                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()

                            }}>
                                <View style={{
                                    flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                }}>
                                    <View>
                                        <Text
                                            style={this.state.arrayvar[`${item}.month_start`] ? styles.textBody : {
                                                fontSize: 15,
                                                color: '#E0E0E0',
                                                fontWeight: '500'
                                            }}>{this.state.arrayvar[`${item}.month_start`] ? this.state.arrayvar[`${item}.month_start`] : 'Tháng'}</Text>
                                    </View>
                                    <Field type="text" name={`${item}.month_start`}
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Animated.Image style={styles.arrow_item}
                                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                await this.setState({
                                    dataList: this.years,
                                    title: 'Chọn năm',
                                    functionPropIDHere: `${item}.year_start`
                                });

                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()

                            }}>
                                <View style={{
                                    flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                }}>
                                    <View>
                                        <Text
                                            style={this.state.arrayvar[`${item}.year_start`] ? styles.textBody : {
                                                fontSize: 15,
                                                color: '#E0E0E0',
                                                fontWeight: '500'
                                            }}>{this.state.arrayvar[`${item}.year_start`] ? this.state.arrayvar[`${item}.year_start`] : 'Năm'}</Text>
                                    </View>
                                    <Field type="text" name={`${item}.year_start`}
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Animated.Image style={styles.arrow_item}
                                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Kết thúc
                        </Text>

                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                await this.setState({
                                    dataList: months,
                                    title: 'Chọn tháng',
                                    functionPropIDHere: `${item}.month_end`
                                });

                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()

                            }}>
                                <View style={{
                                    flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                }}>
                                    <View>
                                        <Text
                                            style={this.state.arrayvar[`${item}.month_end`] ? styles.textBody : {
                                                fontSize: 15,
                                                color: '#E0E0E0',
                                                fontWeight: '500'
                                            }}>{this.state.arrayvar[`${item}.month_end`] ? this.state.arrayvar[`${item}.month_end`] : 'Tháng'}</Text>
                                    </View>
                                    <Field type="text" name={`${item}.month_end`}
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Animated.Image style={styles.arrow_item}
                                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flex: 1}} onPress={async () => {
                                await this.setState({
                                    dataList: this.years,
                                    title: 'Chọn năm',
                                    functionPropIDHere: `${item}.year_end`
                                });

                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()

                            }}>
                                <View style={{
                                    flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                                }}>
                                    <View>
                                        <Text
                                            style={this.state.arrayvar[`${item}.year_end`] ? styles.textBody : {
                                                fontSize: 15,
                                                color: '#E0E0E0',
                                                fontWeight: '500'
                                            }}>{this.state.arrayvar[`${item}.year_end`] ? this.state.arrayvar[`${item}.year_end`] : 'Năm'}</Text>
                                    </View>
                                    <Field type="text" name={`${item}.year_end`}
                                           component={this.renderPickerInput}>
                                    </Field>
                                    <Animated.Image style={styles.arrow_item}
                                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                    />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Chuyên ngành
                        </Text>
                        <Field type={'text'} name={`${item}.position_name`} component={this.renderInput}
                               style={styles.textPlaceholder}
                               placeholder="Nhập chuyên ngành">
                        </Field>
                    </View>
                </View>
            ))}

            <TouchableOpacity onPress={() => fields.push({})}>
                <View style={[styles.boxAdvanced]}>
                    <View style={{
                        width: 150,
                        left: 20
                    }}>
                        <Animated.Image
                            source={require('../../../vendor/images/profile/add.png')}
                        />
                    </View>

                    <View>
                        <Text style={{fontSize: 15, color: '#33B8E0', fontWeight: '500'}}>Thêm
                            kinh
                            nghiệm</Text>
                    </View>

                </View>
            </TouchableOpacity>
        </View>);
    };

    renderSkill = ({fields}) => {
        return (<View style={{
            top: 50, marginBottom: 200
        }}>
            {fields.map((item, index) => (
                <View key={index} style={index > 0 ? {marginTop: 30} : ''}>
                    <TouchableOpacity onPress={async () => {
                        await this.setState({
                            dataList: this.props.listSkill,
                            title: 'Kỹ năng',
                            functionPropIDHere: `${item}.skill_name`
                        });

                        await this.refs.listModel.changeDataList();
                        await this.refs.listModel.showAddModal()

                    }}>
                        <View style={[styles.boxAdvanced, {borderTopWidth: 1}]}>
                            <Text style={styles.textAdvanced}>
                                Kỹ năng
                            </Text>

                            <View>
                                <Text
                                    style={this.state.arrayvar[`${item}.skill_name`] ? styles.textBody : {
                                        fontSize: 15,
                                        color: '#E0E0E0',
                                        fontWeight: '500'
                                    }}>{this.state.arrayvar[`${item}.skill_name`] ? this.state.arrayvar[`${item}.skill_name`] : 'Chọn kỹ năng'}</Text>
                            </View>
                            <Field type="text" name={`${item}.skill_name`}
                                   component={this.renderPickerInput}>
                            </Field>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={async () => {
                        await this.setState({
                            dataList: listLevel,
                            title: 'Thành thạo',
                            functionPropIDHere: `${item}.level`
                        });

                        await this.refs.listModel.changeDataList();
                        await this.refs.listModel.showAddModal()

                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Thành thạo
                            </Text>

                            <View>
                                <Text
                                    style={this.state.arrayvar[`${item}.level`] ? styles.textBody : {
                                        fontSize: 15,
                                        color: '#E0E0E0',
                                        fontWeight: '500'
                                    }}>{this.state.arrayvar[`${item}.level`] ? this.state.arrayvar[`${item}.level`] : 'Mức độ thành thạo'}</Text>
                            </View>
                            <Field type="text" name={`${item}.level`}
                                   component={this.renderPickerInput}>
                            </Field>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                </View>
            ))}

            <TouchableOpacity onPress={() => fields.push({})}>
                <View style={[styles.boxAdvanced]}>
                    <View style={{
                        width: 150,
                        left: 20
                    }}>
                        <Animated.Image
                            source={require('../../../vendor/images/profile/add.png')}
                        />
                    </View>
                    <View>
                        <Text style={{fontSize: 15, color: '#33B8E0', fontWeight: '500'}}>Thêm
                            kỹ năng</Text>
                    </View>

                </View>
            </TouchableOpacity>
        </View>);
    };

    renderInput = ({label, placeholder, type, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View>
                <Input placeholder={placeholder} placeholderTextColor={'#E0E0E0'}
                       style={[styles.textPlaceholder, {height: 40, paddingLeft: 0}]}
                       keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                       value={`${value}`}
                />
                {touched && ((error && toastError(error)) ||
                    (warning && toastWarning(warning)))}
            </View>
        );
    };

    renderPickerTextarea = ({label, placeholder, keyboardType, meta: {touched, error, warning}, input: {onChange, ...restInput}}) => {

        return (
            <View>
                <Textarea rowSpan={6} bordered placeholder={placeholder} keyboardType={keyboardType}
                          onChangeText={onChange} {...restInput} style={[StylesAll.textAdvanced2, {
                    marginTop: 0,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                    borderTopWidth: 0,
                    borderBottomWidth: 0
                }]}/>
                {touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||
                    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}
            </View>
        );
    };

    renderPickerInput = ({label, placeholder, type, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {
        return (
            <View>
                <Input
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={[styles.textPlaceholder, {height: 40, paddingLeft: 0}]}
                    keyboardType={keyboardType} onChangeText={(value) => {
                    console.log(value)
                }} {...restInput}
                    editable={false}
                />
                {touched && ((error && toastError(error)) ||
                    (warning && toastWarning(warning)))}
            </View>
        );
    };

    renderPickerDate = ({input: {onChange, value, ...inputProps}, label, meta: {touched, error, warning}, ...restInput}) => {
        // let date = this.state.chosenDate;
        // let formattedDate = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        console.log('vietanh')

        return (
            <View>
                <DatePicker
                    style={{width: 150}}
                    date={this.state.date_of_birth === null ? '' : this.state.date_of_birth}
                    mode="date"
                    placeholder="Chọn ngày..."
                    format="DD/MM/YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    hideText={true}
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginRight: 20
                        },
                        dateInput: {
                            width: 0,
                            height: 0,
                            borderWidth: 0,
                            alignItems: 'flex-start',
                        },
                        placeholderText: {
                            color: '#5a5e62'
                        }
                    }}
                    onDateChange={value => {
                        this.setState({date_of_birth: value})
                        this.props.change('date_of_birth', value)
                    }}
                    {...restInput}
                />
                {touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||
                    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}
            </View>

        );
    };


    static
    navigationOptions = () => ({
        header: null
    });

    static
    navigationOptions = () => ({
        header: null
    });

    show() {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = {uri: response.uri};

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source,
                });
                this.props.onUpdateAvatar(response);
            }
        });
    }

    render() {
        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -60, -80],
            extrapolate: 'clamp',
        });


        return (
            <View style={[styles.fill]}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(ShowProfile)}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Tạo CV</Title>
                    </Body>
                    <Right>
                        <SubmitCVUser/>
                    </Right>
                </Header>

                {
                    this.state.bgButton === 1 ?
                        <ScrollView style={[styles.fill]}
                                    scrollEnabled={this.state.scrollEnabled}>
                            <Animated.View style={{
                                top: 70, marginBottom: 20
                            }}>
                                <Animated.Image
                                    style={{
                                        width: 160,
                                        height: 160,
                                        borderRadius: 163,
                                        borderWidth: 4,
                                        borderColor: "white",
                                        marginBottom: 10,
                                        alignSelf: 'center',
                                        backgroundColor: 'red'
                                    }}
                                    source={this.state.avatarSource === null ? (this.props.userInfo && this.props.userInfo.items.avatar !== '' ? {uri: `${this.props.userInfo.items.avatar}`} : require('../../../vendor/images/profile/avatar_default.png')) : this.state.avatarSource}
                                />
                                <Animated.View style={styles.bodyContent}>
                                    <TouchableOpacity onPress={() => this.show()}>
                                        <Animated.Text style={styles.name}>Thay đổi ảnh đại diện</Animated.Text>
                                    </TouchableOpacity>

                                </Animated.View>
                            </Animated.View>

                            <View style={{
                                top: 90, marginBottom: 200
                            }}>
                                <View style={[styles.boxAdvanced]}>
                                    <Text style={styles.textAdvanced}>
                                        Họ và tên
                                    </Text>
                                    <Field type={'text'} name="name" component={this.renderInput}
                                           style={styles.textPlaceholder}
                                           placeholder="Nhập họ tên">
                                    </Field>
                                </View>

                                <View style={[styles.boxAdvanced]}>
                                    <Text style={styles.textAdvanced}>
                                        Số điện thoại
                                    </Text>
                                    <Field type={'text'} name="phone" component={this.renderInput}
                                           style={styles.textPlaceholder}
                                           placeholder="Nhập số điện thoại">
                                    </Field>
                                </View>

                                <View style={[styles.boxAdvanced]}>
                                    <Text style={styles.textAdvanced}>
                                        Địa chỉ hiện tại
                                    </Text>
                                    <Field type={'text'} name="address" component={this.renderInput}
                                           style={styles.textPlaceholder}
                                           placeholder="Nhập địa chỉ hiện tại">
                                    </Field>
                                </View>


                                <View style={[styles.boxAdvanced]}>
                                    <Text style={styles.textAdvanced}>
                                        Ngày sinh
                                    </Text>
                                    <Text style={this.state.date_of_birth !== null ? styles.textBody : {
                                        color: '#E0E0E0', fontSize: 15,
                                        fontWeight: 'bold'
                                    }}>
                                        {this.state.date_of_birth === null ? 'Chọn ngày sinh' : this.state.date_of_birth}
                                    </Text>
                                    <Right>
                                        <Field type="text" name="date_of_birth" component={this.renderPickerDate}>
                                        </Field>
                                    </Right>
                                </View>

                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: listGender,
                                        title: 'Giới tính',
                                        functionPropIDHere: 'gender'
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }}>
                                    <View style={[styles.boxAdvanced]}>
                                        <Text style={styles.textAdvanced}>
                                            Giới tính
                                        </Text>

                                        <View>
                                            <Text style={this.state.gender ? styles.textBody : {
                                                fontSize: 15,
                                                color: '#E0E0E0',
                                                fontWeight: '500'
                                            }}>{this.state.gender ? this.state.gender : "Chọn giới tính"}</Text>
                                        </View>
                                        <Field type="text" keyboardType={'numeric'} name="gender"
                                               component={this.renderPickerInput}>
                                        </Field>
                                        <Right style={styles.arrow_item}>
                                            <Animated.Image
                                                source={require('../../../vendor/images/profile/arrow_down.png')}
                                            />
                                        </Right>
                                    </View>
                                </TouchableOpacity>

                                <View style={[styles.boxAdvanced]}>
                                    <Text style={styles.textAdvanced}>
                                        Email
                                    </Text>
                                    <Field type={'text'} name="email" component={this.renderInput}
                                           style={styles.textPlaceholder}
                                           placeholder="Nhập địa chỉ email">
                                    </Field>
                                </View>

                                <TouchableOpacity onPress={async () => {
                                    await this.setState({
                                        dataList: listStatusMarital,
                                        title: 'Hôn nhân',
                                        functionPropIDHere: 'is_married'
                                    });
                                    await this.refs.listModel.changeDataList()
                                    await this.refs.listModel.showAddModal()
                                }}>
                                    <View style={[styles.boxAdvanced]}>
                                        <Text style={styles.textAdvanced}>
                                            Hôn nhân
                                        </Text>

                                        <View>
                                            <Text style={this.state.is_married ? styles.textBody : {
                                                fontSize: 15,
                                                color: '#E0E0E0',
                                                fontWeight: '500'
                                            }}>{this.state.is_married ? this.state.is_married : 'Chọn TTHN'}</Text>
                                        </View>
                                        <Field type="text" name="is_married"
                                               component={this.renderPickerInput}>
                                        </Field>
                                        <Right style={styles.arrow_item}>
                                            <Animated.Image
                                                source={require('../../../vendor/images/profile/arrow_down.png')}
                                            />
                                        </Right>
                                    </View>
                                </TouchableOpacity>

                                <View style={{
                                    borderRadius: 12,
                                    borderColor: '#D0C9D6',
                                    borderWidth: 1,
                                    margin: 20,
                                }}>
                                    <Field name="job_desire.short_description" component={this.renderPickerTextarea}
                                           style={[styles.textPlaceholder]}
                                           placeholder="Tóm tắt sơ qua về bản thân">
                                    </Field>
                                </View>

                            </View>
                        </ScrollView>
                        : (
                            this.state.bgButton === 2 ?
                                <ScrollView style={[styles.fill]}
                                            scrollEnabled={this.state.scrollEnabled}>
                                    <FieldArray name="educations" component={this.renderEducations} rerenderOnEveryChange/>
                                </ScrollView> : (
                                    this.state.bgButton === 3 ?
                                        <ScrollView style={[styles.fill]}
                                                    scrollEnabled={this.state.scrollEnabled}>
                                            <FieldArray name="experiences" component={this.renderExp} rerenderOnEveryChange/>

                                        </ScrollView> : (
                                            this.state.bgButton === 4 ?
                                                <ScrollView style={[styles.fill]}
                                                            scrollEnabled={this.state.scrollEnabled}>
                                                    <View style={{
                                                        top: 50, marginBottom: 200
                                                    }}>

                                                        <TouchableOpacity onPress={async () => {
                                                            await this.setState({
                                                                dataList: this.props.carrer,
                                                                title: 'Chọn ngành nghề',
                                                                functionPropIDHere: 'job_desire.carrer_id'
                                                            });
                                                            await this.refs.listModel.changeDataList()
                                                            await this.refs.listModel.showAddModal()
                                                        }}>
                                                            <View style={[styles.boxAdvanced]}>
                                                                <Text style={styles.textAdvanced}>
                                                                    Ngành nghề
                                                                </Text>

                                                                <View>
                                                                    <Text
                                                                        style={this.state.arrayvar['job_desire.carrer_id'] ? styles.textBody : {
                                                                            fontSize: 15,
                                                                            color: '#E0E0E0',
                                                                            fontWeight: '500'
                                                                        }}>{this.state.arrayvar['job_desire.carrer_id'] ? this.state.arrayvar['job_desire.carrer_id'] : 'Chọn ngành nghề'}</Text>
                                                                </View>
                                                                <Field type="text" keyboardType={'numeric'}
                                                                       name="job_desire.carrer_id"
                                                                       component={this.renderPickerInput}>
                                                                </Field>
                                                                <Right style={styles.arrow_item}>
                                                                    <Animated.Image
                                                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                                                    />
                                                                </Right>
                                                            </View>
                                                        </TouchableOpacity>


                                                        <TouchableOpacity onPress={async () => {
                                                            await this.setState({
                                                                dataList: this.props.province,
                                                                title: 'Chọn nơi làm việc',
                                                                functionPropIDHere: 'job_desire.province_id',
                                                            });
                                                            await this.refs.listModel.changeDataList()
                                                            await this.refs.listModel.showAddModal()
                                                        }}>
                                                            <View style={[styles.boxAdvanced]}>
                                                                <Text style={styles.textAdvanced}>
                                                                    Nơi làm việc
                                                                </Text>
                                                                <View>
                                                                    <Text
                                                                        style={this.state.arrayvar['job_desire.province_id'] ? styles.textBody : {
                                                                            fontSize: 15,
                                                                            color: '#E0E0E0',
                                                                            fontWeight: '500'
                                                                        }}>{this.state.arrayvar['job_desire.province_id'] ? this.state.arrayvar['job_desire.province_id'] : 'Chọn nơi làm việc'}</Text>
                                                                </View>
                                                                <Field type="text" keyboardType={'numeric'}
                                                                       name="job_desire.province_id"
                                                                       component={this.renderPickerInput}></Field>
                                                                <Right style={styles.arrow_item}>
                                                                    <Animated.Image
                                                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                                                    />
                                                                </Right>
                                                            </View>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={async () => {
                                                            await this.setState({
                                                                dataList: this.props.salary,
                                                                title: 'Chọn mức lương',
                                                                functionPropIDHere: 'job_desire.job_salary_id'
                                                            });
                                                            await this.refs.listModel.changeDataList()
                                                            await this.refs.listModel.showAddModal()
                                                        }}>
                                                            <View style={[styles.boxAdvanced]}>
                                                                <Text style={styles.textAdvanced}>
                                                                    Mức lương
                                                                </Text>
                                                                <View>
                                                                    <Text
                                                                        style={this.state.arrayvar['job_desire.job_salary_id'] ? styles.textBody : {
                                                                            fontSize: 15,
                                                                            color: '#E0E0E0',
                                                                            fontWeight: '500'
                                                                        }}>{this.state.arrayvar['job_desire.job_salary_id'] ? this.state.arrayvar['job_desire.job_salary_id'] : 'Chọn mức lương'}</Text>
                                                                </View>
                                                                <Field type="text" keyboardType={'numeric'}
                                                                       name="job_desire.job_salary_id"
                                                                       component={this.renderPickerInput}></Field>
                                                                <Right style={styles.arrow_item}>
                                                                    <Animated.Image
                                                                        source={require('../../../vendor/images/profile/arrow_down.png')}
                                                                    />
                                                                </Right>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </ScrollView> :
                                                <ScrollView style={[styles.fill]}
                                                            scrollEnabled={this.state.scrollEnabled}>
                                                    <FieldArray name="skills" component={this.renderSkill} rerenderOnEveryChange/>
                                                </ScrollView>
                                        )
                                )
                        )
                }

                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                // { scale: titleScale },
                                {translateY: titleTranslate},
                            ],
                        },
                    ]}
                >
                    <FlatList
                        horizontal={true}
                        data={menu}
                        showsHorizontalScrollIndicator={false}
                        ref={(ref) => {
                            this.flatListRef = ref;
                        }}
                        style={{
                            backgroundColor: '#fff',
                            borderBottomWidth: 1,
                            borderTopWidth: 1,
                            borderColor: '#ECEBED',
                            width: '100%'
                        }}
                        keyExtractor={(item, index) => item.id.toString()}
                        renderItem={({item, index}) =>
                            <View style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: this.state.bgButton === item.id ? '#33B8E0' : '#FFFFFF',
                            }}>
                                <TouchableOpacity style={styles.ScrollTextContainer} onPress={() => {
                                    this.flatListRef.scrollToIndex({animated: true, index: index});
                                    this.setState({
                                        bgButton: item.id
                                    })
                                }}>
                                    <Text style={{
                                        fontSize: 16,
                                        fontWeight: 'bold',
                                        color: this.state.bgButton === item.id ? '#FFFFFF' : '#D9D3DE'
                                    }}>{item.name}</Text>
                                </TouchableOpacity>
                            </View>


                        }
                    />
                </Animated.View>

                {this.state.dataList !== '' ?
                    <ListDetailModal
                        ref={'listModel'}
                        dataList={this.state.dataList}
                        title={this.state.title}
                        functionPropIDHere={(value) => {
                            this.props.change(this.state.functionPropIDHere, `${value}`);

                            let key = this.state.functionPropIDHere;

                            this.state.dataList.map((item) => {
                                if (item.id === parseInt(`${value}`)) {
                                    this.arrayvar[key] = item.name;
                                    this.setState({arrayvar: this.arrayvar});
                                }
                            });

                            //neu chon truong reder list nganh
                            if (key.includes("university_id")) {
                                this.props.fetchMajor(`${value}`)
                            }

                            console.log(this.state.arrayvar)

                        }}
                        PropNameHere={(value) => {
                            if (this.state.functionPropIDHere === 'carrer') {
                                this.setState({carrer: `${value}`})
                            } else if (this.state.functionPropIDHere === 'province') {
                                this.setState({province: `${value}`})
                            } else if (this.state.functionPropIDHere === 'salary') {
                                this.setState({salary: `${value}`})
                            } else if (this.state.functionPropIDHere === 'experience') {
                                this.setState({experience: `${value}`})
                            } else if (this.state.functionPropIDHere === 'jobTypes') {
                                this.setState({jobTypes: `${value}`})
                            } else if (this.state.functionPropIDHere === 'gender') {
                                this.setState({gender: `${value}`})
                            } else if (this.state.functionPropIDHere === 'is_married') {
                                this.setState({is_married: `${value}`})
                            }
                        }}
                    />
                    : null}

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userInfo: state.userInfoReducers,
        userCv: state.cvReducers,
        user: state.loginReducers,
        province: state.provinceReducers,
        salary: state.salaryReducers,
        carrer: state.carrerReducers,
        jobTypes: state.jobTypesReducers,
        experience: state.experienceReducers,
        listUniversity: state.universityReducers,
        listMajor: state.majorReducers,
        listDegree: state.degreeReducers,
        listSkill: state.skillReducers
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        fetchMajor: (universityId) => {
            dispatch(FetchListMajor(universityId));
        },
        onUpdateAvatar: (params) => {
            dispatch(UpdateAvatarUser(params))
        },
    };
};


const cvUser = connect(mapStateToProps, mapDispatchToProps)(CVFullComponent);
//
export default reduxForm({
    form: FORM_CV, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    validate,
    onSubmit: submitEditCV,
})(cvUser)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4',
        flexDirection: 'column'

    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 80,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 50,
        height: 38,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#D0C9D6',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    textAdvanced: {
        width: 150,
        left: 20,
        fontSize: 15,
        color: '#33B8E0',
        fontWeight: '500',
    },
    textBody: {
        fontSize: 15,
        color: '#525252',
        fontWeight: '500'
    },
    textPlaceholder: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },
    wrapper: {
        paddingTop: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal4: {
        height: 400,
    },
    text: {
        color: 'black',
        fontSize: 16,
        textAlign: 'left',
        left: 10,
        marginBottom: 10
    },
    button: {
        backgroundColor: 'green',
        width: 300,
        marginTop: 16,
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        color: '#d0d0d0',
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF'
    }
});

