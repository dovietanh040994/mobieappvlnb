import {createSwitchNavigator, createStackNavigator, createAppContainer} from 'react-navigation';
import EditProfileComponent from "./EditProfileComponent";
import JobApplyComponent from "./JobApplyComponent";
import CvComponent from "./CVFullComponent";
import PolicyComponent from "./PolicyComponent";
import ProfileComponent from "./ProfileComponent";
import JobCareComponent from "./JobCareComponent";
import ProfileJobDetail from "./ProfileJobDetail";
import LoginComponent from "../Login/LoginComponent";
import React from "react";
import {ShowProfile} from "../../../vendor/screen";
import SinginComponent from "../Login/SinginComponent";
import VerifyComponent from "../Login/VerifyComponent";
import VerifyResetPassComponent from "../Login/VerifyResetPassComponent";
import ResetPassComponent from "../Login/ResetPassComponent";



const ProfileNavigator = createStackNavigator(
    {

        ShowProfile: {
            screen: ProfileComponent,

        },
        EditProfile: {
            screen: EditProfileComponent,

        },
        JobApply: {
            screen: JobApplyComponent,

        },
        JobCare: {
            screen: JobCareComponent
        },
        JobCv: {
            screen: CvComponent,

        },
        Policy: {
            screen: PolicyComponent,

        },
        ProfileJobDetail: {
            screen: ProfileJobDetail,

        },
        Login: {
            screen: LoginComponent,

        },
        ResetPass: {
            screen: ResetPassComponent,

        },
        Singin: {
            screen: SinginComponent,

        },
        Verify: {
            screen: VerifyComponent,

        },
        VerifyResetPass: {
            screen: VerifyResetPassComponent,

        },
    },
);

export default createAppContainer(ProfileNavigator);
