import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, TextInput, Dimensions, BackHandler
} from 'react-native';
import {
    Body,
    CardItem,
    Container,
    Left,
    Right,
    Switch,
    Button,
    Input,
    Picker,
    Icon,
    Title,
    Header,
    Toast
} from "native-base";
import {StylesAll} from "../../../vendor/styles";
import {Field, reduxForm, SubmissionError, submit} from 'redux-form';
import {USER_FORM, FORM_EDIT} from "../../../vendor/formNames";
import DatePicker from "react-native-datepicker";
import Modal from 'react-native-modalbox';
import {connect} from "react-redux";
import {AdvancedDetail, Home, HomeBox, Search, ShowProfile} from "../../../vendor/screen";
import {FetchDetailUser, SwitchNotify, FetchProvinceList, EditUserAction, UpdateAvatarUser} from "../../../actions";
import SubmitEditUser from './SubmitEditUser'
import NavigationService from "../../../vendor/NavigationService";
import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select Avatar',
    // customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

var screen = Dimensions.get('window');
var screenWidth = Dimensions.get('window').width;

const submitEditForm = async (values, dispatch) => {

    console.log(values);

    if (!values.name || values.name.trim().length <= 0) {
        toastError('Tên không được để trống');
    } else if (values.name.length > 20) {
        toastError('Tên không được vượt quá 20 kí tự');
    } else if (!values.province || values.province === 0) {
        toastError('Tỉnh/thành phố không được để trống');
    } else if (!values.email || values.email.trim().length <= 0) {
        toastError('Email không được để trống');
    } else if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        toastError('Email không đúng định dạng');
    } else if (!values.phone || values.phone.trim().length <= 0) {
        toastError('Số điện thoại không được để trống');
    } else if (isNaN(Number(values.phone))) {
        toastError('Số điện thoại phải là số');
    } else if (values.date === null) {
        toastError('Ngày sinh không được để trống');
    } else if (!values.gender) {
        toastError('Giới tính không được để trống');
    } else {
        await dispatch(EditUserAction(values));

        await Toast.show({
            text: "Cập nhật thông tin thành công!",
            buttonText: "Đóng",
            type: "success",
            position: 'top',
        })
        await NavigationService.navigate(ShowProfile);
    }


};

const listGender = [
    {
        id: 1,
        name: "Nam",
    },
    {
        id: 2,
        name: "Nữ",
    }
];

const toastError = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};
const toastWarning = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};

const validate = values => {

    const errors = {};

    return errors

};

export class EditProfileComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            enableScrollViewScroll: true,

            selectedProvinceName: this.props.user.length !== 0 && this.props.user.items.province_name !== 0 ? this.props.user.items.province_name : "Chọn tỉnh thành",
            selectedProvince: this.props.user.length !== 0 && this.props.user.items.province_id !== 0 ? this.props.user.items.province_id : 0,
            selectedGender: this.props.user.items.gender !== '' && this.props.user.items.gender !== 'undefined' ? this.props.user.items.gender : 'Chọn giới tinh',
            date: this.props.user.items.date_of_birth !== '' && this.props.user.items.date_of_birth !== 'undefined' ? this.props.user.items.date_of_birth : null,
            avatarSource: null
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('ShowProfile', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(ShowProfile)
        return true;

    }

    componentWillMount() {

        const {navigation} = this.props;

        const token = navigation.getParam('token', 'NO-ID');

        //tạo đầu vào mặc định cho trường trong redux-form
        this.props.initialize({
            token: token,
            name: this.props.user.length !== 0 && this.props.user.items.name !== 'undefined' ? this.props.user.items.name : '',
            province: this.props.user.items.province_id !== 0 ? this.props.user.items.province_id : 0,
            email: this.props.user.length !== 0 && this.props.user.items.email !== 'undefined' ? this.props.user.items.email : '',
            phone: this.props.user.length !== 0 && this.props.user.items.phone !== 'undefined' ? this.props.user.items.phone : '',
            gender: this.props.user.items.gender !== '' && this.props.user.items.gender !== 'undefined' ? this.props.user.items.gender : '',
            date: this.props.user.items.date_of_birth !== '' && this.props.user.items.date_of_birth !== 'undefined' ? this.props.user.items.date_of_birth : null,
            address: '',
        });

        BackHandler.addEventListener('ShowProfile', this.handleBackButtonClick);
    }

    onEnableScroll = (value) => {
        this.setState({
            enableScrollViewScroll: value,
        });
    };

    renderPickerInput = ({label, placeholder, type, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View>
                <Input placeholder={placeholder}
                       style={[styles.textPlaceholder, {height: 40, paddingLeft: 0}]}
                       keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                       value={`${value}`}
                />
                {touched && ((error && toastError(error)) ||
                    (warning && toastWarning(warning)))}
            </View>
        );
    }

    renderPickerSelect = ({label, placeholder, type, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {
        return (
            <View>
                <Input underlineColorAndroid={'transparent'}
                       placeholder={placeholder}
                       style={{opacity: 0, position: 'absolute'}}
                       keyboardType={keyboardType} onChangeText={onChange} {...restInput} editable={false}
                       value={`${value}`}
                />
                {touched && ((error && toastError(error)) ||
                    (warning && toastWarning(warning)))}
            </View>
        );
    }

    renderPickerDate = ({input: {onChange, value, ...inputProps}, label, meta: {touched, error, warning}, ...restInput}) => {
        // let date = this.state.chosenDate;
        // let formattedDate = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        return (
            <View>
                <DatePicker
                    style={{width: 200}}
                    date={this.state.date === 'null' ? '' : this.state.date}
                    mode="date"
                    placeholder="Chọn ngày..."
                    format="DD/MM/YYYY"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    hideText={true}
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginRight: 10
                        },
                        dateInput: {
                            width: 0,
                            height: 0,
                            borderWidth: 0,
                            alignItems: 'flex-start',
                        },
                        placeholderText: {
                            color: '#5a5e62'
                        }
                    }}
                    onDateChange={value => {
                        this.setState({date: value})
                        this.props.change('date', value)
                    }}
                    {...restInput}
                />
                {touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||
                    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}
            </View>

        );
    };

    static navigationOptions = () => ({
        header: null
    });

    show() {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = {uri: response.uri};

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source,
                });
                this.props.onUpdateAvatar(response);
            }
        });
    }


    render() {

        return (
            <ScrollView scrollEnabled={this.state.enableScrollViewScroll}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(ShowProfile)}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Chỉnh sửa thông tin</Title>
                    </Body>
                    <Right>
                        <SubmitEditUser/>
                    </Right>
                </Header>

                <View style={[styles.fill, {marginTop: 20, marginBottom: 40}]}>
                    <Animated.View>
                        <View>
                            <Animated.Image
                                style={{
                                    width: 160,
                                    height: 160,
                                    borderRadius: 163,
                                    borderWidth: 4,
                                    borderColor: "white",
                                    marginBottom: 10,
                                    alignSelf: 'center',
                                    backgroundColor: 'red'
                                }}
                                source={this.state.avatarSource === null ? (this.props.user && this.props.user.items.avatar !== '' ? {uri: `${this.props.user.items.avatar}`} : require('../../../vendor/images/profile/avatar_default.png')) : this.state.avatarSource}
                            />
                            <Animated.View style={styles.bodyContent}>
                                <TouchableOpacity onPress={() => this.show()}>
                                    <Animated.Text style={styles.name}>Thay đổi ảnh đại diện</Animated.Text>
                                </TouchableOpacity>

                            </Animated.View>
                        </View>
                    </Animated.View>

                    <View style={[styles.boxAdvanced, {marginTop: 20}]}>
                        <Text style={styles.textAdvanced}>
                            Tên
                        </Text>
                        <Field type={'text'} name="name" component={this.renderPickerInput}
                               style={styles.textPlaceholder}
                               placeholder="Nhập họ và tên...">
                        </Field>
                    </View>
                    {/*<View style={[styles.boxAdvanced]}>*/}
                    {/*    <Text style={styles.textAdvanced}>*/}
                    {/*        Mật khẩu*/}
                    {/*    </Text>*/}
                    {/*    <Field name="password" type={'password'} component={this.renderPickerInput}*/}
                    {/*           style={styles.textPlaceholder}*/}
                    {/*           placeholder="******">*/}
                    {/*    </Field>*/}
                    {/*</View>*/}
                    <TouchableOpacity onPress={() => {
                        this.refs.modelSelect.open();
                        this.props.onFetchProvinceList();
                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Tỉnh thành
                            </Text>
                            <Text style={styles.textBody}>
                                {this.state.selectedProvinceName}
                            </Text>
                            <Field type="text" keyboardType={'numeric'} name="province"
                                   component={this.renderPickerSelect}>
                            </Field>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>


                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Email
                        </Text>
                        <Field type={'email'} name="email" component={this.renderPickerInput}
                               style={styles.textPlaceholder}
                               placeholder="Nhập email...">
                        </Field>
                    </View>
                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Số điện thoại
                        </Text>
                        <Field type="text" keyboardType={'numeric'} name="phone" component={this.renderPickerInput}
                               style={styles.textPlaceholder} placeholder="Nhập số điện thoại...">
                        </Field>
                    </View>

                    <TouchableOpacity onPress={() => {
                        this.refs.gender.open();
                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Giới tính
                            </Text>
                            <Text style={styles.textBody}>
                                {this.state.selectedGender}
                            </Text>
                            <Field type="text" name="gender" component={this.renderPickerSelect}>
                            </Field>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>

                        </View>
                    </TouchableOpacity>


                    <View style={[styles.boxAdvanced]}>
                        <Text style={styles.textAdvanced}>
                            Ngày sinh
                        </Text>
                        <Text style={styles.textBody}>
                            {this.state.date}
                        </Text>
                        <Field type="text" name="date" component={this.renderPickerDate}>
                        </Field>
                    </View>
                </View>


                <Modal
                    style={[styles.modal]}
                    position={'center'}
                    ref={'modelSelect'}
                    swipeToClose={false}
                >
                    <View style={{
                        flew: 1,
                        flexDirection: 'column',
                        width: screen.width,
                        paddingTop: 30,
                        paddingBottom: 10
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            height: 50,
                            alignItems: 'center',
                            borderBottomWidth: 1,
                            borderColor: '#ccc',
                            marginBottom: 20
                        }}>
                            <Text style={styles.textAdvanced}>Tỉnh thành</Text>
                        </View>

                        <FlatList onTouchStart={() => {
                            this.onEnableScroll(false);
                        }}
                                  onMomentumScrollEnd={() => {
                                      this.onEnableScroll(true);
                                  }}
                                  data={this.props.provinces}
                                  renderItem={({item, index}) =>
                                      <TouchableOpacity onPress={() => {
                                          this.setState({
                                              selectedProvinceName: item.name,
                                              selectedProvince: item.id
                                          });
                                          this.props.change('province', parseInt(item.id))
                                          this.refs.modelSelect.close();
                                      }}>
                                          <Text style={styles.text} key={index}>
                                              {item.name}
                                          </Text>
                                      </TouchableOpacity>
                                  }
                                  keyExtractor={(item, index) => index.toString()}
                        />
                    </View>

                </Modal>
                <Modal
                    style={[styles.modal]}
                    position={'center'}
                    ref={'gender'}
                    swipeToClose={false}
                >
                    <View style={{flew: 1, flexDirection: 'column', width: screen.width}}>
                        <View style={{
                            flexDirection: 'row',
                            height: 50,
                            alignItems: 'center',
                            borderBottomWidth: 1,
                            borderColor: '#ccc',
                            marginBottom: 50
                        }}>
                            <Text style={styles.textAdvanced}>Giới tính</Text>
                        </View>

                        <FlatList onTouchStart={() => {
                            this.onEnableScroll(false);
                        }}
                                  onMomentumScrollEnd={() => {
                                      this.onEnableScroll(true);
                                  }}
                                  data={listGender}
                                  renderItem={({item, index}) =>
                                      <TouchableOpacity onPress={() => {
                                          this.setState({
                                              selectedGender: item.name,
                                          });
                                          this.props.change('gender', item.name)
                                          this.refs.gender.close();
                                      }}>
                                          <Text style={styles.text} key={index}>
                                              {item.name}
                                          </Text>
                                      </TouchableOpacity>
                                  }
                                  keyExtractor={(item, index) => index.toString()}
                        />
                    </View>

                </Modal>

            </ScrollView>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.loginReducers,
        provinces: state.provinceReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        onFetchProvinceList: () => {
            dispatch(FetchProvinceList())
        },
        onUpdateUser: (params) => {
            dispatch(EditUserAction(params))
        },
        onUpdateAvatar: (params) => {
            dispatch(UpdateAvatarUser(params))
        },
    };
}

const EditProfileUser = connect(mapStateToProps, mapDispatchToProps)(EditProfileComponent);
//
export default reduxForm({
    form: FORM_EDIT, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    validate,
    onSubmit: submitEditForm
})(EditProfileUser)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    textAdvanced: {
        width: 140,
        left: 20,
        fontSize: 17,
        color: '#33B8E0',
        fontWeight: 'normal',
    },
    textBody: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    textPlaceholder: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },
    wrapper: {
        paddingTop: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        alignItems: 'center',
        flex: 1,
    },
    modal4: {
        height: 400,
        width: screen.width
    },
    gender: {
        height: 200,
        width: screen.width
    },
    text: {
        color: 'black',
        fontSize: 16,
        textAlign: 'left',
        paddingLeft: 20,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderColor: '#ccc',
        height: 40,
        alignItems: 'center'
    },
    button: {
        backgroundColor: 'green',
        width: 300,
        marginTop: 16,
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        color: '#d0d0d0',
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF'
    }
});
