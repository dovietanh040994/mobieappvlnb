import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ScrollView, Alert
} from 'react-native';
import {Body, CardItem, Container, Left, Right, Switch, Button, ListItem} from "native-base";
import {StylesAll} from "../../../vendor/styles";
import {Field} from "redux-form";
import {withNavigation, withNavigationFocus} from 'react-navigation';
import {
    EditProfile,
    JobApply,
    JobCv,
    Policy,
    JobCare,
    Search,
    Login,
    HomeBox,
    HomeBoxEmployer,
    LoginEmployer
} from "../../../vendor/screen";
import {
    DeleteJobCare,
    SwitchNotify,
    FetchDetailUser,
    FetchCvUser,
    FetchUserInfo,
    FetchListUniversity, FetchListMajor, FetchListDegree, FetchListSkill
} from "../../../actions";
import {connect} from "react-redux";
import notifyReducers from "../../../reducers/NotifyReducers";
import ToggleSwitch from 'toggle-switch-react-native'
import firebase from "react-native-firebase";
import {LoginManager} from "react-native-fbsdk";
import {GoogleSignin} from "react-native-google-signin";
import AsyncStorage from "@react-native-community/async-storage";
import AdMob from "../../AdMob"

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


class ProfileComponent extends Component {

    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            switchNoti: true,
            userInfo: false,
            token: false,
        };
    }

    toggleSwitch = (value) => {
        if (this.state.userInfo) {
            this.props.onSwitchAction(value);
            this.props.fetchUserDetail(1);
        } else {
            this.props.navigation.navigate(Login, {
                onGoBack: () => this.refresh()
            });
        }
    };

    toggleSwitchNTD = () => {
        Alert.alert(
            'Thông báo',
            `Bạn muốn sử dụng app với vai trò là nhà tuyển dụng?`,
            [
                {
                    text: 'Đóng',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Xác nhận', onPress: () => {
                        AsyncStorage.getItem('userInfoEmployer', (err, result) => {
                            if (result !== null) {
                                this.props.navigation.navigate(HomeBoxEmployer);
                            } else {
                                this.props.navigation.navigate(LoginEmployer);
                            }
                        });
                    }
                },
            ],
            {cancelable: false},
        );

    };


    componentDidMount() {
        GoogleSignin.configure({
            webClientId: '188924080895-jihd852be68j03sdt4gdlop9qrm4572g.apps.googleusercontent.com',
        })

        const {navigation} = this.props;
        this.focusListener = navigation.addListener("didFocus", () => {
            AsyncStorage.getItem('userInfo', (err, result) => {
                console.log(898989111);
                console.log(result)
                if (result !== null) {
                    this.setState({userInfo: true, token: result});
                    this.props.fetchUserDetail(this.state.token);
                    this.props.onFetchCVUser(this.state.token);
                    this.props.onFetchUserInfo();
                    this.props.fetchUniversity();
                    this.props.fetchDegree();
                    this.props.fetchSkill();
                    this.props.fetchMajor('');
                } else {
                    this.setState({userInfo: false})
                }
            });
        });

        // const unitId = 'ca-app-pub-3717610865114273/3047817089';
        // const advert = firebase.admob().interstitial(unitId);
        // const AdRequest = firebase.admob.AdRequest;
        // const request = new AdRequest();
        // advert.loadAd(request.build());
        //
        // advert.on('onAdLoaded', () => {
        //     console.log('Advert ready to show.');
        //     advert.show();
        // });

    }

    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
    }

    //hàm logout
    logoutAcc = () => {
        firebase.auth().signOut().then(function () {
            // LoginManager.logOut()
            GoogleSignin.revokeAccess();
            GoogleSignin.signOut()
            console.log("dang xuat thanh cong")

        }).catch(function (error) {
            console.log(error)
            console.log("Đã có lỗi xảy ra trong quá trình logout. Xin thử lại")
        });
        AsyncStorage.removeItem('userInfo');
        AsyncStorage.removeItem('accessToken');
        firebase.auth().signOut();
        this.setState({userInfo: false})

    }

    refresh() {
        this.setState({userInfo: true})
    }

    render() {
        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 1],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 0, -55],
            extrapolate: 'clamp',
        });

        const {navigation} = this.props;

        const poinCheck = navigation.getParam('poinCheck', null);

        if (poinCheck === 1 && this.poinCheck === 0) {
            this.refresh()
        }

        return (

            <ScrollView>

                <View style={styles.fill}>
                    <Animated.View
                        pointerEvents="none"
                        style={[
                            styles.header,
                            {transform: [{translateY: headerTranslate}]},
                        ]}
                    >
                        <Animated.Image
                            style={[
                                styles.backgroundImage,
                                {
                                    opacity: imageOpacity,
                                    transform: [{translateY: imageTranslate}],
                                },
                            ]}
                            source={require('../../../vendor/images/profile/profile_header.png')}
                        />
                    </Animated.View>

                    <Animated.View
                        style={[
                            styles.bar,
                            {
                                transform: [
                                    {scale: titleScale},
                                    {translateY: titleTranslate},
                                ],
                            },
                        ]}
                    >
                        <Text style={{
                            fontWeight: '900',
                            color: '#fff',
                            fontSize: 25,
                            alignSelf: 'center',
                            marginTop: 50
                        }}>Hồ Sơ</Text>

                    </Animated.View>

                    <Animated.View>

                        <View style={{
                            flex: 1, flexDirection: 'column', alignItems: 'center', position: 'relative'
                        }}>

                            <Animated.Image
                                style={{
                                    width: 80,
                                    height: 80,
                                    borderRadius: 63,
                                    borderWidth: 4,
                                    borderColor: "white",
                                    marginBottom: 10,
                                    alignSelf: 'center',
                                    marginTop: 110,
                                }}
                                source={this.state.userInfo && this.props.user && this.props.user.avatar !== '' ? {uri: `${this.props.user.avatar}`} : require('../../../vendor/images/profile/avatar_default.png')}
                            />
                            <View style={{
                                alignSelf: 'center',
                                position: 'absolute',
                                marginTop: 110,
                                right: 0
                            }}>
                                <TouchableOpacity onPress={() => {
                                    if (this.state.userInfo) {
                                        this.props.navigation.navigate(EditProfile, {token: this.state.token});
                                    } else {
                                        this.props.navigation.navigate(Login, {
                                            onGoBack: () => this.refresh()
                                        });
                                    }
                                }}>
                                    <Animated.Image source={require('../../../vendor/images/profile/icon_edit.png')}
                                    />
                                </TouchableOpacity>
                            </View>

                            <Animated.View style={styles.bodyContent}>
                                <Animated.Text
                                    style={styles.name}>{this.state.userInfo && this.props.user ? (this.props.user.name !== '' ? this.props.user.name : this.props.user.email) : "Xin chào"}</Animated.Text>
                            </Animated.View>
                        </View>
                    </Animated.View>

                    <Animated.View>
                        {/*<AdMob/>*/}
                        <Animated.View style={{}}>

                            <TouchableOpacity onPress={() => {
                                if (this.state.userInfo) {
                                    this.props.navigation.navigate(JobCare, {token: this.state.token});
                                } else {
                                    this.props.navigation.navigate(Login, {
                                        onGoBack: () => this.refresh()
                                    });
                                }
                            }}>
                                <View style={styles.boxAdvanced}>
                                    <Text style={styles.title_item}>Việc làm quan tâm</Text>

                                    <Right style={styles.love_item}>
                                        <Animated.Image
                                            source={require('../../../vendor/images/profile/love.png')}
                                        />
                                    </Right>
                                    <Right style={styles.arrow_item}>
                                        <Animated.Image
                                            source={require('../../../vendor/images/profile/arrow_right.png')}
                                        />
                                    </Right>
                                </View>
                            </TouchableOpacity>
                        </Animated.View>


                        <Animated.View>

                            <TouchableOpacity onPress={() => {
                                if (this.state.userInfo) {
                                    this.props.navigation.navigate(JobApply, {token: this.state.token});
                                } else {
                                    this.props.navigation.navigate(Login, {
                                        onGoBack: () => this.refresh()
                                    });
                                }
                            }}>
                                <View style={styles.boxAdvanced}>
                                    <Text style={styles.title_item}>Việc làm đã ứng tuyển</Text>

                                    <Right style={styles.apply_item}>
                                        <Animated.Image
                                            source={require('../../../vendor/images/profile/apply.png')}
                                        />
                                    </Right>
                                    <Right style={styles.arrow_item}>
                                        <Animated.Image
                                            source={require('../../../vendor/images/profile/arrow_right.png')}
                                        />
                                    </Right>
                                </View>
                            </TouchableOpacity>

                        </Animated.View>
                        <Animated.View>
                            <View style={styles.boxAdvanced}>
                                <Text style={styles.title_item}>Thông báo</Text>
                                <Right style={{right: 10}}>
                                    <ToggleSwitch
                                        isOn={this.props.user ? (this.props.user.is_subscriber === 0 ? false : true) : true}
                                        onColor='#33B8E0'
                                        onToggle={this.toggleSwitch}
                                    />

                                    {/*<Switch onValueChange={this.toggleSwitch}*/}
                                    {/*        value={this.state.switchNoti} style={{right: 10}}/>*/}
                                </Right>
                            </View>
                        </Animated.View>

                        <Animated.View>
                            <View style={styles.boxAdvanced}>
                                <Text style={styles.title_item}>Chuyển quyền ứng dụng</Text>
                                <Right style={{right: 10}}>
                                    <ToggleSwitch
                                        isOn={true}
                                        onColor='#33B8E0'
                                        onToggle={this.toggleSwitchNTD}
                                    />

                                    {/*<Switch onValueChange={this.toggleSwitch}*/}
                                    {/*        value={this.state.switchNoti} style={{right: 10}}/>*/}
                                </Right>
                            </View>
                        </Animated.View>

                        <Animated.View>
                            <TouchableOpacity onPress={() => {
                                if (this.state.userInfo) {
                                    this
                                    this.props.navigation.navigate(JobCv, {token: this.state.token});
                                } else {
                                    this.props.navigation.navigate(Login, {
                                        onGoBack: () => this.refresh()
                                    });
                                }
                            }}>
                                <View style={styles.boxAdvanced}>
                                    <Text style={styles.title_item}>Tạo CV</Text>

                                    <Right>
                                        <Animated.Image style={styles.arrow_item}
                                                        source={require('../../../vendor/images/profile/arrow_right.png')}
                                        />
                                    </Right>
                                </View>
                            </TouchableOpacity>
                        </Animated.View>

                        <Animated.View>
                            <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate(Policy);
                            }}>
                                <View style={styles.boxAdvanced}>
                                    <Text style={styles.title_item}>Chính sách và quyền riêng tư</Text>
                                    <Right>
                                        <Animated.Image style={styles.arrow_item}
                                                        source={require('../../../vendor/images/profile/arrow_right.png')}
                                        />
                                    </Right>
                                </View>
                            </TouchableOpacity>
                        </Animated.View>
                    </Animated.View>
                    {
                        this.state.userInfo === true ?
                            <Animated.View style={{padding: 20}}>
                                <Button style={{
                                    borderRadius: 6,
                                    backgroundColor: 'red',
                                    height: 48
                                }} full info onPress={() => {
                                    this.logoutAcc();
                                    // this.props.navigation.navigate(Login)
                                }}>
                                    <Text style={styles.logout_name}>Đăng xuất</Text>
                                </Button>
                            </Animated.View> :
                            <View style={{padding: 20}}>
                                <Button style={{
                                    borderRadius: 6,
                                    backgroundColor: '#33B8E0',
                                    height: 48
                                }} full info onPress={() => {
                                    // this.props.navigation.navigate(Login);
                                    this.props.navigation.navigate(Login, {
                                        onGoBack: () => this.refresh()
                                    });
                                }}>
                                    <Text style={styles.logout_name}>Đăng nhập</Text>
                                </Button>
                            </View>
                    }

                </View>
            </ScrollView>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        userCv: state.cvReducers,
        user: state.loginReducers.items,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        onSwitchAction: (value) => {
            dispatch(SwitchNotify(value))
        },
        fetchUserDetail: (token) => {
            dispatch(FetchDetailUser(token))
        },
        onFetchCVUser: (token) => {
            dispatch(FetchCvUser(token))
        },
        onFetchUserInfo: () => {
            dispatch(FetchUserInfo())
        },
        fetchUniversity: () => {
            dispatch(FetchListUniversity());
        },
        fetchMajor: (universityId) => {
            dispatch(FetchListMajor(universityId));
        },
        fetchDegree: () => {
            dispatch(FetchListDegree());
        },
        fetchSkill: () => {
            dispatch(FetchListSkill());
        },
    };
}
const Profile = connect(mapStateToProps, mapDispatchToProps)(ProfileComponent);

export default withNavigation(Profile)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#ccc',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    title_item: {
        fontSize: 15,
        color: "#33B8E0",
        fontWeight: "500",
        left: 25

    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },

    love_item: {
        width: 16,
        height: 16,
        left: 29
    },
    apply_item: {
        width: 16,
        height: 16,
        left: 26
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF',
        fontWeight: 'bold'
    }

});
