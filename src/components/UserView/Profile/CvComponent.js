import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl, ImageBackground, Image, TouchableOpacity, ScrollView, FlatList, TextInput, Dimensions, BackHandler
} from 'react-native';
import {
    Body,
    CardItem,
    Container,
    Left,
    Right,
    Switch,
    Button,
    Input,
    Picker,
    Textarea,
    Title,
    Header, Toast
} from "native-base";
import {Field, reduxForm, SubmissionError} from 'redux-form';
import {FORM_EDIT, FORM_CV} from "../../../vendor/formNames";
import Modal from 'react-native-modalbox';
import {connect} from "react-redux";
import {StylesAll} from "../../../vendor/styles";
import {HomeBox, ShowProfile} from "../../../vendor/screen";
import ListDetailModal from "../Home/Search/ListDetailModal";
import {FetchCvUser, UpdateCvUser} from "../../../actions";
import {EditProfileComponent} from "./EditProfileComponent";
import SubmitCVUser from "./SubmitCVUser";
import NavigationService from "../../../vendor/NavigationService";

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 55;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screen = Dimensions.get('window');


const submitEditCV = async (values, dispatch) => {

    if (!values.carrer || values.carrer === 0) {
        toastError("Vui lòng chọn ngành nghề !")
    }

    if (!values.jobTypes || values.jobTypes === 0) {
        toastError("Vui lòng chọn loại hình !")
    }

    if (!values.province || values.province === 0) {
        toastError("Vui lòng chọn nơi muốn làm !")
    }

    if (!values.experience || values.experience === 0) {
        toastError("Vui lòng chọn kinh nghiệm !")
    }

    if (!values.salary || values.salary === 0) {
        toastError("Vui lòng chọn mức lương!")
    }

    if (!values.short_description || values.short_description.trim().length <= 0) {
        toastError("Nhập giới thiệu ngắn gọn bản thân!")
    }

    if (!values.desire_for_work || values.desire_for_work.trim().length <= 0) {
        toastError("Nhập mong muốn trong công việc!")
    }

    await dispatch(UpdateCvUser(values));
    await dispatch(FetchCvUser(values.token));
    await Toast.show({
        text: "Cập nhật CV thành công!",
        buttonText: "Đóng",
        type: "success",
        position: 'top',
    })
    await NavigationService.navigate(ShowProfile);
};

const toastError = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};
const toastWarning = (text) => {
    Toast.show({
        text: text,
        type: "danger",
        position: 'top',
        buttonText: "Đóng",
    });
};


const validate = values => {

    const errors = {};

    return errors

};

export class CvComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
            ),
            refreshing: false,
            scrollEnabled: true,
            dislay: false,
            dataList: '',
            title: '',
            functionPropIDHere: '',
            PropNameHere: '',

            province: this.props.userCv.length === 0 || this.props.userCv.items.province_name === '' ? "Chọn tỉnh thành" : this.props.userCv.items.province_name,
            jobTypes: this.props.userCv.length === 0 || this.props.userCv.items.job_type_name === '' ? "Chọn loại hình" : this.props.userCv.items.job_type_name,
            experience: this.props.userCv.length === 0 || this.props.userCv.items.job_experience_name === '' ? "Chọn kinh nghiệm" : this.props.userCv.items.job_experience_name,
            carrer: this.props.userCv.length === 0 || this.props.userCv.items.carrer_name === '' ? "Chọn ngành nghề" : this.props.userCv.items.carrer_name,
            salary: this.props.userCv.length === 0 || this.props.userCv.items.job_salary_name === '' ? "Chọn mức lương" : this.props.userCv.items.job_salary_name,

        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    componentWillMount() {

        const {navigation} = this.props;

        const token = navigation.getParam('token', 'NO-ID');

        //tạo đầu vào mặc định cho trường trong redux-form

        this.props.initialize({
            carrer: this.props.userCv.length !== 0 && this.props.userCv.items.carrer_id !== 0 ? this.props.userCv.items.carrer_id : 0,
            jobTypes: this.props.userCv.length !== 0 && this.props.userCv.items.job_type_id !== 0 ? this.props.userCv.items.job_type_id : 0,
            province: this.props.userCv.length !== 0 && this.props.userCv.items.province_id !== 0 ? this.props.userCv.items.province_id : 0,
            experience: this.props.userCv.length !== 0 && this.props.userCv.items.job_experience_id !== 0 ? this.props.userCv.items.job_experience_id : 0,
            salary: this.props.userCv.length !== 0 && this.props.userCv.items.job_salary_id !== 0 ? this.props.userCv.items.job_salary_id : 0,
            short_description: this.props.userCv.length !== 0 && this.props.userCv.items.short_description !== 'undefined' ? this.props.userCv.items.short_description : '',
            desire_for_work: this.props.userCv.length !== 0 && this.props.userCv.items.desire_for_work !== 'undefined' ? this.props.userCv.items.desire_for_work : '',
        });

        BackHandler.addEventListener('ShowProfile', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('ShowProfile', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(ShowProfile)
        return true;

    }


    renderPickerTextarea = ({label, placeholder, keyboardType, meta: {touched, error, warning}, input: {onChange, ...restInput}}) => {

        return (
            <View>
                <Textarea rowSpan={6} bordered placeholder={placeholder} keyboardType={keyboardType}
                          onChangeText={onChange} {...restInput} style={[StylesAll.textAdvanced2, {
                    marginTop: 0,
                    borderLeftWidth: 0,
                    borderRightWidth: 0,
                    borderTopWidth: 0,
                    borderBottomWidth: 0
                }]}/>
                {touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||
                    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}
            </View>
        );
    }

    renderPickerInput = ({label, placeholder, type, keyboardType, meta: {touched, error, warning}, input: {value, onChange, ...restInput}}) => {

        return (
            <View>
                <Input
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={[styles.textPlaceholder, {height: 40, paddingLeft: 0}]}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}
                    editable={false}
                />
                {touched && ((error && toastError(error)) ||
                    (warning && toastWarning(warning)))}
            </View>
        );
    }


    static
    navigationOptions = () => ({
        header: null
    });


    render() {
        return (
            <ScrollView scrollEnabled={this.state.scrollEnabled}>

                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(ShowProfile)}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Tạo CV</Title>
                    </Body>
                    <Right>
                        <SubmitCVUser/>
                    </Right>
                </Header>

                <View style={styles.fill}>
                    <TouchableOpacity onPress={async () => {
                        await this.setState({
                            dataList: this.props.carrer,
                            title: 'Chọn ngành nghề',
                            functionPropIDHere: 'carrer'
                        });
                        await this.refs.listModel.changeDataList()
                        await this.refs.listModel.showAddModal()
                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Ngành nghề
                            </Text>

                            <View>
                                <Text style={styles.textBody}>{this.state.carrer}</Text>
                            </View>
                            <Field type="text" keyboardType={'numeric'} name="carrer"
                                   component={this.renderPickerInput}>
                            </Field>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={async () => {
                        await this.setState({
                            dataList: this.props.jobTypes,
                            title: 'Chọn loại hình',
                            functionPropIDHere: 'jobTypes'
                        });
                        await this.refs.listModel.changeDataList()
                        await this.refs.listModel.showAddModal()
                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Loại hình
                            </Text>
                            <View>
                                <Text style={styles.textBody}>{this.state.jobTypes}</Text>
                            </View>
                            <Field type="text" keyboardType={'numeric'} name="jobTypes"
                                   component={this.renderPickerInput}/>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={async () => {
                        await this.setState({
                            dataList: this.props.province,
                            title: 'Chọn địa điểm',
                            functionPropIDHere: 'province',
                        });
                        await this.refs.listModel.changeDataList()
                        await this.refs.listModel.showAddModal()
                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Địa chỉ làm việc
                            </Text>
                            <View>
                                <Text style={styles.textBody}>{this.state.province}</Text>
                            </View>
                            <Field type="text" keyboardType={'numeric'} name="province"
                                   component={this.renderPickerInput}/>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={async () => {
                        await this.setState({
                            dataList: this.props.experience,
                            title: 'Chọn kinh nghiệm',
                            functionPropIDHere: 'experience'
                        });
                        await this.refs.listModel.changeDataList()
                        await this.refs.listModel.showAddModal()
                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Kinh nghiệm
                            </Text>
                            <View>
                                <Text style={styles.textBody}>{this.state.experience}</Text>
                            </View>
                            <Field type="text" keyboardType={'numeric'} name="experience"
                                   component={this.renderPickerInput}/>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={async () => {
                        await this.setState({
                            dataList: this.props.salary,
                            title: 'Chọn mức lương',
                            functionPropIDHere: 'salary'
                        });
                        await this.refs.listModel.changeDataList()
                        await this.refs.listModel.showAddModal()
                    }}>
                        <View style={[styles.boxAdvanced]}>
                            <Text style={styles.textAdvanced}>
                                Mức lương
                            </Text>
                            <View>
                                <Text style={styles.textBody}>{this.state.salary}</Text>
                            </View>
                            <Field type="text" keyboardType={'numeric'} name="salary"
                                   component={this.renderPickerInput}/>
                            <Right style={styles.arrow_item}>
                                <Animated.Image
                                    source={require('../../../vendor/images/profile/arrow_down.png')}
                                />
                            </Right>
                        </View>
                    </TouchableOpacity>

                    <View style={{
                        borderRadius: 12,
                        borderColor: '#D0C9D6',
                        borderWidth: 1,
                        margin: 20,
                    }}>
                        <Field name="short_description" component={this.renderPickerTextarea}
                               style={[styles.textPlaceholder]}
                               placeholder="Giới thiệu bản thân">
                        </Field>
                    </View>


                    <View style={{
                        borderRadius: 12,
                        borderColor: '#D0C9D6',
                        borderWidth: 1,
                        marginRight: 20,
                        marginLeft: 20,
                    }}>
                        <Field name="desire_for_work" component={this.renderPickerTextarea}
                               style={[styles.textPlaceholder]}
                               placeholder="Mong muốn trong công việc">
                        </Field>
                    </View>
                </View>

                {this.state.dataList !== '' ?
                    <ListDetailModal
                        ref={'listModel'}
                        dataList={this.state.dataList}
                        title={this.state.title}
                        functionPropIDHere={(value) => {
                            this.props.change(this.state.functionPropIDHere, `${value}`)
                        }}
                        PropNameHere={(value) => {
                            if (this.state.functionPropIDHere === 'carrer') {
                                this.setState({carrer: `${value}`})
                            } else if (this.state.functionPropIDHere === 'province') {
                                this.setState({province: `${value}`})
                            } else if (this.state.functionPropIDHere === 'salary') {
                                this.setState({salary: `${value}`})
                            } else if (this.state.functionPropIDHere === 'experience') {
                                this.setState({experience: `${value}`})
                            } else if (this.state.functionPropIDHere === 'jobTypes') {
                                this.setState({jobTypes: `${value}`})
                            }
                        }}
                    />
                    : null}

            </ScrollView>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        userCv: state.cvReducers,
        user: state.loginReducers,
        province: state.provinceReducers,
        salary: state.salaryReducers,
        carrer: state.carrerReducers,
        jobTypes: state.jobTypesReducers,
        experience: state.experienceReducers
    }
};
const mapDispatchToProps = (dispatch) => {
    return {};
}

const cvUser = connect(mapStateToProps, mapDispatchToProps)(CvComponent);
//
export default reduxForm({
    form: FORM_CV, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    validate,
    onSubmit: submitEditCV
})(cvUser)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',

        overflow: 'hidden',
        height: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        height: 32,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        // padding:30,
    },
    name: {
        fontSize: 17,
        color: "#33B8E0",
        fontWeight: "bold"
    },
    boxAdvanced: {
        borderBottomWidth: 1,
        borderColor: '#D0C9D6',
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    },
    textAdvanced: {
        width: 150,
        left: 20,
        fontSize: 15,
        color: '#33B8E0',
        fontWeight: 'normal',
    },
    textBody: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    textPlaceholder: {
        fontSize: 15,
        color: '#525252',
        fontWeight: 'bold'
    },
    arrow_item: {
        width: 16,
        height: 16,
        right: 20
    },
    wrapper: {
        paddingTop: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    modal4: {
        height: 400,
    },
    text: {
        color: 'black',
        fontSize: 16,
        textAlign: 'left',
        left: 10,
        marginBottom: 10
    },
    button: {
        backgroundColor: 'green',
        width: 300,
        marginTop: 16,
        textAlign: 'center',
        marginLeft: 10,
        marginRight: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        color: '#d0d0d0',
    },
    logout_name: {
        fontSize: 17,
        color: '#FFFFFF'
    }
});
