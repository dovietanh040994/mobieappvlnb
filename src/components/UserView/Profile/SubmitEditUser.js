import React from 'react';
import {connect} from 'react-redux';
import {submit} from 'redux-form';
import {StyleSheet, View, TouchableOpacity, TextInput, Text, Animated} from 'react-native';
import {Button, Left} from 'native-base'
import {FORM_EDIT} from "../../../vendor/formNames";
import {ShowProfile} from "../../../vendor/screen";


//dispatch(submit(CONTACT_FORM)) taoj luôn ra 1 action là submit chứ k chạy qua action(tạo sẵn) như redux
const submitEdituser = ({dispatch}) => {
    return (
        <TouchableOpacity onPress={() => {
            dispatch(submit(FORM_EDIT))
        }
        }>
            <Text style={{
                fontSize: 17,
                color: "#3F3356",
                fontWeight: 'bold',
                width: 80,
                alignItems: 'center'
            }}>Cập nhật</Text>
        </TouchableOpacity>
    );
};

export default connect()(submitEdituser);
