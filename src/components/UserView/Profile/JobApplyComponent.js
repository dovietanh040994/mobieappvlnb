import React, {Component} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler
} from 'react-native';
import FlatListItemJob from "./FlatListItemJob";
import {Body, Button, Container, Header, Item, Left, Title} from "native-base";
import {HomeBox, ShowProfile} from "../../../vendor/screen";
import {withNavigation} from "react-navigation";

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;


class JobApplyComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        //hàm tạo ref
        this.detailModal = React.createRef();
        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                0,
            ),
            refreshing: false,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            token: "",

        };
        this.page = 1;
        this.saved = [];
        this.applied = [];
        this.handleChange = this.handleChange.bind(this);

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    componentWillMount() {
        BackHandler.addEventListener('ShowProfile', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('ShowProfile', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(ShowProfile)
        return true;

    }

    handleChange(event) {
        const filteredData = this.state.serverData.filter(item => item.id !== event.idJob);
        this.setState({serverData: filteredData});
    }

    componentDidMount() {
        const {navigation} = this.props;

        this.state.token = navigation.getParam('token', 'NO-ID');

        this.loadMoreData();
    }

    loadMoreData = async () => {

        if (!this.state.fetching_from_server && !this.state.isListEnd) {

            this.setState({fetching_from_server: true})

            const Url = `http://vieclamnambo.vn:9002/api/vlnb/job/getjobpost?post_apply=1&sort_name=id&rows_start=${this.page}`;

            console.log(111);

            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.token
                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {

                    if (responseJson.jobPosts.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.jobPosts],
                            fetching_from_server: false,
                            refreshing: false
                        });
                        console.log(responseJson.jobPosts)

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                            refreshing: false
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (
            <View style={styles.footer}>
                {this.state.fetching_from_server ? (
                    <ActivityIndicator size="large" color={'#ccc'}
                                       style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    static navigationOptions = () => ({
        header: null
    });

    _handleRefresh = () => {
        this.setState({
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            refreshing: true
        }, () => {
            this.page = 1
            this.loadMoreData()
        });
    };

    saveHeart(){
        const { navigation } = this.props;
        const jobId = navigation.getParam('jobId', null)
        const saveHeart = navigation.getParam('saveHeart', null)
        if(jobId !== null){
            if(saveHeart === 1){
                var index = this.saved.indexOf(jobId);
                if (index === -1) {
                    this.saved.push(jobId)
                }

            }else{
                var index = this.saved.indexOf(jobId);

                if (index !== -1) {
                    this.saved.splice(index,1);
                }
            }
        }
    }
    jobApplied(){
        const { navigation } = this.props;
        const jobApplied = navigation.getParam('jobApplied', null)
        if(jobApplied !== null){
            var index = this.applied.indexOf(jobApplied);
            if (index === -1) {
                this.applied.push(jobApplied)
            }
        }
    }

    render() {
        this.saveHeart();
        this.jobApplied();

        return (
            <View style={styles.fill}>

                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(ShowProfile)}>
                            <Animated.Image
                                source={require('../../../vendor/images/profile/back.png')}
                            />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33B8E0'}}>Việc làm ứng tuyển</Title>
                    </Body>
                </Header>

                <Animated.FlatList
                    style={styles.fill}
                    // data={this.props.salary.items.jobPosts}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true},
                    )}
                    data={this.state.serverData}
                    onEndReached={() => this.loadMoreData()}
                    onEndReachedThreshold={0.5}
                    ListHeaderComponent={<View/>}
                    renderItem={({item, index}) => {
                        return (
                            <FlatListItemJob
                                item={item}
                                index={index}
                                loadMoreData={this.loadMoreData()}
                                navigation={this.props.navigation}
                                screen={"JobApply"}
                                token={this.state.token}
                                handleChange = {this.handleChange}
                                saveHeart={ this.saved.indexOf(item.id) !== -1? 1 :item.saved}
                                applied={ this.applied.indexOf(item.id) !== -1? 1 : item.applied }
                                nameScreen={'JobApply'}
                                random = {index % 4}
                            >

                            </FlatListItemJob>);
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    refreshing={this.state.refreshing}
                    onRefresh={this._handleRefresh}
                />
            </View>
        );
    }
}

export default withNavigation(JobApplyComponent)

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4'
    },
    content: {
        flex: 1,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 78,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
});
