import React, {PureComponent} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler,
    Alert
} from 'react-native';
import FlatListItemPost from "../ChildComponent/FlatListItemPost";
import InformationJob from "./InformationJob";
import CompanyJob from "./CompanyJob";
import {
    Body,
    Button,
    CardItem,
    Container,
    Header,
    Item,
    Left,
    Picker,
    Title,
    Footer,
    FooterTab,
    Icon, Toast
} from "native-base";
import {connect} from "react-redux";

import categoryPostReducers from "../../../reducers/CategoryPostReduser";
import {
    appliedAction,
    CategoryPost, CompanyDetailAction,
    JobDetailAction,
    JobSameCompanyAction,
    LoginAction,
    SaveJobAction,
    UnSaveJobAction
} from "../../../actions";
import {Home, HomeBox, JobApply, JobCare, Login, Search, Notification} from "../../../vendor/screen";
import FlatListItemJob from "../ChildComponent/FlatListItemJob";
import companyDetailReducers from "../../../reducers/CompanyDetailReduser";
import AsyncStorage from "@react-native-community/async-storage";
import PlaceholderLoadingJob from "../../Helpers/PlaceholderLoadingJob";
import firebase from "react-native-firebase";


const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;
const menu = [
    {
        id: 1,
        name: "Thông tin",
    },
    {
        id: 2,
        name: "Công ty",
    },
    {
        id: 3,
        name: "Việc cùng công ty",
    },

]

class JobDetail extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                0,
            ),
            refreshing: false,
            bgButton: 0,
            idCategory: 1,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            saveHeart: this.props.navigation.getParam('saveHeart', null),
            applied: this.props.navigation.getParam('applied', null),
            userInfo: false,
            imageLoading: true,

        };
        this.page = 1;
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount() {
        AsyncStorage.getItem('userInfo', (err, result) => {
            if (result !== null) {
                this.setState({userInfo: true})
            } else {
                this.setState({userInfo: false})
            }
        });

        AsyncStorage.getItem('adsPost', (err, result) => {
            console.log((parseInt(result) + 1).toString());
            AsyncStorage.setItem('adsPost', (parseInt(result) + 1).toString());
            if (parseInt(result) === 2 || parseInt(result) === 7) {
                const unitId = 'ca-app-pub-3717610865114273/3047817089';
                const advert = firebase.admob().interstitial(unitId);
                const AdRequest = firebase.admob.AdRequest;
                const request = new AdRequest();
                advert.loadAd(request.build());
                advert.on('onAdLoaded', () => {
                    console.log(result);
                    console.log('Advert ready to show.');
                    advert.show();
                });

            }
        });

    }

    componentWillMount() {
        BackHandler.addEventListener('JobDetail', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('JobDetail', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.navigatePage()
        return true;

    }

    navigatePage = () => {
        console.log(1)
        const {navigation} = this.props;
        const nameScreen = navigation.getParam('nameScreen', null);
        const idJobContent = this.props.jobContent.itemsDetaiJob.id;

        navigation.popToTop()
        if (nameScreen === 'Search') {
            if (this.state.saveHeart === 1) {
                navigation.navigate(Search, {
                    jobId: idJobContent,
                    saveHeart: 1,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })

            } else {
                navigation.navigate(Search, {
                    jobId: idJobContent,
                    saveHeart: 0,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })
            }
        } else if (nameScreen === "Notify") {
            console.log('Notify')
            if (this.state.saveHeart === 1) {
                navigation.navigate(Notification, {
                    jobId: idJobContent,
                    saveHeart: 1,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })

            } else {
                navigation.navigate(Notification, {
                    jobId: idJobContent,
                    saveHeart: 0,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })
            }
        } else if (nameScreen === "JobApply") {
            if (this.state.saveHeart === 1) {
                navigation.navigate(JobApply, {
                    jobId: idJobContent,
                    saveHeart: 1,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })

            } else {
                navigation.navigate(JobApply, {
                    jobId: idJobContent,
                    saveHeart: 0,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })
            }
        } else if (nameScreen === "JobCare") {
            if (this.state.saveHeart === 1) {
                navigation.navigate(JobCare, {
                    jobId: idJobContent,
                    saveHeart: 1,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })

            } else {
                this.props.navigation.navigate(JobCare, {
                    jobId: idJobContent,
                    saveHeart: 0,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })
            }
        } else {
            if (this.state.saveHeart === 1) {
                navigation.navigate(HomeBox, {
                    jobId: idJobContent,
                    saveHeart: 1,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })

            } else {
                navigation.navigate(HomeBox, {
                    jobId: idJobContent,
                    saveHeart: 0,
                    jobApplied: this.state.applied === 1 ? idJobContent : null
                })
            }
        }
    }
    loadMoreData = async () => {

        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})
            const Url = `http://vieclamnambo.vn:9002/api/vlnb/job/getjobpost?company_id=${this.props.jobContent.itemsDetaiJob.company_id}&rows_start=${this.page}`;
            // console.log('sdsd');
            // console.log(Url);
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {

                    if (responseJson.jobPosts.length > 0) {

                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.jobPosts],
                            fetching_from_server: false,
                        });

                    } else {

                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (

            <View style={styles.footer}>
                {this.state.fetching_from_server ? (this.page === 1 ?
                        <PlaceholderLoadingJob/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'}
                                           style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    rednderContent() {
        // if (this.state.fetching_from_server === true) {
        //     <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
        //
        // }else {
        if (this.state.idCategory === 1) {
            const {navigation} = this.props;
            const province = navigation.getParam('provine', null);
            return (

                <Animated.ScrollView
                    style={styles.fill}
                    // data={this.props.salary.items.jobPosts}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true},
                    )}>
                    <View style={{marginTop: 265}}/>
                    <InformationJob parentJob={this.props.jobContent.itemsDetaiJob} province={province}/>
                </Animated.ScrollView>


            );
        } else if (this.state.idCategory === 2) {
            return (
                <Animated.ScrollView
                    style={styles.fill}
                    // data={this.props.salary.items.jobPosts}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true},
                    )}>
                    <View style={{marginTop: 265}}/>
                    <CompanyJob parentJob={this.props.jobContent.itemsDetaiJob}/>
                </Animated.ScrollView>
            );
        } else if (this.state.idCategory === 3) {
            // console.log(11111111)
            // console.log(this.state.serverData)

            return (

                <Animated.FlatList
                    style={styles.fill}
                    // data={this.props.salary.items.jobPosts}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true},
                    )}
                    data={this.state.serverData}
                    onEndReached={() => this.loadMoreData()}
                    onEndReachedThreshold={0.5}
                    ListHeaderComponent={<View style={{height: 250}}/>}
                    renderItem={({item, index}) => {
                        return (
                            <View style={{
                                marginTop: index === 0 ? 10 : 0,
                                paddingLeft: 20,
                                paddingRight: 20,
                                backgroundColor: 'white',
                            }}>
                                <CardItem
                                    style={{
                                        borderBottomWidth: 1, borderColor: '#33bbe0', paddingLeft: 0, paddingRight: 0
                                    }}>

                                    <Body style={{paddingTop: 10, paddingBottom: 10}}>
                                        <TouchableOpacity onPress={
                                            () => {

                                                this.props.jobDetail(item.id);
                                                this.setState({
                                                    bgButton: 0,
                                                    idCategory: 1,
                                                });
                                            }
                                        }><Text numberOfLines={2} style={{
                                            fontSize: 18,
                                            color: '#33bbe0',
                                            fontWeight: 'bold',
                                            marginBottom: 10
                                        }}>{item.job_title} </Text></TouchableOpacity>

                                        <View style={{flexDirection: 'row', flex: 1, marginBottom: 10}}>
                                            <View style={{flex: 1}}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <View style={{width: 14}}>
                                                        <Image source={require('../../../vendor/images/Group2.png')}
                                                               style={{marginTop: 3}}/>
                                                    </View>
                                                    <Text style={{
                                                        paddingLeft: 5,
                                                        color: '#525252',
                                                        fontSize: 12,
                                                        fontWeight: 'bold'
                                                    }}>Mức lương: <Text
                                                        style={styles.textContent}>{item.job_salary}</Text></Text>
                                                </View>
                                            </View>


                                        </View>
                                        <View style={{flexDirection: 'row', marginBottom: 1}}>
                                            <View style={{flex: 1}}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <View style={{width: 14}}>
                                                        <Image source={require('../../../vendor/images/Group3.png')}
                                                               style={{marginTop: 3}}/>
                                                    </View>
                                                    <Text style={{
                                                        paddingLeft: 5,
                                                        color: '#525252',
                                                        fontSize: 12,
                                                        fontWeight: 'bold'
                                                    }}>Thời hạn: <Text
                                                        style={styles.textContent}>{item.ended_at}</Text></Text>
                                                </View>
                                            </View>
                                            <View style={{flex: 1}}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <View style={{width: 14}}>
                                                        <Image source={require('../../../vendor/images/Shape1.png')}
                                                               style={{marginTop: 3}}/>
                                                    </View>
                                                    <Text style={{
                                                        paddingLeft: 5,
                                                        color: '#525252',
                                                        fontSize: 12,
                                                        fontWeight: 'bold'
                                                    }}>Địa điểm: <Text
                                                        style={styles.textContent}>{item.provine_name}</Text></Text>
                                                </View>
                                            </View>

                                        </View>
                                    </Body>
                                </CardItem>

                            </View>
                        );
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    ListFooterComponent={this.renderFooter.bind(this)}
                />
            );
        }
        // }

    }

    TabName(stateName, idCategory) {
        console.log(1)
        this.setState({
            bgButton: stateName,
            idCategory: idCategory,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
        }, () => {
            this.page = 1
            this.loadMoreData()

        });
    }

    static navigationOptions = () => ({
        header: null
    });

    ImageLoading_Error() {
        console.log(1444, this.state.imageLoading)
        this.setState({imageLoading: false});

        // Write Your Code Here Which You Want To Execute If Image Not Found On Server.
    }

    render() {

        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -100, -200],
            extrapolate: 'clamp',
        });

        const {loadingDetaiJob} = this.props.jobContent;
        console.log('asdasdad: ', this.props.randomImageProp)
        return (
            <View style={styles.fill}>
                {this.rednderContent()}
                <Footer style={{borderTopWidth: 1, borderColor: '#ccc'}}>
                    <FooterTab style={{backgroundColor: '#fff', flex: 1}}>
                        <View style={{flex: 1, paddingRight: 5, paddingLeft: 10, marginTop: 8}}>
                            <TouchableOpacity style={{
                                borderRadius: 50,
                                borderColor: this.state.saveHeart === 0 ? '#33B8E0' : 'red',
                                backgroundColor: this.state.saveHeart === 0 ? '#fff' : 'red',
                                borderWidth: 1,
                                height: 40,
                                textAlign: 'center'
                            }}
                                              onPress={() => {
                                                  if (this.state.userInfo === true) {
                                                      this.setState({
                                                          saveHeart: this.state.saveHeart === 0 ? 1 : 0
                                                      }, () => {

                                                          if (this.state.saveHeart === 1) {
                                                              this.props.SaveHeartJob(this.props.jobContent.itemsDetaiJob.id)
                                                              Toast.show({
                                                                  text: ' Việc làm đã được lưu. Bạn có thể xem lại trong mục Việc làm quan tâm. Xin cảm ơn',
                                                                  // buttonText: "Đóng",
                                                                  type: "success",
                                                                  position: "top"
                                                              })
                                                          } else {
                                                              this.props.UnSaveHeartJob(this.props.jobContent.itemsDetaiJob.id)
                                                              Toast.show({
                                                                  text: ' Việc làm đã được bỏ lưu. Xin cảm ơn',
                                                                  // buttonText: "Đóng",
                                                                  type: "success",
                                                                  position: "top"
                                                              })
                                                          }
                                                      })
                                                  } else {
                                                      AsyncStorage.setItem('screen', 'jobDetail');
                                                      this.props.navigation.navigate(Login)
                                                  }


                                              }}>
                                <Text style={{
                                    color: this.state.saveHeart === 0 ? '#33B8E0' : 'white',
                                    fontWeight: 'bold',
                                    fontSize: 16,
                                    alignSelf: 'center',
                                    marginTop: 8
                                }}>Quan tâm</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, paddingRight: 10, paddingLeft: 5, marginTop: 8}}>
                            <TouchableOpacity style={{
                                borderRadius: 50,
                                borderColor: this.state.applied === 0 ? '#33B8E0' : '#ccc',
                                backgroundColor: this.state.applied === 0 ? '#33B8E0' : '#ccc',
                                borderWidth: 1,
                                height: 40,
                                textAlign: 'center'
                            }}
                                              onPress={() => {
                                                  if (this.state.userInfo === true) {
                                                      if (this.state.applied === 0) {
                                                          Alert.alert(
                                                              'Thông báo',
                                                              `Bạn xác nhận muốn ứng tuyển vào ${this.props.jobContent.itemsDetaiJob.company_name} ?`,
                                                              [
                                                                  {
                                                                      text: 'Đóng',
                                                                      onPress: () => console.log('Cancel Pressed'),
                                                                      style: 'cancel',
                                                                  },
                                                                  {
                                                                      text: 'Xác nhận', onPress: () => {

                                                                          this.setState({
                                                                              applied: 1
                                                                          }, () => {
                                                                              this.props.appliedJob(this.props.jobContent.itemsDetaiJob.id)
                                                                              Toast.show({
                                                                                  text: ' Bạn đã ứng tuyển thành công. Bạn có thể xem lại trong mục Việc làm ứng tuyển. Xin cảm ơn',
                                                                                  // buttonText: "Đóng",
                                                                                  type: "success",
                                                                                  position: "top"
                                                                              })
                                                                          })
                                                                      }
                                                                  },
                                                              ],
                                                              {cancelable: false},
                                                          );
                                                      } else {
                                                          Toast.show({
                                                              text: ' Bạn đã ứng tuyển công việc này.',
                                                              // buttonText: "Đóng",
                                                              type: "warning",
                                                              position: "top"
                                                          })
                                                      }

                                                  } else {
                                                      AsyncStorage.setItem('screen', 'jobDetail');
                                                      this.props.navigation.navigate(Login)
                                                  }
                                              }}>
                                <Text style={{
                                    color: '#fff',
                                    fontWeight: 'bold',
                                    fontSize: 16,
                                    alignSelf: 'center',
                                    marginTop: 8
                                }}>{this.state.applied === 0 ? 'Ứng tuyển ngay' : 'Đã ứng tuyển'}</Text>
                            </TouchableOpacity>
                        </View>
                    </FooterTab>
                </Footer>
                <Animated.View
                    // pointerEvents="none"
                    style={[
                        styles.header,
                        {transform: [{translateY: headerTranslate}]},
                    ]}
                >

                    <Animated.View style={[

                        {
                            opacity: imageOpacity,
                            transform: [{translateY: imageTranslate}],
                        },
                    ]}>
                        <Header style={{
                            backgroundColor: '#fff',
                            paddingLeft: 20,
                            height: 50,
                            borderBottomWidth: 0,
                            borderBottomColor: '#ccc',
                            elevation: 1
                        }}>
                            <Left>
                                <TouchableOpacity
                                    style={{width: 50, height: 30, position: 'absolute', left: -20, top: -13}}
                                    transparent onPress={() => {
                                    this.navigatePage()
                                }}
                                >
                                    <Image source={require('../../../vendor/images/arr_left.jpg')} style={{
                                        tintColor: '#33B8E0', marginTop: 5, marginLeft: 23
                                    }}/>
                                </TouchableOpacity>
                            </Left>
                            <Body style={{flex: 3}}>
                                <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Chi
                                    tiết việc làm</Title>
                            </Body>
                        </Header>
                        <Header
                            style={{backgroundColor: '#fff', height: 151, borderBottomWidth: 0.5, borderColor: '#ccc'}}>
                            {/*<ImageBackground source={require('../../vendor/images/Rectangle8.png')} style={{width: '100%', height: '100%'}}>*/}
                            {/*    <Text style={{fontWeight:'900',color:'#fff',fontSize:25,alignSelf:'center',marginTop:65}}>KHÁM PHÁ</Text>*/}

                            {/*</ImageBackground>*/}
                            {loadingDetaiJob ? <ActivityIndicator size="large" color="#ccc"/> :
                                <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>

                                    <View style={{width: 130}}>
                                        {this.props.navigation.getParam('randomImageProp', null) === 0 && (
                                            <Image resizeMode={'contain'}
                                                   source={
                                                       this.state.imageLoading
                                                           ?
                                                           {uri: `${this.props.jobContent.itemsDetaiJob.logo}`}
                                                           :
                                                           require('../../../vendor/images/VLNB1.png')
                                                   }
                                                   onError={this.ImageLoading_Error.bind(this)}
                                                   style={{width: '100%', height: '100%'}}
                                            />
                                        )}
                                        {this.props.navigation.getParam('randomImageProp', null) === 1 && (
                                            <Image resizeMode={'contain'}
                                                   source={
                                                       this.state.imageLoading
                                                           ?
                                                           {uri: `${this.props.jobContent.itemsDetaiJob.logo}`}
                                                           :
                                                           require('../../../vendor/images/VLNB2.png')
                                                   }
                                                   onError={this.ImageLoading_Error.bind(this)}
                                                   style={{width: '100%', height: '100%'}}
                                            />
                                        )}
                                        {this.props.navigation.getParam('randomImageProp', null) === 2 && (
                                            <Image resizeMode={'contain'}
                                                   source={
                                                       this.state.imageLoading
                                                           ?
                                                           {uri: `${this.props.jobContent.itemsDetaiJob.logo}`}
                                                           :
                                                           require('../../../vendor/images/VLNB3.png')
                                                   }
                                                   onError={this.ImageLoading_Error.bind(this)}
                                                   style={{width: '100%', height: '100%'}}
                                            />
                                        )}
                                        {this.props.navigation.getParam('randomImageProp', null) === 3 && (
                                            <Image resizeMode={'contain'}
                                                   source={
                                                       this.state.imageLoading
                                                           ?
                                                           {uri: `${this.props.jobContent.itemsDetaiJob.logo}`}
                                                           :
                                                           require('../../../vendor/images/VLNB4.png')
                                                   }
                                                   onError={this.ImageLoading_Error.bind(this)}
                                                   style={{width: '100%', height: '100%'}}
                                            />
                                        )}

                                    </View>
                                    <View style={{marginLeft: 10, width: screenWidth - 150}}>
                                        <Text numberOfLines={3} style={{
                                            color: '#33B8E0',
                                            fontSize: 16,
                                            fontWeight: '600',
                                            lineHeight: 20
                                        }}>{this.props.jobContent.itemsDetaiJob.job_title} </Text>
                                        <Text style={{
                                            color: '#525252',
                                            fontSize: 12,
                                            fontWeight: '500',
                                            marginTop: 5,
                                            marginBottom: 5
                                        }} numberOfLines={1}>{this.props.jobContent.itemsDetaiJob.company_name}</Text>
                                        <View style={{flexDirection: 'row', marginBottom: 5}}>
                                            <Image source={require('../../../vendor/images/luotxem.png')} style={{
                                                marginTop: 5, marginRight: 5
                                            }}/>
                                            <Text style={{color: '#888686', fontSize: 12}}>Lượt
                                                xem: {this.props.jobContent.itemsDetaiJob.all_time_stats}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row'}}>
                                            <Image source={require('../../../vendor/images/hannop.png')} style={{
                                                marginTop: 5, marginRight: 5, marginLeft: 1
                                            }}/>
                                            <Text style={{color: '#888686', fontSize: 12}}>Hạn nộp hồ
                                                sơ: {this.props.jobContent.itemsDetaiJob.ended_at !== '00/00/0000' ? this.props.jobContent.itemsDetaiJob.ended_at : 'Luôn tuyển'}</Text>

                                        </View>


                                    </View>
                                </View>}

                        </Header>

                    </Animated.View>

                </Animated.View>
                {loadingDetaiJob ? null :
                    <Animated.View
                        style={[
                            styles.bar,
                            {
                                transform: [
                                    // { scale: titleScale },
                                    {translateY: titleTranslate},
                                ],
                            },
                        ]}
                    >
                        <FlatList
                            horizontal={true}
                            data={menu}
                            showsHorizontalScrollIndicator={false}
                            ref={(ref) => {
                                this.flatListRef = ref;
                            }}
                            style={{
                                backgroundColor: '#fff',
                                borderBottomWidth: 1,
                                borderTopWidth: 1,
                                borderColor: '#ECEBED',
                                width: '100%'
                            }}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) =>
                                <View style={{
                                    borderBottomWidth: this.state.bgButton === index ? 1 : 0,
                                    borderColor: this.state.bgButton === index ? '#33B8E0' : '#D9D3DE',
                                    paddingLeft: 5,
                                    paddingRight: 5
                                }}>
                                    <TouchableOpacity style={styles.ScrollTextContainer} onPress={() => {
                                        this.flatListRef.scrollToIndex({animated: true, index: index});
                                        const stateName = index;
                                        const idCategory = item.id;
                                        // if(idCategory === 3){
//                                         console.log(this.props.jobContent.itemsDetaiJob.company_id)
                                        // this.props.jobSameCompany(this.props.jobContent.itemsDetaiJob.company_id)
                                        // }
                                        this.TabName(stateName, idCategory)
                                    }}>
                                        <Text style={{
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            color: this.state.bgButton === index ? '#33B8E0' : '#D9D3DE'
                                        }}>{item.name}</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                        />
                    </Animated.View>

                }
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        // user: state.loginReducers,
        jobContent: state.jobDetailReducers,
        // companyContent: state.companyDetailReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        category: () => {

            dispatch(CategoryPost());
        },

        jobDetail: (idJob) => {
            dispatch(JobDetailAction(idJob));

        },
        SaveHeartJob: (id) => {

            dispatch(SaveJobAction(id));
        },
        UnSaveHeartJob: (id) => {

            dispatch(UnSaveJobAction(id));
        },
        appliedJob: (id) => {

            dispatch(appliedAction(id));
        },
    };
}
const JobDetailComponent = connect(mapStateToProps, mapDispatchToProps)(JobDetail);
export default JobDetailComponent

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#fdfdfd'
        // paddingTop:70
    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 200,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 200,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
});
