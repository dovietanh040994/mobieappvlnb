import React, {Component} from "react";
import {Image, ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text} from "native-base";
import {CategoryPost, JobDetailAction, keyWorksAction, SaveJobAction} from "../../../actions";
import {connect} from "react-redux";
import YouTube from "react-native-youtube";

class Company extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saved: [],
            shareId: 0
        };

    }
    render() {
        console.log('description',this.props.companyContent.itemsDetaiCompany.description)
        console.log('website',this.props.companyContent.itemsDetaiCompany.website)

        return (

            <View style={{paddingLeft:15,paddingRight:15}}>
                <View style={{marginBottom: 8}}>
                    {this.props.parentJob.company_name ?
                        <View>
                            <Text style={{color:'#33B8E0',fontSize:20,fontWeight:'700'}}>{this.props.parentJob.company_name}</Text>
                        </View>
                        : null
                    }
                </View>
                <View style={{marginBottom: 2}}>
                    {this.props.parentJob.contact_address ?
                        <View style={{flexDirection:'row'}}>
                            <View style={{width: 14 }}>
                                <Image source={require('../../../vendor/images/Shape1.png')} style={{marginTop: 6}} />
                            </View>
                            <Text  style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Trụ sở chính: <Text
                                style={styles.textContent}>{this.props.parentJob.contact_address}</Text></Text>
                        </View>
                        : null}
                </View>
                <View style={{marginBottom: 2}}>
                        <View style={{flexDirection:'row'}}>
                            <View style={{width: 14 }}>
                                <Image source={require('../../../vendor/images/Group6.png')} style={{marginTop: 6}} />
                            </View>
                            <Text  style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Website: <Text
                                style={[styles.textContent,{color:'#33B8E0'}]}>{this.props.companyContent.itemsDetaiCompany.website ? this.props.companyContent.itemsDetaiCompany.website : 'Đang cập nhật'}
                            </Text></Text>
                        </View>

                </View>

                <View >
                        <View style={{marginBottom:15}}>
                            <Text style={styles.textHeader}>Giới thiệu công ty</Text>
                            {this.props.companyContent.itemsDetaiCompany.description ?
                                 <Text style={[styles.textContent,{ textAlign: 'justify'}]}>{this.props.companyContent.itemsDetaiCompany.description}</Text>
                                : null}
                        </View>

                        <View>
                            {this.props.parentJob.contact_name ?
                                <Text  style={{color:'#525252',fontSize:16,fontWeight: 'bold',marginBottom:5}}>Liên hệ: <Text
                                    style={styles.textContent}>{this.props.parentJob.contact_name}</Text></Text>
                                : null}
                            {this.props.parentJob.contact_phone ?
                                <Text  style={{color:'#525252',fontSize:16,fontWeight: 'bold',marginBottom:5}}>Số điện thoại: <Text
                                    style={styles.textContent}>{this.props.parentJob.contact_phone}</Text></Text>
                                : null}
                            {this.props.parentJob.contact_email ?
                                <Text  style={{color:'#525252',fontSize:16,fontWeight: 'bold',marginBottom:5}}>Email: <Text
                                    style={styles.textContent}>{this.props.parentJob.contact_email}</Text></Text>
                                : null}

                        </View>
                </View>
            </View>
        );
    }
}

// companyDetailReducers

const mapStateToProps = (state) => {
    return {
        // user: state.loginReducers,
        // jobContent: state.jobDetailReducers,
        companyContent: state.companyDetailReducers,
    }
};

const CompanyJob = connect(mapStateToProps, null)(Company);
export default CompanyJob
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    item: {
        padding: 10,
    },

    textContent: {
        fontWeight: '400',
        color:'#525252'
    },
    textTitle: {
        fontSize: 15,
        color: 'green',
    },
    textHeader: {
        fontSize: 20,
        fontWeight: '500',
        color:'#525252',
        marginTop: 15,
        marginBottom: 5,

    },

    modal: {
        // justifyContent: 'center',
        // alignItems: 'center'
        padding: 15
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 300
    },


});
