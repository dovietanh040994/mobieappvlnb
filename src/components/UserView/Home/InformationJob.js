import React, {Component} from "react";
import {Image, ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";
import {Body, Button, CardItem, Icon, Left, Right, Text} from "native-base";
import {JobDetailAction, SaveJobAction} from "../../../actions";
import {connect} from "react-redux";
import YouTube from "react-native-youtube";

class Information extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props.parentJob)
        return (

          <View style={{paddingLeft:15,paddingRight:15}}>
              {this.props.parentJob.video_id ?
                  <View style={{marginBottom: 8}}><YouTube
                      apiKey='AIzaSyCSi4zmrk7iusmID8zBNugozgT8UCfIXgI'
                      videoId={this.props.parentJob.video_id}   // The YouTube video ID
                      play={true}             // control playback of video with true/false
                      fullscreen={false}       // control whether the video should play in fullscreen or inline
                      loop={true}             // control whether the video should loop when ended
                      style={{alignSelf: 'stretch', height: 250}}
                  /></View> : null}

              <View style={{marginBottom: 5}}>
                  {this.props.parentJob.job_salary ?
                      <View style={{flexDirection:'row'}}>
                          <View style={{width: 14 }}>
                              <Image source={require('../../../vendor/images/Group2.png')} style={{marginTop: 6}} />
                          </View>

                          <Text style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Mức lương: <Text
                              style={styles.textContent}>{this.props.parentJob.job_salary}</Text></Text>
                      </View>
                      : null}
              </View>
              <View style={{marginBottom: 5}}>
                  {this.props.parentJob.job_type ?
                      <View style={{flexDirection:'row'}}>
                          <View style={{width: 14 }}>
                          <Image source={require('../../../vendor/images/Group3.png')} style={{marginTop: 6}} />
                          </View>
                          <Text  style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Hình thức làm việc: <Text
                              style={styles.textContent}>{this.props.parentJob.job_type}</Text></Text>
                      </View>
                      : null}
              </View>
              <View style={{marginBottom: 5}}>
                  {this.props.parentJob.number_of_recruitment ?
                      <View style={{flexDirection:'row'}}>
                          <View style={{width: 14 }}>
                          <Image source={require('../../../vendor/images/Group4.png')} style={{marginTop: 6}} />
                          </View>
                          <Text  style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Số lượng cần tuyển: <Text
                              style={styles.textContent}>{this.props.parentJob.number_of_recruitment}</Text></Text>
                      </View>

                      : null}
              </View>
              <View style={{marginBottom: 5}}>
                  {this.props.parentJob.job_level ?
                      <View style={{flexDirection:'row'}}>
                          <View style={{width: 14 }}>
                          <Image source={require('../../../vendor/images/Vector2.png')} style={{marginTop: 6}} />
                          </View>
                          <Text  style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Trình độ: <Text
                              style={styles.textContent}>{this.props.parentJob.job_level}</Text></Text>
                      </View>
                      : null}
              </View>
              <View style={{marginBottom: 5}}>
                  {this.props.parentJob.job_experience ?
                      <View style={{flexDirection:'row'}}>
                          <View style={{width: 14 }}>
                          <Image source={require('../../../vendor/images/Group5.png')} style={{marginTop: 6}} />
                          </View>
                          <Text  style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Kinh nghiệm: <Text
                              style={styles.textContent}>{this.props.parentJob.job_experience}</Text></Text>
                      </View>
                      : null}
              </View>
              <View style={{marginBottom: 5}}>
                  {this.props.province && this.props.parentJob.id ?
                      <View style={{flexDirection:'row'}}>
                          <View style={{width: 14 }}>
                          <Image source={require('../../../vendor/images/Shape1.png')} style={{marginTop: 6}} />
                          </View>
                          <Text  style={{paddingLeft:5,color:'#525252',fontSize:16,fontWeight: 'bold'}}>Địa điểm làm việc: <Text
                              style={styles.textContent}>{this.props.province}</Text></Text>
                      </View>
                      : null}
              </View>
              <View >
                  {this.props.parentJob.job_description ?
                      <View>
                          <Text style={styles.textHeader}>Mô tả công việc</Text>
                          <Text
                              style={styles.textContent}>{this.props.parentJob.job_description}</Text>
                      </View>
                      : null
                  }
              </View>
              <View>
                  {this.props.parentJob.job_benefit ?
                      <View>
                          <Text style={styles.textHeader}>Quyền lợi được hưởng</Text>
                          <Text
                              style={styles.textContent}>{this.props.parentJob.job_benefit}</Text>
                      </View>
                      : null}
              </View>

              <View>
                  {this.props.parentJob.job_requirement ?
                      <View>
                          <Text style={styles.textHeader}>Yêu cầu công việc</Text>
                          <Text
                              style={styles.textContent}>{this.props.parentJob.job_requirement}</Text>
                      </View>
                      : null}
              </View>
              <View>
              {this.props.parentJob.job_cv ?
                  <View>
                      <Text style={styles.textHeader}>Yêu cầu hồ sơ</Text>
                      <Text
                          style={styles.textContent}>{this.props.parentJob.job_cv}</Text>
                  </View>
                  : null}
              </View>
          </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        jobContent: state.jobDetailReducers,
    }
};

const InformationJob = connect(mapStateToProps, null)(Information);

export default InformationJob
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    item: {
        padding: 10,
    },

    textContent: {
        fontWeight: '400',
        color:'#525252',
        textAlign: 'justify'
    },
    textTitle: {
        fontSize: 15,
        color: 'green',
    },
    textHeader: {
        fontSize: 20,
        fontWeight: '500',
        color:'#525252',
        marginTop: 15,
        marginBottom: 5,

    },

    modal: {
        // justifyContent: 'center',
        // alignItems: 'center'
        padding: 15
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 300
    },


});
