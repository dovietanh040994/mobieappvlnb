import React, {Component, PureComponent} from 'react';
import {
    Image,
    Platform,
    View,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    ScrollView, Animated, StyleSheet, ActivityIndicator, BackHandler
} from 'react-native';
import {
    Container,
    Header,
    Footer,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Body,
    Content,
    Text,
    Input,
    Card,
    CardItem,
    Item, Picker, Form, Toast,Textarea
} from "native-base";
import {Field, reduxForm} from 'redux-form';
import {SEARCH_ADVANCED, USER_FORM} from '../../../../vendor/formNames'

//Styles
import {StylesAll} from '../../../../vendor/styles'
import {AdvancedDetail, HomeBox, Profile, Search, ShowProfile} from "../../../../vendor/screen";
import {connect} from "react-redux";
import BtnSearchAdvanced from './BtnSearchAdvanced'
import DatePicker from 'react-native-datepicker'
// import submitEditForm from './SubmitServer/submitEditForm'
import {EditUserAction, SearchAdvancedAction, SearchFormAction} from "../../../../actions";
import {getStatusBarHeight} from "react-native-status-bar-height";
import Modal from 'react-native-modalbox';
import {bindActionCreators} from "redux/es/redux";
import ListDetailModal from "./ListDetailModal"
import NavigationService from "../../../../vendor/NavigationService";



// const validate = values => {
//     console.log('asdasd111')
//     console.log(values)
//
//     const errors = {};
//     if (!values.province) {
//         errors.province = 'Tỉnh/thành phố không được để trống'
//     }
//     if (!values.jobTypes) {
//         errors.jobTypes = 'Công việc không được để trống'
//     }
//     if (!values.experience) {
//         errors.experience = 'Kinh nghiệm không được để trống'
//     }
//     if (!values.carrer) {
//         errors.carrer = 'Ngành nghề không được để trống'
//     }
//     if (!values.salary) {
//         errors.salary = 'Mức lương không được để trống'
//     }
//
//
//     return errors
// }


// const warn = values => {
//     const warnings = {};
//     if (values.age < 19) {
//         warnings.age = 'You seem a bit young...'
//     }
//     return warnings
// }
const submitEditForm = (values, dispatch) => {
    // dispatch(SearchFormAction(values));
    // Toast.show({
    //     text: "Cập nhật thông tin thành công!",
    //     // buttonText: "Đóng",
    //     type: "success"
    // })
    NavigationService.navigate(AdvancedDetail, { search: values });

};


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

class EditProfileForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            dislay:false,
            dataList:'',
            title:'',
            functionPropIDHere:'',
            PropNameHere:'',
            province:'',
            jobTypes:'',
            experience:'',
            carrer:'',
            salary:'',
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);

    }

    static navigationOptions = () => ({
        header: null
    });


    componentWillMount() {
        BackHandler.addEventListener('AdvancedSearch', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('AdvancedSearch', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(Search)
        return true;

    }

    renderPickerInput = ({label,placeholder, keyboardType, meta: {touched, error, warning}, input: {onChange, ...restInput}}) => {

        return (
            <View>
                <TextInput
                    underlineColorAndroid={'transparent'}
                    placeholder={placeholder}
                    style={{opacity: 0,position:'absolute'}}
                    keyboardType={keyboardType} onChangeText={onChange} {...restInput}  editable={false}
                />
                {/*{touched && ((error && <Text style={{color: 'red', fontSize: 14}}>{error}</Text>) ||*/}
                {/*    (warning && <Text style={{color: 'orange'}}>{warning}</Text>))}*/}


            </View>
        );
    }

    render() {
        // const DismissKeyboard = ({ children }) => (
        //     <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        //         {children}
        //     </TouchableWithoutFeedback>
        // );
        // const pushData = ( value ) => (
        //
        // );
        // console.log(this.state.dataList)

        return (
            // ,marginTop: getStatusBarHeight(true)
            <Container style={{backgroundColor: '#fff'}}>
                <Header style={{
                    backgroundColor: '#fff',
                    paddingLeft: 20,
                    height: 50,
                    borderBottomWidth: 0,
                    borderBottomColor: '#ccc',
                    elevation: 1
                }}>
                    <Left>
                        <TouchableOpacity style={{width:50,height:30,position:'absolute',left:-20,top:-13}} transparent onPress={() => {
                            this.props.navigation.navigate(Search)
                        }}>
                            <Image source={require('../../../../vendor/images/arr_left.jpg')} style={{
                                tintColor: '#33B8E0', marginTop: 5,marginLeft:23
                            }}/>
                        </TouchableOpacity>
                    </Left>
                    <Body style={{flex: 3}}>
                        <Title style={{fontSize: 17, color: '#33b8e0', fontWeight: 'bold', marginLeft: -15}}>Tìm kiếm nâng cao</Title>
                    </Body>
                </Header>
                <Content style={{paddingTop:5}} >
                    {/*<Content padder  keyboardShouldPersistTaps={'handled'}>*/}
                    {/*<DismissKeyboard>*/}

                    <View style={{
                        // borderWidth: 1,
                        borderRadius: 5,
                        overflow: 'hidden',
                        // borderColor: '#ccc',
                        paddingLeft: 15,
                        paddingRight: 15,
                        backgroundColor: 'white'

                    }}>
                        <View style={[styles.boxAdvanced]} >
                            <Text style={styles.title}>
                                Địa điểm
                            </Text>
                            <TouchableOpacity onPress={ async () => {
                                await this.setState({
                                    dataList:this.props.province,
                                    title:'Chọn địa điểm',
                                    functionPropIDHere:'province',
                                });
                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()
                            }} style={styles.button}
                            >
                                <View style={styles.field}>
                                    <Text style={styles.fieldText}>{this.state.province}</Text>
                                </View>
                                <Field   name="province" component={this.renderPickerInput}   >
                                </Field>

                                <Image source={require('../../../../vendor/images/Polygon.png')} style={styles.iconDropdown}/>
                            </TouchableOpacity>

                        </View>
                        <View style={[styles.boxAdvanced]} >
                            <Text style={styles.title}>
                                Ngành nghề
                            </Text>
                            <TouchableOpacity onPress={ async () => {
                                await this.setState({
                                    dataList:this.props.carrer,
                                    title:'Chọn ngành nghề',
                                    functionPropIDHere:'carrer'
                                });
                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()
                            }} style={styles.button}
                            >
                                <View style={styles.field}>
                                    <Text style={styles.fieldText}>{this.state.carrer}</Text>
                                </View>
                                <Field   name="carrer" component={this.renderPickerInput}   >
                                </Field>
                                <Image source={require('../../../../vendor/images/Polygon.png')} style={styles.iconDropdown}/>
                            </TouchableOpacity>

                        </View>
                        <View style={[styles.boxAdvanced]} >
                            <Text style={styles.title}>
                                Mức lương
                            </Text>
                            <TouchableOpacity onPress={ async () => {
                                await this.setState({
                                    dataList:this.props.salary,
                                    title:'Chọn mức lương',
                                    functionPropIDHere:'salary'
                                });
                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()
                            }} style={styles.button}
                            >
                                <View style={styles.field}>
                                    <Text style={styles.fieldText}>{this.state.salary}</Text>
                                </View>
                                <Field   name="salary" component={this.renderPickerInput}   >
                                </Field>
                                <Image source={require('../../../../vendor/images/Polygon.png')} style={styles.iconDropdown}/>
                            </TouchableOpacity>

                        </View>
                        <View style={[styles.boxAdvanced]} >
                            <Text style={styles.title}>
                                Kinh nghiệm
                            </Text>
                            <TouchableOpacity onPress={ async () => {
                                await this.setState({
                                    dataList:this.props.experience,
                                    title:'Chọn kinh nghiệm',
                                    functionPropIDHere:'experience'
                                });
                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()
                            }} style={styles.button}
                            >
                                <View style={styles.field}>
                                    <Text style={styles.fieldText}>{this.state.experience}</Text>
                                </View>
                                <Field   name="experience" component={this.renderPickerInput}   >
                                </Field>
                                <Image source={require('../../../../vendor/images/Polygon.png')} style={styles.iconDropdown}/>
                            </TouchableOpacity>

                        </View>
                        <View style={[styles.boxAdvanced]} >
                            <Text style={styles.title}>
                                Loại hình
                            </Text>
                            <TouchableOpacity onPress={ async () => {

                                await this.setState({
                                    dataList:this.props.jobTypes,
                                    title:'Chọn loại hình',
                                    functionPropIDHere:'jobTypes'
                                });
                                await this.refs.listModel.changeDataList()
                                await this.refs.listModel.showAddModal()
                            }} style={styles.button}
                            >
                                <View style={styles.field}>
                                    <Text style={styles.fieldText}>{this.state.jobTypes}</Text>
                                </View>
                                <Field   name="jobTypes" component={this.renderPickerInput}   >
                                </Field>
                                <Image source={require('../../../../vendor/images/Polygon.png')} style={styles.iconDropdown}/>
                            </TouchableOpacity>

                        </View>

                        <BtnSearchAdvanced/>
                    </View>
                    {/*</DismissKeyboard>*/}

                </Content>
                {/*{console.log(this.state.dataList)}*/}
                {this.state.dataList !== ''?
                    <ListDetailModal
                        ref={'listModel'}
                        dataList={this.state.dataList}
                        title={this.state.title}
                        functionPropIDHere={(value)=>{this.props.change(this.state.functionPropIDHere, `${value}`)}}
                        PropNameHere={(value)=>{
                            if(this.state.functionPropIDHere === 'carrer'){
                                this.setState({carrer:`${value}`})
                            }else if (this.state.functionPropIDHere === 'province'){
                                this.setState({province:`${value}`})
                            }else if (this.state.functionPropIDHere === 'salary'){
                                this.setState({salary:`${value}`})
                            }else if (this.state.functionPropIDHere === 'experience'){
                                this.setState({experience:`${value}`})
                            }else if (this.state.functionPropIDHere === 'jobTypes'){
                                this.setState({jobTypes:`${value}`})
                            }
                        }}
                    />
                    :null}


            </Container>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.loginReducers,
        province: state.provinceReducers,
        salary: state.salaryReducers,
        carrer: state.carrerReducers,
        jobTypes: state.jobTypesReducers,
        experience: state.experienceReducers,
        search: state.searchHomeReducers,
    }
};

const EditProfileUser = connect(mapStateToProps, null)(EditProfileForm);
//
export default reduxForm({
    form: SEARCH_ADVANCED, // a unique identifier for this form
    keepDirtyOnReinitialize: true,
    enableReinitialize: true,
    updateUnregisteredFields: true,
    // validate,
    // warn,
    onSubmit: submitEditForm
})(EditProfileUser)
const styles = StyleSheet.create({
    boxAdvanced: {
        // borderBottomWidth: 1,
        // borderColor: '#ccc',
        flexDirection:'column',
        paddingBottom: 0,
        paddingTop: 0,
        marginTop: 15
    },
    title: {
        fontSize:18,
        color:'#33B8E0',
        marginBottom:5
    },
    button:{
        borderWidth:1,
        borderColor:'#ccc',
        borderBottomWidth:1,
        borderRadius:5
    },
    field:{
        color:'#525252',
        height:48

    },
    fieldText:{
        color:'#525252',
        marginTop:13,
        marginLeft:15
    },
    iconDropdown:{
        tintColor: '#33B8E0',
        position:'absolute',
        right:20,top:20
    }

});
