import React, {PureComponent} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler
} from 'react-native';
import FlatListItemJob from "../../ChildComponent/FlatListItemJob";
import {Button, Container, Header, Item} from "native-base";
import {Home, HomeBox, JobDetail, Search, SearchAdvanced} from "../../../../vendor/screen";
import {keyWorksAction} from "../../../../actions";
import {connect} from "react-redux";
import PostsComponent from "../JobDetailComponent";
import AsyncStorage from "@react-native-community/async-storage";
import PlaceholderLoadingJob from "../../../Helpers/PlaceholderLoadingJob";


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

class SearchHome extends PureComponent {

    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);
        //hàm tạo ref
        this.state = {
            refreshing: false,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            query: '',
            keyWords: [],
            recentSearches: null,
            userInfo:null,
            notData: false
        };
        this.page = 1;
        this.check1 = 0;
        this.check2 = 0;
        this.saved = [];
        this.applied = [];
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount() {
        AsyncStorage.getItem('userInfo',(err, result) =>{
            this.setState({
                userInfo:result
            })
        })
    }

    componentWillMount() {
        BackHandler.addEventListener('Search', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('Search', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate(HomeBox)
        return true;

    }

    onChange = value => {

        this.check1 = this.check1 + 1
        sleep(500).then(() => {
            this.check2 = this.check2 + 1
            if (this.check1 === this.check2) {
                if (value.trim() !== '') {
                    this.setState({

                        isListEnd: false,
                        serverData: [],
                        fetching_from_server: false,
                    })
                    this.page = 1;
                    this.loadMoreData(value.trim())


                    //luu keywords da search
                    AsyncStorage.getItem('recentSearches', (err, result) => {
                        const data = [value.trim()];
                        const data2 = [result];
                        if (result !== null) {
                            // console.log('Data Found', result);
                            var newData = JSON.parse(data2).concat(data);
                            AsyncStorage.setItem('recentSearches', JSON.stringify(newData));
                        } else {
                            // console.log('Data Not Found');
                            AsyncStorage.setItem('recentSearches', JSON.stringify(data));
                        }
                    });


                }

            }
        })

    };

    loadMoreData = async (value) => {
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})
            const Url = `http://vieclamnambo.vn:9002/api/vlnb/job/getjobpost?key_search=${value}&rows_start=${this.page}`;
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo

                },
            }).then((response) => response.json())

                .then((responseJson) => {
                    if (responseJson.total === 0){
                        this.setState({
                            notData: true
                        });
                    }
                    if (responseJson.jobPosts.length > 0) {

                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.jobPosts],
                            fetching_from_server: false,
                            notData: false

                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (

            <View style={styles.footer}>
                {this.state.fetching_from_server ? (  this.page === 1?
                        <PlaceholderLoadingJob/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    saveHeart(){
        const { navigation } = this.props;
        const jobId = navigation.getParam('jobId', null)
        const saveHeart = navigation.getParam('saveHeart', null)
        if(jobId !== null){
            if(saveHeart === 1){
                var index = this.saved.indexOf(jobId);
                if (index === -1) {
                    this.saved.push(jobId)
                }

            }else{
                var index = this.saved.indexOf(jobId);
                if (index !== -1) {
                    this.saved.splice(index,1);
                }
            }
        }
    }

    jobApplied(){
        const { navigation } = this.props;
        const jobApplied = navigation.getParam('jobApplied', null)
        if(jobApplied !== null){
            var index = this.applied.indexOf(jobApplied);
            if (index === -1) {
                this.applied.push(jobApplied)
            }
        }
    }

    render() {
        AsyncStorage.getItem('recentSearches', (err, result) => {
            this.setState({recentSearches: result})
            // console.log('xem', this.state.recentSearches);

        });

        const arrRecentSearches = () => {
            //loai bo phan tu trung lap trong mang
            if (this.state.recentSearches !== null) {
                const intArray = JSON.parse(this.state.recentSearches)
                const arrayOfUniques = [];
                intArray.forEach((element) => {
                    if (!arrayOfUniques.includes(element)) {
                        arrayOfUniques.push(element)
                    }
                });
                return arrayOfUniques;
            }
        }


        this.saveHeart();
        this.jobApplied();
        return (
            <View style={[styles.fill]}>

                <Header rounded style={{
                    backgroundColor: '#fff',
                    height: 60,
                    borderBottomWidth: 0.5,
                    borderColor: '#ccc',
                    paddingLeft: 0,
                    paddingRight: 0,
                    elevation: 0
                }}>
                    {/*<ImageBackground source={require('../../vendor/images/Rectangle.png')}*/}
                    {/*                 style={{width: '100%', height: '100%'}}>*/}
                    <View style={{flex: 1, flexDirection: 'row', marginTop: 10, paddingLeft: 15, paddingRight: 10}}>
                        <View style={{flex: 1}}>
                            <TouchableOpacity style={{width: 60, height: 40, position: 'absolute', left: -15}}
                                              transparent onPress={() => {
                                this.props.navigation.navigate(HomeBox)
                            }}>
                                <Image source={require('../../../../vendor/images/arr_left.jpg')} style={{
                                    tintColor: '#33B8E0', marginTop: 11, marginLeft: 25
                                }}/>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 7, paddingLeft: 10, paddingRight: 10}}>


                            <View style={{position: 'absolute', left: 25, top: 7}}>
                                <Image source={require('../../../../vendor/images/Search.png')} style={{
                                    tintColor: '#33B8E0'
                                }}/>
                            </View>

                            <TextInput
                                style={{
                                    width: '100%',
                                    borderWidth: 1,
                                    borderColor: '#ECEBED',
                                    borderRadius: 5,
                                    top: 0,
                                    height: 40,
                                    fontSize: 15,
                                    paddingLeft: 50,
                                    paddingRight: 10
                                }}
                                placeholder="Nhập công việc bạn muốn tìm kiếm"
                                // onChangeText={this.onChange}
                                onChangeText={(text) => {
                                    this.setState({query: text})
                                    this.onChange(text)
                                }}
                                value={this.state.query}


                            />


                        </View>
                        <View style={{flex: 1}}>
                            <Button style={{
                                backgroundColor: '#33B8E0',
                                borderRadius: 5,
                                height: 40,
                                paddingTop: 0,
                                paddingBottom: 0,
                                paddingLeft: 12,
                                paddingRight: 12
                            }} onPress={()=>{
                                this.props.navigation.navigate(SearchAdvanced);
                            }}>
                                <Image source={require('../../../../vendor/images/Group.png')} style={{
                                    tintColor: '#fff'
                                }}/>
                            </Button>
                        </View>
                    </View>
                    {/*</ImageBackground>*/}


                </Header>
                {
                    this.state.query === '' ?
                        <ScrollView>
                            <View style={{paddingLeft: 10, paddingRight: 10, marginTop: 20}}>

                                <View style={{flexDirection: 'row', flexWrap: 'wrap',}}>
                                    {this.props.keyWorks.itemsKeyWorks.map((value, index) => {
                                        return (
                                            <View key={index} style={{
                                                backgroundColor: '#fff',
                                                borderColor: '#ccc',
                                                borderRadius: 5,
                                                padding: 10, marginBottom: 10, marginRight: 10
                                            }}>
                                                <TouchableOpacity onPress={() => {
                                                    this.setState({query: value.name})
                                                    this.onChange(value.name)
                                                }}>
                                                    <Text>{value.name}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })}
                                </View>

                                <View style={{
                                    flexDirection: 'column',
                                    backgroundColor: '#fff',
                                    borderColor: '#ccc',
                                    borderRadius: 5,
                                    padding: 12
                                }}>

                                    <View style={{flexDirection: 'row'}}>
                                        <View style={{flex: 3, alignItems: 'flex-start'}}>
                                            <Text style={{color: '#bababa', fontSize: 16, fontWeight: '500'}}>TÌM KIẾM
                                                GẦN ĐÂY</Text>
                                        </View>
                                        <TouchableOpacity style={{flex: 1, alignItems: 'flex-end',right:-10,top:-10,width:60, height: 40,position:'absolute'}}
                                                          onPress={() => {
                                                              AsyncStorage.removeItem('recentSearches');
                                                              this.setState({recentSearches: null})
                                                          }}>
                                            <Text style={{
                                                color: '#33B8E0',
                                                fontSize: 12,
                                                fontWeight: '500',
                                                marginTop: 14,
                                                marginRight: 20
                                            }}>Xóa</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {this.state.recentSearches !== null ? arrRecentSearches().map((value, index) => {
                                        return (
                                            <View style={{marginTop: 15, paddingLeft: 10}}  key={index}>
                                                <TouchableOpacity onPress={() => {
                                                    this.setState({query: value})
                                                    this.onChange(value)
                                                }}>
                                                    <Text style={{
                                                        color: '#525252',
                                                        fontSize: 15,
                                                        fontWeight: '500',
                                                        marginBottom: 5
                                                    }}>{value}</Text>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    }) : null}
                                </View>

                            </View>
                        </ScrollView>
                        :
                            this.state.notData === false?
                                <FlatList
                                style={[styles.fill]}
                                data={this.state.serverData}
                                onEndReached={() => this.loadMoreData(this.state.query)}
                                onEndReachedThreshold={0.5}
                                renderItem={({item, index}) => {
                                    return (
                                        <View style={{marginBottom: 15, marginTop: index === 0 ? 15 : 0}}>
                                            <FlatListItemJob
                                                item={item}
                                                index={index}
                                                parentFlatList={this}
                                                nameScreen={'Search'}
                                                saveHeart={ this.saved.indexOf(item.id) !== -1? 1 :item.saved}
                                                applied={ this.applied.indexOf(item.id) !== -1? 1 : item.applied }
                                                navigation={this.props.navigation}
                                                random = {index % 4}
                                            />
                                        </View>
                                    );
                                }}
                                keyExtractor={(item, index) => index.toString()}
                                ItemSeparatorComponent={() => <View style={styles.separator}/>}
                                ListFooterComponent={this.renderFooter.bind(this)}
                            />
                            :
                            <View>
                                <View style={{alignItems: 'center',marginTop:90}}>
                                    <Image source={require('../../../../vendor/images/Frame.png')} />
                                    <Text style={{color:'#8C8888',fontWeight:'500',fontSize:16,marginTop:30}}>Không tìm thấy công việc phù hợp</Text>
                                </View>
                            </View>

                }


            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        keyWorks: state.keyWorksReducers,
    }
};
const SearchComponent = connect(mapStateToProps, null)(SearchHome);
export default SearchComponent

const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4',
        // paddingTop:70

    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 80,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 78,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },

    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },

    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
