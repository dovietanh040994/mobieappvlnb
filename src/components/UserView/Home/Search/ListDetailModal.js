import React, {Component, PureComponent} from 'react';
import Modal from 'react-native-modalbox';
import {Button, Container, Icon, Text,Left,Body,Header,Right} from "native-base";
import {
    ActivityIndicator,
    Image,
    ScrollView,
    StyleSheet,
    View,
    Linking,
    TouchableOpacity,
    TextInput, Dimensions, FlatList, Animated
} from "react-native";
import YouTube from "react-native-youtube";
import {connect} from "react-redux";
import {Field, reduxForm} from 'redux-form';
import email from 'react-native-email'
import {getStatusBarHeight} from "react-native-status-bar-height";
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;
var change_alias = (alias) => {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
    str = str.replace(/đ/g,"d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    str = str.replace(/ + /g," ");
    str = str.trim();
    return str;
}
class ListDetail extends PureComponent{
    constructor(props) {
        super(props)

        this.state = {
            dataSource: '',
        };
    }


    showAddModal = () => {
        return (
            this.refs.myModal.open()
        )
    }
    someFunctionInChildComponent(value){
        this.props.functionPropIDHere(value);

    }
    NameComponent(value){
        this.props.PropNameHere(value);

    }
    changeDataList(){
        this.setState({
            dataSource: this.props.dataList
        })
    }
    SearchFilterFunction(text) {
        //passing the inserted text in textinput
        const newData = this.props.dataList.filter(function(item) {
            //applying filter for the inserted text in search bar
            const itemData = change_alias(item.name) ? change_alias(item.name).toUpperCase() : ''.toUpperCase();

            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            dataSource: newData,
        });
    }
    render() {
        return (
            <Modal
                position={"center"}
                ref={'myModal'}
                swipeToClose={false}
                style={{justifyContent: 'center',
                    alignItems: 'center',height: screenHeight-200,
                    width: screenWidth - 50,backgroundColor:'#fff'}}
                // onClosed={()=>{
                //     console.log(1111)
                //     this.props.array.removeAll("name");
                // }}
            >
                <View style={{width:'100%',flex:1,alignItems:'flex-start'}}>
                    <View style={{marginTop:5,position:'absolute',width:'100%'}}>
                        <View style={{borderBottomWidth:1,height:35,paddingRight:10,paddingLeft:10,borderColor:'#ccc',width:'100%'}}>
                            <View style={{alignItems:'center'}}>
                                <Text style={{color:'#33B8E0',fontSize:18,fontWeight: 'bold',marginTop:2}}>{this.props.title}</Text>
                            </View>
                            <TouchableOpacity  style={{position:'absolute',flex:1,justifyContent:'center',alignItems:'center',right:0,top:-5,width:70,height:40}}
                                               onPress={() => {
                                                   this.refs.myModal.close()
                                               }}>
                                <Image source={require('../../../../vendor/images/delete.png')} style={{
                                }}/>
                            </TouchableOpacity>

                        </View>
                        <TouchableOpacity  style={{backgroundColor:'#fff',paddingTop:10,paddingLeft:15,paddingRight:15,width:'100%'}}  onPress={()=>{
                            // this.props.navigation.navigate(Search);
                        }}>
                            <View  style={{position:'absolute',left:25,top:17}}>
                                <Image source={require('../../../../vendor/images/Search.png')} style={{
                                    tintColor: '#33B8E0'
                                }}/>
                            </View>
                            <TextInput
                                style={{width:'100%',borderWidth:1,borderColor: '#ccc',borderRadius:5,top:0,height:40,fontSize:15,paddingLeft:50,paddingRight:10}}
                                placeholder="Nhập vào từ khóa"
                                onChangeText={text => this.SearchFilterFunction(text)}
                            />

                        </TouchableOpacity>
                    </View>

                    <FlatList
                        style={{marginTop:100,marginBottom:10,width:'100%'}}
                        data={this.state.dataSource}
                        renderItem={({ item }) => (
                            <View
                                style={{
                                    paddingLeft:15,
                                    // borderBottomWidth:1,
                                    // borderColor:'#ccc'
                                }}
                            >
                                <TouchableOpacity style={{paddingTop:5,paddingBottom :7}}  onPress={() => {
                                    this.someFunctionInChildComponent(item.id)
                                    this.NameComponent(item.name)
                                    this.refs.myModal.close()
                                }}>
                                    <Text  style={{color:'#525252',fontSize:18}}>{item.name}</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                        enableEmptySections={true}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </Modal>



        );
    }
}

const mapStateToProps = (state) => {
    return {
        province: state.provinceReducers,
    }
};

const ListDetailModal = connect(mapStateToProps, null, null, { forwardRef: true })(ListDetail);

export default ListDetailModal
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 30,
    },
    item: {
        padding: 10,
    },

    textContent: {
        fontSize: 15,
        color: 'black',
    },
    textTitle: {
        fontSize: 15,
        color: 'green',
    },
    textHeader: {
        fontSize: 15,
        fontWeight: '500',
        marginTop: 15,
        marginBottom: 5,

    },

    modal: {
        // justifyContent: 'center',
        // alignItems: 'center'
        padding: 15
    },

    modal2: {
        height: 230,
        backgroundColor: "#3B5998"
    },

    modal3: {
        height: 300,
        width: 300
    },

    modal4: {
        height: 300
    },


});
