import React, {PureComponent} from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    View,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    ImageBackground,
    TouchableOpacity,
    Image,
    TextInput,
    ScrollView,
    Dimensions,
    BackHandler
} from 'react-native';
import FlatListItemJob from "../ChildComponent/FlatListItemJob";
import PlaceholderLoadingJob from "../../Helpers/PlaceholderLoadingJob";
import {Body, Button, CardItem, Container, Header, Item, Left, Right, Toast} from "native-base";
import {HomeBox, JobDetail, Search, SearchAdvanced, SelectInfo} from "../../../vendor/screen";
import {
    CategoryPost,
    FetchCarrerList,
    FetchExperienceList, FetchJobTypesList,
    FetchProvinceList,
    FetchSalaryList,
    keyWorksAction
} from "../../../actions";
import {connect} from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import {withNavigation} from "react-navigation";
import {GoogleSignin} from "react-native-google-signin";



const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 60 : 0;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
var screenWidth = Dimensions.get('window').width;


const menu = [
    {
        id: 0,
        name: "Nổi bật",
    },
    {
        id: 1,
        name: "Việc phù hợp",
    },
    {
        id: 2,
        name: "Việc mới",
    },
    {
        id: 3,
        name: "Khu công nghiệp",
    },
    {
        id: 4,
        name: "Lương cao",
    },

]
class Home extends PureComponent {

    static navigationOptions = () => ({
        header: null
    });

    constructor(props) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(
                // iOS has negative initial scroll value because content inset...
                0,
            ),
            refreshing: false,
            bgButton: 0,
            idCategory: 0,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false,
            userInfo:null,
            notData: false,
            doubleBackToExitPressedOnce: false,

        };
        this.page = 1;
        this.saved = [];
        this.applied = [];
        this.handleBackButton = this.handleBackButton.bind(this);
        this.jobId = '';
        this.jobAppliedId = ''
    }
    componentDidMount() {
        AsyncStorage.getItem('userInfo',(err, result) =>{
            this.setState({
                userInfo:result
            },()=>{
                this.loadMoreData();
                this.props.keyWorks();
                this.props.category();
            })
        })
        const { navigation } = this.props;

        this.focusListener = navigation.addListener('didFocus', () => {
            AsyncStorage.getItem('homeReload', (err, result) => {
                console.log('vietanh1',result)

                if(result){
                    console.log('vietanh11',result)

                    this.setState({
                        isListEnd: false,
                        serverData: [],
                        fetching_from_server: false,
                        // jobId:null
                    }, () => {
                        this.page = 1
                        this.loadMoreData()
                        this.saved = []
                        this.jobId = this.props.navigation.setParams({jobId :  null})
                        this.applied = []
                        this.jobAppliedId = this.props.navigation.setParams({jobApplied :  null})
                    });
                    AsyncStorage.removeItem('homeReload');

                }

            });

        });
    }

    // shouldComponentUpdate(nextProps, nextState, nextContext) {
    //     if(this.state.notData === false){
    //         return false
    //     }
    // }

    componentWillMount() {
        BackHandler.addEventListener('HomeComponent', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('HomeComponent', this.handleBackButton);
        this.focusListener.remove();
    }

    handleBackButton = () => {
        if (this.state.doubleBackToExitPressedOnce) {
            BackHandler.exitApp();
        }else{
            Toast.show({
                text: "Bấm thêm một lần nữa để thoát",
            })
            this.setState({doubleBackToExitPressedOnce: true});
            setTimeout(() => {
                this.setState({doubleBackToExitPressedOnce: false});
            }, 2000);
        }

        return true;
    }

    loadMoreData = async () => {
        console.log(!this.state.fetching_from_server && !this.state.isListEnd)
        if (!this.state.fetching_from_server && !this.state.isListEnd) {
            this.setState({fetching_from_server: true})

            var sort = () => {
                if (this.state.idCategory === 0) {
                    return `is_hot=1`;
                }else if (this.state.idCategory === 1) {
                    const { navigation } = this.props;
                    const selectInfo = navigation.getParam('selectInfo', null);

                    return selectInfo;

                } else if (this.state.idCategory === 2) {
                    return `sort_name=id`;

                } else if (this.state.idCategory === 3) {
                    return `sort_name=industrial`;

                } else if (this.state.idCategory === 4) {
                    return `sort_name=job_salary_id`;

                }
            }
            const Url = `http://vieclamnambo.vn:9002/api/vlnb/job/getjobpost?${sort()}&rows_start=${this.page}`;

            // console.log(this.state.userInfo);
            await fetch(Url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': this.state.userInfo

                },
                // body: ``
            }).then((response) => response.json())

                .then((responseJson) => {
                    console.log(responseJson);
                    if (responseJson.total === 0){
                        this.setState({
                            notData: true
                        });
                    }
                    if (responseJson.jobPosts.length > 0) {
                        this.page = this.page + 1;
                        this.setState({
                            serverData: [...this.state.serverData, ...responseJson.jobPosts],
                            fetching_from_server: false,
                            notData: false
                        });

                    } else {
                        this.setState({
                            fetching_from_server: false,
                            isListEnd: true,
                        });

                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }

    }

    renderFooter() {
        return (

            <View style={styles.footer}>
                {this.state.fetching_from_server ? (  this.page === 1?
                        <PlaceholderLoadingJob/>
                        :
                        <ActivityIndicator size="large" color={'#ccc'} style={{marginBottom: 20, marginTop: 30, zIndex: 999}}/>
                ) : null}
            </View>
        );
    }

    TabName(stateName, idCategory) {
        // console.log(1)
        this.setState({
            bgButton: stateName,
            idCategory: idCategory,
            isListEnd: false,
            serverData: [],
            fetching_from_server: false
        }, () => {
            this.page = 1
            this.loadMoreData()

        });
    }

    saveHeart(){
        const { navigation } = this.props;
        this.jobId = this.props.navigation.getParam('jobId', null)
        console.log('vietanh22')

        console.log('jobId',this.jobId)
        console.log('jobId1',this.props.navigation.getParam('jobId', null))
        const saveHeart = navigation.getParam('saveHeart', null)
        if(this.jobId !== null){
            if(saveHeart === 1){
                var index = this.saved.indexOf(this.jobId);
                if (index === -1) {
                    this.saved.push(this.jobId)
                }

            }else{
                var index = this.saved.indexOf(this.jobId);
                if (index !== -1) {
                    this.saved.splice(index,1);
                }
            }
        }
    }

    jobApplied(){
        const { navigation } = this.props;
        this.jobAppliedId = navigation.getParam('jobApplied', null)
        if(this.jobAppliedId !== null){
                var index = this.applied.indexOf(this.jobAppliedId);
                if (index === -1) {
                    this.applied.push(this.jobAppliedId)
                }
        }
    }

    render() {

        // Because of content inset the scroll value will be negative on iOS so bring
        // it back to 0.
        const scrollY = Animated.add(
            this.state.scrollY,
            Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
        );
        const headerTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -HEADER_SCROLL_DISTANCE],
            extrapolate: 'clamp',
        });

        const imageOpacity = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, 100],
            extrapolate: 'clamp',
        });

        const titleScale = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0.8],
            extrapolate: 'clamp',
        });
        const titleTranslate = scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -60, -80],
            extrapolate: 'clamp',
        });


        this.saveHeart();
        this.jobApplied();
        console.log('kkkas',)

        return (
            <View style={[styles.fill]}>
                {  this.state.notData === false?
                <Animated.FlatList
                    style={[styles.fill]}
                    // data={this.props.salary.items.jobPosts}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
                        {useNativeDriver: true},
                    )}
                    data={this.state.serverData}
                    onEndReached={() =>
                        {
                            if (!this.onEndReachedCalledDuringMomentum) {
                                this.loadMoreData()
                                this.onEndReachedCalledDuringMomentum = true;
                            }
                        }
                    }
                    onEndReachedThreshold={0.01}
                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                    ListHeaderComponent={<View style={{height: 130}}/>}
                    renderItem={({item, index}) => {
                        return (
                            <View style={{marginBottom: 15, marginTop: index === 0 ? 15 : 0}}>
                                <FlatListItemJob
                                    item={item}
                                    index={index}
                                    parentFlatList={this}
                                    saveHeart={ this.saved.indexOf(item.id) !== -1? 1 :item.saved}
                                    applied={ this.applied.indexOf(item.id) !== -1? 1 : item.applied }
                                    navigation={this.props.navigation}
                                    random = {index % 4}
                                />
                            </View>
                        );
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    ListFooterComponent={this.renderFooter.bind(this)}
                />
                :
                    <View>
                        <View style={{height:130}}/>
                        <View style={{alignItems: 'center',marginTop:60}}>
                            <Image source={require('../../../vendor/images/Loading_Job.png')}  />
                            <Text style={{color:'#8C8888',fontWeight:'500',fontSize:16,marginTop:30}}>Không tìm thấy công việc phù hợp</Text>
                        </View>
                    </View>
                }
                <Animated.View
                    // pointerEvents="none"
                    style={[
                        styles.header,
                        {transform: [{translateY: headerTranslate}]},
                    ]}
                >

                    <Animated.View style={[

                        {
                            opacity: imageOpacity,
                            transform: [{translateY: imageTranslate}],
                        },
                    ]}>
                        <Header rounded style={{
                            backgroundColor: '#fff',
                            height: 80,
                            borderBottomWidth: 0.5,
                            borderColor: '#ccc',
                            paddingLeft: 0,
                            paddingRight: 0
                        }}>
                            <ImageBackground source={require('../../../vendor/images/Rectangle.png')}
                                             style={{width: '100%', height: '100%'}}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    marginTop: 20,
                                    paddingLeft: 15,
                                    paddingRight: 15
                                }}>
                                    <View style={{flex: 1}}>
                                        <TouchableOpacity style={{backgroundColor: '#fff', borderBottomWidth: 0}}
                                                          onPress={() => {
                                                              this.props.navigation.navigate(Search);
                                                          }}>


                                            <View style={{position: 'absolute', left: 15, top: 7}}>
                                                <Image source={require('../../../vendor/images/Search.png')} style={{
                                                    tintColor: '#33B8E0'
                                                }}/>
                                            </View>
                                            <TextInput editable={false} style={{
                                                width: '100%',
                                                borderWidth: 1,
                                                borderColor: '#ECEBED',
                                                borderRadius: 5,
                                                top: 0,
                                                height: 40,
                                                fontSize: 15,
                                                paddingLeft: 50,
                                                paddingRight: 10
                                            }} placeholder="Nhập công việc bạn muốn tìm kiếm"/>

                                        </TouchableOpacity>

                                    </View>
                                    {/*<View style={{flex:1}}>*/}
                                    {/*    <Button style={{backgroundColor:'#33B8E0',borderRadius:5,height:40,paddingTop:0,paddingBottom:0,paddingLeft:12,paddingRight:12}} onPress={()=>{*/}
                                    {/*        this.props.navigation.navigate(SearchAdvanced);*/}
                                    {/*    }}>*/}
                                    {/*        <Image source={require('../../vendor/images/Group.png')} style={{*/}
                                    {/*            tintColor: '#fff'*/}
                                    {/*        }}/>*/}
                                    {/*    </Button>*/}
                                    {/*</View>*/}
                                </View>
                            </ImageBackground>


                        </Header>
                    </Animated.View>

                </Animated.View>
                <Animated.View
                    style={[
                        styles.bar,
                        {
                            transform: [
                                // { scale: titleScale },
                                {translateY: titleTranslate},
                            ],
                        },
                    ]}
                >
                    <FlatList
                        horizontal={true}
                        data={menu}
                        showsHorizontalScrollIndicator={false}
                        ref={(ref) => {
                            this.flatListRef = ref;
                        }}
                        style={{
                            backgroundColor: '#fff',
                            borderBottomWidth: 1,
                            borderTopWidth: 1,
                            borderColor: '#ECEBED',
                            width: '100%'
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item, index}) =>
                                <View style={{
                                    borderBottomWidth:this.state.bgButton===index?1:0,
                                    borderColor:this.state.bgButton===index?'#33B8E0':'#D9D3DE',
                                    paddingLeft:5,
                                    paddingRight:5
                                }}>
                                    <TouchableOpacity style={styles.ScrollTextContainer} onPress={() => {
                                        this.flatListRef.scrollToIndex({animated: true, index: index});
                                        const stateName = index;
                                        const idCategory = item.id;
                                        this.TabName(stateName, idCategory)
                                    }}>
                                        <Text style={{
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            color: this.state.bgButton === index ? '#33B8E0' : '#D9D3DE'
                                        }}>{item.name}</Text>
                                    </TouchableOpacity>
                                </View>



                        }
                    />
                </Animated.View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        // user: state.loginReducers,
        // jobContent: state.jobDetailReducers,
        // companyContent: state.companyDetailReducers,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        category: () => {
            dispatch(CategoryPost());
        },
        keyWorks: () => {
            dispatch(keyWorksAction());

        },

    };
}

const HomeComponent = connect(mapStateToProps, mapDispatchToProps)(Home);
export default withNavigation(HomeComponent)
const styles = StyleSheet.create({
    fill: {
        flex: 1,
        backgroundColor: '#F5F4F4',
        // paddingTop:70

    },
    content: {
        flex: 1,
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#03A9F4',
        overflow: 'hidden',
        height: 80,

    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: 80,
        resizeMode: 'cover',
    },
    bar: {
        backgroundColor: 'transparent',
        marginTop: Platform.OS === 'ios' ? 28 : 78,
        height: 52,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    title: {
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        // iOS uses content inset, which acts like padding.
        paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    MainContainer: {
        backgroundColor: '#abe3a8',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'

    },
    ScrollContainer: {
        backgroundColor: '#cdf1ec',
        flexGrow: 1,
        marginTop: 0,
        width: screenWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    ScrollTextContainer: {
        fontSize: 20,
        padding: 15,
        color: '#000',
        textAlign: 'center'
    },
    ButtonViewContainer: {
        flexDirection: 'row',
        paddingTop: 35,
    },
    ButtonContainer: {
        padding: 30,
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
