import {Animated, Text, TouchableOpacity, View} from "react-native";
import {Field} from "redux-form";
import {Right} from "native-base";
import React from "react";

<View style={{
    top: 50, marginBottom: 200
}}>


    <View style={[styles.boxAdvanced]}>
        <Text style={styles.textAdvanced}>
            Trường
        </Text>
        <Field type={'text'} name="name" component={this.renderInput}
               style={styles.textPlaceholder}
               placeholder="Nhập trường">
        </Field>
    </View>

    <View style={{
        flexDirection: 'row',
        height: 56,
        alignItems: 'center'
    }}>
        <Text style={styles.textAdvanced}>
            Thời gian
        </Text>
        <Text style={styles.textBody}>
            {this.state.date_of_birth}
        </Text>
        <Right>
            <Field type="text" name="date_of_birth" component={this.renderPickerDate}>
            </Field>
        </Right>
    </View>

    <View style={[styles.boxAdvanced]}>
        <Text style={styles.textAdvanced}>

        </Text>
        <Text style={styles.textBody}>
            {this.state.date_of_birth}
        </Text>
        <Right>
            <Field type="text" name="date_of_birth" component={this.renderPickerDate}>
            </Field>
        </Right>
    </View>


    <View style={[styles.boxAdvanced]}>
        <Text style={styles.textAdvanced}>
            Chuyên ngành
        </Text>
        <Field type={'text'} name="address" component={this.renderInput}
               style={styles.textPlaceholder}
               placeholder="Nhập chuyên ngành">
        </Field>
    </View>


    <TouchableOpacity onPress={async () => {
        await this.setState({
            dataList: listGender,
            title: 'Bằng cấp',
            functionPropIDHere: 'gender'
        });
        await this.refs.listModel.changeDataList()
        await this.refs.listModel.showAddModal()
    }}>
        <View style={[styles.boxAdvanced]}>
            <Text style={styles.textAdvanced}>
                Bằng cấp
            </Text>

            <View>
                <Text style={styles.textBody}>{this.state.gender}</Text>
            </View>
            <Field type="text" keyboardType={'numeric'} name="gender"
                   component={this.renderPickerInput}>
            </Field>
            <Right style={styles.arrow_item}>
                <Animated.Image
                    source={require('../../vendor/images/profile/arrow_down.png')}
                />
            </Right>
        </View>
    </TouchableOpacity>


    <TouchableOpacity onPress={async () => {
        await this.setState({
            dataList: listStatusMarital,
            title: 'Loại tốt nghiệp',
            functionPropIDHere: 'marital'
        });
        await this.refs.listModel.changeDataList()
        await this.refs.listModel.showAddModal()
    }}>
        <View style={[styles.boxAdvanced]}>
            <Text style={styles.textAdvanced}>
                Loại tốt nghiệp
            </Text>

            <View>
                <Text style={styles.textBody}>{this.state.marital_status}</Text>
            </View>
            <Field type="text" keyboardType={'numeric'} name="marital_status"
                   component={this.renderPickerInput}>
            </Field>
            <Right style={styles.arrow_item}>
                <Animated.Image
                    source={require('../../vendor/images/profile/arrow_down.png')}
                />
            </Right>
        </View>
    </TouchableOpacity>

    <TouchableOpacity onPress={async () => {

    }}>
        <View style={[styles.boxAdvanced]}>
            <Text style={styles.textAdvanced}>
                +
            </Text>

            <View>
                <Text style={{fontSize: 15, color: '#33B8E0', fontWeight: '500'}}>Thêm
                    học
                    vấn</Text>
            </View>

        </View>
    </TouchableOpacity>

</View>
