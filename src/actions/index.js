import {
    LOGIN,
    SEARCH_FORM_HOME,
    EDITUSER,
    DETAIL_NEW_ACTION,
    DETAIL_BIG_NUMBER_ACTION,
    DETAIL_SALARY_ACTION,
    SAVE_JOB_ACTION,
    LIST_SAVE_ACTION,
    CARRER_DETAIL_ALL,
    SALARY_PROVINCE,
    SALARY_INDUSTRIAL,
    JOB_DETAIL_ACTION,
    SEARCH_ADVANCED_ACTION,
    CATEGORY_POST,
    COMPANY_DETAIL_ACTION,
    JOB_SAME_COMPANY_ACTION,
    DELETE_JOB_APPLY,
    DELETE_JOB_APPLY_SUCCEEDED,
    DELETE_JOB_CARE,
    DELETE_JOB_CARE_SUCCEEDED,
    SWITCH_NOTIFY,
    SWITCH_NOTIFY_SUCCEEDED,
    FETCH_DETAIL_USER,
    FETCH_DETAIL_USER_SUCCEEDED,
    PROVINCE_lIST,
    PROVINCE_SUCCEEDED,
    KEY_WORKS,
    CARRER_LIST,
    SALARY_LIST,
    EXPERIENCE_LIST,
    JOB_TYPES_LIST,
    UN_SAVE_JOB_ACTION,
    APPLIED_ACTION,
    SINGIN,
    VERIFY,
    FETCH_CV_USER, FETCH_CV_USER_SUCCEEDED,
    FETCH_MEDIA_RELATION, FETCH_MEDIA_RELATION_SUCCEEDED,
    UPDATE_AVATAR_USER, UPDATE_AVATAR_USER_SUCCEEDED,
    UPDATE_CV_USER, UPDATE_CV_USER_SUCCEEDED,
    FETCH_LIST_UNIVERSITY, FETCH_LIST_UNIVERSITY_SUCCEEDED,
    FETCH_LIST_MAJOR, FETCH_LIST_MAJOR_SUCCEEDED,
    FETCH_LIST_DEGREE, FETCH_LIST_DEGREE_SUCCEEDED,
    FETCH_LIST_SKILL, FETCH_LIST_SKILL_SUCCEEDED,
    FETCH_USER_INFO, FETCH_USER_INFO_SUCCEEDED,
    UPDATE_USER_INFO, UPDATE_USER_INFO_SUCCEEDED, LOGIN_EMPLOYER, SINGIN_EMPLOYER,
    FETCH_USER_CV_EMPLOYER, FETCH_USER_CV_EMPLOYER_SUCCEEDED,
    FETCH_JOB_LEVEL, FETCH_JOB_LEVEL_SUCCEEDED,
    FETCH_DISTRICT, FETCH_DISTRICT_SUCCEEDED,
    CREATE_JOB_POST, CREATE_JOB_POST_SUCCEEDED,
    UPDATE_JOB_POST, UPDATE_JOB_POST_SUCCEEDED,
    FETCH_INFO_COMPANY, FETCH_INFO_COMPANY_SUCCEEDED,
    UPDATE_INFO_COMPANY, UPDATE_INFO_COMPANY_SUCCEEDED,
    POST_DETAIL_EMPLOYER_ACTION,
    SAVE_CV_USER, SAVE_CV_USER_SUCCEEDED,
    UPDATE_APPROVE_JOB_POST, UPDATE_APPROVE_JOB_POST_SUCCEEDED,
    BUY_CV, BUY_CV_SUCCEEDED,
    UNSAVE_CV_USER, UNSAVE_CV_USER_SUCCEEDED,
    RESETPASS

}
    from
        './actionTypes';
import {
    LOADDING_BEGIN,
    FETCH_DATA_FAIL,
    LOADDING_BEGIN_SAVE,
    FETCH_DATA_FAIL_SAVE,
    LOADDING_CARRER_DETAIL,
    FETCH_CARRER_DETAIL_FAIL,
    LOADDING_SALARY_TAB_DETAIL,
    FETCH_SALARY_TAB_FAIL,
    LOADDING_USERDETAIL,
    FETCH_USERDETAIL
} from '../actions/actionLoading'
//Loadding
export const LoaddingBegin = () => ({
    type: LOADDING_BEGIN
});

export const fetchDataFail = error => ({
    type: FETCH_DATA_FAIL,
    error
});

export const LoaddingBeginSave = () => ({
    type: LOADDING_BEGIN_SAVE
});

export const fetchDataFailSave = error => ({
    type: FETCH_DATA_FAIL_SAVE,
    error
});

export const LoaddingCarrerDetail = () => ({
    type: LOADDING_CARRER_DETAIL
});

export const fetchCarrerDetaiFail = error => ({
    type: FETCH_CARRER_DETAIL_FAIL,
    error
});

export const LoaddingSalaryTab = () => ({
    type: LOADDING_SALARY_TAB_DETAIL
});

export const fetchSalaryTabFail = error => ({
    type: FETCH_SALARY_TAB_FAIL,
    error
});

export const LoaddingUserDetail = () => ({
    type: LOADDING_USERDETAIL
});

export const fetchUserDetail = error => ({
    type: FETCH_USERDETAIL,
    error
});

//loginEmployer
export const LoginEmployerAction = (user) => {

    return {
        type: LOGIN_EMPLOYER,
        user
    }
}
//SinginEmployer
export const SinginEmployerAction = (user) => {

    return {
        type: SINGIN_EMPLOYER,
        user,
    }
}
//login
export const LoginAction = (user, typeLG) => {

    return {
        type: LOGIN,
        user,
        typeLG
    }
}
//Singin
export const SinginAction = (user) => {

    return {
        type: SINGIN,
        user,
    }
}
//VERIFY
export const VerifyAction = (sdt, key) => {

    return {
        type: VERIFY,
        sdt,
        key
    }
}

//RESETPASS
export const ResetPass = (sdt, pass) => {

    return {
        type: RESETPASS,
        sdt,
        pass
    }
}

export const EditUserAction = (userInfo) => {
    return {
        type: EDITUSER,
        userInfo
    }
}


//search
// export const SearchFormAction = (info) => {
//     return {
//         type: SEARCH_FORM_HOME,
//         info
//     }
// }

//search detail
export const DetailNewAction = (page) => {
    return {
        type: DETAIL_NEW_ACTION,
        page
    }
}

export const DetailBigNumberAction = (page) => {
    return {
        type: DETAIL_BIG_NUMBER_ACTION,
        page
    }
}

export const DetailSalaryAction = (page) => {
    return {
        type: DETAIL_SALARY_ACTION,
        page
    }
}

// SAVE_JOB
export const SaveJobAction = (saveUserID) => {
    return {
        type: SAVE_JOB_ACTION,
        saveUserID
    }
}

// UN_SAVE_JOB
export const UnSaveJobAction = (saveUserID) => {
    return {
        type: UN_SAVE_JOB_ACTION,
        saveUserID
    }
}
// APPLIED_ACTION
export const appliedAction = (saveUserID) => {
    return {
        type: APPLIED_ACTION,
        saveUserID
    }
}

//LIST_SAVE

export const ListSaveAction = () => {
    return {
        type: LIST_SAVE_ACTION,
    }
}


//CARRER LIST DETAIL(HOME)
export const CarrerDetailAction = (carrer) => {
    return {
        type: CARRER_DETAIL_ALL,
        carrer
    }
}


// SALARY TAB
export const SalaryProvinceAction = () => {
    return {
        type: SALARY_PROVINCE,
    }
}

export const SalaryIndustrialAction = () => {
    return {
        type: SALARY_INDUSTRIAL,
    }
}


// SEARCH_ADVANCED_
export const SearchAdvancedAction = (formInfo) => {
    return {
        type: SEARCH_ADVANCED_ACTION,
        formInfo

    }
}


/////
//get category post
export const CategoryPost = () => {

    return {
        type: CATEGORY_POST
    }
}

// JOB_DETAIL
export const JobDetailAction = (idJob) => {
    return {
        type: JOB_DETAIL_ACTION,
        idJob
    }
}

// COMPANY_DETAIL
export const CompanyDetailAction = (idJob) => {
    return {
        type: COMPANY_DETAIL_ACTION,
        idJob
    }
}

// KEY_WORKS
export const keyWorksAction = () => {
    return {
        type: KEY_WORKS,
    }
}

// delete job apply
export const DeleteJobApply = (params) => {
    return {
        type: DELETE_JOB_APPLY,
        params
    }
}

export const DeleteJobApplySuccess = (params) => {
    return {
        type: DELETE_JOB_APPLY_SUCCEEDED,
        params
    }
}

// delete job care
export const DeleteJobCare = (params) => {
    return {
        type: DELETE_JOB_CARE,
        params
    }
}

export const DeleteJobCareSuccess = (params) => {
    return {
        type: DELETE_JOB_CARE_SUCCEEDED,
        params
    }
}

//noify
export const SwitchNotify = (status) => {
    return {
        type: SWITCH_NOTIFY,
        status
    }
}

export const SwitchNotifySuccess = (status) => {
    return {
        type: SWITCH_NOTIFY_SUCCEEDED,
        status
    }
}

//fetch detail user
export const FetchDetailUser = (token) => {
    return {
        type: FETCH_DETAIL_USER,
        token
    }
}

export const FetchDetailUserSuccess = (token) => {
    return {
        type: FETCH_DETAIL_USER_SUCCEEDED,
        token
    }
}

//fetch province list
export const FetchProvinceList = () => {
    return {
        type: PROVINCE_lIST
    }
}

export const FetchProvinceListSuccess = () => {
    return {
        type: PROVINCE_SUCCEEDED
    }
}

export const FetchCarrerList = () => {
    return {
        type: CARRER_LIST
    }
}
export const FetchSalaryList = () => {
    return {
        type: SALARY_LIST
    }
}
export const FetchExperienceList = () => {
    return {
        type: EXPERIENCE_LIST
    }
}
export const FetchJobTypesList = () => {
    return {
        type: JOB_TYPES_LIST
    }
}

//fetch cv user
export const FetchCvUser = (token) => {
    return {
        type: FETCH_CV_USER,
        token
    }
}

export const FetchCvUserSuccess = (token) => {
    return {
        type: FETCH_CV_USER_SUCCEEDED,
        token
    }
}

//update cv user
export const UpdateCvUser = (params) => {
    return {
        type: UPDATE_CV_USER,
        params
    }
}

export const UpdateCvUserSuccess = (params) => {
    return {
        type: UPDATE_CV_USER_SUCCEEDED,
        params
    }
}

//update avatar user
export const UpdateAvatarUser = (params) => {
    return {
        type: UPDATE_AVATAR_USER,
        params
    }
}

export const UpdateAvaterUserSuccess = (params) => {
    return {
        type: UPDATE_AVATAR_USER_SUCCEEDED,
        params
    }
}


//fetch media relation
export const FetchMediaRelation = (id) => {
    return {
        type: FETCH_MEDIA_RELATION,
        id
    }
}

export const FetchMediaRelationSuccess = (id) => {
    return {
        type: FETCH_MEDIA_RELATION_SUCCEEDED,
        id
    }
}

// fetch university

export const FetchListUniversity = () => {
    return {
        type: FETCH_LIST_UNIVERSITY
    }
}

export const FetchListUniversitySuccess = () => {
    return {
        type: FETCH_LIST_UNIVERSITY_SUCCEEDED
    }
}

//fetch major
export const FetchListMajor = (universityId) => {
    return {
        type: FETCH_LIST_MAJOR,
        universityId
    }
}

export const FetchListMajorSuccess = (universityId) => {
    return {
        type: FETCH_LIST_MAJOR_SUCCEEDED,
        universityId
    }
}


// fetch degree

export const FetchListDegree = () => {
    return {
        type: FETCH_LIST_DEGREE
    }
}

export const FetchListDegreeSuccess = () => {
    return {
        type: FETCH_LIST_DEGREE_SUCCEEDED
    }
}

export const FetchListSkill = () => {
    return {
        type: FETCH_LIST_SKILL
    }
}

export const FetchListSkillSuccess = () => {
    return {
        type: FETCH_LIST_SKILL_SUCCEEDED
    }
}


//fetch user info
export const FetchUserInfo = () => {
    return {
        type: FETCH_USER_INFO
    }
}

export const FetchUserInfoSuccess = () => {
    return {
        type: FETCH_USER_INFO_SUCCEEDED
    }
}

//update user info
export const UpdateUserInfo = (params) => {
    return {
        type: UPDATE_USER_INFO,
        params
    }
}

export const UpdateUserInfoSuccess = (params) => {
    return {
        type: UPDATE_USER_INFO_SUCCEEDED,
        params
    }
}

//fetch user cv
export const FetchUserCVEmployer = (userId) => {
    return {
        type: FETCH_USER_CV_EMPLOYER,
        userId
    }
}

export const FetchUserCVEmployerSuccess = (userId) => {
    return {
        type: FETCH_USER_CV_EMPLOYER_SUCCEEDED,
        userId
    }
}

//fetch job level
export const FetchJobLevel = () => {
    return {
        type: FETCH_JOB_LEVEL

    }
}

export const FetchJobLevelSuccess = () => {
    return {
        type: FETCH_JOB_LEVEL_SUCCEEDED

    }
};

//fetch district
export const FetchDistrict = (provinceId) => {
    return {
        type: FETCH_DISTRICT,
        provinceId
    }
}

export const FetchDistrictSuccess = (provinceId) => {
    return {
        type: FETCH_DISTRICT_SUCCEEDED,
        provinceId
    }
}

//create job
export const CreateJobPost = (params) => {
    return {
        type: CREATE_JOB_POST,
        params
    }
}

export const CreateJobPostSuccess = (params) => {
    return {
        type: CREATE_JOB_POST_SUCCEEDED,
        params
    }
}

//update job
export const UpdateJobPost = (params) => {
    return {
        type: UPDATE_JOB_POST,
        params
    }
}

export const UpdateJobPostSuccess = (params) => {
    return {
        type: UPDATE_JOB_POST_SUCCEEDED,
        params
    }
}

//fetch info company
export const FetchInfoCompany = () => {
    return {
        type: FETCH_INFO_COMPANY
    }
}

export const FetchInfoCompanySuccess = () => {
    return {
        type: FETCH_INFO_COMPANY_SUCCEEDED
    }
}

//update job
export const UpdateInfoCompany = (params) => {
    return {
        type: UPDATE_INFO_COMPANY,
        params
    }
}

export const UpdateInfoCompanySuccess = (params) => {
    return {
        type: UPDATE_INFO_COMPANY_SUCCEEDED,
        params
    }
}
// Post Employer
export const PostEmployerDetailAction = (idPost) => {
    return {
        type: POST_DETAIL_EMPLOYER_ACTION,
        idPost
    }
}

//save user cv
export const SaveUserCVEmployer = (userId) => {
    return {
        type: SAVE_CV_USER,
        userId
    }
}

export const SaveUserCVEmployerSuccess = (userId) => {
    return {
        type: SAVE_CV_USER_SUCCEEDED,
        userId
    }
}

//update approve jobpost
export const UpdateApproveJobPost = (jobPostId) => {
    return {
        type: UPDATE_APPROVE_JOB_POST,
        jobPostId
    }
}

export const UpdateApproveJobPostSuccess = (jobPostId) => {
    return {
        type: UPDATE_APPROVE_JOB_POST_SUCCEEDED,
        jobPostId
    }
}


//buy cv
export const BuyCv = (params) => {
    return {
        type: BUY_CV,
        params
    }
}

export const BuyCvSuccess = (params) => {
    return {
        type: BUY_CV_SUCCEEDED,
        params
    }
}

//unsave user cv
export const UnSaveUserCVEmployer = (userId) => {
    return {
        type: UNSAVE_CV_USER,
        userId
    }
}

export const UnSaveUserCVEmployerSuccess = (userId) => {
    return {
        type: UNSAVE_CV_USER_SUCCEEDED,
        userId
    }
}

