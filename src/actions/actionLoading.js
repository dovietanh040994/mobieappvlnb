//loading search(home)
export const LOADDING_BEGIN = 'LOADDING_BEGIN';
export const FETCH_DATA_FAIL = 'FETCH_DATA_FAIL';

//loading search(home)
export const LOADDING_LOGOUT_BEGIN = 'LOADDING_LOGOUT_BEGIN';
export const FETCH_LOGOUT_FAIL = 'FETCH_LOGOUT_FAIL';


//loadding page save
export const LOADDING_BEGIN_SAVE = 'LOADDING_BEGIN_SAVE';
export const FETCH_DATA_FAIL_SAVE = 'FETCH_DATA_FAIL_SAVE';

//loadding carrer detail(home)
export const LOADDING_CARRER_DETAIL= 'LOADDING_CARRER_DETAIL';
export const FETCH_CARRER_DETAIL_FAIL = 'FETCH_CARRER_DETAIL_FAIL';

//loadding salary tab(home)
export const LOADDING_SALARY_TAB_DETAIL= 'LOADDING_SALARY_TAB_DETAIL';
export const FETCH_SALARY_TAB_FAIL = 'FETCH_SALARY_TAB_FAIL';

//loadding detail job
export const LOADDING_DETAIL_JOB= 'LOADDING_DETAIL_JOB';
export const FETCH_DETAIL_JOB_FAIL = 'FETCH_DETAIL_JOB_FAIL';

//loading search(home)
export const LOADDING_EDIT_USER = 'LOADDING_EDIT_USER';
export const FETCH_DATA_EDIT_USER = 'FETCH_DATA_EDIT_USER';

export const LOADDING_USERDETAIL = 'LOADDING_USERDETAIL';
export const FETCH_USERDETAIL = 'FETCH_USERDETAIL';


export const LOADDING_DETAIL_COMPANY = 'LOADDING_DETAIL_COMPANY';
export const FETCH_DETAIL_COMPANY_FAIL = 'FETCH_DETAIL_COMPANY_FAIL';

//KEY_WORKS
export const LOADDING_KEY_WORKS = 'LOADDING_KEY_WORKS';
export const FETCH_KEY_WORKS_FAIL = 'FETCH_KEY_WORKS_FAIL';

//CATEGORY_POST
export const LOADDING_CATEGORY_POST = 'LOADDING_CATEGORY_POST';
export const FETCH_CATEGORY_POST_FAIL = 'FETCH_CATEGORY_POST_FAIL';

//LOADDING_DETAIL_EMPLOYER_POST
export const LOADDING_DETAIL_EMPLOYER_POST = 'LOADDING_DETAIL_EMPLOYER_POST';
export const FETCH_DETAIL_EMPLOYER_POST = 'FETCH_DETAIL_EMPLOYER_POST';


