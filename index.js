/**
 * @format
 */
import React, {Component} from 'react';
import {AppRegistry,Image,View,TextInput,TouchableOpacity,ScrollView} from 'react-native';
import {name as appName} from './app.json';

//redux
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from 'redux';
import {Body, Container, Header, Left, Root, Title,Item,Input,Right} from "native-base";

//Redux saga
import createSagaMiddleware from 'redux-saga';
import rootSaga from './src/sagas/rootSaga';


//sidebar
import AllReducers from './src/reducers/index';
import {createBottomTabNavigator, createAppContainer, createStackNavigator} from 'react-navigation';

//component
import bgMessaging from './src/vendor/bgMessaging';
import NavigationService from "./src/vendor/NavigationService";
import LoadingNavigator from "./src/components/Navigation/LoadingNavigator";

//setting
const AppNav =   createAppContainer(LoadingNavigator);
const sagaMiddleware = createSagaMiddleware();
const store = createStore(AllReducers, applyMiddleware(sagaMiddleware));

export default class App extends Component {

    render() {
        return (
            <Provider store={store}>
                <Root>
                    <AppNav ref={navigatorRef => {
                        NavigationService.setTopLevelNavigator(navigatorRef);
                    }} />
                </Root>
            </Provider>
        );
    }
}

sagaMiddleware.run(rootSaga);
AppRegistry.registerComponent(appName, () => App);

AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging); // <-- Add this line
